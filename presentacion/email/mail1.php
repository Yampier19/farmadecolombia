<?php 
require_once('AttachMailer.php');
$fecha= date("Y-m-d");
$ID_CLIENTE;
$DISTRIBUIDOR=$distribuidor;

$directorio = opendir("../DOCUMENTOS_ADJUNTOS/$ID_CLIENTE/rut"); //ruta actual
while ($archivo = readdir($directorio)) //obtenemos un archivo y luego otro sucesivamente
{
    if (is_dir($archivo))//verificamos si es o no un directorio
    {}else
    {  $imagen1=$archivo;}
}

$directorio2 = opendir("../DOCUMENTOS_ADJUNTOS/$ID_CLIENTE/doc_identidad_adelante"); //ruta actual
while ($archivo2 = readdir($directorio2)) //obtenemos un archivo y luego otro sucesivamente
{
    if (is_dir($archivo2))//verificamos si es o no un directorio
    {}else
    {  $imagen2=$archivo2;}
}


$directorio2_atras = opendir("../DOCUMENTOS_ADJUNTOS/$ID_CLIENTE/doc_identidad_atras"); //ruta actual
while ($archivo2_atras = readdir($directorio2_atras)) //obtenemos un archivo y luego otro sucesivamente
{
    if (is_dir($archivo2_atras))//verificamos si es o no un directorio
    {}else
    {  $imagen2_atras=$archivo2_atras;}
}

$directorio3 = opendir("../DOCUMENTOS_ADJUNTOS/$ID_CLIENTE/referencia_bancaria"); //ruta actual
while ($archivo3 = readdir($directorio3)) //obtenemos un archivo y luego otro sucesivamente
{
    if (is_dir($archivo3))//verificamos si es o no un directorio
    {}else
    {  $imagen3=$archivo3;}
}

$directorio4 = opendir("../DOCUMENTOS_ADJUNTOS/$ID_CLIENTE/referencia_comercial"); //ruta actual
while ($archivo4 = readdir($directorio4)) //obtenemos un archivo y luego otro sucesivamente
{
    if (is_dir($archivo4))//verificamos si es o no un directorio
    {}else
    {  $imagen4=$archivo4;}
}



$BODY_TODO = "
Buen dia,
<br />
<br />
Adjunto envio reporte de creaci&oacute;n cliente nuevo con los siguientes datos.
<br />
<br />
Nombre:".$nombre." ".$apellido."
<br />
<br />
Documento de identificacion:".$tipo_identificacion." ".$identificacion."
<br />
<br />
Direccion:".$direccion."
<br />
<br />
Telefono y/o celular:".$celular." ".$telefono."
<br />
<br />
Razon social:".$razon_social."
<br />
<br />
NIT:".$nit."
<br />
<br />
Distribuidor:".$distribuidor."
<br />
<br />
Fue registrado por el usuario:".$dato_usu.".
<br />
Cualquier inquietud con gusto sera atendida.
<br />
<br />
Correo enviado de manera automatica.";
if($DISTRIBUIDOR=='Yina Moncayo-Dental Palermo')
{
  $mailer = new AttachMailer("ingreso_3m@encontactopeoplemarketing.com", "programacion@peoplecontact.cc,soporte@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO);
}
if($DISTRIBUIDOR=='Carlos Henao-Dental Nader')
{
	$mailer = new AttachMailer("ingreso_3m@encontactopeoplemarketing.com", "programacion@peoplecontact.cc,soporte@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO);
}
if($DISTRIBUIDOR=='Paola Linares-Dental 83 Portafolio 3M ESPE')
{
	$mailer = new AttachMailer("ingreso_3m@encontactopeoplemarketing.com", "programacion@peoplecontact.cc,soporte@peoplecontact.cc", "Solicitud pedido- ".$fecha,$BODY_TODO);
}
$mailer->attachFile('../DOCUMENTOS_ADJUNTOS/'.$ID_CLIENTE.'/rut/'.$imagen1);
$mailer->attachFile('../DOCUMENTOS_ADJUNTOS/'.$ID_CLIENTE.'/doc_identidad_adelante/'.$imagen2);
$mailer->attachFile('../DOCUMENTOS_ADJUNTOS/'.$ID_CLIENTE.'/doc_identidad_atras/'.$imagen2_atras);
$mailer->attachFile('../DOCUMENTOS_ADJUNTOS/'.$ID_CLIENTE.'/referencia_bancaria/'.$imagen3);
$mailer->attachFile('../DOCUMENTOS_ADJUNTOS/'.$ID_CLIENTE.'/referencia_comercial/'.$imagen4);
$mailer->send() ? "Enviado": "Problema al enviar";
?>