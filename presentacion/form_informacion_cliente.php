<?php
	include("../logica/session.php");
        header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/paciente_nuevo.css" type="text/css" rel="stylesheet" />
<link href="css/validacion_campos.css"type="text/css" rel="stylesheet" />
<title>ODONTOLOGO NUEVO</title>
<meta name="viewport" content="width=device-width, user-scalable=no , initial-scale=1.0, maximun-scale=1.0, minimum-scale=1.0">

<script src="js/jquery.js"></script>
<!--AJAX-->
<script>
function ciudades()
{
	var DEPT=$('#departamento').val();
	//alert(DEPT)
	$.ajax(
	{
		url:'../presentacion/consulta_ciudades.php',
		data:
		{
			DEPT: DEPT,
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#ciudad").attr('disabled','disabled');
		},
		success: function(data)
		{
			$('#ciudad').html(data);
			$("#ciudad").removeAttr('disabled');
		}
	})
}
$(document).ready(function()
{
	$('#departamento').change(function()
	{
		ciudades();
	});
    
});
</script>
<style>
.error
{
	font-size: 130%;
	font-weight: bold;
	color: #fb8305;
	text-transform:uppercase;
	background-color:transparent;
	text-align: center;
	padding:10px;
}
</style>
</head>
<?php
include('../datos/conex.php');
$ID_CLIENTE;
$consulta=mysql_query("SELECT * FROM 3m_cliente WHERE ID_CLIENTE='$ID_CLIENTE'",$conex);
?>
<body>
<?php
if(empty($id_usu)){ ?>
				<span style="margin-top:5%;">
			<center>
			<img src="../presentacion/imagenes/advertencia.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
			<p class="error" style=" width:68.9%; margin:auto auto;">
			 LA SESI&Oacute;N HA CADUCADO, POR FAVOR INICIE SESION.
			</p>
			<br />
			<br />
			<center>
					<a href="http://aplicacionesarc.com/3M/"  class="btn_continuar">
						<img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" />
					</a>
				</center>
			</center>
			</span>
				
			<?php }else{?>
<table align="right">
    	<tr>
            <!--MUESTRA UN MENSAJE DE BIENVENIDA Y EL NOMBRE DEL USUARIO-->
            <th>Bienvenid@  <?php echo $usua?></th>
            <td><a href="../presentacion/inicio_visitas.php">
        <img src="../presentacion/imagenes/atras.png" width="56" height="57" id="cambiar" title="ATRAS" ><span style="color:#000;">
        </a></td>
        </tr>
    </table>
    <br />
    <br />
    <br />
<!--MUESTRA LA IMAGEN DE 3M-->    
<center><img src="../presentacion/imagenes/3m.png" width="24%" height="17%" /></center>
<!--FORMULARIO PARA REGISTRO DE ODONTOLOGO-->
<form id="tranferencias" name="tranferencias" action="../logica/registrar_paciente.php" method="post" style="width:100%;" enctype="multipart/form-data">
	<header>
        <div class="container" id="div_header">
        	<center><h1>ODONTOLOGO NUEVO</h1></center>
        </div>
    </header>
    <div class="form-group">
        <label for="cliente">Apellido</label><span class="asterisco">*</span>
        <br>
        <input name="apellido" id="apellido" class="form-control" type="text" placeholder="Apellido" required="required"/>
    </div>
    <div class="form-group">
        <label for="cliente">Nombre</label><span class="asterisco">*</span>
        <br>
        <input name="nombre" id="nombre" class="form-control" type="text" placeholder="Nombre" required="required"/>
    </div>
    <div class="form-group">
        <label for="cliente">Tipo de Identificaci&oacute;n</label><span class="asterisco">*</span>
        <br>
<!--OPCIONES DE TIPO DE IDENTIFICACION-->
        <select name="tipo_identificacion" id="tipo_identificacion" class="form-control" required>
        	<option value="">Elija...</option>
            <option value="CC">Cedula de Ciudadania</option>
            <option value="CE">Cedula de Extrajeria</option>
        </select>
    </div>
    <div class="form-group">
        <label for="cliente">N&uacute;mero de Identificaci&oacute;n</label><span class="asterisco">*</span>
        <br>
        <input name="identificacion" id="identificacion" class="form-control" type="text" placeholder="identificacion" required="required" maxlength="20"/>
    </div>
    <div class="form-group">
        <label for="cliente">Direcci&oacute;n Residencial</label><span class="asterisco">*</span>
        <br>
        <input name="direccion" id="direccion" class="form-control" type="text" placeholder="Direccion" required="required"/>
    </div>
    <div class="form-group">
        <label for="cliente">Departamento</label><span class="asterisco">*</span>
        <br>
        <select name="departamento" id="departamento" class="form-control" required>
        	<option value="">ELIJA...</option>
        	<?php
 /*CONSULTA  Y MUESTRA TODOS LOS DEPARTAMENTOS*/
			$consulta_departamento=mysql_query("SELECT * FROM 3m_departamento WHERE NOMBRE_DEPARTAMENTO!='' ORDER BY NOMBRE_DEPARTAMENTO ASC",$conex);
			echo mysql_error($conex);
			while($datos=mysql_fetch_array($consulta_departamento))
			{
			?>
        		<option><?php echo $datos['NOMBRE_DEPARTAMENTO'] ?></option>
            <?php
			}
            ?>
        </select>
    </div>
    <div class="form-group">
        <label for="cliente">Ciudad</label><span class="asterisco">*</span>
        <br>
        <select name="ciudad" id="ciudad" class="form-control" required disabled="disabled">
        </select>
    </div>
    <div class="form-group">
        <label for="cliente">Celular</label>
        <br>
        <input name="celular" id="celular" class="form-control" type="tel" pattern="[0-9]{10}" title="Ej: 1115468975" placeholder="Celular"  />
    </div>
    <div class="form-group">
        <label for="cliente">Tel&eacute;fono</label>
        <br>
        <input name="telefono" id="telefono" class="form-control" type="tel" pattern="[0-9]{7}" title="Ej: 2547896" placeholder="Tel&eacute;fono"/>
    </div>
    <div class="form-group">
        <label for="cliente">E-mail</label>
        <br>
        <input name="email" id="email" class="form-control" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Ej: ejemplo@ejemplo.com" placeholder="E-mail"/>
    </div>
    <div class="form-group">
        <label for="cliente">Raz&oacute;n Social</label><span class="asterisco">*</span>
        <br>
        <input name="razon_social" id="razon_social" class="form-control" type="text" placeholder="Raz&oacute;n Social" required="required"/>
    </div>
    <div class="form-group">
        <label for="cliente">NIT</label><span class="asterisco">*</span>
        <br>
        <input name="nit" id="nit" class="form-control" type="tel" title="Ej: 1023546894-2" placeholder="NIT" required="required"/>
    </div>
    <div class="form-group">
        <label for="cliente">P&aacute;gina Web</label>
        <br>
        <input name="pagina_web" id="pagina_web" class="form-control" type="text" placeholder="Pagina web"/>
    </div>
    <div class="form-group">
        <label for="cliente">No. De Empleados</label><span class="asterisco">*</span>
        <br>
        <input name="num_empleados" id="num_empleados" class="form-control" type="tel" pattern="[0-9]{1,3}" title="Ej: 1023546894-2" placeholder="No. De Empleados" required="required"/>
    </div>
    
    <div class="form-group">
        <label for="cliente">Distribuidor que compra</label><span class="asterisco">*</span>
        <br>
        <select name="distribuidor" id="distribuidor" class="form-control" required>
        	<option value="">Elija...</option>           
            <option>Yina Moncayo-Dental Palermo</option>
            <option>Carlos Henao-Dental Nader</option>
            <option>Paola Linares-Dental 83 Portafolio 3M ESPE</option>
        </select>
    </div>
    <fieldset>
    	<legend>DOCUMENTOS REQUERIDOS</legend>
        <div id="dental_nader">
            <label for="cliente">Imagen Rut</label><span class="asterisco">*</span>
            <input class="form-control" name="rut" id="rut" type="file" data-preview-file-type="any" multiple=true/>
            <br />
            <label for="cliente">Fotocopia Documento de Identidad Adelante</label><span class="asterisco">*</span>
            <input class="form-control" name="ima_cedula" id="ima_cedula" type="file" />
            <br />
            <label for="cliente">Fotocopia Documento de Identidad Atras</label><span class="asterisco">*</span>
            <input class="form-control" name="ima_cedula_atras" id="ima_cedula_atras" type="file" />
            <br />
            <label for="cliente">Referencia Bancaria</label><span class="asterisco">*</span>
            <input class="form-control" name="referencia_bacaria" id="referencia_bacaria" type="file" />
            <br />
            <label for="cliente">Referencia Comercial</label><span class="asterisco">*</span>
            <input class="form-control" name="referencia_comercial" id="referencia_comercial" type="file" />
        </div>
    </fieldset>
    <br />
    <br />
    <div class="form-group">
            <center>
            	<input name="confirmar" type="submit" id="confirmar" value="GUARDAR" class="form-control btn" onclick="return validar(tranferencias)"/>
            </center>
    </div>
    <br />
</form>
<?php } ?>
</body>
</html>
<script>
$("#rut").fileinput({
	idioma :  "es" ,
	allowedFileExtensions :  [ "jpg" ,  "png" ,  "gif" ],
	showUpload: false,
	showRemove: false,
	showBrowse: false,
	browseClass: "btn form-control",
	fileType: "any",
	uploadLabel: 'Cargar Archivo',
	uploadTitle: 'Cargar archivos seleccionados'
});
</script>
<script>
 /* MENSAJE DE CONFIRMACION SI DESEA GUARDAR LA INFORMACION DEL FORMULARIO */   
function validar(tuformulario)
{
	if (confirm('ESTA SEGURO(A) DE GUARDAR LA INFORMACION'))
	{ 
		document.tuformulario.submit() 
		return (true)
	}
	else
	{
		return (false);
	}
}
</script>