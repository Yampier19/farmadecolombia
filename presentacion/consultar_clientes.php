<?php
    error_reporting(0);
    include ('../logica/session.php');
    header('Content-Type: text/html; charset=UTF-8'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CONSULTAR GESTION</title>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css"/>
<link href="css/tablas.css" rel="stylesheet" /> 
<link href="css/bootstrap.css" rel="stylesheet" /> 

<script>
$(document).ready(function() {
    $('#gestion').DataTable();
} );

$(document).ready(function() {
    $('#gestionTodos').DataTable();
} );

function ventanaSecundaria (URL)
{ 
   window.open(URL,"ventana1","width=800,height=500,Top=150,Left=50%") 
}


</script>
</head>
<?php
include('../datos/conex.php');
	mysql_query("SET NAMES utf8");
	$string_intro = getenv("QUERY_STRING"); 
	parse_str($string_intro);
$usua;
?>
<body>
<br /><br />

<div class="table table-responsive">

<form action="consultar_clientes.php" method="post">
<div class="col-md-12">
	   <div class="col-md-1">
    <label>Clasificaci&oacute;n</label>
    </div>
    <div class="col-md-2">
    <select name="clasificacion" class="form-control">
    <option value="">Seleccione</option>
        <?php
		$CONSULTA_CATEGORIA = mysql_query("
			SELECT DISTINCT (CLASIFICACION_ODONTOLOGO)
			FROM 3m_encuesta
			WHERE CLASIFICACION_ODONTOLOGO <> ''
			ORDER BY CLASIFICACION_ODONTOLOGO ASC;",$conex);
			
			while($datos_clasificacion = mysql_fetch_array($CONSULTA_CATEGORIA)){ ?>
				<option value="<?php echo $datos_clasificacion["CLASIFICACION_ODONTOLOGO"]; ?>"><?php echo $datos_clasificacion["CLASIFICACION_ODONTOLOGO"]; ?></option>
			<?php	}		 ?>
    </select>
    </div>
    
    <div class="col-md-1">
    <label>Departamento</label>
    </div>
    <div class="col-md-2">
    <select name="ciudad" class="form-control">
	   <option value="">Seleccione</option>
	        <?php
		$CONSULTA_CATEGORIA = mysql_query("
			SELECT DISTINCT DEPARTAMENTO_CLIENTE FROM 3m_cliente WHERE DEPARTAMENTO_CLIENTE <>'' ",$conex);
			
			while($datos_clasificacion = mysql_fetch_array($CONSULTA_CATEGORIA)){ ?>
				<option value="<?php echo $datos_clasificacion["DEPARTAMENTO_CLIENTE"]; ?>"><?php echo $datos_clasificacion["DEPARTAMENTO_CLIENTE"]; ?></option>
			<?php	}		 ?>
	</select>
    </div>
    
    <div class="col-md-12" style="margin-top:10px; text-align:right;">
    <button name="btn_consultar" class="btn btn-default">CONSULTAR</button>
    </div>
</div>

</form>
<br /><br />
<hr />

<?php 
	if(isset($_POST["btn_consultar"])){
		$CIUDAD =  $_POST["ciudad"];
		$CLASIFICACION =  $_POST["clasificacion"];
				
		$CONSULTAR_USUARIO = mysql_query("
		SELECT USER
		FROM 3m_usuario
		WHERE ID_USUARIO=".$USUARIO.";",$conex);
		
		$fila = mysql_fetch_array($CONSULTAR_USUARIO);
		$USER = $fila['USER'];
		
		if(empty($CIUDAD) &&  empty($CLASIFICACION)){ ?>
        <script>
        	alert("Seleccione una opción");
        </script>
        
        	<?php } 
					//BUSQUEDA CLASIFICACION	
			 else if(empty($CIUDAD) &&  empty($CLASIFICACION)== false){
				$CONSULTA_POR_CLASIFICACION = mysql_query("SELECT A.ID_CLIENTE,
A.NOMBRE_CLIENTE,
A.APELLIDO_CLIENTE,
A.TIPO_IDENTIFICACION,A.IDENTIFICACION_CLIENTE,A.DIRECCION_CLIENTE,A.DEPARTAMENTO_CLIENTE,
CONVERT(CAST(CONVERT(A.CIUDAD_CLIENTE USING latin1) AS BINARY) USING utf8) AS CIUDAD_CLIENTE,
A.CELULAR_CLIENTE,A.TELEFONO_CLIENTE,A.EMAIL_CLIENTE,A.RAZON_SOCIAL,A.NIT,A.PAGINA_WEB_NEGOCIO,B.CANTIDAD_PACIENTES,A.FECHA_REGISTRO,A.AUTOR_REGISTRO_FK,A.ESTADO_ASIGNADO,B.CLASIFICACION_ODONTOLOGO, A.AUTOR_REGISTRO_FK 
FROM 3m_cliente AS A
INNER JOIN 3m_encuesta AS B ON B.ID_CLIENTE_FK_5 = A.ID_CLIENTE
WHERE B.CLASIFICACION_ODONTOLOGO='$CLASIFICACION' AND A.AUTOR_REGISTRO_FK<>'visita' AND A.AUTOR_REGISTRO_FK<>'MEDELLIN'
ORDER BY NOMBRE_CLIENTE ASC;",$conex);	

	 ?>
						 
			 <table style="width:99%; margin:auto auto;" rules="none" class="table table-responsive" >
            <tr>
                <th colspan='11' class="principal">CLIENTES CLASIFICACION: <?PHP echo $CLASIFICACION;?> </th>
            </tr>
            </table>
        <br />
        
        <table style="width:99%; margin:auto auto;"  id="gestion" class="table table-striped">
    <thead>
	<tr style=" text-align:center;">
 
    <th class="TITULO">IDENTIFICACI&Oacute;N</th>
    <th class="TITULO">CLIENTE</th>
    <th class="TITULO">DIRECCI&Oacute;N</th>
    <th class="TITULO">CELULAR</th>
    <th class="TITULO">TEL&Eacute;FONO</th>
    <th class="TITULO">CIUDAD</th>
    <th class="TITULO">CLASIFICACI&Oacute;N</th>
    <th class="TITULO">USUARIO REGISTRO</th>
    <th class="TITULO">VER GESTI&Oacute;N</th>
    
	</tr>
    </thead>
    <tbody>
    <?PHP
	$i=1;
    while($dato=mysql_fetch_array($CONSULTA_POR_CLASIFICACION))
	{
		
	?>
		<tr class="datos" >
        	<td style="text-align:left;"><?php echo $dato["TIPO_IDENTIFICACION"]?>  <?php echo $dato["IDENTIFICACION_CLIENTE"]?></td>
          
            <td style="text-align:left;"><?php echo $dato["NOMBRE_CLIENTE"]?> <?php echo $dato["APELLIDO_CLIENTE"]?></td>
            <td style="text-align:left;"><?php echo $dato["DIRECCION_CLIENTE"]?></td>
            <td style="text-align:left;"><?php echo $dato["CELULAR_CLIENTE"]?></td>
            <td style="text-align:left;"><?php echo $dato["TELEFONO_CLIENTE"]?></td>
            <td style="text-align:left;"><?php echo $dato["CIUDAD_CLIENTE"]?></td>
            <td style="text-align:left;"><?php echo $dato["CLASIFICACION_ODONTOLOGO"]?></td>
            <td style="text-align:left;"><?php echo $dato["AUTOR_REGISTRO_FK"]?></td> 
            <th>
                <a  href="javascript:ventanaSecundaria('../presentacion/consulta_gestion_usuario.php?x=<?php echo base64_encode($dato['ID_CLIENTE'])?>')" >
              <img src="imagenes/lupa1.png" width="43" height="32" style="background-size:cover" title="DETALLE GESTIÓN"/>
                </a> 
            </th>   
		</tr>
	<?php 
	}
	?>
    </tbody>
</table>
        
			<?php }
					//bUSQUEDA CIUDAD
			else if(empty($CIUDAD)==false &&  empty($CLASIFICACION)){
			 $CONSULTAR_POR_CIUDAD = mysql_query("SELECT A.ID_CLIENTE,
A.NOMBRE_CLIENTE,
A.APELLIDO_CLIENTE,
A.TIPO_IDENTIFICACION,A.IDENTIFICACION_CLIENTE,A.DIRECCION_CLIENTE,A.DEPARTAMENTO_CLIENTE,
CONVERT(CAST(CONVERT(A.CIUDAD_CLIENTE USING latin1) AS BINARY) USING utf8) AS CIUDAD_CLIENTE,
A.CELULAR_CLIENTE,A.TELEFONO_CLIENTE,A.EMAIL_CLIENTE,A.RAZON_SOCIAL,A.NIT,A.PAGINA_WEB_NEGOCIO,B.CANTIDAD_PACIENTES,A.FECHA_REGISTRO,A.AUTOR_REGISTRO_FK,A.ESTADO_ASIGNADO,B.CLASIFICACION_ODONTOLOGO, A.AUTOR_REGISTRO_FK 
FROM 3m_cliente AS A
INNER JOIN 3m_encuesta AS B ON B.ID_CLIENTE_FK_5 = A.ID_CLIENTE
WHERE A.DEPARTAMENTO_CLIENTE='$CIUDAD' AND A.AUTOR_REGISTRO_FK<>'visita' AND A.AUTOR_REGISTRO_FK<>'MEDELLIN'
ORDER BY NOMBRE_CLIENTE ASC;",$conex);

 ?>

            <table style="width:99%; margin:auto auto;" rules="none" class="table table-responsive" >
            <tr>
                <th colspan='11' class="principal">CLIENTES CIUDAD: <?PHP echo $CIUDAD;?> </th>
            </tr>
            </table>
        <br />
            <table style="width:99%; margin:auto auto;"  id="gestion" class="table table-striped">
    <thead>
	<tr style=" text-align:center;">
 
    <th class="TITULO">IDENTIFICACI&Oacute;N</th>
    <th class="TITULO">CLIENTE</th>
    <th class="TITULO">DIRECCI&Oacute;N</th>
    <th class="TITULO">CELULAR</th>
    <th class="TITULO">TEL&Eacute;FONO</th>
    <th class="TITULO">CIUDAD</th>
    <th class="TITULO">CLASIFICACI&Oacute;N</th>
    <th class="TITULO">USUARIO REGISTRO</th>
    <th class="TITULO">VER GESTI&Oacute;N</th>
	</tr>
    </thead>
    <tbody>
    <?PHP
	$i=1;
    while($dato=mysql_fetch_array($CONSULTAR_POR_CIUDAD))
	{
		
	?>
		<tr class="datos" >
        	<td style="text-align:left;"><?php echo $dato["TIPO_IDENTIFICACION"]?>  <?php echo $dato["IDENTIFICACION_CLIENTE"]?></td>
          
            <td style="text-align:left;"><?php echo $dato["NOMBRE_CLIENTE"]?> <?php echo $dato["APELLIDO_CLIENTE"]?></td>
            <td style="text-align:left;"><?php echo $dato["DIRECCION_CLIENTE"]?></td>
            <td style="text-align:left;"><?php echo $dato["CELULAR_CLIENTE"]?></td>
            <td style="text-align:left;"><?php echo $dato["TELEFONO_CLIENTE"]?></td>
            <td style="text-align:left;"><?php echo $dato["CIUDAD_CLIENTE"]?></td>
            <td style="text-align:left;"><?php echo $dato["CLASIFICACION_ODONTOLOGO"]?></td>
            <td style="text-align:left;"><?php echo $dato["AUTOR_REGISTRO_FK"]?></td> 
            <th>
                <a  href="javascript:ventanaSecundaria('../presentacion/consulta_gestion_usuario.php?x=<?php echo base64_encode($dato['ID_CLIENTE'])?>')" >
              <img src="imagenes/lupa1.png" width="43" height="32" style="background-size:cover" title="DETALLE GESTÓN"/>
                </a> 
            </th>   
		</tr>
	<?php 
	}
	?>
    </tbody>
</table>
            
            <?php }
					//BUSQUEDA CIUDAD Y CLASIFICACION
		 else if(empty($CIUDAD)==false  &&  empty($CLASIFICACION)== false){
			 	$CONSULTA_POR_CLASIFICACION_CIUDAD = mysql_query("SELECT A.ID_CLIENTE,
A.NOMBRE_CLIENTE,
A.APELLIDO_CLIENTE,
A.TIPO_IDENTIFICACION,A.IDENTIFICACION_CLIENTE,A.DIRECCION_CLIENTE,A.DEPARTAMENTO_CLIENTE,
CONVERT(CAST(CONVERT(A.CIUDAD_CLIENTE USING latin1) AS BINARY) USING utf8) AS CIUDAD_CLIENTE,
A.CELULAR_CLIENTE,A.TELEFONO_CLIENTE,A.EMAIL_CLIENTE,A.RAZON_SOCIAL,A.NIT,A.PAGINA_WEB_NEGOCIO,B.CANTIDAD_PACIENTES,A.FECHA_REGISTRO,A.AUTOR_REGISTRO_FK,A.ESTADO_ASIGNADO,B.CLASIFICACION_ODONTOLOGO, A.AUTOR_REGISTRO_FK 
FROM 3m_cliente AS A
INNER JOIN 3m_encuesta AS B ON B.ID_CLIENTE_FK_5 = A.ID_CLIENTE
WHERE A.DEPARTAMENTO_CLIENTE='$CIUDAD' AND B.CLASIFICACION_ODONTOLOGO='$CLASIFICACION' AND A.AUTOR_REGISTRO_FK<>'visita' AND A.AUTOR_REGISTRO_FK<>'MEDELLIN'
ORDER BY NOMBRE_CLIENTE ASC;",$conex);	
?>

				  <table style="width:99%; margin:auto auto;" rules="none" class="table table-responsive" >
	<tr>
		<th colspan='11' class="principal">CLIENTES (ODONTOLOGOS)</th>
	</tr>
    </table>
  <table style="width:99%; margin:auto auto;"  id="gestion2" class="table table-striped">
         	<thead>
            	<tr style=" text-align:center;">
                <th class="TITULO">IDENTIFICACI&Oacute;N</th>
                <th class="TITULO">CLIENTE</th>
                <th class="TITULO">DIRECCI&Oacute;N</th>
                <th class="TITULO">CELULAR</th>
                <th class="TITULO">TEL&Eacute;FONO</th>
                <th class="TITULO">CIUDAD</th>
                <th class="TITULO">CLASIFICACI&Oacute;N</th>
                <th class="TITULO">USUARIO REGISTRO</th>
                <th class="TITULO">VER GESTI&Oacute;N</th>
         	</thead>
              <tbody>
                      <?PHP
			$i=1;
			while($dato = mysql_fetch_array($CONSULTA_POR_CLASIFICACION_CIUDAD) ){ ?>
                      <tr class="datos" >
                        <td style="text-align:left;"><?php echo $dato["TIPO_IDENTIFICACION"]?> <?php echo $dato["IDENTIFICACION_CLIENTE"]?></td>
                        <td style="text-align:left;"><?php echo $dato["NOMBRE_CLIENTE"]?> <?php echo $dato["APELLIDO_CLIENTE"]?></td>
                        <td style="text-align:left;"><?php echo $dato["DIRECCION_CLIENTE"]?></td>
                        <td style="text-align:left;"><?php echo $dato["CELULAR_CLIENTE"]?></td>
                        <td style="text-align:left;"><?php echo $dato["TELEFONO_CLIENTE"]?></td>
                        <td style="text-align:left;"><?php echo $dato["CIUDAD_CLIENTE"]?></td>
                        <td style="text-align:left;"><?php echo $dato["CLASIFICACION_ODONTOLOGO"]?></td>
                        <td style="text-align:left;"><?php echo $dato["AUTOR_REGISTRO_FK"]?></td>
                        <th> <a  href="javascript:ventanaSecundaria('../presentacion/consulta_gestion_usuario.php?x=<?php echo base64_encode($dato['ID_CLIENTE'])?>')" > <img src="imagenes/lupa1.png" width="43" height="32" style="background-size:cover" title="DETALLE GESTIÓN"/> </a> </th>
                      </tr>
                      <?php 
	}
	?>
                    </tbody>
  </table>
  <br />
<?php }
	}
	else{
	?>
</div>


<table style="width:99%; margin:auto auto;" rules="none" class="table table-responsive" >
	<tr>
		<th colspan='11' class="principal">CLIENTES (ODONTOLOGOS)</th>
	</tr>
</table>
<br />
<table style="width:99%; margin:auto auto;"  id="gestion" class="table table-striped">
    <thead>
	<tr style=" text-align:center;">
 
    <th class="TITULO">IDENTIFICACI&Oacute;N</th>
    <th class="TITULO">CLIENTE</th>
    <th class="TITULO">DIRECCI&Oacute;N</th>
    <th class="TITULO">CELULAR</th>
    <th class="TITULO">TEL&Eacute;FONO</th>
    <th class="TITULO">CIUDAD</th>
    <th class="TITULO">CLASIFICACI&Oacute;N</th>
    <th class="TITULO">USUARIO REGISTRO</th>
    <th class="TITULO">VER GESTI&Oacute;N</th>
	</tr>
    </thead>
    <tbody>
    <?PHP
	
$CONSULTA_NUEVOS_CLIENTES =mysql_query("
SELECT A.ID_CLIENTE,
A.NOMBRE_CLIENTE,
A.APELLIDO_CLIENTE,
A.TIPO_IDENTIFICACION,A.IDENTIFICACION_CLIENTE,A.DIRECCION_CLIENTE,A.DEPARTAMENTO_CLIENTE,
CONVERT(CAST(CONVERT(A.CIUDAD_CLIENTE USING latin1) AS BINARY) USING utf8) AS CIUDAD_CLIENTE,
A.CELULAR_CLIENTE,A.TELEFONO_CLIENTE,A.EMAIL_CLIENTE,A.RAZON_SOCIAL,A.NIT,A.PAGINA_WEB_NEGOCIO,B.CANTIDAD_PACIENTES,A.FECHA_REGISTRO,A.AUTOR_REGISTRO_FK,A.ESTADO_ASIGNADO,B.CLASIFICACION_ODONTOLOGO, A.AUTOR_REGISTRO_FK 
FROM 3m_cliente AS A
INNER JOIN 3m_encuesta AS B ON B.ID_CLIENTE_FK_5 = A.ID_CLIENTE
WHERE A.AUTOR_REGISTRO_FK<>'visita' AND A.AUTOR_REGISTRO_FK<>'MEDELLIN'
ORDER BY NOMBRE_CLIENTE ASC;",$conex);

	$i=1;
    while($dato=mysql_fetch_array($CONSULTA_NUEVOS_CLIENTES))
	{
		
	?>
		<tr class="datos" >
        	<td style="text-align:left;"><?php echo $dato["TIPO_IDENTIFICACION"]?>  <?php echo $dato["IDENTIFICACION_CLIENTE"]?></td>
          
            <td style="text-align:left;"><?php echo $dato["NOMBRE_CLIENTE"]?> <?php echo $dato["APELLIDO_CLIENTE"]?></td>
            <td style="text-align:left;"><?php echo $dato["DIRECCION_CLIENTE"]?></td>
            <td style="text-align:left;"><?php echo $dato["CELULAR_CLIENTE"]?></td>
            <td style="text-align:left;"><?php echo $dato["TELEFONO_CLIENTE"]?></td>
            <td style="text-align:left;"><?php echo $dato["CIUDAD_CLIENTE"]?></td>
            <td style="text-align:left;"><?php echo $dato["CLASIFICACION_ODONTOLOGO"]?></td>
            <td style="text-align:left;"><?php echo $dato["AUTOR_REGISTRO_FK"]?></td>
            <th>
                <a  href="javascript:ventanaSecundaria('../presentacion/consulta_gestion_usuario.php?x=<?php echo base64_encode($dato['ID_CLIENTE'])?>')" >
              <img src="imagenes/lupa1.png" width="43" height="32" style="background-size:cover" title="DETALLE GESTIÓN"/>
                </a> 
            </th>    
		</tr>
        
	<?php 
	}
	?>
     
      </tbody>
</table>
	 
	<?php } 
	?>
    </div>
    
   
</body>
</html>