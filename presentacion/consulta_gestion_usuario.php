<?php
	include("../logica/session.php");
        header('Content-Type: text/html; charset=utf-8');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Detalle Gesti&oacute;n</title>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css"/>
<link href="css/tablas.css" rel="stylesheet" /> 
<link href="css/bootstrap.css" rel="stylesheet" /> 
<script>
$(document).ready(function() {
    $('#gestion').DataTable();
} );
</script>
</head>
<?php
include('../datos/conex_copia.php');

	$string_intro = getenv("QUERY_STRING"); 
	parse_str($string_intro);
$usua;
?>
<body>
<br /><br />
<?php
$cliente = $_REQUEST["x"];
$cliente=base64_decode($cliente);
?>
<form id="cambio_contacto" name="cambio_contacto" method="post" action="#" enctype="multipart/form-data" class="letra">
<fieldset style="margin:auto auto; width:90%;">
  <div class="col-md-12">
    <div class="col-md-6" style="float:left">
      <label for="mes">MES</label><span class="asterisco">*</span><br />
                      <select class="form-control" name="mes">
                       	  <option value="">SELECCIONE</option>
                          <option value="1">ENERO</option>
                          <option value="2">FEBRERO</option>
                          <option value="3">MARZO</option>
                          <option value="4">ABRIL</option>
                          <option value="5">MAYO</option>
                          <option value="6">JUNIO</option>
                          <option value="7">JULIO</option>
                          <option value="8">AGOSTO</option>
                          <option value="9">SEPTIEMBRE</option>
                          <option value="10">OCTUBRE</option>
                          <option value="11">NOVIEMBRE</option>
                          <option value="12">DICIEMBRE</option>
                      </select>
              </div>
        <div class="col-md-2" style="float:left">
          <label for="cliente"></label><br /><br />
    <button title="Consultar" name="consultar"><img src="imagenes/lupa1.png" width="43" height="32" style="background-size:cover" title="CONSULTAR"></button>

    
    </div>
</div>
     
  </fieldset>
    
</form>
<br />

<div class="table table-responsive">
<?php 



if(isset($_POST["consultar"])){ 
	$mes  			= $_POST["mes"];	
		if(empty($mes)){
			echo 'esta vacia la busqueda';
			}
			//BUSQUEDA MES
			else if(empty($mes)==false ) {
			$consulta =mysqli_query($conex,"
			   SELECT DISTINCT A.FECHA_GESTION, A.ACCION, A.TIPIFICACION_GESTION, A.SUB_TIPIFICACION, A.OBSERVACION_GESTION, A.ASESOR_GESTION
FROM 3m_gestion AS A
INNER JOIN 3m_pedido AS B ON A.ID_CLIENTE_FK=B.ID_CLIENTE_FK
INNER JOIN 3m_cliente AS C ON B.ID_CLIENTE_FK=C.ID_CLIENTE
WHERE A.ID_CLIENTE_FK='$cliente' AND MONTH(A.FECHA_GESTION)='$mes'");
			}
		}
			else{
				
		$consulta=mysqli_query($conex,"SELECT DISTINCT A.FECHA_GESTION, A.ACCION, A.TIPIFICACION_GESTION, A.SUB_TIPIFICACION, A.OBSERVACION_GESTION, A.ASESOR_GESTION
FROM 3m_gestion AS A
INNER JOIN 3m_pedido AS B ON A.ID_CLIENTE_FK=B.ID_CLIENTE_FK
INNER JOIN 3m_cliente AS C ON B.ID_CLIENTE_FK=C.ID_CLIENTE
WHERE A.ID_CLIENTE_FK='$cliente'");
			
			}
 ?>
<table style='width:99%;border:1px solid transparent; margin:auto auto;' rules='all' id="gestion" class="table table-striped">
	<thead>
    <tr>
		<th colspan='6' class="principal" style="border-radius:0px;text-align:center;">DETALLE GESTI&Oacute;N </th>
	</tr>
    
	<tr style="text-align:center;">
		<th class="TITULO"><center>FECHA GESTI&Oacute;N</center></th>
		<th class="TITULO"><center>ACCION</center></th>
		<th class="TITULO"><center>TIPIFICACION</center></th>
      	<th class="TITULO"><center>SUB-TIPIFICACION</center></th>
        <th class="TITULO"><center>OBSERVACIONES</center></th>
        <th class="TITULO"><center>USUARIO</center></th>
	</tr>
    </thead>
        <tbody>
<?php	while($dato=mysqli_fetch_array($consulta))
		{ ?>
    <tr class="datos">
            <td><?php echo $dato["FECHA_GESTION"]?></td>
            <td><?php echo $dato["ACCION"]?></td>
            <td><?php echo $dato["TIPIFICACION_GESTION"]?></td>
            <td><?php echo $dato["SUB_TIPIFICACION"]?></td>
            <td><?php echo $dato["OBSERVACION_GESTION"]?></td>
            <td><?php echo $dato["ASESOR_GESTION"]?></td>	
   	</tr>	
	<?php 
		}
	?>
    </tbody>
	</table>
    </body>
</html>