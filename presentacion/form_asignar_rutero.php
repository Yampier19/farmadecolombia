<?php
	include ('../logica/session.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CARGAR DOCUMENTO</title>
<style>
.form-control
{
	display:block;
	padding:6px 12px;
	line-height:1.42857143;
	color:#555;
	background-color:#fff;
	background-image:none;
	border:1px solid #ccc;
	border-radius:4px;
	-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,.075);
	box-shadow:inset 0 1px 1px rgba(0,0,0,.075);
	-webkit-transition:border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
	-o-transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;
	transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;
	font-size:90%;
}
.btn_exp
{
	background-position:center;
	background-image:url(../presentacion/imagenes/exportar.png);
	background-color:inherit; 
	background-repeat:no-repeat;
	width:40px;
	height:40px;
	border-radius:100%;
	color:transparent;
}
.btn:focus,
.btn.focus {
  color: #fff;
  background-color: #286090;
  border-color: #122b40;
}
.btn:hover {
  color: #fff;
  background-color: #286090;
  border-color: #204d74;
}
.btn:active,
.btn.active,
.open > .dropdown-toggle.btn{
  color: #fff;
  background-color: #286090;
  border-color: #204d74;
}
.btn:active:hover,
.btn.active:hover,
.open > .dropdown-toggle.btn:hover,
.btn:active:focus,
.btn.active:focus,
.open > .dropdown-toggle.btn:focus,
.btn:active.focus,
.btn.active.focus,
.open > .dropdown-toggle.btn.focus {
  color: #fff;
  background-color: #204d74;
  border-color: #122b40;
}
.btn
{
  color: #fff;
  background-color: #337ab7;
  border-color: #2e6da4;
}
.form-control[disabled],.form-control[readonly],fieldset[disabled] .form-control
{
	background-color:#eee;
	opacity:1;
	cursor:not-allowed;
}
.asterisco
{
	color:#F00;	
}
body
{
	width:97%;
	font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
}
</style>
</head>
<?php
$string_intro = getenv("QUERY STRING"); 
parse_str($string_intro);
$usua;
?>
<body>
<form id="cambio_contacto" name="cambio_contacto" method="post" action="../logica/importar_rutero.php" enctype="multipart/form-data" class="letra">
	<fieldset style="margin:auto auto; width:50%;">
        <div class="form-group">
            <label for="cliente">USUARIO:</label><span class="asterisco">*</span>
            <br> 
            <input type="text" name="usuario" id="usuario" value="<?php echo $usua ?>" readonly="readonly" style="width:80%; display:inline-block" class="form-control"/>
            <input type="submit" name="descargar" id="descargar" value="Exportar" class="btn_exp" title="DESCARGAR ESTRUCTURA" formaction="doc/asignacion_rutas.csv"/>
        </div>
        <br />
        <br />
        <input id="archivo" name="archivo" type="file" class="form-control" style="width:80%;"/>
        <br />
        <br />
        <center>
        	<input type="submit" value="ENVIAR ARCHIVO" class="form-control btn"/>
        </center>
    </fieldset>
</form>
</body>
</html>