<?php
error_reporting(0);
include ('../logica/session.php');
header('Content-Type: text/html; charset=UTF-8'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>CONSULTAR GESTION</title>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css"/>
<link href="css/tablas.css" rel="stylesheet" /> 
<link href="css/bootstrap.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/alertify.js/0.3.11/alertify.min.js"></script>
<link href="https://cdn.jsdelivr.net/alertify.js/0.3.11/themes/alertify.core.css" rel="stylesheet" type="text/css"/>
<link href="https://cdn.jsdelivr.net/alertify.js/0.3.11/themes/alertify.default.css" rel="stylesheet" type="text/css"/> 
<script>
$(document).ready(function() {
    $('#gestion').DataTable();
} );

$(document).ready(function() {
    $('#gestionTodos').DataTable();
} );
function ventanaSecundaria (URL)
{ 
   window.open(URL,"ventana1","width=800,height=500,Top=150,Left=50%") 
}

</script>
</head>
<?php
include('../datos/conex_copia.php');
	$string_intro = getenv("QUERY_STRING"); 
	parse_str($string_intro);
$usua;
?>
<body>
<br /><br />

<form id="rutero" name="rutero" method="post" action="../logica/guardar_kpi.php" >
<fieldset style="margin:auto auto; width:90%;">
<legend>INGRESAR DATOS</legend>
<div class="col-md-12">
		<div class="col-md-6">
            <div class="col-md-6">
                <label for="cliente">MES:</label><span class="asterisco">*</span><br />
                <select class="form-control" name="mes">
                    <option value="" disabled="disabled" selected="selected">Seleccione</option> 
                    <option value="Enero">Enero</option>
                    <option value="Febrero">Febrero</option>
                    <option value="Marzo">Marzo</option>
                    <option value="Abril">Abril</option>
                    <option value="Mayo">Mayo</option>
                    <option value="Junio">Junio</option>
                    <option value="Julio">Julio</option>
                    <option value="Agosto">Agosto</option>
                    <option value="Septiembre">Septiembre</option>
                    <option value="Octubre">Octubre</option>
                    <option value="Noviembre">Noviembre</option>
                    <option value="Diciembre">Diciembre</option>
                </select> 
            </div>
            <div class="col-md-6">
                <label for="cliente">A&Ntilde;O:</label><span class="asterisco">*</span><br />
                <select class="form-control" name="anio">
                    <option value="" disabled="disabled" selected="selected">Seleccione</option> 
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                </select> 
            </div>
        </div>
        <div class="col-md-6">
            <label for="fecha">USUARIO:</label><span class="asterisco">*</span><br />
            <select class="form-control" name="idUsuario" multiple="multiple" required="required"> 
        <option value="" disabled="disabled" selected="selected">Seleccione</option>           
                <?php 
                    $consulta =mysqli_query($conex,"
         SELECT ID_USUARIO, USER FROM 3m_usuario
		 WHERE PRIVILEGIOS =2 AND ESTADO =1 AND USER <> 'medellin' AND USER <> 'emesa' AND USER <> 'visita' 
		  ORDER BY USER ASC ;");
                while($dato=mysqli_fetch_array($consulta)) { ?>
            <option value="<?php echo $dato['ID_USUARIO']; ?>"><?php echo $dato['USER']; ?></option>
                <?php } ?>
            </select>
        </div>
    
</div>
<br><br>
<br><br>
<br><br>
<div class="col-md-12">
        <div class="col-md-6">
        <label for="fecha">Conversión</label><span class="asterisco">*</span><br/>
        <input type="number" min="0" max="100" step="any" name="conversion" class="form-control" required="required"/>
        </div>
      
        <div class="col-md-6">
        <label for="fecha">Productos del mes</label><span class="asterisco">*</span><br />
        <input type="number" min="0" max="100" step="any" name="producto" class="form-control" required="required"/>
        </div>
  </div>
<div class="col-md-12">
	<div class="col-md-5"></div>
    <div class="col-md-2">
        <label for="cliente"></label><br /><br />
    	<button class="btn btn-primary" style="width: 152px" name="btn_ingresar_datos">Registrar</button>
    </div>
    <div class="col-md-5"></div>
</div>

    </fieldset>

</form>
<br>
<div style="border: 3px solid #9E1F1F"></div>
<br>
<form id="rutero" name="rutero" method="post" action="../logica/guardar_kpi.php" >
<fieldset style="margin:auto auto; width:90%;">
<legend>INGRESAR ACADEMICO</legend>
<div class="col-md-12">
        <div class="col-md-6">
            <div class="col-md-6">
                <label for="cliente">MES:</label><span class="asterisco">*</span><br />
                <select class="form-control" name="mes_a">
                    <option value="" disabled="disabled" selected="selected">Seleccione</option> 
                    <option value="Enero">Enero</option>
                    <option value="Febrero">Febrero</option>
                    <option value="Marzo">Marzo</option>
                    <option value="Abril">Abril</option>
                    <option value="Mayo">Mayo</option>
                    <option value="Junio">Junio</option>
                    <option value="Julio">Julio</option>
                    <option value="Agosto">Agosto</option>
                    <option value="Septiembre">Septiembre</option>
                    <option value="Octubre">Octubre</option>
                    <option value="Noviembre">Noviembre</option>
                    <option value="Diciembre">Diciembre</option>
                </select> 
            </div>
            <div class="col-md-6">
                <label for="cliente">A&Ntilde;O:</label><span class="asterisco">*</span><br />
                <select class="form-control" name="anio_a">
                    <option value="" disabled="disabled" selected="selected">Seleccione</option> 
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                </select> 
            </div>
        </div>
        <div class="col-md-6">
            <label for="fecha">USUARIO:</label><span class="asterisco">*</span><br />
            <select class="form-control" name="idUsuario" multiple="multiple" required="required"> 
        <option value="" disabled="disabled" selected="selected">Seleccione</option>           
                <?php 
                    $consulta = mysqli_query($conex,"
         SELECT ID_USUARIO, USER FROM 3m_usuario
         WHERE PRIVILEGIOS =2 AND ESTADO =1 AND USER <> 'medellin' AND USER <> 'emesa' AND USER <> 'visita' 
          ORDER BY USER ASC ;");
                while($dato=mysqli_fetch_array($consulta)) { ?>
            <option value="<?php echo $dato['ID_USUARIO']; ?>"><?php echo $dato['USER']; ?></option>
                <?php } ?>
            </select>
        </div>

</div>
<div class="col-md-12">

        <div class="col-md-8">
        <label for="fecha">Porcentaje Academico</label><span class="asterisco">*</span><br />
        <input type="number" min="0" max="100" step="any" name="academico" class="form-control" required="required"/>
        </div>
    
        <div class="col-md-4">
           <label for="cliente"></label><br /><br />
    <button class="btn btn-primary" style="width: 152px" name="btn_ingresar_academico">Registrar</button>
    </div>
</div>
     
    </fieldset>

</form>
<div class="table table-responsive">
<?php 
	$consult_user=mysqli_query($conex,"SELECT * FROM 3m_usuario WHERE ESTADO=1 AND PRIVILEGIOS=2 AND USER<>'VISITA' AND USER<>'MEDELLIN' ORDER BY CIUDAD ASC")
	?>
	<br>
	<table style="width:99%; margin:auto auto;" rules="none" class="table table-responsive" >
        <tr>
            <th colspan='11' class="principal">USUARIOS KPI'S</th>
        </tr>
        </table>
    <br />
      <table style="width:99%; margin:auto auto; text-align:center;" id="gestion">
        <thead>
        <tr>
    		<th class="TITULO">NOMBRE USUARIO</th>
    		<th class="TITULO">NOMBRES</th>
            <th class="TITULO">APELLIDOS</th>
            <th class="TITULO">CIUDAD</th>
			<th class="TITULO">KPI'S</th>

        </tr>
        </thead>
        <tbody>
		<?PHP
        while($dato=mysqli_fetch_array($consult_user))
        { ?>
            <tr class="datos" >
           		<td style="text-align:center;"><?php echo $dato["USER"]?></td>
                <td style="text-align:center;"><?php echo $dato["NOMBRES"]?></td>
                <td style="text-align:center;"><?php echo $dato["APELLIDOS"]?></td>
                <td style="text-align:center;"><?php echo $dato["CIUDAD"]?></td>
                <td>
                	<a href="javascript:ventanaSecundaria('../presentacion/detalle_kpi.php?id=<?php echo base64_encode($dato['ID_USUARIO']) ?>')" >
                        <img src="imagenes/lupa1.png" width="43" height="32" style="background-size:cover" title="KPI'S"/>
                    </a> 
                </td>
            </tr>
        <?php 
        }
        ?>
  </tbody>
  </table>
</div>
</body>
</html>