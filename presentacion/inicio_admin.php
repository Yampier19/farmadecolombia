<?php
	include("../logica/session.php");
        header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Coordinador</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <style type="text/css" class="ng-binding ng-scope">#cssmenu ul,
#cssmenu li,
#cssmenu span,
#cssmenu a {
  margin: 0;
  padding: 0;
  position: relative;
}
#cssmenu {
  line-height: 1;
  border-radius: 5px 5px 0 0;
  -moz-border-radius: 5px 5px 0 0;
  -webkit-border-radius: 5px 5px 0 0;
  background: #141414;
  background: -moz-linear-gradient(top, #333333 0%, #141414 100%);
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #333333), color-stop(100%, #141414));
  background: -webkit-linear-gradient(top, #333333 0%, #141414 100%);
  background: -o-linear-gradient(top, #333333 0%, #141414 100%);
  background: -ms-linear-gradient(top, #333333 0%, #141414 100%);
  background: linear-gradient(to bottom, #333333 0%, #141414 100%);
  border-bottom: 2px solid #069ae4;
  width: 1000px;
}
#cssmenu:after,
#cssmenu ul:after {
  content: '';
  display: block;
  clear: both;
}
#cssmenu a {
  background: #141414;
  background: -moz-linear-gradient(top, #333333 0%, #141414 100%);
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #333333), color-stop(100%, #141414));
  background: -webkit-linear-gradient(top, #333333 0%, #141414 100%);
  background: -o-linear-gradient(top, #333333 0%, #141414 100%);
  background: -ms-linear-gradient(top, #333333 0%, #141414 100%);
  background: linear-gradient(to bottom, #333333 0%, #141414 100%);
  color: #ffffff;
  display: block;
  font-family: Helvetica, Arial, Verdana, sans-serif;
  padding: 19px 20px;
  text-decoration: none;
}
#cssmenu ul {
  list-style: none;
}
#cssmenu > ul {
  font-size: 0;
}
#cssmenu > ul > li {
  display: inline-block;
  float: left;
  margin: 0;
}
#cssmenu.align-center {
  text-align: center;
}
#cssmenu.align-center > ul > li {
  float: none;
}
#cssmenu.align-center ul ul {
  text-align: left;
}
#cssmenu.align-right > ul {
  float: right;
}
#cssmenu.align-right ul ul {
  text-align: right;
}
#cssmenu > ul > li > a {
  color: #ffffff;
  font-size: 13px;
}
#cssmenu > ul > li:hover:after {
  content: '';
  display: block;
  width: 0;
  height: 0;
  position: absolute;
  left: 50%;
  bottom: 0;
  border-left: 10px solid transparent;
  border-right: 10px solid transparent;
  border-bottom: 10px solid #069ae4;
  margin-left: -10px;
}
#cssmenu > ul > li:first-child > a {
  border-radius: 5px 0 0 0;
  -moz-border-radius: 5px 0 0 0;
  -webkit-border-radius: 5px 0 0 0;
}
#cssmenu.align-right > ul > li:first-child > a,
#cssmenu.align-center > ul > li:first-child > a {
  border-radius: 0;
  -moz-border-radius: 0;
  -webkit-border-radius: 0;
}
#cssmenu.align-right > ul > li:last-child > a {
  border-radius: 0 5px 0 0;
  -moz-border-radius: 0 5px 0 0;
  -webkit-border-radius: 0 5px 0 0;
}
#cssmenu > ul > li.active > a,
#cssmenu > ul > li:hover > a {
  color: #ffffff;
  box-shadow: inset 0 0 3px #000000;
  -moz-box-shadow: inset 0 0 3px #000000;
  -webkit-box-shadow: inset 0 0 3px #000000;
  background: #070707;
  background: -moz-linear-gradient(top, #262626 0%, #070707 100%);
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #262626), color-stop(100%, #070707));
  background: -webkit-linear-gradient(top, #262626 0%, #070707 100%);
  background: -o-linear-gradient(top, #262626 0%, #070707 100%);
  background: -ms-linear-gradient(top, #262626 0%, #070707 100%);
  background: linear-gradient(to bottom, #262626 0%, #070707 100%);
}
#cssmenu .has-sub {
  z-index: 1;
}
#cssmenu .has-sub:hover > ul {
  display: block;
}
#cssmenu .has-sub ul {
  display: none;
  position: absolute;
  width: 200px;
  top: 100%;
  left: 0;
}
#cssmenu.align-right .has-sub ul {
  left: auto;
  right: 0;
}
#cssmenu .has-sub ul li {
  *margin-bottom: -1px;
}
#cssmenu .has-sub ul li a {
  background: #069ae4;
  border-bottom: 1px dotted #84c8ee;
  font-size: 12px;
  filter: none;
  display: block;
  line-height: 120%;
  padding: 10px;
  color: #ffffff;
}
#cssmenu .has-sub ul li:hover a {
  background: #056b9e;
}
#cssmenu ul ul li:hover > a {
  color: #ffffff;
}
#cssmenu .has-sub .has-sub:hover > ul {
  display: block;
}
#cssmenu .has-sub .has-sub ul {
  display: none;
  position: absolute;
  left: 100%;
  top: 0;
}
#cssmenu.align-right .has-sub .has-sub ul,
#cssmenu.align-right ul ul ul {
  left: auto;
  right: 100%;
}
#cssmenu .has-sub .has-sub ul li a {
  background: #056b9e;
  border-bottom: 1px dotted #84c8ee;
}
#cssmenu .has-sub .has-sub ul li a:hover {
  background: #b30000;
}
#cssmenu ul ul li.last > a,
#cssmenu ul ul li:last-child > a,
#cssmenu ul ul ul li.last > a,
#cssmenu ul ul ul li:last-child > a,
#cssmenu .has-sub ul li:last-child > a,
#cssmenu .has-sub ul li.last > a {
  border-bottom: 0;
}
</style>

<script>

var height= window.innerHeight-10;

var ancho=window.innerWidth;
//alert(ancho);
/*alert(height);*/
$(document).ready(function()
{
    $('#cssmenu').css('display','block');

    if(ancho<1001)
    {
        
        $('#info').css('height',height);
        var tama_menu=$("#cssmenu").height();
        tama_menu=tama_menu*(-1);
        $('#cssmenu li').click(function()
        {
            $("#cssmenu").animate({marginLeft:'-120%'},500);
            $("#cssmenu").css('display','none');
            $('#info').toggle(1000);
        });
    }
    else
    {
        $('#info').css('height',height);
        $('#info').css('display','block');
    }
});

function ventanaSecundaria (URL){ 
   window.open(URL,"ventana1","width=1000,height=500,Top=100,Left=200") 
} 
</script>

</head>


<body>
<center><img src="imagenes/logo2.png" width="34%" height="17%"/>
<h2 style="color:#045491; font-family: Arial Black; margin-left: 30px;"><strong>VISITA ODONT&Oacute;LOGO</strong></h2>


<div id="cssmenu">
  <ul>
    <li>
      <a href="../presentacion/ver_inicio_sesion.php" target="info"><span><i class="fa fa-fw fa-map-marker"></i> INICIO SESION</span></a>
    </li>


    <li class="has-sub"><a><span><i class="fa fa-fw fa-bar-chart"></i> GESTIONES</span></a>
      <ul>
	  
        <li style="text-align: left;">
          <a href="../presentacion/ver_gestion_usuario.php" target="info"><span>CONSULTAR GESTIONES</span></a>
        </li>
        <li style="text-align: left;">
          <a href="../presentacion/consulta_eliminar_gestion.php" target="info"><span>ELIMINAR GESTION</span></a>
        </li>
      </ul>
    </li>
    
    <!--<li class="has-sub"><a><span><i class="fa fa-fw fa-credit-card"></i> PEDIDO</span></a>
        <ul>
          <li style="text-align: left;">
            <a href="../presentacion/consulta_pedidos.php" target="info"><span>PEDIDOS Y TRASFERENCIAS</span></a>
          </li>
          <li style="text-align: left;">
            <a href="../presentacion/consulta_pedido_entregado.php" target="info"><span>PEDIDOS ENTREGADOS</span></a>
          </li>
          <li style="text-align: left;">
            <a href="../presentacion/consulta_dentales_pedidos.php" target="info"><span>PEDIDOS DISTRIBUIDORES</span></a>
          </li>
          <li style="text-align: left;">
            <a href="../presentacion/consulta_eliminar_pedido.php" target="info"><span>ELIMINAR PEDIDO</span></a>
          </li>
        </ul>
    </li>-->
    
    <li>
        <a href="../presentacion/consultar_nuevos_clientes.php" target="info"><span><i class="fa fa-fw fa-group"></i> CLIENTES</span></a>
    </li>
    
    <li class="has-sub"><a><span><i class="fa fa-fw fa-shopping-cart"></i> PRODUCTOS</span></a>
      <ul>
        <li style="text-align: left;">
          <a href="../presentacion/consultar_productos.php" target="info"><span>CONSULTAR PRODUCTOS</span></a>
        </li>
        <li style="text-align: left;">
          <a href="../presentacion/consultar_promociones.php" target="info"><span>CONSULTAR PROMOCIONES</span></a>
        </li>
      </ul>
    </li>
    
    <li>
      <a href="../presentacion/asignar_ruta.php" target="info"><span><i class="fa fa-fw fa-street-view"></i> RUTERO</span></a>
    </li>
    
    <li>
      <a href="../presentacion/novedades.php" target="info"><span><i class="fa fa-fw fa-inbox"></i> NOVEDADES</span></a>
    </li>
    
    <!--<li>
      <a href="../presentacion/form_kpi.php" target="info"><span><i class="fa fa-fw fa-inbox"></i> KPI'S</span></a>
    </li>-->

    <li class="has-sub"><a><span><i class="fa fa-fw fa-tasks"></i>REPORTE</span></a>
        <ul>
            <li style="text-align: left;">
              <a href="../informes/index.php" target="info"><span>TABLAS</span></a>
            </li>
            <li style="text-align: left;">  
              <a href="../logica/reporte_descuento_pedido.php" target="info"><span>DESCUENTO PEDIDOS</span></a>
            </li>
            <li style="text-align: left;">
              <a href="../presentacion/reporte_transferencias_rechazada.php" target="info"><span>TRANSFERENCIAS RECHAZADAS</span></a>
            </li>
        </ul>
    </li>
    <li>
      <a href="../logica/cerrar_sesion2.php"><span><i class="fa fa-fw fa-power-off"></i></span></a>
    </li>
  </ul>
</div>
</center>
    <iframe style="width:100%;border:1px solid transparent; height:500px;" name="info" id="info" scrolling="auto"></iframe>
</body>
</html>