function consultar_productos_consumo()
{
	var categoria=$('#categoria').val();
	var subcategoria=$('#subcategoria').val();
	var Descripcion=$('#Descripcion').val();
	var id_encuesta=$('#id_encuesta').val();
	$.ajax(
	{
	//	url:'../presentacion/ingresar_productos.php',
		data:
		{
			categoria:categoria,
			subcategoria:subcategoria,
			descripcion:Descripcion,
			id_encuesta:id_encuesta
		},
		type: 'post',
		beforeSend: function () 
		{
			//alert('Cargando')
		},
		success: function(data)
		{
			$('#categoria').find('option:first').attr('selected', 'selected').parent('select');
			$('#subcategoria').html('');
			$('#Descripcion').html('');
			$("#subcategoria").attr('disabled', 'disabled');
			$("#Descripcion").attr('disabled', 'disabled');
			
		}
	})
}

function consultar_productos_consumo2()
{
	$.ajax(
	{
		url:'../presentacion/consulta_productos.php',
		data:
		{
		},
		type: 'post',
		beforeSend: function () 
		{
			
		},
		success: function(data)
		{
			
		}
	})
}
//productos de la competencias tipo dental
function categoria_competencia()
{
	$.ajax(
	{
		url:'../presentacion/consulta_categoria_competencia.php',
		type: 'post',
		beforeSend: function () 
		{
			$("#categoria_competencia").attr('disabled', 'disabled');
			//$("#subcategoria_competencia").attr('disabled', 'disabled');
			$("#Descripcion_competencia").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#categoria_competencia").removeAttr('disabled');
			$('#categoria_competencia').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}


function Descripcion_competencia()
{
	var categoria=$('#categoria_competencia').val();
	$.ajax(
	{
		url:'../presentacion/consulta_descripcion_competencia.php',
		data:
		{
			categoria: categoria
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#Descripcion_competencia").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#Descripcion_competencia").removeAttr('disabled');
			$('#Descripcion_competencia').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}

function categoria()
{
	$.ajax(
	{
		url:'../presentacion/consulta_categoria.php',
		type: 'post',
                //contentType: "application/x-www-form-urlencoded;charset=utf-8",
		beforeSend: function () 
		{
			$("#categoria").attr('disabled', 'disabled');
			$("#subcategoria").attr('disabled', 'disabled');
			$("#Descripcion").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#categoria").removeAttr('disabled');
			$('#categoria').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}

function subcategoria()
{
	var categoria=$('#categoria').val();
	$.ajax(
	{
		url:'../presentacion/consulta_subcategoria.php',
		data:
		{
			categoria: categoria
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#subcategoria").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#subcategoria").removeAttr('disabled');
			$('#subcategoria').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}
function Descripcion()
{
	var subcategoria=$('#subcategoria').val();
	var categoria=$('#categoria').val();
	$.ajax(
	{
		url:'../presentacion/consulta_descripcion.php',
		data:
		{
			categoria: categoria,
			subcategoria:subcategoria
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#Descripcion").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#Descripcion").removeAttr('disabled');
			$('#Descripcion').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}

function categoria_ortodoncia(){
    $.ajax(
	{
		url:'../presentacion/consulta_ortodoncia.php',
		data:
		{
			tipo: 1
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
		    $('#ortodoncia_categoria').html(data);
		}
	})
}

function Descripcion_ortodoncia()
{
	var categoria=$('#ortodoncia_categoria').val();
	$.ajax(
	{
		url:'../presentacion/consulta_ortodoncia.php',
		data:
		{
			categoria: categoria,
                        tipo:2
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#Descripcion_ortodoncia").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#Descripcion_ortodoncia").removeAttr('disabled');
			$('#Descripcion_ortodoncia').html(data);
                        $('#guardar_ortodoncia').css("display","block");
			$("#cargando").fadeOut([1000]);
		}
	})
}

/* productos de la competencia encuesta ortodoncia*/

function categoria_competencia_ortodoncia()
{
	$.ajax(
	{
		url:'../presentacion/consulta_ortodoncia.php',
		data:
		{
                        tipo:3
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#categoria_competencia_ortodoncia").attr('disabled', 'disabled');
			$("#subcategoria_competencia_ortodoncia").attr('disabled', 'disabled');
			$("#Descripcion_competencia_ortodoncia").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#categoria_competencia_ortodoncia").removeAttr('disabled');
			$('#categoria_competencia_ortodoncia').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}


function Descripcion_competencia_ortodoncia()
{
	var categoria=$('#categoria_competencia_ortodoncia').val();
	$.ajax(
	{
		url:'../presentacion/consulta_ortodoncia.php',
		data:
		{
			categoria: categoria,   
                        tipo:5
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#Descripcion_competencia_ortodoncia").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#Descripcion_competencia_ortodoncia").removeAttr('disabled');
			$('#Descripcion_competencia_ortodoncia').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}



function consultar()
{
	var id_encuesta=$('#id_encuesta').val();
	var id_cliente=$('#id_cliente').val();
	$.ajax(
	{
		url:'../presentacion/consultar_productos_usados.php',
		data:
		{
			id_encuesta: id_encuesta,
            id_cliente:id_cliente
		},
		type: 'post',
		beforeSend: function () 
		{
			//alert('Cargando')
		},
		success: function(data)
		{
			$('#tablas_usado').html(data);
		}
	})
}
$(document).ready(function()
{
	consultar_productos_consumo();
	categoria();
	categoria_competencia();
        categoria_ortodoncia();
        categoria_competencia_ortodoncia();
	consultar();
	setInterval("consultar_productos_consumo2()", 5000);
	//SetTimeout(Nombre_funcion, milisegundos);
	var height= 700;
	//alert(height)
	//$('#cargando').css('margin-top',tama_header);
	$('#cargando').css('height',height);
	var div_caragr=($("#cargando").height()/2);
	var div_cargar_ancho=($("#cargando").width()/2);
	var div_cargar_ancho_img=$("#img_cargando").width();
	var total=div_caragr;
	var total_ancho_img=(div_cargar_ancho-div_cargar_ancho_img)/2;
	//$('#img_cargando').css('margin','auto'
	$('#img_cargando').css('margin-top',total);
	$('#categoria').change(function()
	{
		Descripcion();
		//$('#Descripcion').html('');
		
	});
	$('#subcategoria').change(function()
	{
		Descripcion();
	});
	$('#Descripcion').change(function()
	{
		descripcion=$('#Descripcion').val();
		
		if(descripcion!='')
		{
			$('#guardar').css("display","block");
		}
	});
        //inicio ortodoncia
        $('#ortodoncia_categoria').change(function()
	{
		Descripcion_ortodoncia();
	});
        
	$('#categoria_competencia_ortodoncia').change(function()
	{
		Descripcion_competencia_ortodoncia();
		$('#Descripcion_competencia_ortodoncia').html('');
		$('#Descripcion_competencia_ortodoncia').html('');
		$("#Descripcion_competencia_ortodoncia").attr('disabled', 'disabled');
	});
	
	
	
	$('#Descripcion_competencia_ortodoncia').change(function()
	{
		descripcion=$('#Descripcion_competencia_ortodoncia').val();
		
		if(descripcion!='')
		{
			$('#guardar_competencia_ortodoncia').css("display","block");
		}
	});  
        // fin ortodoncia
	
	$('#guardar').click(function()
	{
		//INGRESAR();
		consultar_productos_consumo()
		$('#guardar').css("display","none");
		$('#confirmar').css("display","block");
	});
	$('#elim').click(function()
	{
		alert('ok')
	});
	
	$('#categoria_competencia').change(function()
	{
		Descripcion_competencia();
		$('#Descripcion_competencia').html('');
		$('#Descripcion_competencia').html('');
		$("#Descripcion_competencia").attr('disabled', 'disabled');
	});
	

	
	$('#Descripcion_competencia').change(function()
	{
		descripcion=$('#Descripcion_competencia').val();
		
		if(descripcion!='')
		{
			$('#guardar_competencia').css("display","block");
		}
	});
	
});

function validar(tuformulario)
{	

	var cantidad_pacientes= $('#cantidad_pacientes').val();
        var tipo_odontologo=$('#tipo_odontologo').val();
	var mensaje='';
        if(tipo_odontologo==1){//odontologo
           mensaje ='la cantidad de pacientes que atiende por semana';
            var resina  =$("#resinas",productos_usados_lista.document).val();
            var cemento =$("#cemento",productos_usados_lista.document).val();
            var adhesivo  =$("#adhesivo",productos_usados_lista.document).val();
            var pulido  =$("#pulido",productos_usados_lista.document).val();
            var productos_usados =$("#productos_usados").val();
        
             conteo_consumo  =$("#conteo_consumo",productos_competencia_lista.document).val();
             conteo_competencia  =$("#conteo_competencia",productos_competencia_lista.document).val();
       
        }
	else if(tipo_odontologo==2){// ortodoncista
	mensaje ='el numero de valoraciones mensuales';
             conteo_consumo  =$("#conteo_consumo",productos_competencia_lista_ortodoncia.document).val();
             conteo_competencia  =$("#conteo_competencia",productos_competencia_lista_ortodoncia.document).val();           
		}
       productos_3m= parseInt(productos_usados) + parseInt(conteo_consumo);
		
      var fecha_copia = $("#fecha_copia").val();  
      var observacion_copia = $("#observacion_copia").val();
	  var hora_ini_visita = $("#hora_ini_visita").val();
	  var hora_fin_visita =$("#hora_fin_visita").val();
	  var dia_visita =  $("#dia_visita").val();
	
	if(fecha_copia.length==0){
            swal(
             {title:"Seleccione la fecha de la proxima visita",
             confirmButtonColor: '#BC3228'});
		
	}
	else if(observacion_copia==''){
            swal(
             {title:'Ingrese una observacion',
             confirmButtonColor: '#BC3228'});
		
	}
	else if(dia_visita==''){
		swal(
             {title:'Seleccione el dia de la visita',
             confirmButtonColor: '#BC3228'});
	}
	else if(hora_ini_visita==''){
		 swal(
             {title:'Seleccione la hora de inicio de la visita',
             confirmButtonColor: '#BC3228'});
	}
	else if(hora_fin_visita==''){
		swal(
             {title:'Seleccione la hora de finalizacion de la visita',
             confirmButtonColor: '#BC3228'});
	}

	else if(cantidad_pacientes==0){
            swal(
             {title:'Ingrese '+mensaje+".",
             confirmButtonColor: '#BC3228'});
		
	}

	else if(productos_3m==0){
             swal(
		  {title:'Ingrese al menos un producto que utiliza de 3M',
		  confirmButtonColor: '#BC3228'});
		
         
	}
    else if(conteo_competencia==0){
             swal(
		  {title:'Ingrese al menos un producto de la competencia',
		  confirmButtonColor: '#BC3228'});
		        
        }
        else{
            
           if(tipo_odontologo==1) {
                if(resina == 0 && cemento == 0 && adhesivo==0 && pulido==0 ){
                     alertify.error(' Cliente potencial para: <center><b> Resinas, Cemento, Adhesivo y Sistema de Pulido</b></center>');
                   }
                else if(resina > 0 && cemento == 0 && adhesivo==0 && pulido==0 ){
                         alertify.error(' Cliente potencial para:: <center><b>  Cemento, Adhesivo y Sistema de Pulido</b></center>'); 
                     }        
                else if(resina == 0 && cemento > 0 && adhesivo==0 && pulido==0 ){
                         alertify.error(' Cliente potencial para: <center><b> Resinas, Adhesivo y Sistema de Pulido</b></center>');
                        }
                else if(resina == 0 && cemento == 0 && adhesivo >0 && pulido==0 ){
                         alertify.error(' Cliente potencial para: <center><b> Resinas, Cemento y Sistema de Pulido</b></center>');
                }
                else if(resina == 0 && cemento == 0 && adhesivo==0 && pulido>0 ){
                    alertify.error(' Cliente potencial para: <center><b> Resinas, Adhesivo y Cemento</b></center>');
                 }
                else if(resina > 0 && cemento > 0 && adhesivo==0 && pulido==0 ){
                   alertify.error(' Cliente potencial para: <center><b> Resinas, Cemento y Sistema de Pulido</b></center>');          
                      }
                else if(resina > 0 && cemento == 0 && adhesivo >0 && pulido==0 ){
                     alertify.error(' Cliente potencial para: <center><b>Cemento y Sistema de Pulido</b></center>');
                }
                else if(resina > 0 && cemento == 0 && adhesivo==0 && pulido>0 ){
                     alertify.error(' Cliente potencial para: <center><b>Adhesivo y Cemento</b></center>');
                }
                else if(resina == 0 && cemento > 0 && adhesivo>0 && pulido==0 ){
                     alertify.error(' Cliente potencial para: <center><b>Resina y Sistema de Pulido</b></center>');
                }
                else if(resina == 0 && cemento > 0 && adhesivo==0 && pulido >0 ){
                     alertify.error(' Cliente potencial para: <center><b>Cemento y Adhesivo</b></center>');
                }
                else if(resina == 0 && cemento == 0 && adhesivo >0 && pulido >0 ){
                     alertify.error(' Cliente potencial para: <center><b>Resina y Cemento</b></center>');
                }
                else if(resina > 0 && cemento > 0 && adhesivo>0 && pulido==0 ){
                     alertify.error(' Cliente potencial para: <center><b>Sistema de Pulido</b></center>');
                 }
                else if(resina == 0 && cemento > 0 && adhesivo >0 && pulido >0 ){
                     alertify.error(' Cliente potencial para: <center><b>Resina</b></center>');
                 }
                else if(resina > 0 && cemento == 0 && adhesivo >0 && pulido >0 ){
                     alertify.error(' Cliente potencial para: <center><b>Cemento</b></center>');	
                }
                else if(resina > 0 && cemento > 0 && adhesivo==0 && pulido>0 ){
                     alertify.error(' Cliente potencial para: <center><b> Adhesivo</b></center>');
                }
            }
	swal({
		  title: 'ESTA SEGURO(A) DE GUARDAR LA ENCUESTA',
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Confirmar',
		  cancelButtonText: 'Cancelar'
		}).then((result) => {
		  if(result.value) {
			//document.getElementById("tranferencias").submit();
			 $('#tranferencias').submit();
		return (true);
		  }  
		});
	}
	return (false);
}
