// JavaScript Document

var height= window.innerHeight-10;/*tamaño ventana*/
var ancho=window.innerWidth;

$(document).ready(function()
{
	$('#proxima_visita').change(function()
	{
		var proxima_visita = $("#proxima_visita").val();
		var encuesta=$('#encuesta_copia').val();
		
		if(encuesta==1){
			$("#fecha_copia").val(proxima_visita);
		}
	});
	
	$('#observacion').keyup(function()
	{
		var observacion= $("#observacion").val();
		var encuesta=$('#encuesta_copia').val();
		
		if(encuesta==1){
			$("#observacion_copia").val(observacion);
		}
	});
	
	$('#resultado_text').change(function()
	{
	
		var resultado=$('#resultado_text').val();
		var encuesta=$('#encuesta').val();
		var tipo_odontologo =0;

		if(resultado==2){
			tipo_odontologo=1;
			
		}else{
			 tipo_odontologo=$('#tipo_odontologo').val();
		}
		
	console.log("tipo de odontologo: "+tipo_odontologo+"<br>");
	console.log("resultado: "+resultado+"<br>");
		
		if(tipo_odontologo==0){
		swal({title: 'Seleccione el tipo de odontologo',
			confirmButtonColor: '#BC3228'});

			$('#visita_no_efectiva').css('display','none'); //visita no efectiva
			$('#boton_no_efectiva').css('display','none'); //boton no efectiva
			$('#boton_televenta').css('display','none'); //boton televenta
			$('#boton_venta_evento').css('display','none'); //boton venta evento
			$('#efectiva_sin_encuesta').css('display','none'); //boton efectiva sin encuesta
			$('#encuesta_consumo').css('display','none'); //encuesta
		}
		else{
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	if(resultado==1){ //efectiva
			var distribuidor=$('#distribuidor').val();
			var email=$('#email').val();
			var correo=$('#distribuidor').val();
            var numero_efectivas = $("#numero_efectivas").val();
            console.log("numero de efectivas "+numero_efectivas);
                        
            if(numero_efectivas > 2){

                $('#fecha_observaciones').css('display','none'); //campo fecha proxima visita-observaciones
                $('#visita_no_efectiva').css('display','none'); //visita no efectiva
                $('#boton_no_efectiva').css('display','none'); //boton no efectiva
                $('#boton_televenta').css('display','none'); //boton televenta
                $('#boton_venta_evento').css('display','none'); //boton venta evento
                $('#efectiva_sin_encuesta').css('display','none'); //boton efectiva sin encuesta
                $('#encuesta_consumo').css('display','none'); //encuesta
                                
                swal(
                	{title: 'Anteriormente se han registrado las 2 gestiones efectivas del mes.<br> Seleccione una opcion diferente a Efectiva o comuniquese con el Coordinador',
                    confirmButtonColor: '#BC3228'});
            }else{   
                        
			var sub_tipificacion=$('#sub_tipificacion_efectiva').val(0);

			if(distribuidor!='')
			{// tiene distribuidor
				$('#visita_efectiva').css('display','block'); // listado de subtipificaciones efectivas
				$('#fecha_observaciones').css('display','block'); //campo fecha proxima visita-observaciones
				$('#encuesta_copia').val(0);
			}
			/*else if(distribuidor=='')
			{
				swal(
					{title: 'Ingrese el Distribuidor',
					confirmButtonColor: '#BC3228'});
					
					$('#fecha_observaciones').css('display','none'); //campo fecha proxima visita-observaciones
					$('#visita_efectiva').css('display','none'); //visita efectiva
					$('#encuesta_copia').val(0);
			}*/
			
					$('#visita_no_efectiva').css('display','none'); //visita no efectiva
					$('#boton_no_efectiva').css('display','none'); //boton no efectiva
					$('#boton_televenta').css('display','none'); //boton televenta
					$('#boton_venta_evento').css('display','none'); //boton venta evento
					$('#efectiva_sin_encuesta').css('display','none'); //boton efectiva sin encuesta
					$('#encuesta_consumo').css('display','none'); //encuesta

        } 
        if(email!='')
			{// tiene email
				if(distribuidor=='')
				{
					/*$('#fecha_observaciones').css('display','none'); //campo fecha proxima visita-observaciones
					$('#visita_efectiva').css('display','none'); //visita efectiva
					$('#encuesta_copia').val(0);*/
					$('#visita_efectiva').css('display','block'); // listado de subtipificaciones efectivas
					$('#fecha_observaciones').css('display','block'); //campo fecha proxima visita-observaciones
					$('#encuesta_copia').val(0);
				}else{	
					$('#visita_efectiva').css('display','block'); // listado de subtipificaciones efectivas
					$('#fecha_observaciones').css('display','block'); //campo fecha proxima visita-observaciones
					$('#encuesta_copia').val(0);
				}
			}
			else if(email=='')
			{
				swal(
					{title: 'Ingrese el E-mail',
					confirmButtonColor: '#BC3228'});
					
					$('#fecha_observaciones').css('display','none'); //campo fecha proxima visita-observaciones
					$('#visita_efectiva').css('display','none'); //visita efectiva
					$('#encuesta_copia').val(0);
			}
		obtener();
		}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		else if(resultado==2){// no efectiva
			
			$('#fecha_observaciones').css('display','block'); //campo fecha proxima visita-observaciones
			$('#visita_no_efectiva').css('display','block');//listado subtipificaciones NO efectivas
			$('#boton_no_efectiva').css('display','block'); // listado de subtipificaciones efectivas					
			$('#visita_efectiva').css('display','none'); //visita efectiva
			$('#boton_televenta').css('display','none'); //boton televenta
			$('#boton_venta_evento').css('display','none'); //boton venta evento
			$('#efectiva_sin_encuesta').css('display','none'); //boton efectiva sin encuesta
			$('#encuesta_consumo').css('display','none'); //encuesta
			$('#encuesta_copia').val(0);
			
			obtener();
		}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		else if(resultado==3){// información
			
			$('#fecha_observaciones').css('display','block'); //campo fecha proxima visita-observaciones
			$('#visita_no_efectiva').css('display','block');//listado subtipificaciones NO efectivas
			//('#boton_no_efectiva').css('display','block'); // listado de subtipificaciones efectivas					
			$('#visita_efectiva').css('display','none'); //visita efectiva
			$('#boton_televenta').css('display','none'); //boton televenta
			$('#boton_venta_evento').css('display','none'); //boton venta evento
			$('#efectiva_sin_encuesta').css('display','none'); //boton efectiva sin encuesta
			$('#encuesta_consumo').css('display','none'); //encuesta
			$('#encuesta_copia').val(0);
			
			obtener();
		}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		else if(resultado ==4){ //venta en evento
				
				var distribuidor=$('#distribuidor').val();
				var email=$('#email').val();
					if(distribuidor!='')
					{
						
						if(encuesta==1){
							$("#encuesta_consumo").css('display','block');
							$('#encuesta_copia').val(1);
						}
						else{
							
							$("#encuesta_consumo").css('display','none');
							$('#boton_venta_evento').css('display','block'); 
							$('#encuesta_copia').val(0);
							}
						
						$('#fecha_observaciones').css('display','block'); //campo fecha proxima visita-observaciones
						$('#visita_no_efectiva').css('display','none'); //visita no efectiva
						$('#visita_efectiva').css('display','none'); //visita efectiva
						$('#boton_no_efectiva').css('display','none'); //boton no efectiva
						$('#boton_televenta').css('display','none'); //boton televenta
						$('#efectiva_sin_encuesta').css('display','none'); //boton efectiva sin encuesta
					
					}
					/*else {
						swal(
							{title: 'Ingrese el Distribuidor',
							confirmButtonColor: '#BC3228'});
							
					$('#fecha_observaciones').css('display','none'); //campo fecha proxima visita-observaciones
					$('#visita_no_efectiva').css('display','none'); //visita no efectiva
					$('#visita_efectiva').css('display','none'); //visita efectiva
					$('#boton_no_efectiva').css('display','none'); //boton no efectiva
					$('#boton_televenta').css('display','none'); //boton televenta
					$('#boton_venta_evento').css('display','none'); //boton venta evento
					$('#efectiva_sin_encuesta').css('display','none'); //boton efectiva sin encuesta
					$('#encuesta_consumo').css('display','none'); //encuesta
						
				}*/
				if(email!=''){// tiene email
					if(distribuidor=='')
					{
						/*$('#fecha_observaciones').css('display','none'); //campo fecha proxima visita-observaciones
						$('#visita_no_efectiva').css('display','none'); //visita no efectiva
						$('#visita_efectiva').css('display','none'); //visita efectiva
						$('#boton_no_efectiva').css('display','none'); //boton no efectiva
						$('#boton_televenta').css('display','none'); //boton televenta
						$('#boton_venta_evento').css('display','none'); //boton venta evento
						$('#efectiva_sin_encuesta').css('display','none'); //boton efectiva sin encuesta
						$('#encuesta_consumo').css('display','none');*/ //encuesta
					}else{	
						if(encuesta==1){
							$("#encuesta_consumo").css('display','block');
							$('#encuesta_copia').val(1);
						}
						else{
							
							$("#encuesta_consumo").css('display','none');
							$('#boton_venta_evento').css('display','block'); 
							$('#encuesta_copia').val(0);
							}
						
						$('#fecha_observaciones').css('display','block'); //campo fecha proxima visita-observaciones
						$('#visita_no_efectiva').css('display','none'); //visita no efectiva
						$('#visita_efectiva').css('display','none'); //visita efectiva
						$('#boton_no_efectiva').css('display','none'); //boton no efectiva
						$('#boton_televenta').css('display','none'); //boton televenta
						$('#efectiva_sin_encuesta').css('display','none'); //boton efectiva sin encuesta
					}
				}
				else if(email=='')
				{
					swal(
						{title: 'Ingrese el E-mail',
						confirmButtonColor: '#BC3228'});
						
						$('#fecha_observaciones').css('display','none'); //campo fecha proxima visita-observaciones
						$('#visita_no_efectiva').css('display','none'); //visita no efectiva
						$('#visita_efectiva').css('display','none'); //visita efectiva
						$('#boton_no_efectiva').css('display','none'); //boton no efectiva
						$('#boton_televenta').css('display','none'); //boton televenta
						$('#boton_venta_evento').css('display','none'); //boton venta evento
						$('#efectiva_sin_encuesta').css('display','none'); //boton efectiva sin encuesta
						$('#encuesta_consumo').css('display','none'); //encuesta
				}
			}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
		else if(resultado==3){//televenta
		
			var distribuidor=$('#distribuidor').val();
			var email=$('#email').val();
			if(distribuidor!='')
			{
				if(encuesta==1){
					$("#encuesta_consumo").css('display','block');
					$('#encuesta_copia').val(1);
				}else {
					$("#boton_televenta").css('display','block');
					$("#encuesta_consumo").css('display','none');
					$('#encuesta_copia').val(0);
					}
					
					$('#fecha_observaciones').css('display','block'); //campo fecha proxima visita-observaciones
					$('#visita_televenta').css('display','block'); //suptipificacion de televenta
					$('#visita_no_efectiva').css('display','none'); //visita no efectiva
					$('#visita_efectiva').css('display','none'); //visita efectiva
					$('#boton_no_efectiva').css('display','none'); //boton no efectiva
					$('#boton_venta_evento').css('display','none'); //boton venta evento
					$('#efectiva_sin_encuesta').css('display','none'); //boton efectiva sin encuesta
			}
			else if(distribuidor=='')
				{
				/*swal(
  					{title: 'Ingrese el Distribuidor',
  					confirmButtonColor: '#BC3228'});
					
					$('#fecha_observaciones').css('display','none'); //campo fecha proxima visita-observaciones
					$('#visita_no_efectiva').css('display','none'); //visita no efectiva
					$('#visita_efectiva').css('display','none'); //visita efectiva
					$('#boton_no_efectiva').css('display','none'); //boton no efectiva
					$('#boton_televenta').css('display','none'); //boton televenta
					$('#boton_venta_evento').css('display','none'); //boton venta evento
					$('#efectiva_sin_encuesta').css('display','none'); //boton efectiva sin encuesta
					$('#encuesta_consumo').css('display','none'); //encuesta
					$('#visita_televenta').css('display','none'); //suptipificacion de televenta
					$('#encuesta_copia').val(0);*/
			}
			if(email!='')
			{// tiene email
				if(distribuidor=='')
				{
					/*$('#fecha_observaciones').css('display','none'); //campo fecha proxima visita-observaciones
					$('#visita_no_efectiva').css('display','none'); //visita no efectiva
					$('#visita_efectiva').css('display','none'); //visita efectiva
					$('#boton_no_efectiva').css('display','none'); //boton no efectiva
					$('#boton_televenta').css('display','none'); //boton televenta
					$('#boton_venta_evento').css('display','none'); //boton venta evento
					$('#efectiva_sin_encuesta').css('display','none'); //boton efectiva sin encuesta
					$('#encuesta_consumo').css('display','none'); //encuesta
					$('#visita_televenta').css('display','none'); //suptipificacion de televenta
					$('#encuesta_copia').val(0);*/
				}else{	
					if(encuesta==1){
						$("#encuesta_consumo").css('display','block');
						$('#encuesta_copia').val(1);
					}else {
						$("#boton_televenta").css('display','block');
						$("#encuesta_consumo").css('display','none');
						$('#encuesta_copia').val(0);
					}
					
					$('#fecha_observaciones').css('display','block'); //campo fecha proxima visita-observaciones
					$('#visita_televenta').css('display','block'); //suptipificacion de televenta
					$('#visita_no_efectiva').css('display','none'); //visita no efectiva
					$('#visita_efectiva').css('display','none'); //visita efectiva
					$('#boton_no_efectiva').css('display','none'); //boton no efectiva
					$('#boton_venta_evento').css('display','none'); //boton venta evento
					$('#efectiva_sin_encuesta').css('display','none'); //boton efectiva sin encuesta
				}
			}
			else if(email=='')
			{
				swal(
					{title: 'Ingrese el E-mail',
					confirmButtonColor: '#BC3228'});
					
					$('#fecha_observaciones').css('display','none'); //campo fecha proxima visita-observaciones
					$('#visita_no_efectiva').css('display','none'); //visita no efectiva
					$('#visita_efectiva').css('display','none'); //visita efectiva
					$('#boton_no_efectiva').css('display','none'); //boton no efectiva
					$('#boton_televenta').css('display','none'); //boton televenta
					$('#boton_venta_evento').css('display','none'); //boton venta evento
					$('#efectiva_sin_encuesta').css('display','none'); //boton efectiva sin encuesta
					$('#encuesta_consumo').css('display','none'); //encuesta
					$('#visita_televenta').css('display','none'); //suptipificacion de televenta
					$('#encuesta_copia').val(0);
			}
			obtener();
			}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////			
	}
});
	
	$('#sub_tipificacion_efectiva').change(function()
	{
		
			var encuesta=$('#encuesta').val();
			var sub_tipificacion=$('#sub_tipificacion_efectiva').val();
	
		 if(sub_tipificacion==1 || sub_tipificacion==2 || sub_tipificacion==17){ //encuesta obligatoria por seleccion de subtipificacion
			 
				$("#visita_efectiva").css('display','block');
				$("#fecha_observaciones").css('display','block');
				$("#encuesta_consumo").css('display','block');
				$("#efectiva_sin_encuesta").css('display','none');
				$('#encuesta_copia').val(1);
		}
		else{
				if(encuesta==1){
					$("#encuesta_consumo").css('display','block');
					$("#visita_efectiva").css('display','block');
					$("#fecha_observaciones").css('display','block');
					$("#efectiva_sin_encuesta").css('display','none');
					$('#encuesta_copia').val(1);
				}else{
					$("#visita_efectiva").css('display','block');
					$("#fecha_observaciones").css('display','block');
					$("#efectiva_sin_encuesta").css('display','block');
					$("#encuesta_consumo").css('display','none');
					$('#encuesta_copia').val(0);
			    }
			}
		$('#visita_no_efectiva').css('display','none'); //visita no efectiva
		$('#boton_no_efectiva').css('display','none'); //visita efectiva
		$('#boton_televenta').css('display','none'); //boton no efectiva
		$('#boton_venta_evento').css('display','none'); //boton televenta
		
		
	});
	
	
	if(ancho<1001)
	{
		$('#info').css('height',height);
		var tama_menu=$("#div_menu").height();
		tama_menu=tama_menu*(-1);
		$('#info').css('margin-bottom',tama_menu);
		$("#info").animate({marginTop:tama_menu},1);

		$('#div_menu li').click(function()
		{
			$("#div_menu").animate({marginLeft:'-120%'},500);
			var tama_menu=$("#div_menu").height();
			tama_menu=tama_menu*(-1);
			$("#info").animate({marginTop:tama_menu},10);
			$('#info').toggle(1000);
			
		});
		$('#menu_bar').click(function()
		{
			$("#div_menu").animate({marginLeft:'0'},500);
			$('#info').fadeOut(1000);
			$("#div_menu").css('display','block');
			var compro=$('#compro').val();
			if(compro=='1')
			{
				$("#div_menu").css('display','block');
				$("#link2").css('display','block');
				if(distribuidor!='')
				{
					$("#link3").css('display','block');
				}
				else
				{
					$("#link3").css('display','none');
				}
			}
			if(compro!='1')
			{
				resultado();
			}
			
		});
	}
	else
	{
		$('#info').css('height',height);
		$('#info').css('display','block');
	}
});