<?php
error_reporting(0);
include ('../logica/session.php');
header('Content-Type: text/html; charset=utf-8');
?>
<?php header('Content-Type: text/html; charset=UTF-8'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>CONSULTAR GESTION</title>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.dataTables.js"></script>
        <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css"/>
        <link href="css/tablas.css" rel="stylesheet" /> 
        <link href="css/bootstrap.css" rel="stylesheet" /> 
        <style>

        </style>
        <script>
            $(document).ready(function () {
                $('#gestion').DataTable();
            });

            $(document).ready(function () {
                $('#gestionTodos').DataTable();
            });
            function ventanaSecundaria(URL)
            {
                window.open(URL, "ventana1", "width=800,height=500,Top=150,Left=50%")
            }
        </script>


        <script>
            function noback() {
                window.location.hash = "no-back-button";
                window.location.hash = "Again-No-back-button"

            }
        </script>
    </head>

    <?php
    include('../datos/conex_copia.php');

    $string_intro = getenv("QUERY_STRING");
    parse_str($string_intro);
    $usua;
    ?>
    <body onload="noback()">
        <br /><br />

        <form id="cambio_contacto" name="cambio_contacto" method="post" action="#" enctype="multipart/form-data" class="letra">
            <fieldset style="margin:auto auto; width:90%;">
                <div class="col-md-12">
                    <!--<div class="col-md-2">
                        <label for="año">AÑO</label><span class="asterisco">*</span><br />
                        <select class="form-control" name="anio">
                            <option value="">SELECCIONE</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                        </select>
                    </div>-->
                    <div class="col-md-2">
                        <label for="mes">MES</label><span class="asterisco">*</span><br />
                        <select class="form-control" name="mes">
                            <option value="">SELECCIONE</option>
                            <option value="1">ENERO</option>
                            <option value="2">FEBRERO</option>
                            <option value="3">MARZO</option>
                            <option value="4">ABRIL</option>
                            <option value="5">MAYO</option>
                            <option value="6">JUNIO</option>
                            <option value="7">JULIO</option>
                            <option value="8">AGOSTO</option>
                            <option value="9">SEPTIEMBRE</option>
                            <option value="10">OCTUBRE</option>
                            <option value="11">NOVIEMBRE</option>
                            <option value="12">DICIEMBRE</option>
                        </select>
                    </div>

                    <div class="col-md-2">
                        <label for="cliente">FECHA GESTI&Oacute;N:</label><span class="asterisco">*</span><br />
                        <input type="date" class="form-control" name="fecha" id="fecha" max="<?php echo date("Y-m-d"); ?>"/>
                    </div>
                    <div class="col-md-4">
                        <label for="fecha">USUARIO:</label><span class="asterisco">*</span><br />
                        <select class="form-control" name="idUsuario[]" multiple="multiple"> 
                            <option value="" disabled="disabled" selected="selected">Seleccione</option>           
                            <?php
                            $consulta = mysqli_query($conex, "
         SELECT ID_USUARIO, USER FROM 3m_usuario
		 WHERE PRIVILEGIOS =2 AND ESTADO =1 AND USER <> 'VISITA' AND USER <> 'MEDELLIN' AND USER <> 'emesa'  
		  ORDER BY USER ASC ;");


                            while ($dato = mysqli_fetch_array($consulta)) {
                                ?>
                                <option value="<?php echo $dato['ID_USUARIO']; ?>"><?php echo $dato['USER']; ?></option>
<?php } ?>

                        </select>
                    </div>
                    <div class="col-md-2">
                        <label for="fecha">TIPIFICACI&Oacute;N</label><span class="asterisco">*</span><br />
                        <select id="tipificacion" name="tipificacion" class="form-control">
                            <option value="">Seleccione</option>
                            <option value="EFECTIVA">EFECTIVA</option>
                            <option value="NO EFECTIVA">NO EFECTIVA</option>
                        </select>
                    </div>

                    <div class="col-md-3">
                        <label for="cliente"></label><br /><br />
                        <button title="Consultar" name="consultar"><img src="imagenes/lupa1.png" width="43" height="32" style="background-size:cover" title="CONSULTAR"></button>


                    </div>
                </div>

            </fieldset>

        </form>
        <div class="table table-responsive">
            <?php
            if (isset($_POST["consultar"])) {
                $idUsuario = $_POST["idUsuario"];
                $fecha = $_POST["fecha"];
                $tipificacion = $_POST["tipificacion"];
                $mes = $_POST["mes"];

                $usuarios_seleccionados = implode(',', $idUsuario);

                if (empty($idUsuario) && empty($fecha) && empty($tipificacion) && empty($mes)) {
                    echo 'esta vacia la busqueda';
                }
                //BUSQUEDA USUARIO
                else if (empty($idUsuario) == false && empty($fecha) && empty($tipificacion) && empty($mes)) {
                    $consultaGestionUsuario = mysqli_query($conex,"
			  SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICAION_ACTUAL
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_historia_clasificacion AS c ON c.ID_CLIENTE_FK_7 = b.ID_CLIENTE 
			INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE a.ACCION<>'PEDIDO ELIMINADO' AND a.ACCION<>'GESTION ELIMINADA' AND ID_ASESOR_GESTION IN ($usuarios_seleccionados)
			 AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' AND
			 ASESOR_GESTION <> 'cali' 
			 ORDER BY a.FECHA_GESTION DESC LIMIT 3000");
                }
                //BUSQUEDA FECHA 
                else if (empty($idUsuario) && empty($fecha) == false && empty($tipificacion) && empty($mes)) {
                    $consultaGestionUsuario = mysqli_query($conex,"
			    SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICAION_ACTUAL
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_historia_clasificacion AS c ON c.ID_CLIENTE_FK_7 = b.ID_CLIENTE 
			INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE a.ACCION<>'PEDIDO ELIMINADO' AND a.ACCION<>'GESTION ELIMINADA' AND a.ACCION='REGISTRO VISITA' AND DATE(a.FECHA_GESTION)='" . $fecha . "'
			 AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' 
			  ORDER BY a.FECHA_GESTION DESC LIMIT 3000");
                }
                //BUSQUEDA TIPIFICACION	
                else if (empty($idUsuario) && empty($fecha) && empty($tipificacion) == false && empty($mes)) {
                    $consultaGestionUsuario = mysqli_query($conex,"
			     SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICAION_ACTUAL
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_historia_clasificacion AS c ON c.ID_CLIENTE_FK_7 = b.ID_CLIENTE 
			INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE a.ACCION<>'PEDIDO ELIMINADO' AND a.ACCION<>'GESTION ELIMINADA' AND a.ACCION='REGISTRO VISITA' AND TIPIFICACION_GESTION='" . $tipificacion . "'
			 AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' 
			  ORDER BY a.FECHA_GESTION DESC LIMIT 3000");
                }
                //BUSQUEDA MES
                else if (empty($idUsuario) && empty($fecha) && empty($tipificacion) && empty($mes) == false) {
                    $consultaGestionUsuario = mysqli_query($conex,"
			    SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICAION_ACTUAL
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_historia_clasificacion AS c ON c.ID_CLIENTE_FK_7 = b.ID_CLIENTE 
			INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE a.ACCION<>'PEDIDO ELIMINADO' AND a.ACCION<>'GESTION ELIMINADA' AND a.ACCION='REGISTRO VISITA' AND MONTH(a.FECHA_GESTION)=" . $mes . "
			 AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' 
			  ORDER BY a.FECHA_GESTION DESC LIMIT 3000");
                }
                //BUSQUEDA USUARIO Y FECHA
                else if (empty($idUsuario) == false && empty($fecha) == false && empty($tipificacion) && empty($mes)) {
                    $consultaGestionUsuario = mysqli_query($conex,"
			    SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICAION_ACTUAL
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_historia_clasificacion AS c ON c.ID_CLIENTE_FK_7 = b.ID_CLIENTE 
			INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE a.ACCION<>'PEDIDO ELIMINADO' AND a.ACCION<>'GESTION ELIMINADA' AND a.ACCION='REGISTRO VISITA' AND ID_ASESOR_GESTION IN ($usuarios_seleccionados) AND  DATE(a.FECHA_GESTION)='" . $fecha . "'
			 AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' 
			  ORDER BY a.FECHA_GESTION DESC LIMIT 3000");
                }
                //BUSQUEDA USUARIO Y TIPIFICACION
                else if (empty($idUsuario) == false && empty($fecha) && empty($tipificacion) == false && empty($mes)) {
                    $consultaGestionUsuario = mysqli_query($conex,"
			    SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICAION_ACTUAL
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_historia_clasificacion AS c ON c.ID_CLIENTE_FK_7 = b.ID_CLIENTE 
			INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION 
			 WHERE a.ACCION<>'PEDIDO ELIMINADO' AND a.ACCION<>'GESTION ELIMINADA' AND a.ACCION='REGISTRO VISITA' AND ID_ASESOR_GESTION IN ($usuarios_seleccionados) AND  TIPIFICACION_GESTION='".$tipificacion."' AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' ORDER BY a.FECHA_GESTION DESC LIMIT 3000");
                }
                //BUSQUEDA USUARIO Y MES
                else if (empty($idUsuario) == false && empty($fecha) && empty($tipificacion) && empty($mes) == false) {
                    $consultaGestionUsuario = mysqli_query($conex,"
			    SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICAION_ACTUAL
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_historia_clasificacion AS c ON c.ID_CLIENTE_FK_7 = b.ID_CLIENTE 
			INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE a.ACCION<>'PEDIDO ELIMINADO' AND a.ACCION<>'GESTION ELIMINADA' AND a.ACCION='REGISTRO VISITA' AND ID_ASESOR_GESTION IN ($usuarios_seleccionados) AND MONTH(a.FECHA_GESTION)=" . $mes . "
			 AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' 
			  ORDER BY a.FECHA_GESTION DESC LIMIT 3000");
                }
                //BUSQUEDA FECHA Y TIPIFICACION
                else if (empty($idUsuario) && empty($fecha) == false && empty($tipificacion) == false && empty($mes)) {
                    $consultaGestionUsuario = mysqli_query($conex,"
			   SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICAION_ACTUAL
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_historia_clasificacion AS c ON c.ID_CLIENTE_FK_7 = b.ID_CLIENTE 
			INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE a.ACCION<>'PEDIDO ELIMINADO' AND a.ACCION<>'GESTION ELIMINADA' AND a.ACCION='REGISTRO VISITA' AND DATE(a.FECHA_GESTION)='" . $fecha . "' AND TIPIFICACION_GESTION='" . $tipificacion . "'
			 AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' 
			  ORDER BY a.FECHA_GESTION DESC LIMIT 3000");
                }
                //BUSQUEDA TIPIFICACION Y MES
                else if (empty($idUsuario) && empty($fecha) == false && empty($tipificacion) == false && empty($mes) == false) {
                    $consultaGestionUsuario = mysqli_query($conex,"
			    SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICAION_ACTUAL
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_historia_clasificacion AS c ON c.ID_CLIENTE_FK_7 = b.ID_CLIENTE 
			INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE a.ACCION<>'PEDIDO ELIMINADO' AND a.ACCION<>'GESTION ELIMINADA' AND a.ACCION='REGISTRO VISITA' AND TIPIFICACION_GESTION='" . $tipificacion . "' AND MONTH(a.FECHA_GESTION)=" . $mes . "
			 AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' 
			  ORDER BY a.FECHA_GESTION DESC LIMIT 100");
                }
                //BUSQUEDA USUARIO , FECHA Y TIPIFICACION
                else if (empty($idUsuario) == false && empty($fecha) == false && empty($tipificacion) == false && empty($mes)) {
                    $consultaGestionUsuario = mysqli_query($conex,"
			    SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICAION_ACTUAL
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_historia_clasificacion AS c ON c.ID_CLIENTE_FK_7 = b.ID_CLIENTE 
			INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE a.ACCION<>'PEDIDO ELIMINADO' AND a.ACCION<>'GESTION ELIMINADA' AND a.ACCION='REGISTRO VISITA' AND ID_ASESOR_GESTION IN ($usuarios_seleccionados) AND DATE(a.FECHA_GESTION)='" . $fecha . "' AND   TIPIFICACION_GESTION='" . $tipificacion . "'
			AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' 
			  ORDER BY a.FECHA_GESTION DESC LIMIT 100");
                }
                //BUSQUEDA USUARIO , MES Y TIPIFICACION
                else if (empty($idUsuario) == false && empty($fecha) && empty($tipificacion) == false && empty($mes) == false) {
                    $consultaGestionUsuario = mysqli_query($conex,"
			   SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICAION_ACTUAL
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_historia_clasificacion AS c ON c.ID_CLIENTE_FK_7 = b.ID_CLIENTE 
			INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE a.ACCION<>'PEDIDO ELIMINADO' AND a.ACCION<>'GESTION ELIMINADA' AND a.ACCION='REGISTRO VISITA' AND ID_ASESOR_GESTION IN ($usuarios_seleccionados) AND MONTH(a.FECHA_GESTION)=" . $mes . " AND   TIPIFICACION_GESTION='" . $tipificacion . "'
			 AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' 
			  ORDER BY a.FECHA_GESTION DESC LIMIT 100");
                }
            } else {
                $consultaGestionUsuario = mysqli_query($conex,"
			  SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICAION_ACTUAL
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_historia_clasificacion AS c ON c.ID_CLIENTE_FK_7 = b.ID_CLIENTE 
			INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			WHERE a.ACCION<>'PEDIDO ELIMINADO' AND a.ACCION<>'GESTION ELIMINADA' AND a.ACCION='REGISTRO VISITA' AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN'
			  ORDER BY a.FECHA_GESTION DESC LIMIT 100");
            }
            ?>
            <table style="width:99%; margin:auto auto;" rules="none" class="table table-responsive" >
                <tr>
                    <th colspan='11' class="principal">ELIMINAR GESTI&Oacute;N</th>
                </tr>
            </table>
            <br />
            <table style="width:99%; margin:auto auto;"  id="gestion">
                <thead>
                    <tr style=" text-align:center;">

                        <th class="TITULO">IDENTIFICACI&Oacute;N</th>
                        <th class="TITULO">CLIENTE</th>
                        <th class="TITULO">ACCI&Oacute;N</th>
                        <th class="TITULO">TIPIFICACCI&Oacute;N</th>
                        <th class="TITULO">SUB TIPIFICACI&Oacute;N</th>
                        <th class="TITULO">OBSERVACION </th>
                        <th class="TITULO">FECHA GESTI&Oacute;N</th>
                        <th class="TITULO">USUARIO</th>
                        <th class="TITULO">ELIMINAR</th>

                    </tr>
                </thead>
                <tbody>
<?PHP
while ($dato = mysqli_fetch_array($consultaGestionUsuario)) {

    $CLIENTE = $dato["NOMBRE_CLIENTE"] . " " . $dato["APELLIDO_CLIENTE"];
    ?>
                        <tr class="datos" >

                            <td style="text-align:center;"><?php echo $dato["IDENTIFICACION_CLIENTE"] ?></p></td>
                            <td style="text-align:center;"><?php echo $CLIENTE; ?></td>
                            <td style="text-align:center;"><?php echo $dato["ACCION"] ?></td>
                            <td style="text-align:center;"><?php echo $dato["TIPIFICACION_GESTION"] ?></td>
                            <td style="text-align:center;"><?php echo $dato["SUB_TIPIFICACION"] ?></td>
                            <td style="text-align:center;"><?php echo $dato["OBSERVACION_GESTION"] ?></td>	
                            <td style="text-align:center;"><?php echo $dato["FECHA_GESTION"] ?></td>		
                            <td style="text-align:center;"><?php echo $dato["ASESOR_GESTION"] ?></td>
                            <td>
                                <form id="form1" name="form1" method="POST" action="../logica/actualizar_cliente.php" target="_self" ONSUBMIT="return preguntar();">
                                    <input name="gestion" value="<?php echo $dato["ID_GESTION"]; ?>" type="hidden"/>
                                    <button type="submit" name="button_eliminar_gestion" id="button_eliminar_gestion" value="ELIMINAR" class="btn btn-default"><img src="imagenes/no.png" width="25" height="20" style="background-size:cover" title="CONSULTAR"></button>

                                </form> 
                            </td>

                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>

        </div>
        <script>
            function preguntar() {

                if (!confirm('Desea eliminar la gestion?')) {
                    return false;
                }

            }
        </script>  

    </body>
</html>