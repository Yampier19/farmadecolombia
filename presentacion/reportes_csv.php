<?php
	include("../logica/session.php");
	error_reporting(0);
?>
<?php header('Content-Type: text/html; charset=UTF-8'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css"/>
<link href="css/tablas.css" rel="stylesheet" /> 
<link href="css/bootstrap.css" rel="stylesheet" /> 
<link href="../presentacion/css/tablas.css" rel="stylesheet" />

<title>CONSULTA PEDIDO</title>
<script>
$(document).ready(function() {
    $('#pedidos').DataTable();
} );
</script>

<script language=javascript> 
function ventanaSecundaria (URL)
{ 
   window.open(URL,"ventana1","width=800,height=500,Top=150,Left=50%") 
}

</script> 

</head>
<?PHP
include('../datos/conex.php');

?>
<body>


<div class="col-md-12">
   
 
    <table class='table table-hover'>   
        <tr><td> <a href='excel_gestiones.php'>Reporte de Gesti&oacute;n</a> </td></tr>
         <tr><td> <a href='excel_clientes.php'>Reporte de Clientes</a> </td></tr>
         <tr><td> <a href='excel_clientes_nuevos.php'>Reporte de Clientes Nuevos</a> </td></tr>
        <tr><td>  <a href='excel_encuesta.php'>Reporte de Encuesta</a> </td></tr>
         <tr><td> <a href='excel_pedido.php'>Reporte de Pedidos</a> </td></tr>
          <tr><td><a href='excel_detalle_pedido.php'>Reporte de Detalle Pedido</a> </td></tr>
         <tr><td> <a href='excel_conversion.php'>Reporte Conversi&oacute;n</a> </td></tr>
        <tr><td>  <a href='excel_competencia.php'>Reporte Competencia</a> </td></tr>
         <tr><td> <a href='excel_rutero.php'>Reporte Rutero</a> </td></tr>
		 <tr><td> <a href='excel_historial.php'>Reporte Historial</a> </td></tr>
            </table>
  </div>
</body>
</html>