<?php
	include("../logica/session.php");
        header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css"/>
<link href="css/tablas.css" rel="stylesheet" /> 
<link href="css/bootstrap.css" rel="stylesheet" /> 
<title>CONSULTA PEDIDO</title>
</script>
<script language=javascript> 
function ventanaSecundaria (URL)
{ 
   window.open(URL,"ventana1","width=800,height=500,Top=150,Left=50%") 
}
function consultar()
{
	var id_encuesta=$('#id_encuesta').val();
	$.ajax(
	{
		url:'../presentacion/consultar_productos_usados_encuesta.php',
		data:
		{
			id_encuesta: id_encuesta
		},
		type: 'post',
		beforeSend: function () 
		{
			//alert('Cargando')
		},
		success: function(data)
		{
			$('#tablas_usado').html(data);
		}
	})
}


$(document).ready(function() {
  
	consultar();
	
} );
</script>
<style>
.tit
{
	padding:5px;
	text-transform:uppercase;
	font-weight:bold;
	text-align:left;
}
</style>
</head>
<?PHP
include('../datos/conex_copia.php');
mysqli_set_charset($conex,"utf8");
if(isset($pedid))
{
	$ID_CLIENTE=base64_decode($pedid);
}
else
{
	$ID_CLIENTE=$ID_CLIENTE;
}
$consulta=mysqli_query($conex," SELECT * FROM 3m_pedido AS p
 INNER JOIN 3m_cliente AS cli ON cli.ID_CLIENTE=p.ID_CLIENTE_FK
 INNER JOIN 3m_usuario AS u ON p.ID_USUARIO_FK=u.ID_USUARIO
 WHERE ID_CLIENTE_FK='$ID_CLIENTE'");

 $consulta_encuesta=mysqli_query($conex,"SELECT * FROM 3m_encuesta WHERE ID_CLIENTE_FK_5='$ID_CLIENTE'");

 $consulta_gestiones = mysqli_query($conex," SELECT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,a.FECHA_PROXIMA_VISITA,
	 b.ID_CLIENTE,
	 a.ASESOR_GESTION,a.ID_ASESOR_GESTION,a.OBSERVACION_GESTION
	 FROM 3m_gestion AS a
	 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
	 WHERE ID_CLIENTE_FK=".$ID_CLIENTE." 
	 ORDER BY a.FECHA_GESTION DESC;");
$consulta_competencia = mysqli_query($conex,"SELECT B.id_competencia AS id_temp, A.id_competencia,A.categoria,A.producto_3m,A.descripcion_competencia
		 FROM 3m_categoria_competencia AS A
		INNER JOIN 3m_productos_competencia_temp AS B ON A.id_competencia = B.ID_PRODUCTO_COMPETENCIA
		WHERE ID_CLIENTE= ".$ID_CLIENTE.";");	 
 
?>
<body>
<table align="right">
    	<tr>
            <th>Bienvenid@  <?php echo $usua?></th>
            <td><a href="../presentacion/inicio_consulta_cliente.php?x=<?php echo base64_encode($ID_CLIENTE)?>">
        <img src="../presentacion/imagenes/atras.png" width="56" height="57" id="cambiar" title="ATRAS" ><span style="color:#000;">
        </a></td>
        <td><a href="../presentacion/consulta_pedidos_usuario.php?">
        <img src="../presentacion/imagenes/agn.png" width="56" height="57" id="cambiar" title="CONSULTAR PEDIDOS" ><span style="color:#000;">
        </a></td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />

	<div class="table table-responsive" style="padding-left:10px;">
		
        <h4 style="color:#93271b; font-weight:bold; text-transform:uppercase"><center>Gestiones Realizadas</center></h4>
<table style="width:90%;border:1px solid #000; margin:auto auto;" class="table table-hover" rules="all" >
 <thead>
	<tr>
		<th colspan='9' class="principal">GESTIONES</th>
	</tr>
	<tr>
		<th class="TITULO">FECHA GESTION</th>
		<th class="TITULO">ACCION</th>
        <th class="TITULO">TIPIFICACION</th>
        <th class="TITULO">SUBTIPIFICACION </th>
        <th class="TITULO">PROXIMA VISITA </th>
        <th class="TITULO">VISITADOR</th>
		<th class="TITULO">OBSERVACION</th>
	</tr>
</thead>
<tbody>	
    <?PHP
    while($dato_gestion=mysqli_fetch_array($consulta_gestiones))
	{
	?>
		<tr class="datos">
            <td><?php echo $dato_gestion["FECHA_GESTION"]?></td>
            <td><?php echo $dato_gestion["ACCION"]?></td>
            <td><?php echo $dato_gestion["TIPIFICACION_GESTION"]?></td>
            <td><?php echo $dato_gestion["SUB_TIPIFICACION"]?></td>
            <td><?php echo $dato_gestion["FECHA_PROXIMA_VISITA"]?></td>	
            <td><?php echo $dato_gestion["ASESOR_GESTION"]?></td>	
            <td><?php echo $dato_gestion["OBSERVACION_GESTION"]?></td>
            
		</tr>
	<?php 
	}
	?>
	</tbody>
</table>
</div>

<hr style="width:100%;border-bottom:red;">

	

<br />
<br />
<?php
while($datos=(mysqli_fetch_array($consulta_encuesta)))
{
	$ID_ENCUESTA = $datos['ID_ENCUESTA'];
	$CANTIDAD_PACIENTES=$datos['CANTIDAD_PACIENTES'];
	$ESPECIALIDAD_ODONTOLOGO=$datos['ESPECIALIDAD_ODONTOLOGO'];
	$CLASIFICACION_ODONTOLOGO=$datos['CLASIFICACION_ODONTOLOGO'];
	
	$FECHA_ACTUALIZACION=$datos['FECHA_ACTUALIZACION'];
	$DIA_VISITA=$datos['DIA_VISITA'];
	$HORA_INI_VISITA=$datos['HORA_INI_VISITA'];
	$HORA_FIN_VISITA=$datos['HORA_FIN_VISITA'];
}
?>
<center>
<input id="id_encuesta" type="hidden" value="<?php echo $ID_ENCUESTA; ?>" >

<hr style="width:100%;border-bottom:red;">
<fieldset style="border:1px solid #93271b; border-radius:10px; margin:auto auto;width:90%">
        <legend style="color:#93271b; font-weight:bold; text-transform:uppercase"><center>Informaci&Oacute;n Ultima Encuesta Cliente</center>
        
        </legend>
		
<hr style="width:100%;border-bottom:red;">
        <table align="center" width="100%">
            <tr style=" text-align: left;">
                <td width="54%" class="tit">
                    Especialidad Odontologo:
                </td>
                <td width="46%">
                    <span><?php echo $ESPECIALIDAD_ODONTOLOGO ?></span>
                </td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit" style="width:45%;">
                    Cantidad Paciente Que Atiende:
                </td>
                <td>
                    <span><?php echo $CANTIDAD_PACIENTES ?></span>
               </td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit">
                    Clasificaci&Oacute;n Odontologo:
                </td>
                <td>
                    <span><?php echo $CLASIFICACION_ODONTOLOGO ?></span>
                </td>
            </tr>
            
            <tr style=" text-align: left;">
                <td class="tit">
                    Fecha Modificaci&oacute;n:
                </td>
                <td>
                    <span><?php echo $FECHA_ACTUALIZACION ?></span>
                </td>
            </tr>
            <!--<tr style=" text-align: left;">
                <td class="tit">
                    D&iacute;a Visita:
                </td>
                <td>
                    <span><?php //echo $DIA_VISITA ?></span>
                </td>
            </tr> 
            <tr style=" text-align: left;">
                <td class="tit">
                    Hora Inicio Visita:
                </td>
                <td>
                    <span><?php //echo $HORA_INI_VISITA ?></span>
                </td>
            </tr>
            <tr style="text-align: left;">
                <td class="tit">Hora Fin Visita:</td>
                <td>
                	<span><?php //echo $HORA_FIN_VISITA ?></span>
                </td>
            </tr>-->
      </table>
</fieldset>
</center>

<center>
<div class="table table-responsive" style="width:90%;">
		<br />
	
        <table style="width:100%;border:1px solid #000; margin:auto auto;" class="table table-hover" rules="all" id="tablas_usado">
        </table>
        <br />
	
<table style="width:100%;border:1px solid #000; margin:auto auto;" rules="all" class="table table-hover" id="tablas"  >
    <tr>
        <th class='principal' colspan='5' style='border-radius: 15px 15px 0 0;'>PRODUCTOS COMPETENCIA</th>
        </tr>
        <tr>
        <th class='TITULO'>CATEGORIA</th>
        <th class='TITULO'>SUBCATEGORIA</th>
        <th class='TITULO'>NOMBRE PRODUCTO</th>
    </tr>
    <?php
	
    while($dato=(mysqli_fetch_array($consulta_competencia)))
    {
    ?>
    <tr>
        <td><?PHP echo $dato["categoria"]?></td>
        <td><?PHP echo $dato['producto_3m']?></td>
        <td><?PHP echo $dato["descripcion_competencia"]?> </td>
    </tr>
	<?PHP	
    }
    ?>
</table>	
    </div>
</center>

</body>
</html>