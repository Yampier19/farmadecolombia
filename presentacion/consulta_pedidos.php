<?php
	include("../logica/session.php");
	error_reporting(0);
header('Content-Type: text/html; charset=UTF-8'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css"/>
<link href="css/tablas.css" rel="stylesheet" /> 
<link href="css/bootstrap.css" rel="stylesheet" /> 

<title>CONSULTA PEDIDO</title>
<script>
$(document).ready(function() {
    $('#pedidos').DataTable();
} );

function ventanaSecundaria (URL)
{ 
   window.open(URL,"ventana1","width=800,height=500,Top=150,Left=50%") 
}

</script> 

</head>
<?PHP
include('../datos/conex_copia.php');

	$string_intro = getenv("QUERY_STRING"); 
	parse_str($string_intro);
if(isset($pedid))
{
	$ID_CLIENTE=base64_decode($pedid);
}
else
{
	$ID_CLIENTE=$ID_CLIENTE;
}
?>
<body>
<br />
<br />

<div class="col-md-12">
<form id="cambio_contacto" name="cambio_contacto" method="post" action="#" enctype="multipart/form-data" class="letra">
<fieldset style="margin:auto auto; width:90%;">
<div class="col-md-12">

    <div class="col-md-2">
        <label for="año">AÑO</label><span class="asterisco">*</span><br />
        <select class="form-control" name="anio">
            <option value="">SELECCIONE</option>
            <option value="2017">2017</option>
            <option value="2018">2018</option>
            <option value="2019">2019</option>
            <option value="2020">2020</option>
            <option value="2021">2021</option>
        </select>
    </div>
    <div class="col-md-2">
        <label for="mes">MES</label><span class="asterisco">*</span><br />
            <select class="form-control" name="mes">
            	<option value="">SELECCIONE</option>
                <option value="1">ENERO</option>
                <option value="2">FEBRERO</option>
                <option value="3">MARZO</option>
                <option value="4">ABRIL</option>
                <option value="5">MAYO</option>
                <option value="6">JUNIO</option>
                <option value="7">JULIO</option>
                <option value="8">AGOSTO</option>
                <option value="9">SEPTIEMBRE</option>
                <option value="10">OCTUBRE</option>
                <option value="11">NOVIEMBRE</option>
                <option value="12">DICIEMBRE</option>
            </select>
    </div>

        <div class="col-md-2">
             <label for="cliente">FECHA GESTI&Oacute;N:</label><span class="asterisco">*</span><br />
                <input type="date" class="form-control" name="fecha" id="fecha" max="<?php echo date("Y-m-d");?>"/>
        </div>
        <div class="col-md-4">
             <label for="fecha">USUARIO:</label><span class="asterisco">*</span><br />
              <select class="form-control" name="idUsuario[]" multiple="multiple"> 
        <option value="" disabled="disabled" selected="selected">Seleccione</option>           
                    <?php 
                        $consultaGestionUsuario =mysqli_query($conex,"
         SELECT ID_USUARIO, USER FROM 3m_usuario
		 WHERE PRIVILEGIOS =2 AND ESTADO =1 
		 AND USER <> 'VISITA' AND USER <> 'MEDELLIN' AND USER <> 'emesa'
		  ORDER BY USER ASC ;");
                 
                 
                  while($dato=mysqli_fetch_array($consultaGestionUsuario)) { ?>
                <option value="<?php echo $dato['ID_USUARIO']; ?>"><?php echo $dato['USER']; ?></option>
                <?php } ?>
                    
            </select>
        </div>
    
        <div class="col-md-2">
        <label for="mes">TIPO PEDIDO</label><span class="asterisco">*</span><br />
            <select class="form-control" name="mes">
                <option value="" selected="selected" disabled="disabled">SELECCIONE</option>
                <option value="Transferencia">TRANSFERENCIA</option>
                <option value="Sugerido">SUGERIDO</option>
                <option value="Transferencia Compartida">TRANSFERENCIA COMPARTIDA</option>
            </select>
    </div>

        <div class="col-md-2">
           <label for="cliente"></label><br /><br />
    <button title="Consultar" name="consulta"><img src="imagenes/lupa1.png" width="43" height="32" style="background-size:cover" title="CONSULTAR"></button>
      </div>

</div>
    </fieldset>
</form>
<div class="table table-responsive">

<!--<div class="col-md-6">
        <?php
    //    $mes1 = date("m");
    //    $year = date("Y");
    //     if($mes1 ==01){ $nombre_mes ='Enero';}
    //     else if($mes1 ==02){$nombre_mes ='Febrero';}
    //     else if($mes1 ==03){$nombre_mes ='Marzo';}
    //     else if($mes1 ==04){$nombre_mes ='Abril';}
    //     else if($mes1 ==05){$nombre_mes ='Mayo';}
    //     else if($mes1 ==06){$nombre_mes ='Junio';}
    //     else if($mes1 ==07){$nombre_mes ='Julio';}
    //     else if($mes1 ==08){$nombre_mes ='Agosto';}
    //     else if($mes1 ==09){$nombre_mes ='Septiembre';}
    //     else if($mes1 ==10){$nombre_mes ='Octubre';}
    //   else if($mes1 ==11){$nombre_mes ='Noviembre';}
    //   else if($mes1 ==12){$nombre_mes ='Diciembre';}

     //   $consulta_suma_pedidos = mysqli_query($conex,"SELECT IF(SUM(TOTAL_PEDIDO)IS NULL,0,SUM(TOTAL_PEDIDO))  AS SUMA FROM 3m_pedido WHERE MONTH(FECHA_PEDIDO)='".$mes1."' AND YEAR(FECHA_PEDIDO)='".$year."' AND TIPO_PEDIDO <> 'Sugerido'");

        // while($dato_pedidos=mysqli_fetch_array($consulta_suma_pedidos))
        {
        //   $valor_pedidos = $dato_pedidos["SUMA"];
        }
         ?>
        <h4>Transferencias <b> <?php // echo $nombre_mes; ?>:</b>
             <span class="label label-danger">
                 <?php // if($valor_pedidos==0){echo "$ ". $valor_pedidos; }else{echo "$ ".number_format($valor_pedidos,0,',','.');} ?></span></h4>
</div>-->

<?php 

$total_fecha='';

if(isset($_POST["consulta"])){ 
	$idUsuario	= $_POST["idUsuario"];
	$fecha	    = $_POST["fecha"];
	$mes  		= $_POST["mes"];
    $anio       = $_POST["anio"];
	if(empty($idUsuario)==false){
 	$usuarios_seleccionados = implode(',',$idUsuario);
	}
		if(empty($idUsuario) && empty($fecha) && empty($mes) && empty($anio)){
			echo 'esta vacia la busqueda';
			}
			//BUSQUEDA USUARIO
		else if(empty($idUsuario)==false && empty($fecha) && empty($mes)) {
			$consultaGestionUsuario =mysqli_query($conex,"
			  
SELECT DISTINCT b.TOTAL_PEDIDO, CONCAT(NOMBRE_CLIENTE, ' ', APELLIDO_CLIENTE) AS NOMBRE,  b.DISTRIBUIR, b.FECHA_PEDIDO, b.ESTADO_PEDIDO, a.CELULAR_CLIENTE, a.TELEFONO_CLIENTE, 
a.DIRECCION_CLIENTE, b.TIPO_PEDIDO, d.RANGO_ENTREGA, e.USER, b.ID_PEDIDO
FROM 3m_cliente AS a
INNER JOIN 3m_pedido AS b ON a.ID_CLIENTE = b.ID_CLIENTE_FK
INNER JOIN 3m_detalle_pedido AS d ON b.ID_PEDIDO = d.ID_PEDIDO_FK_2
INNER JOIN 3m_usuario AS e ON b.ID_USUARIO_FK = e.ID_USUARIO
WHERE e.ID_USUARIO IN($usuarios_seleccionados) AND USER <> 'VISITA' AND USER <> 'MEDELLIN' AND USER <> 'emesa'");
			 
			$valor = mysqli_query("SELECT SUM(TOTAL_PEDIDO),ID_USUARIO_FK FROM 3m_pedido WHERE ID_USUARIO_FK IN ($usuarios_seleccionados) AND ESTADO_PEDIDO<>'ELIMINADO' GROUP BY ID_USUARIO_FK");
	while ($datos=mysqli_fetch_array($valor)){
		$total_fecha = $datos['SUM(TOTAL_PEDIDO)'];
		}
			
			$consulta_suma_pedidos = mysqli_query($conex,"SELECT IF(SUM(TOTAL_PEDIDO)IS NULL,0,SUM(TOTAL_PEDIDO))  AS SUMA FROM 3m_pedido WHERE MONTH(FECHA_PEDIDO)='".$mes."' AND YEAR(FECHA_PEDIDO)='".$year."' AND TIPO_PEDIDO <> 'Sugerido' AND ESTADO_PEDIDO<>'ELIMINADO' AND ID_USUARIO_FK IN ($usuarios_seleccionados)");

          while($dato_pedidos=mysqli_fetch_array($consulta_suma_pedidos))
        {
           $valor_pedidos = $dato_pedidos["SUMA"];
        }
        ?>
        <h4>Transferencias <b> <?php echo $nombre_mes; ?>:</b>
             <span class="label label-danger">
                 <?php if($valor_pedidos==0){echo "$ ". $valor_pedidos; }else{echo "$ ".number_format($valor_pedidos,0,',','.');} ?></span></h4>
            <?php
		}
			//BUSQUEDA FECHA 
		 else if(empty($idUsuario) && empty($fecha)==false && empty($mes)) {
			$consultaGestionUsuario =mysqli_query($conex,"
			   SELECT DISTINCT b.TOTAL_PEDIDO, CONCAT(NOMBRE_CLIENTE, ' ', APELLIDO_CLIENTE) AS NOMBRE,  b.DISTRIBUIR, b.FECHA_PEDIDO, b.ESTADO_PEDIDO, a.CELULAR_CLIENTE, a.TELEFONO_CLIENTE, 
a.DIRECCION_CLIENTE, b.TIPO_PEDIDO, d.RANGO_ENTREGA, e.USER, b.ID_PEDIDO
FROM 3m_cliente AS a
INNER JOIN 3m_pedido AS b ON a.ID_CLIENTE = b.ID_CLIENTE_FK
INNER JOIN 3m_detalle_pedido AS d ON b.ID_PEDIDO = d.ID_PEDIDO_FK_2
INNER JOIN 3m_usuario AS e ON b.ID_USUARIO_FK = e.ID_USUARIO
WHERE DATE(b.FECHA_PEDIDO)='$fecha' AND USER <> 'VISITA' AND USER <> 'MEDELLIN' AND USER <> 'emesa'");
	
	$valor = mysqli_query($conex,"SELECT SUM(TOTAL_PEDIDO) FROM 3m_pedido WHERE DATE(FECHA_PEDIDO)='$fecha' AND ID_USUARIO_FK  <> 2 AND ID_USUARIO_FK  <> 4 AND ID_USUARIO_FK  <> 12 AND ESTADO_PEDIDO<>'ELIMINADO'");
	while ($datos=mysqli_fetch_array($valor)){
		$total_fecha = $datos['SUM(TOTAL_PEDIDO)'];
		}
		
			}
			//BUSQUEDA MES
			else if(empty($idUsuario) && empty($fecha) && empty($mes)==false ) {
			$consultaGestionUsuario =mysqli_query($conex,"
			   SELECT DISTINCT b.TOTAL_PEDIDO, CONCAT(NOMBRE_CLIENTE, ' ', APELLIDO_CLIENTE) AS NOMBRE,  b.DISTRIBUIR, b.FECHA_PEDIDO, b.ESTADO_PEDIDO, a.CELULAR_CLIENTE, a.TELEFONO_CLIENTE, 
a.DIRECCION_CLIENTE, b.TIPO_PEDIDO, d.RANGO_ENTREGA, e.USER, b.ID_PEDIDO
FROM 3m_cliente AS a
INNER JOIN 3m_pedido AS b ON a.ID_CLIENTE = b.ID_CLIENTE_FK
INNER JOIN 3m_detalle_pedido AS d ON b.ID_PEDIDO = d.ID_PEDIDO_FK_2
INNER JOIN 3m_usuario AS e ON b.ID_USUARIO_FK = e.ID_USUARIO
WHERE MONTH(b.FECHA_PEDIDO)='$mes' AND USER <> 'VISITA' AND USER <> 'MEDELLIN' AND USER <> 'emesa'");
			 
	$valor = mysqli_query($conex,"SELECT SUM(TOTAL_PEDIDO) FROM 3m_pedido WHERE MONTH(FECHA_PEDIDO)='$mes' AND ID_USUARIO_FK  <> 2 AND ID_USUARIO_FK  <> 4 AND ID_USUARIO_FK  <> 12 AND ESTADO_PEDIDO<>'ELIMINADO'");
	while ($datos=mysqli_fetch_array($valor)){
		$total_fecha = $datos['SUM(TOTAL_PEDIDO)'];
		}
		
		
			}
			//BUSQUEDA USUARIO Y FECHA
			else if(empty($idUsuario)==false && empty($fecha)==false && empty($mes) ) {
			$consultaGestionUsuario =mysqli_query($conex,"
			   SELECT DISTINCT b.TOTAL_PEDIDO, CONCAT(NOMBRE_CLIENTE, ' ', APELLIDO_CLIENTE) AS NOMBRE,  b.DISTRIBUIR, b.FECHA_PEDIDO, b.ESTADO_PEDIDO, a.CELULAR_CLIENTE, a.TELEFONO_CLIENTE, 
a.DIRECCION_CLIENTE, b.TIPO_PEDIDO, d.RANGO_ENTREGA, e.USER, b.ID_PEDIDO
FROM 3m_cliente AS a
INNER JOIN 3m_pedido AS b ON a.ID_CLIENTE = b.ID_CLIENTE_FK
INNER JOIN 3m_detalle_pedido AS d ON b.ID_PEDIDO = d.ID_PEDIDO_FK_2
INNER JOIN 3m_usuario AS e ON b.ID_USUARIO_FK = e.ID_USUARIO
WHERE e.ID_USUARIO IN($usuarios_seleccionados) AND DATE(b.FECHA_PEDIDO)='$fecha' AND USER <> 'VISITA' AND USER <> 'MEDELLIN' AND USER <> 'emesa'");
		
		$valor = mysqli_query($conex,"SELECT SUM(TOTAL_PEDIDO) FROM 3m_pedido WHERE ID_USUARIO_FK IN ($usuarios_seleccionados) AND DATE(FECHA_PEDIDO)='$fecha' AND ESTADO_PEDIDO<>'ELIMINADO'");
	while ($datos=mysqli_fetch_array($valor)){
		$total_fecha = $datos['SUM(TOTAL_PEDIDO)'];
		}
		
			}
			//BUSQUEDA USUARIO Y MES
			else if(empty($idUsuario)==false && empty($fecha) && empty($mes)==false ) {
			$consultaGestionUsuario =mysqli_query($conex,"
			   SELECT DISTINCT b.TOTAL_PEDIDO, CONCAT(NOMBRE_CLIENTE, ' ', APELLIDO_CLIENTE) AS NOMBRE,  b.DISTRIBUIR, b.FECHA_PEDIDO, b.ESTADO_PEDIDO, a.CELULAR_CLIENTE, a.TELEFONO_CLIENTE, 
a.DIRECCION_CLIENTE, b.TIPO_PEDIDO, d.RANGO_ENTREGA, e.USER, b.ID_PEDIDO
FROM 3m_cliente AS a
INNER JOIN 3m_pedido AS b ON a.ID_CLIENTE = b.ID_CLIENTE_FK
INNER JOIN 3m_detalle_pedido AS d ON b.ID_PEDIDO = d.ID_PEDIDO_FK_2
INNER JOIN 3m_usuario AS e ON b.ID_USUARIO_FK = e.ID_USUARIO
WHERE e.ID_USUARIO IN($usuarios_seleccionados) AND MONTH(b.FECHA_PEDIDO)='$mes' AND USER <> 'VISITA' AND USER <> 'MEDELLIN' AND USER <> 'emesa'");
		
			$valor = mysqli_query($conex,"SELECT SUM(TOTAL_PEDIDO) FROM 3m_pedido WHERE ID_USUARIO_FK IN ($usuarios_seleccionados) AND MONTH(FECHA_PEDIDO)=='$mes' AND ESTADO_PEDIDO<>'ELIMINADO'");
	while ($datos=mysqli_fetch_array($valor)){
		$total_fecha = $datos['SUM(TOTAL_PEDIDO)'];
		}
					}		
						}
			else{
				$consultaGestionUsuario =mysqli_query($conex,"
				SELECT DISTINCT b.TOTAL_PEDIDO, CONCAT(NOMBRE_CLIENTE, ' ', APELLIDO_CLIENTE) AS NOMBRE,  b.DISTRIBUIR, b.FECHA_PEDIDO, b.ESTADO_PEDIDO, a.CELULAR_CLIENTE, a.TELEFONO_CLIENTE, 
a.DIRECCION_CLIENTE, b.TIPO_PEDIDO, d.RANGO_ENTREGA, e.USER, b.ID_PEDIDO
FROM 3m_cliente AS a
INNER JOIN 3m_pedido AS b ON a.ID_CLIENTE = b.ID_CLIENTE_FK
INNER JOIN 3m_detalle_pedido AS d ON b.ID_PEDIDO = d.ID_PEDIDO_FK_2
INNER JOIN 3m_usuario AS e ON b.ID_USUARIO_FK = e.ID_USUARIO
WHERE USER <> 'VISITA' AND USER <> 'MEDELLIN' AND USER <> 'emesa'");
    		}
	?>

</div>

<table style="width:99%; margin:auto auto;" rules="none" >
	<tr>
		<th colspan='11' class="principal">PEDIDOS Y TRANSFERENCIAS</th>
	</tr>
    </table>
    <table style="width:99%; margin:auto auto;" rules="none" id="pedidos">
    <thead>
	<tr>
      <th class="TITULO"></th>
		<th class="TITULO">TOTAL PEDIDO</th>
        <th class="TITULO">NOMBRE CLIENTE</th>
        <th class="TITULO">DISTRIBUIDOR</th>
        <th class="TITULO">FECHA PEDIDO </th>
        <th class="TITULO">ESTADO PEDIDO </th>
		<th class="TITULO">TELEFONO CELULAR</th>
		<th class="TITULO">TELEFONO FIJO</th>
		<th class="TITULO">DIRECCION CLIENTE</th>
        <th class="TITULO">TIPO PEDIDO</th>
        <th class="TITULO">HORARIO ENTREGA</th>
		<th class="TITULO">USUARIO</th>
        <th class="TITULO">VER</th>
      <?php 
	  if($privilegios==1){ ?>
		<th class="TITULO">EDITAR</th>
	<?php	  } ?>
	</tr>
    </thead>
    <tbody>
    <?PHP
	$i=1;
    while($dato=mysqli_fetch_array($consultaGestionUsuario))
	{
	?>
		<tr class="datos">
        <td><b><?php echo  $i++?></b></td>
            <td>$ <?php echo number_format($dato["TOTAL_PEDIDO"], 0, ',', '.')?></td>
            <td><?php echo $dato["NOMBRE"]?></td>
            <td><?php echo $dato["DISTRIBUIR"]?></td>
            <td><?php echo $dato["FECHA_PEDIDO"]?></td>
            <td><?php echo $dato["ESTADO_PEDIDO"]?></td>
            <td><?php echo $dato["CELULAR_CLIENTE"]?></td>	
            <td><?php echo $dato["TELEFONO_CLIENTE"]?></td>	
            <td><?php echo $dato["DIRECCION_CLIENTE"]?></td>
            <td><?php echo $dato["TIPO_PEDIDO"]?></td>	
            <td><?php echo $dato["RANGO_ENTREGA"]?></td>	 
            <td style="text-align:center"><?php echo $dato["USER"]?></td>
            <th>
                <a  href="javascript:ventanaSecundaria('../presentacion/consulta_pedidos_detalle.php?x=<?php echo base64_encode($dato['ID_PEDIDO'])?>')" >
              <img src="imagenes/lupa1.png" width="43" height="32" style="background-size:cover" title="Detalle Gestion"/>
                </a> 
            </th>               
              <?php 
	  if($privilegios ==1){ ?>
      <th>
		<a  href="javascript:ventanaSecundaria('../presentacion/editar_pedidos_detalle.php?x=<?php echo base64_encode($dato['ID_PEDIDO'])?>')" >
              <img src="imagenes/lapiz.png" width="43" height="32" style="background-size:cover" title="EDITAR PEDIDO"/>
                </a> 
	<?php	  } ?>
    </th>	
            
		</tr>
	<?php 
	}
	?>
    </tbody>
</table>
   <div style="font-size:18px; font-weight:1000">
   <?php 
   if($total_fecha!=''){
	   echo 'Total pedidos: $'.number_format($total_fecha, 0, ',', '.');
	   }
   ?>
   </div>
</body>
</html>