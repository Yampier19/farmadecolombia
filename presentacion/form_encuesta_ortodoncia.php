<?php
	include("../logica/session.php");
	error_reporting(0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

<link href="css/encuenta.css" type="text/css" rel="stylesheet" />
<link href="css/bootstrap.css" rel="stylesheet" />
<script src="https://unpkg.com/sweetalert2@7.1.3/dist/sweetalert2.all.js"></script>
<script src="js/jquery.js"></script>
<script src="js/detalle_consumo.js"></script>

<script>
function consultar_productos_consumo()
{
	var categoria=$('#categoria').val();
	var subcategoria=$('#subcategoria').val();
	var Descripcion=$('#Descripcion').val();
	var id_encuesta=$('#id_encuesta').val();
	$.ajax(
	{
	//	url:'../presentacion/ingresar_productos.php',
		data:
		{
			categoria:categoria,
			subcategoria:subcategoria,
			descripcion:Descripcion,
			id_encuesta:id_encuesta
		},
		type: 'post',
		beforeSend: function () 
		{
			//alert('Cargando')
		},
		success: function(data)
		{
			$('#categoria').find('option:first').attr('selected', 'selected').parent('select');
			$('#subcategoria').html('');
			$('#Descripcion').html('');
			$("#subcategoria").attr('disabled', 'disabled');
			$("#Descripcion").attr('disabled', 'disabled');
			
		}
	})
}



function consultar_productos_consumo2()
{
	$.ajax(
	{
		url:'../presentacion/consulta_productos.php',
		data:
		{
		},
		type: 'post',
		beforeSend: function () 
		{
			
		},
		success: function(data)
		{
			
		}
	})
}

function categoria_competencia()
{
	$.ajax(
	{
		url:'../presentacion/consulta_categoria_competencia.php',
		type: 'post',
		beforeSend: function () 
		{
			$("#categoria_competencia").attr('disabled', 'disabled');
			$("#subcategoria_competencia").attr('disabled', 'disabled');
			$("#Descripcion_competencia").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#categoria_competencia").removeAttr('disabled');
			$('#categoria_competencia').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}
function subcategoria_competencia()
{
	var categoria=$('#categoria_competencia').val();
	$.ajax(
	{
		url:'../presentacion/consulta_subcategoria_competencia.php',
		data:
		{
			categoria: categoria
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#subcategoria_competencia").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#subcategoria_competencia").removeAttr('disabled');
			$('#subcategoria_competencia').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}


function Descripcion_competencia()
{
	var subcategoria=$('#subcategoria_competencia').val();
	var categoria=$('#categoria_competencia').val();
	$.ajax(
	{
		url:'../presentacion/consulta_descripcion_competencia.php',
		data:
		{
			categoria: categoria,
			subcategoria:subcategoria
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#Descripcion_competencia").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#Descripcion_competencia").removeAttr('disabled');
			$('#Descripcion_competencia').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}

//productos de consumo

function categoria()
{
	$.ajax(
	{
		url:'../presentacion/consulta_categoria.php',
		type: 'post',
		beforeSend: function () 
		{
			$("#categoria").attr('disabled', 'disabled');
			$("#subcategoria").attr('disabled', 'disabled');
			$("#Descripcion").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#categoria").removeAttr('disabled');
			$('#categoria').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}

function subcategoria()
{
	var categoria=$('#categoria').val();
	$.ajax(
	{
		url:'../presentacion/consulta_subcategoria.php',
		data:
		{
			categoria: categoria
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#subcategoria").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#subcategoria").removeAttr('disabled');
			$('#subcategoria').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}
function Descripcion()
{
	var subcategoria=$('#subcategoria').val();
	var categoria=$('#categoria').val();
	$.ajax(
	{
		url:'../presentacion/consulta_descripcion.php',
		data:
		{
			categoria: categoria,
			subcategoria:subcategoria
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#Descripcion").attr('disabled', 'disabled');
			$("#cargando").fadeIn();
		},
		success: function(data)
		{
			$("#Descripcion").removeAttr('disabled');
			$('#Descripcion').html(data);
			$("#cargando").fadeOut([1000]);
		}
	})
}
function consultar()
{
	var id_encuesta=$('#id_encuesta').val();
	$.ajax(
	{
		url:'../presentacion/consultar_productos_usados.php',
		data:
		{
			id_encuesta: id_encuesta
		},
		type: 'post',
		beforeSend: function () 
		{
			//alert('Cargando')
		},
		success: function(data)
		{
			$('#tablas_usado').html(data);
		}
	})
}
$(document).ready(function()
{
	consultar_productos_consumo();
	categoria();
	categoria_competencia();
	consultar();
	setInterval("consultar_productos_consumo2()", 5000);
	var height= 700;
	$('#cargando').css('height',height);
	var div_caragr=($("#cargando").height()/2);
	var div_cargar_ancho=($("#cargando").width()/2);
	var div_cargar_ancho_img=$("#img_cargando").width();
	var total=div_caragr;
	var total_ancho_img=(div_cargar_ancho-div_cargar_ancho_img)/2;
	//$('#img_cargando').css('margin','auto'
	$('#img_cargando').css('margin-top',total);
	$('#categoria').change(function()
	{
		Descripcion();
		//$('#subcategoria').html('');
		$('#Descripcion').html('');
		//$("#Descripcion").attr('disabled', 'disabled');
	});
	$('#subcategoria').change(function()
	{
		Descripcion();
	});
	$('#Descripcion').change(function()
	{
		descripcion=$('#Descripcion').val();
		
		if(descripcion!='')
		{
			$('#guardar').css("display","block");
		}
	});
	
	
	$('#guardar').click(function()
	{
		//INGRESAR();
		consultar_productos_consumo()
		$('#guardar').css("display","none");
		$('#confirmar').css("display","block");
	});
	$('#elim').click(function()
	{
		alert('ok')
	});
	
	$('#categoria_competencia').change(function()
	{
		subcategoria_competencia();
		$('#subcategoria_competencia').html('');
		$('#Descripcion_competencia').html('');
		$("#Descripcion_competencia").attr('disabled', 'disabled');
	});
	
	$('#subcategoria_competencia').change(function()
	{
		Descripcion_competencia();
	});
	
	$('#Descripcion_competencia').change(function()
	{
		descripcion=$('#Descripcion_competencia').val();
		
		if(descripcion!='')
		{
			$('#guardar_competencia').css("display","block");
		}
	});
	
	
});
</script>
<script>
function obtener()
{
	var parametros={enableHighAccuracy:true}
	navigator.geolocation.watchPosition(mostrar_posicion,gestion_errores,parametros);
	
}
function mostrar_posicion(posicion)
{
	var ubicacion=document.getElementById("UBICACION");
	var latitud=posicion.coords.latitude;
	var longitud=posicion.coords.longitude;
	var exactitud=posicion.coords.accuracy;
	/*alert(exactitud);*/
	document.getElementById("LAT").value=latitud;
	document.getElementById("lon").value=longitud;	
}
function gestion_errores(error)
{
	//alert("ha ocurrido un error "+error.code +" "+error.massage);
	if(error.code==1)
	{
		//alert("debes permitir el uso de la geolocalizacion en tu navegador");
	}
}
</script>
<script>
function validar(tuformulario)
{
	
	resina  =$("#resinas",consul.document).val();
	cemento =$("#cemento",consul.document).val();
	adhesivo  =$("#adhesivo",consul.document).val();
	pulido  =$("#pulido",consul.document).val();

	

	especialidad=$('#especialidad_odontologo').val();
	
	var valor_check_def='Cliente pontencial para: ';
	
	
	if(resina == 0 && cemento == 0 && adhesivo==0 && pulido==0 ){
		alert(valor_check_def +' \n Resina. \n Adhesivo. \n Cemento. \n Sistema de pulido. ');
		}
	else if(resina > 0 && cemento == 0 && adhesivo==0 && pulido==0 ){
		alert(valor_check_def +' \n Adhesivo. \n Cemento. \n Sistema de pulido. ');
		}
	else if(resina == 0 && cemento > 0 && adhesivo==0 && pulido==0 ){
		alert(valor_check_def +' \n Resina. \n Adhesivo. \n Sistema de pulido. ');
		}
		else if(resina == 0 && cemento == 0 && adhesivo >0 && pulido==0 ){
		alert(valor_check_def +' \n Resina.  \n Cemento. \n Sistema de pulido. ');
		}
		else if(resina == 0 && cemento == 0 && adhesivo==0 && pulido>0 ){
		alert(valor_check_def +' \n Resina. \n Adhesivo. \n Cemento. ');
		}
		else if(resina > 0 && cemento > 0 && adhesivo==0 && pulido==0 ){
		alert(valor_check_def +'\n Adhesivo.\n Sistema de pulido. ');
		}
		else if(resina > 0 && cemento == 0 && adhesivo >0 && pulido==0 ){
		alert(valor_check_def +' \n Cemento. \n Sistema de pulido. ');
		}
		else if(resina > 0 && cemento == 0 && adhesivo==0 && pulido>0 ){
		alert(valor_check_def +' \n Adhesivo. \n Cemento. ');
		}
		else if(resina == 0 && cemento > 0 && adhesivo>0 && pulido==0 ){
		alert(valor_check_def +' \n Resina.\n Sistema de pulido. ');
		}
		else if(resina == 0 && cemento > 0 && adhesivo==0 && pulido >0 ){
		alert(valor_check_def +' \n Resina. \n Adhesivo. ');
		}
		else if(resina == 0 && cemento == 0 && adhesivo >0 && pulido >0 ){
		alert(valor_check_def +' \n Resina. \n Cemento.  ');
		}
		else if(resina > 0 && cemento > 0 && adhesivo>0 && pulido==0 ){
		alert(valor_check_def +' \n Sistema de pulido. ');
		}
		else if(resina == 0 && cemento > 0 && adhesivo >0 && pulido >0 ){
		alert(valor_check_def +' \n Resina.  ');
		}
		else if(resina > 0 && cemento == 0 && adhesivo >0 && pulido >0 ){
		alert(valor_check_def +' \n Cemento. ');
		}
		else if(resina > 0 && cemento > 0 && adhesivo==0 && pulido>0 ){
		alert(valor_check_def +' \n Adhesivo. ');
		}
	
	
	var proxima_visita= $('#proxima_visita').val();
	if(proxima_visita==''){
		return (false);
	}else{
	swal({
		  title: 'ESTA SEGURO(A) DE GUARDAR LA ENCUESTA',
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Confirmar',
		  cancelButtonText: 'Cancelar'
		}).then((result) => {
		  if (result.value) {
			//document.getElementById("tranferencias").submit();
			 $('#tranferencias').submit();
		return (true);
		  }
		
		  
		})
	}
	return (false);
}
$(document).ready(function()
{
});
</script>
</head>

<?php
	include('../datos/conex.php');
	$consulta_cliente=mysql_query("SELECT * FROM 3m_cliente WHERE ID_CLIENTE=$ID_CLIENTE",$conex);
	$nreg_clien=mysql_num_rows($consulta_cliente);
	if($nreg_clien>0)
	{
		while($dato_cliente=mysql_fetch_array($consulta_cliente))
		{
			$IDENTIFICACION_CLIENTE=$dato_cliente['IDENTIFICACION_CLIENTE'];
			$CELULAR_CLIENTE=$dato_cliente['CELULAR_CLIENTE'];
			$TELEFONO_CLIENTE=$dato_cliente['TELEFONO_CLIENTE'];
		}
		if($IDENTIFICACION_CLIENTE==0)
		{
			$IDENTIFICACION_CLIENTE='';
			$CELULAR_CLIENTE='';
			$TELEFONO_CLIENTE='';
		}
	}
	else if($nreg_clien<=0)
	{
		$IDENTIFICACION_CLIENTE='';
		$CELULAR_CLIENTE='';
		$TELEFONO_CLIENTE='';
	}
	
	
	
	$consulta=mysql_query("SELECT * FROM 3m_encuesta WHERE ID_CLIENTE_FK_5=$ID_CLIENTE",$conex);
	$nreg=mysql_num_rows($consulta);
	if($nreg>0)
	{
		while($dato=mysql_fetch_array($consulta))
		{
			$id_encuesta=$dato['ID_ENCUESTA'];
			$pacientes=$dato['CANTIDAD_PACIENTES'];
			$especialida=$dato['ESPECIALIDAD_ODONTOLOGO'];
			$clasificacion=$dato['CLASIFICACION_ODONTOLOGO'];
			$cantida_categoria=$dato['CANTIDAD_CATEGORIA'];
			$distribuidor=$dato['DISTRIBUIDOR_COMPRA'];
			$cumpleanios = $dato['FECHA_CUMPLEANIOS'];
			$dia_visita = $dato['DIA_VISITA'];
			$hora_ini_visita = $dato['HORA_INI_VISITA'];
			$hora_fin_visita = $dato['HORA_FIN_VISITA'];
			
		}
	}
	else if($nreg<=0)
	{
		$id_encuesta='';
		$pacientes='';
		$especialida='Elija...';
		$clasificacion='Elija...';
		$cantida_categoria='';
		$distribuidor='Elija...';
	}
	
	$consulta_RD=mysql_query("SELECT DISTINCT(SUBCATEGORIA) FROM 3m_categoria WHERE CATEGORIA='Restauraciones directas'",$conex);
	$nreg_a=mysql_num_rows($consulta_RD);
	
	$consulta_RI=mysql_query("SELECT DISTINCT(SUBCATEGORIA) FROM 3m_categoria WHERE CATEGORIA='Restauraciones indirectas'",$conex);
	$nreg_b=mysql_num_rows($consulta_RI);
	
	$consulta_P=mysql_query("SELECT DISTINCT(SUBCATEGORIA) FROM 3m_categoria WHERE CATEGORIA='Prevencion'",$conex);
	$nreg_c=mysql_num_rows($consulta_P);
	
	if($nreg_a>$nreg_b)
	{
		if($nreg_a>$nreg_c)
		{
			if($nreg_b>$nreg_c)
			{
				$mayor=$nreg_a;
				$medio=$nreg_b;
				$menor=$nreg_c;
				
				$consulta_primera=$consulta_RD;
				$consulta_segunda=$consulta_RI;
				$consulta_tercera=$consulta_P;
				
				$titulo1="Restauraciones Directas";
                $titulo2="Restauraciones Indirectas";
                $titulo3="Preventivo";
			}
			else
			{
				$mayor=$nreg_a;
				$medio=$nreg_C;
				$menor=$nreg_b;
				
				$consulta_primera=$consulta_RD;
				$consulta_segunda=$consulta_P;
				$consulta_tercera=$consulta_RI;
				
				$titulo1="Restauraciones Directas";
				$titulo2="Preventivo";
                $titulo3="Restauraciones Indirectas";
			}
		}
		else
		{
			$mayor=$nreg_a;
			$medio=$nreg_C;
			$menor=$nreg_B;
				
			$consulta_primera=$consulta_P;
			$consulta_segunda=$consulta_RD;
			$consulta_tercera=$consulta_RI;
			
			$titulo1="Restauraciones Directas";
			$titulo2="Preventivo";
			$titulo3="Restauraciones Indirectas";
		}
	}
	else
	{
		if($nreg_b>$nreg_c)
		{
			if($nreg_a>$nreg_c)
			{
				$mayor=$nreg_b;
				$medio=$nreg_a;
				$menor=$nreg_c;
				
				$consulta_primera=$consulta_RI;
				$consulta_segunda=$consulta_RD;
				$consulta_tercera=$consulta_P;
				
				$titulo1="Restauraciones Indirectas";
				$titulo2="Restauraciones Directas";
                $titulo3="Preventivo";
			}
			else
			{
				$mayor=$nreg_b;
				$medio=$nreg_c;
				$menor=$nreg_a;
				
				$consulta_primera=$consulta_RI;
				$consulta_segunda=$consulta_P;
				$consulta_tercera=$consulta_RD;
				
				$titulo1="Restauraciones Indirectas";
				$titulo2="Preventivo";
				$titulo3="Restauraciones Directas";
			}
		}
		else
		{
			$mayor=$nreg_c;
			$medio=$nreg_b;
			$menor=$nreg_a;
				
			$consulta_primera=$consulta_P;
			$consulta_segunda=$consulta_RI;
			$consulta_tercera=$consulta_RD;
			
			$titulo1="Preventivo";
			$titulo2="Restauraciones Indirectas";
			$titulo3="Restauraciones Directas";
		}
	}

if($id_usu!=''&&$id_usu!='0')
{
?>
<style>
.col-md-3{
	margin-top:5px;
	}
.error
{
	font-size: 130%;
	font-weight: bold;
	color: #fb8305;
	text-transform:uppercase;
	background-color:transparent;
	text-align: center;
	padding:10px;
}
</style>
<body onload="obtener()">
<?php
if(empty($id_usu)){ ?>
				<span style="margin-top:5%;">
			<center>
			<img src="../presentacion/imagenes/advertencia.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
			<p class="error" style=" width:68.9%; margin:auto auto;">
			 LA SESI&Oacute;N HA CADUCADO, POR FAVOR INICIE SESION.
			</p>
			<br />
			<br />
			
			</center>
			</span>
				
			<?php }else{?>
	<div class="form-group" style='display:none;' >
  
        <input name="id_cliente" id="id_cliente" type="text" readonly="readonly"value="<?php echo $ID_CLIENTE ?>"/>
        <input name="distribuidor" id="distribuidor" type="text" readonly="readonly" class="form-control1" value="<?php echo $DISTRIBUIDOR ?>"/>

    </div>
    <header>
        <div class="container" id="div_header">
        	<center><h1>ENCUESTA</h1></center>
        </div>
    </header>
    <!--<center>
        <img src="../presentacion/imagenes/construccion.jpg" width="100%" height="50%" />
    </center>-->
    <input name="id_US" id="id_US" class="form-control" type="hidden" readonly="readonly" value="<?php echo $id_usu ?>"/>
    <input name="id_encuesta" id="id_encuesta" class="form-control" type="hidden" placeholder="Cantidad pacientes" readonly="readonly" value="<?php echo $id_encuesta ?>"/>
    
    <div class="form-group">
    
        <div class="col-md-3">
            <label for="cliente">N&uacute;mero de Identificaci&oacute;n</label><span class="asterisco">*</span>
         </div>
         <div class="col-md-3"> 
            <input name="identificacion" id="identificacion" class="form-control" type="text" placeholder="identificacion" maxlength="20" value="<?php echo $IDENTIFICACION_CLIENTE ?>"/>
          </div>
            <div class="col-md-3"> 
            <label for="cliente">N&uacute;mero de Telefono Fijo</label><span class="asterisco">*</span>
           </div>
           
            <div class="col-md-3"> 
            <input name="numero_tel" id="numero_tel" class="form-control" type="text" placeholder="identificacion"   value="<?php echo $TELEFONO_CLIENTE ?>"/>
            </div> 
        </div>
        
    <div class="form-group">
         	<div class="col-md-3">
            <label for="cliente">N&uacute;mero de Celular</label><span class="asterisco">*</span>
          	 </div>
            <div class="col-md-3">
        <input name="numero_cel" id="numero_cel" class="form-control" placeholder="identificacion"  maxlength="20" value="<?php echo $CELULAR_CLIENTE ?>" type="text" />
    		</div>  
          	<div class="col-md-3">
          <label for="cliente">Fecha Cumplea&ntilde;os</label>
         	</div>
         	<div class="col-md-3">       
            <input name="cumpleanios" id="cumpleanios" class="form-control" type="date"  value="<?php echo $cumpleanios ?>"/>
        	</div>
      </div>
      
    <div class="form-group">
         <div class="col-md-3">
            <label for="cliente">Dia Visita</label>
            </div>
             <div class="col-md-3">       
          <select name="dia_visita" id="dia_visita" class="form-control">
                <option><?PHP echo $dia_visita ?></option>
                <option>Lunes</option>
                <option>Martes</option>
                <option>Miercoles</option>
                <option>Jueves</option>
                <option>Viernes</option>            
                <option>Sabado</option>
                <option>Domingo</option>
            </select>
        </div>
         <div class="col-md-3">
          <label for="cliente">Hora Inicio De Visita</label>
         </div>
          <div class="col-md-3">
            <input name="hora_ini_visita" id="hora_ini_visita" class="form-control" type="time"  value="<?php echo $hora_ini_visita ?>"/>
        </div>
    </div>
    
    <div class="form-group">
         <div class="col-md-3">
          <label for="cliente">Hora Fin De Visita</label>
          </div>
          <div class="col-md-3">
            <input name="hora_fin_visita" id="hora_fin_visita" class="form-control" type="time"  value="<?php echo $hora_fin_visita ?>"/>
          </div>
          <div class="col-md-3">
            <label for="cliente">Paciente que atienden por semanas</label><span class="asterisco">*</span>
          </div>
           <div class="col-md-3">
              <input name="cantidad_pacientes" id="cantidad_pacientes" class="form-control" type="number" max="999" min="0"  value="<?php echo $pacientes ?>" autocomplete="off"/>
           </div>
    </div>
    
    <div class="form-group">
         <div class="col-md-3">
            <label for="cliente">Especialidad Odontologo</label><span class="asterisco">*</span>
            </div>
             <div class="col-md-3">
            <select name="especialidad_odontologo" id="especialidad_odontologo" class="form-control" >
                <option><?PHP echo $especialida ?></option>
                <?php
                $consulta_especialidad=mysql_query("SELECT * FROM 3m_especialidad_odontologo WHERE NOMBRE_ESPECIALIDAD!='' AND NOMBRE_ESPECIALIDAD!='$especialida' ORDER BY NOMBRE_ESPECIALIDAD ASC",$conex);
                echo mysql_error($conex);
                while($datos=mysql_fetch_array($consulta_especialidad))
                {
                ?>
                    <option><?php echo $datos['NOMBRE_ESPECIALIDAD'] ?></option>
                <?php
                }
                ?>
            </select>
        </div>
    <div class="col-md-3">
        <label for="cliente">Clasificacion Odontologo</label><span class="asterisco">*</span>
        </div>
         <div class="col-md-3">
       <input name="clasificacion_odontologo" id="clasificacion_odontologo" class="form-control" type="text" readonly="readonly" value="<?php if (empty($clasificacion)==false){ echo $clasificacion;} ?>"/>
    </div>
    </div>
    <div class="table table-responsive">
		<label for="cliente">Categorias de 3M(Ultima encuesta)</label>
        <br />
        <table style="width:100%;border:1px solid #000; margin:auto auto;" rules="all" id="tablas_usado">
        </table>
        <br />
    </div>
    
  
        <div class="form-group">
            <label for="cliente">Productos de 3M Usan</label><span class="asterisco">*</span>
            <br />
         </div>
         <div class="col-md-3">
                  <label for="cliente">Categoria</label><span class="asterisco">*</span>
             </div>
             <div class="col-md-3">
                  <select name="categoria" id="categoria" class="form-control" >
                  </select>
             </div>
             
         <div class="form-group">
            <div class="col-md-3">
                <label for="cliente">Descripci&oacute;n</label><span class="asterisco">*</span>
             </div>
             <div class="col-md-3">
                <select name="Descripcion" id="Descripcion" class="form-control" >
                </select>
            </div>
     
       </div>
       
        <div class="form-group">
            <input name="guardar" type="submit" id="guardar" value="GUARDAR" class="form-control btn" formaction="../presentacion/form_ingresar_producto_consumo.php" formmethod="post" formtarget="consul" style="display:none"/>
        </div>
        
        <div class="form-group">
            <div class="form-group">
            <iframe src="../presentacion/form_ingresar_producto_consumo.php" style="width:99%; border:none" name="consul" height="200px" id="consul">
                
            </iframe>
            </div> 
    </div>
         <div class="form-group">
            <label for="cliente">Producto de la competencia</label><span class="asterisco">*</span>
            <br />
         </div>
         <div class="col-md-3">
                  <label for="cliente">Categoria</label><span class="asterisco">*</span>
             </div>
             
             
             <div class="col-md-3">
                  <select name="categoria_competencia" id="categoria_competencia" class="form-control" >
                  </select>
             </div>
       
             <div class="col-md-3">
                  <label for="cliente">Subcategoria</label><span class="asterisco">*</span>
             </div>
              <div class="col-md-3">
                  <select name="subcategoria_competencia" id="subcategoria_competencia" class="form-control" >
                  </select>
            </div>
              
         <div class="form-group">
            <div class="col-md-3">
                <label for="cliente">Descripci&oacute;n</label><span class="asterisco">*</span>
             </div>
             <div class="col-md-3">
                <select name="Descripcion_competencia" id="Descripcion_competencia" class="form-control" >
                </select>
            </div>
     
       </div>
       
       <div class="form-group">
            <input name="guardar_competencia" type="submit" id="guardar_competencia" value="GUARDAR" class="form-control btn" formaction="../presentacion/form_ingresar_competencia.php" formmethod="post" formtarget="consul_competencia" style="display:none"/>
        </div>
          <div class="form-group">
            <div class="form-group">
            <iframe src="../presentacion/form_ingresar_competencia.php" style="width:99%; border:none" name="consul_competencia" height="300px" id="consul_competencia">
                
            </iframe>
            </div> 
    </div>
         
          <div class="form-group">     
      <div class="col-md-3">
        <label for="cliente">Distribuidor que compra</label><span class="asterisco">*</span>
        </div>
        <div class="col-md-3">
        <select name="distribuidor" id="distribuidor" class="form-control" >
        	<option value="<?php echo $distribuidor ?>"><?php echo $distribuidor ?></option>
           
		    <?php 
                        $consulta =mysql_query("
         SELECT DISTINCT d.NOMBRE_DENTAL, u.USER, u.CIUDAD
FROM 3m_dentales AS d
INNER JOIN 3m_usuario AS u ON d.CIUDAD_DENTAL=u.CIUDAD
WHERE ID_USUARIO='".$id_usu."' AND ID_DENTAL <> 5;",$conex);
                 
                 
                  while($dato=mysql_fetch_array($consulta)) { ?>
                <option value="<?php echo $dato['NOMBRE_DENTAL']; ?>"><?php echo $dato['NOMBRE_DENTAL']; ?></option>
                <?php }  //ANDRES CHACON MANEJA ORTODONCIA
			if($id_usu==9){?>
			<option>Paola Linares-Dental 83 Portafolio 3M ESPE</option>
			<option>Claudia Esperanza Beltran-Dentales Padilla</option>  
			<option>Gina Santamaria-Dental Nader</option>
            <option>Yina Moncayo-Dental Palermo</option>			
			<option>Bracket Ltda-Andres Beltran</option>	
			
			<?php }
			if ($id_usu==7 || $id_usu==19){?>
            <option>Bracket Ltda-Andres Beltran</option>
			<?php 
			}
			if($id_usu==7){//SGONZALES QUE MANEJA BOGOTA Y VILLLAVICENCIO?>
			<option>Dental Nader Villavicencio-Jhovanny Parra</option>	
			<?php }
			if($id_usu==16){?>
			<option>Paola Linares-Dental 83 Portafolio 3M ESPE</option>
			<?php }
			// } ?>	
			<?php 
			if($id_usu==26){ // usuario visita ?>
<option>Faride Maestre-Alfa Representaciones dentales</option>					
			<?php } ?>
        </select>
    </div>
    </div>
   


   
<?php
}
}

?>