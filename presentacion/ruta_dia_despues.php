<?php
	include("../logica/session.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="../presentacion/imagenes/3m.png" />
<title>RUTA MA&Ntilde;ANA</title>
<link href="css/menu_visitas.css" type="text/css" rel="stylesheet" />

<link rel="stylesheet" href="../presentacion/fonts.css" />
<link href="css/tablas.css" rel="stylesheet" />
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
<style>
input
{
	line-height:2%;
	width:90.5%;
}
select
{
	line-height:15%;
	width:99%;
}
.form-control[disabled],.form-control[readonly],fieldset[disabled] .form-control
{
	background-color:#eee;
	opacity:1;
	cursor:not-allowed;
}
.form-control
{
	display:block;
	padding:6px 12px;
	line-height:1.42857143;
	color:#555;
	background-color:#fff;
	background-image:none;
	border:1px solid #ccc;
	border-radius:4px;
	-webkit-box-shadow:inset 0 1px 1px rgba(0,0,0,.075);
	box-shadow:inset 0 1px 1px rgba(0,0,0,.075);
	-webkit-transition:border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
	-o-transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;
	transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;
	font-size:90%;
}
.btn
{
  color: #fff;
  background-color: #337ab7;
  border-color: #2e6da4;
}
.btn:focus,
.btn.focus {
  color: #fff;
  background-color: #286090;
  border-color: #122b40;
}
.btn:hover {
  color: #fff;
  background-color: #286090;
  border-color: #204d74;
}
.btn:active,
.btn.active,
.open > .dropdown-toggle.btn{
  color: #fff;
  background-color: #286090;
  border-color: #204d74;
}
.btn:active:hover,
.btn.active:hover,
.open > .dropdown-toggle.btn:hover,
.btn:active:focus,
.btn.active:focus,
.open > .dropdown-toggle.btn:focus,
.btn:active.focus,
.btn.active.focus,
.open > .dropdown-toggle.btn.focus {
  color: #fff;
  background-color: #204d74;
  border-color: #122b40;
}
.btn
{
  color: #fff;
  background-color: #337ab7;
  border-color: #2e6da4;
}
th
{
	padding:8px;
}
</style>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<script src="../presentacion/js/jquery.js"></script>
</head>

<script>
function ingresar_paso()
{
	var latitud=$('#LAT').val();
	var longitud=$('#lon').val();
	$.ajax(
	{
		url:'../presentacion/ingresar_paso.php',
		data:
		{
			latitud: latitud,
			longitud:longitud
		},
		type: 'post',
		success: function(data)
		{
			alert("Bienvenido(a) a 3M")
		}
	})
}

var height= window.innerHeight-10;/*tamaño ventana*/
var ancho=window.innerWidth;
//alert(ancho);
/*alert(height);*/
$(document).ready(function()
{
	
	if(ancho<1001)
	{
		$('#info_new').css('height',height);
		var tama_menu=$("#div_menu").height();
		var div_buscar=$("#buscar").height();
		tama_menu=tama_menu*(-1);
		$('#info_new').css('margin-bottom',tama_menu);
		$("#info_new").animate({marginTop:tama_menu},1);
		
		$('#div_menu li').click(function()
		{
			$("#div_menu").animate({marginLeft:'-120%'},500);
			$('#info_new').toggle(1000);
			$('#tabla').css('display','none');
		});
		$('#menu_bar').click(function()
		{
			$("#div_menu").animate({marginLeft:'0'},500);
			$('#info_new').fadeOut(1000);
			$("#div_menu").css('display','block');
		});
	}
	else
	{
		$('#info_new').css('height',height);
		$('#info_new').css('display','block');
	}
});
function obtener()
{
	var parametros={enableHighAccuracy:true}
	navigator.geolocation.getCurrentPosition(mostrar_posicion,gestion_errores,parametros);
	
}
function mostrar_posicion(posicion)
{
	var ubicacion=document.getElementById("UBICACION");
	
	var latitud=posicion.coords.latitude;
	var longitud=posicion.coords.longitude;
	var exactitud=posicion.coords.accuracy;
	/*alert(exactitud);*/
	document.getElementById("LAT").value=latitud;
	document.getElementById("lon").value=longitud;
	ingresar_paso()
	
}
function gestion_errores(error)
{
	//alert("ha ocurrido un error "+error.code +" "+error.massage);
	if(error.code==1)
	{
		alert("debes permitir el uso de la geolocalizacion en tu navegador");
	}
}
</script>


<?php
include('../datos/conex.php');
$_SESSION['ID_CLIENTE']='';
$_SESSION['NOMBRE_CLIENTE']='';
$_SESSION['DISTRIBUIDOR_COMPRA']='';
?>
<body>

<center><img src="../presentacion/imagenes/3m.png" width="24%" height="17%" /></center>
	<div class="form-group" style="display:none">
        <input id="LAT" name="LAT" type="text" class="form-control" autocomplete="off" readonly="readonly"/>
        <input id="lon" name="lon" type="text" class="form-control" autocomplete="off" readonly="readonly"/>
    </div>
    <table style="width:99%; margin:auto auto;" rules="none" id="tabla">
    	<tr>
        	<th colspan="4" class="principal">
            	RUTA ASIGNADA
            </th>
        </tr>
        <tr>
        	<!--<th class="TITULO">GESTIONAR</th>-->
            <th class="TITULO">NOMBRE ODONTOLOGO</th>
            <th class="TITULO">DIRECCION</th>
            <th class="TITULO">TELEFONO</th>
        </tr>
        <?PHP
		$consulta=mysql_query("SELECT * FROM 3m_cliente WHERE FECHA_ASIGNADO=DATE_SUB(CURDATE(), INTERVAL -1 DAY) AND USUARIO_ASIGNADO='$id_usu' AND ESTADO_ASIGNADO='ASIGNADO' ORDER BY ORDEN_ASIGNADO ASC",$conex);
		echo mysql_error($conex);
		while($dato=mysql_fetch_array($consulta))
		{
		?>
			<tr class="datos">
            	<!--<th>
					<a  href="../presentacion/inicio_consulta_cliente.php?x=<?php echo base64_encode($dato['ID_CLIENTE'])?>">
					<img src="../presentacion/imagenes/lupa1.png" width="30" height="25" style="background-size:cover" title="GESTIONAR"/>
					</a>                
				</th>-->
				<td><?php echo $dato["NOMBRE_CLIENTE"]." ".$dato["APELLIDO_CLIENTE"]?></td>
				<td><?php echo $dato["DIRECCION_CLIENTE"]?></td>
				<td><?php echo $dato["CELULAR_CLIENTE"]." - ".$dato["TELEFONO_CLIENTE"]?></td>	
			</tr>
		<?php 
		}
		?>
    </table>
</body>
</html>