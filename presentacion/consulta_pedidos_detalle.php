<?php
	include("../logica/session.php");
       header('Content-Type: text/html; charset=UTF-8'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, text/html; charset=utf-8"/>
<link href="css/tablas.css" rel="stylesheet" />
<link href="css/bootstrap.css" rel="stylesheet" />
<title>Detalle pedido</title>
<link rel="shortcut icon" href="imagenes/3m.png" />
<script src="js/jquery.js"></script>
<script>
$(document).ready(function()
{
	var estado_producto_act=$('#estado_producto_act').val();
	if(estado_producto_act=='ENTREGA PARCIAL')
	{
		$('#esta_titu').css('display','block');
		
		$('#esta_titu').css('border','#403f3d');
		
		var cantidad=$('#cantidad').val();
		for(i=1;i<=cantidad;i++)
		{
			$('#esta'+i).css('display','block');
			$("#estado_producto"+i+ "option:eq(0)").attr("selected", "selected");
		}
	}
	function estado()
	{
		var estado=$('#estado_actual').val();
		if(estado!='')
		{
			$('#guardar').css('display','block');
			if(estado=='ENTREGA PARCIAL')
			{
				$('#esta_titu').css('display','block');
				
				$('#esta_titu').css('border','#403f3d');
				
				var cantidad=$('#cantidad').val();
				for(i=1;i<=cantidad;i++)
				{
					$('#esta'+i).css('display','block');
					$("#estado_producto"+i+ "option:eq(0)").attr("selected", "selected");
				}
			}
			if(estado!='ENTREGA PARCIAL')
			{
				$('#esta_titu').css('display','none');
				var cantidad=$('#cantidad').val();
				
				for(i=1;i<=cantidad;i++)
				{
					$('#esta'+i).css('display','none');
				}
				//$('#esta').css('display','none');
			}
		}
		else
		{
			
			$('#guardar').css('display','none');
			$('#esta_titu').css('display','none');
			var cantidad=$('#cantidad').val();
			
			for(i=1;i<=cantidad;i++)
			{
				$('#esta'+i).css('display','none');
			}
			$("#estado_producto option:eq(0)").attr("selected", "selected");
		}
	}
	
	$('#estado_actual').change(function()
	{
        estado()
    });    
});
</script>

</head>
<?PHP
include('../datos/conex.php');
$ID_PEDIDO=base64_decode($x);
echo $ID_PEDIDO;
$consulta_pedido=mysql_query("SELECT * FROM 3m_pedido AS p
 INNER JOIN 3m_cliente AS cli ON cli.ID_CLIENTE=p.ID_CLIENTE_FK
 INNER JOIN 3m_usuario AS u ON p.ID_USUARIO_FK=u.ID_USUARIO
 WHERE ID_PEDIDO='$ID_PEDIDO'",$conex);
?>
<body>
<form action="../logica/cambio_estado_pedido.php" method="post" style="padding:10px;">
<table style="width:99%; margin:auto auto;" rules="none" class="table table-striped">
	<tr>
		<th colspan='6' class="principal">INFORMACION PEDIDO</th>
	</tr>
    <?PHP
    while($dato_pedido=mysql_fetch_array($consulta_pedido))
	{
		$CLIENTE = $dato_pedido["NOMBRE_CLIENTE"]." ".$dato_pedido["APELLIDO_CLIENTE"]; 
	?>
    
		<tr class="datos" style="border:1px solid #000;">
        	<th colspan="2">
            	NOMBRE CLIENTE                
            </th>
            <th colspan="4"><?php echo $CLIENTE?></th>
       </tr>
        <tr class="datos" style="border:1px solid #000;">
        	<th>
            	TOTAL PEDIDO
                <input type="hidden" name="id" id="id" value="<?php echo $dato_pedido["ID_PEDIDO"] ?>" />
                <input type="hidden" name="id_cliente" id="id_cliente" value="<?php echo $ID_CLIENTE ?>" />
            </th>
            <td><?php echo $dato_pedido["TOTAL_PEDIDO"]?></td>
            
            <th>FECHA PEDIDO </th>
            <td><?php echo $dato_pedido["FECHA_PEDIDO"]?></td>
            
            <th>DISTRIBUIDOR</th>
            <td><?php echo $dato_pedido["DISTRIBUIR"]?></td>
       </tr>
       <tr class="datos" style="border:1px solid #000;">
       		<th>CELULAR CLIENTE</th>
            <td><?php echo $dato_pedido["CELULAR_CLIENTE"]?></td>
            
            <th>TELEFONO FIJO</th>
            <td><?php echo $dato_pedido["TELEFONO_CLIENTE"]?></td>
            
            <th>DIRECCION CLIENTE</th>
            <td><?php echo $dato_pedido["DIRECCION_CLIENTE"]?></td>
       </tr> 
       <tr class="datos" style="border:1px solid #000;">     
            <th>USUARIO</th>
            <td style="text-align:center"><?php echo $dato_pedido["USER"]?></td>
            
            <th>ESTADO ACTUAL PEDIDO </th>
            <td>
				<?php echo $dato_pedido["ESTADO_PEDIDO"]?>
                <input type="hidden" value="<?php echo $dato_pedido["ESTADO_PEDIDO"] ?>" name="estado_producto_act" id="estado_producto_act" />
            </td>
            
            <th>CAMBIO ESTADO PEDIDO </th>
            <td>
            	<select name="estado_actual" id="estado_actual" class="form-control">
                	<option value="">ELIJA...</option>
                    <option>ENTREGADO</option>
                    <option>ENTREGA PARCIAL</option>
                    <option>RECHAZADO</option>
                    <option>RECHAZADO POR SERVICIO</option>
					<?php echo $dato_pedido["ESTADO_PEDIDO"]?>
                </select>
            </td>            
		</tr>
               
        <tr class="datos" style="border:1px solid #000;">
        	<th colspan="2">
          			<p>OBSERVACIONES </p>
            </th>
            <th colspan="4"><textarea class="form-control" name="observaciones" id="observaciones" style="width:90%"><?php echo $dato_pedido["OBSERVACIONES_POSTERIORES"]?></textarea>
           
            </th>
       </tr>

	<?php 
		$total=$dato_pedido["TOTAL_PEDIDO"];
	}
	?>
</table>
<br />

<table style='width:99%;border:1px solid transparent; margin:auto auto;' rules='all' class="table table-striped">
	<tr>
		<th colspan='4' class="principal" style="border-radius:0px;">DETALLES ENTREGA PEDIDO</th>
	</tr>
    <?php
	 $consultaEntrega=mysql_query(" 
  SELECT DISTINCT
d.RANGO_ENTREGA,d.OBSERVACIONES
FROM 3m_pedido AS p
INNER JOIN 3m_cliente AS cli ON cli.ID_CLIENTE=p.ID_CLIENTE_FK
INNER JOIN 3m_detalle_pedido AS d ON d.ID_PEDIDO_FK_2 = p.ID_PEDIDO
WHERE p.ID_PEDIDO=  '$ID_PEDIDO' ",$conex);
 
	while($dato_entrega=mysql_fetch_array($consultaEntrega))
	{ ?>
		 <tr class="datos" style="border:1px solid #000;">
       		<th>HORARIO ENTREGA</th>
            <td><?php echo $dato_entrega["RANGO_ENTREGA"]?></td>
          </tr>
          <tr class="datos" style="border:1px solid #000;">  
            <th>OBSERVACIONES / RECOMENDACIONES</th>
            <td><?php echo $dato_entrega["OBSERVACIONES"]?></td>
            
       </tr> 
		<?php }
	 ?>
    </table>

<br />
<table style='width:99%;border:1px solid transparent; margin:auto auto;' rules='all' class="table table-striped">
	<tr>
		<th colspan='4' class="principal" style="border-radius:0px;">DETALLES PEDIDO</th>
	</tr>
	<tr>
		<th class="TITULO">NOMBRE PRODUCTO</th>
		<th class="TITULO">CANTIDAD</th>
		<th class="TITULO">VALOR</th>
        <th class="TITULO" id="esta_titu">ENTREGA PRODUCTO</th>
	</tr>
    <?php
	$ID_PEDIDO;
    $consulta=mysql_query("SELECT * FROM 3m_detalle_pedido AS dp
	INNER JOIN 3m_cliente AS cli ON cli.ID_CLIENTE=dp.ID_CLIENTE_FK_2
	INNER JOIN 3m_categoria AS c ON dp.ID_CATEGORIA_FK_2=c.STOCK
	INNER JOIN 3m_usuario AS u ON dp.ID_USUARIO_FK_2=u.ID_USUARIO
    WHERE ID_PEDIDO_FK_2='$ID_PEDIDO' ORDER BY dp.ID_PEDIDO ASC",$conex);
	$CONT=0;
	while($dato=mysql_fetch_array($consulta))
	{
		$CONT=$CONT+1;
	?>
		<tr class="datos">
            <td><?php echo $dato["DESCRIPCION"]?></td>
            <td><?php echo $dato["CANTIDAD_PRODUCTO"]?></td>
            <td><?php echo $dato["TOTAL_PEDIDO"]?></td>	
            <td id="<?php echo "esta".$CONT ?>">
				<select name="<?php echo "estado_producto".$CONT ?>" id="estado_producto" class="form-control">
                	<option><?php echo $dato["ENTREGADO_PRODUCTO"]?></option>
                    <option>SI</option>
                    <option>NO</option>
                </select>
            </td>		
		</tr>
	<?php 
	}
	?>
	
    
    <input type="hidden" value="<?php echo $CONT ?>" name="cantidad" id="cantidad" />
    <tr >
    	
		<?php
        if($total!='')
		{
			?>
            <th colspan="4" class="TITULO">VALOR TOTAL PEDIDO:
				<?php echo $total ?>
			</th>
			<?php
		}
		else
		{
		?>
			<th colspan="4" class="TITULO">VALOR TOTAL PEDIDO: 0</th>
		<?php
		}
		?>
    </tr>
</table>
<br />
<br />
    <center>
   <input name="guardar" type="submit" id="guardar" value="ACTUALIZAR" class="btn btn-primary" style="display:none;width:100%;"/>
    </center>
</form>
</body>
</html>