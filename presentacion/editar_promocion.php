<?php
include("../logica/session.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="css/tablas.css" rel="stylesheet" />
        <link href="css/bootstrap.css" rel="stylesheet" />
        <title>Detalle pedido</title>
        <link rel="shortcut icon" href="imagenes/3m.png" />
        <script src="js/jquery.js"></script>

    </head>
    <?PHP
    include('../datos/conex.php');

    $ID_CATEGORIA = base64_decode($x);
    $area = $_REQUEST["area"];



    $CONSULTA_CATEGORIA = mysql_query("SELECT ID_CATEGORIA,CATEGORIA, SUBCATEGORIA, STOCK,DESCRIPCION,PRECIO_UNIDAD,ESTADO
 FROM 3m_categoria
 WHERE ID_CATEGORIA = '$ID_CATEGORIA';", $conex);
    ?>
    <body>
        <form action="../logica/cambio_estado_pedido.php" method="post">

            <table style="width:99%; margin:auto auto;" rules="none" class="table table-striped" >
                <tr>
                    <th colspan='6' class="principal">INFORMACI&Oacute;N PROMOCI&Oacute;N</th>
                </tr>
                <?PHP
                while ($dato = mysql_fetch_array($CONSULTA_CATEGORIA)) {
                    ?>

                    <tr>
                        <th>CATEGORIA  </th>
                        <td ><?php echo $dato["CATEGORIA"]; ?></td>
                    </tr>
                    <tr> 
                        <th>SUB CATEGORIA </th>
                        <td><?php echo $dato["SUBCATEGORIA"] ?></td>
                    </tr>
                    <tr> 
                        <th>STOCK</th>
                        <td><?php echo $dato["STOCK"] ?></td>
                    </tr>
                    <tr>
                        <th>DESCRIPCI&Oacute;N</th>
                        <td><?php echo $dato["DESCRIPCION"] ?></td>
                    </tr>
                    <tr>
                        <th>PRECIO</th>
                        <td><input  value="<?php echo $dato["PRECIO_UNIDAD"] ?>" name="precio"  class="form-control"  required="true"/></td>
                    </tr>
                    <tr>  
                        <th>ESTADO</th>
                        <td>
    <?php
    if ($dato["ESTADO"] == 1) {
        $valor = 1;
        $estado = 'Activo';
    } else {
        $valor = 0;
        $estado = 'Inactivo';
    }
    ?>
                            <select class="form-control" name="estado" required="true">
                                <optgroup label="Actual">
                                    <option value="<?php echo $dato["ESTADO"]; ?>"><?php echo $estado; ?></option>
                                </optgroup>
                                <optgroup label="Seleccione">
                                    <?php if ($valor == 1) { ?>
                                        <option value="0">Inactivo</option>
                                    <?php } else { ?>
                                        <option value="1">Activo</option> 
                                    <?php } ?>
                                </optgroup>

                            </select>
                        </td>
                    </tr> 
                    <input name="id_categoria" value="<?php echo $dato["ID_CATEGORIA"]; ?>"  type="hidden"/>
                <?PHP } ?> 
            </table>
            <br /><br />
            <center>
                <input value="<?php echo $area; ?>"  name="area" type="hidden"/>
                <button class="btn btn-group-lg" name="actulizar_categoria">Actualizar</button>
            </center>
            <br />

            <br />

        </form>
    </body>
</html>