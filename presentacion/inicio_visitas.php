<?php  include("../logica/session.php");
if (isset($_SERVER['HTTPS'])) {
    
} else {
    ?>
    <script>
window.onload = window.top.location.href = "https://app-peoplemarketing.com/3M/";
    </script>
    <?php } ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, text/html; charset=utf-8">
<title>Inicio</title>
<link href="../presentacion/css/menu_visitas.css" type="text/css" rel="stylesheet" />
<script src="https://unpkg.com/sweetalert2@7.1.3/dist/sweetalert2.all.js"></script>
<link href="../presentacion/css/tablas.css" rel="stylesheet" />
<script src="../presentacion/js/jquery.js"></script>
<script language=javascript> 
function ventanaSecundaria (URL){ 
   window.open(URL,"ventana1","width=1000,height=500,Top=100,Left=200") 
} 
</script> 
<style>
.form-control[disabled],.form-control[readonly],fieldset[disabled] .form-control
{
	background-color:#eee;
	opacity:1;
	cursor:not-allowed;
}
</style>
</head>
<script language=javascript> 
function ventanaSecundaria (URL){ 
   window.open(URL,"ventana1","width=500,height=850,Top=100,Left=200") 
} 

function ingresar_paso()
{
	var latitud=$('#LAT').val();
	var longitud=$('#lon').val();
	$.ajax(
	{
		url:'../presentacion/ingresar_paso.php',
		data:
		{
			latitud: latitud,
			longitud:longitud
		},
		type: 'post',
		success: function(data)
		{
		swal(
			{title: 'Bienvenido a Farma de Colombia',
			confirmButtonColor: '#045491'});
		}
	})
}
function VER_DATOS()
{
	var NOMBRE=$('#nombre').val(); 
	var USUA=$('#usua').val();
	$.ajax(
	{
		url:'../presentacion/datos_consulta_cliente.php',
		data:
		{
			NOMBRE: NOMBRE,
			USUA: USUA
		},
		type: 'post',
		contentType: "application/x-www-form-urlencoded;charset=utf-8",
		beforeSend: function ()
		{
				$("#tablas").html("Procesando, espere por favor"+'<img src="../presentacion/imagenes/cargando.gif" />');
		},
		success: function(data)
		{
			$('#tablas').html(data);
		}
	})
}
var height= window.innerHeight-10;/*tamaño ventana*/
var ancho=window.innerWidth;

$(document).ready(function()
{


	$('#nombre').on('keyup', function()
	{
		VER_DATOS();
	});
	if(ancho<1001)
	{
		$('#info_new').css('height',height);
		var tama_menu=$("#div_menu").height();
		var div_buscar=$("#buscar").height();
		tama_menu=tama_menu*(-1);
		$('#info_new').css('margin-bottom',tama_menu);
		$("#info_new").animate({marginTop:tama_menu},1);
		
		$('#div_menu li').click(function()
		{
			$("#div_menu").animate({marginLeft:'-120%'},500);
			$('#info_new').toggle(1000);
			$('#tabla').css('display','none');
		});
		$('#menu_bar').click(function()
		{
			$("#div_menu").animate({marginLeft:'0'},500);
			$('#info_new').fadeOut(1000);
			$("#div_menu").css('display','block');
		});
	}
	else
	{
		$('#info_new').css('height',height);
		$('#info_new').css('display','block');
	}
});

function obtener()
{
	var parametros={enableHighAccuracy:true}
	navigator.geolocation.getCurrentPosition(mostrar_posicion,gestion_errores,parametros);
	
}
function mostrar_posicion(posicion)
{
	var ubicacion=document.getElementById("UBICACION");
	
	var latitud=posicion.coords.latitude;
	var longitud=posicion.coords.longitude;
	var exactitud=posicion.coords.accuracy;
	document.getElementById("LAT").value=latitud;
	document.getElementById("lon").value=longitud;
	ingresar_paso()
	
}
function gestion_errores(error)
{
	if(error.code==1)
	{
	swal(
		{title: 'Debes permitir el uso de la geolocalizacion en tu navegador',
  	confirmButtonColor: '#BC3228'});
	 window.onload = window.top.location.href = "https://app-peoplemarketing.com/3M/";
	}
}
</script>

<?php
include('../datos/conex_copia.php');
$_SESSION['ID_CLIENTE']='';
$_SESSION['NOMBRE_CLIENTE']='';
$_SESSION['DISTRIBUIDOR_COMPRA']='';
if(isset($_POST['ingreso']))
{
	?>
	<script>
	$(document).ready(function() {
		if (navigator.geolocation)
		{
				obtener();
		}
		else
		{
		swal(
			{title: 'Por favor, abra la aplicacion desde Google Chrome',
  			confirmButtonColor: '#BC3228'});
                window.onload = window.top.location.href = "https://app-peoplemarketing.com/3M/";
		}
	
	});
	</script>
    <?php
}
if($id_usu!=''&&$id_usu!='0'){
?>
<body>
<?php
if(empty($id_usu)){ ?>


				<span style="margin-top:5%;">
			<center>
			<img src="../presentacion/imagenes/advertencia.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
			<p class="error" style=" width:68.9%; margin:auto auto;">
			 LA SESI&Oacute;N HA CADUCADO, POR FAVOR INICIE SESION.
			</p>
			<br />
			<br />
			<center>
					<a href="https://app-peoplemarketing.com/3M/"  class="btn_continuar">
						<img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" />
					</a>
				</center>
			</center>
			</span>
				
			<?php }else{
                            
                                  if($id_usu==2){?>
                              
                                <a href="../presentacion/email/mail_pedido_hotmail.php">Enviar correo de prueba hotmail</a>    
                          <?php  }
                            ?>

<input type="button" name="del_uno" value="encuesta" id="input_1" style="display:none;"> 
<input type="button" name="del_uno" value="exhibicion" id="input_2" style="display:none;" > 
<input type="button" name="del_uno" value="venta" id="input_3" style="display:none;"> 

<table align="right" style="width:100%;">

    	<tr> 	
          <th colspan="6">Bienvenid@  <?php echo $usua?></th> 
          </tr>
          <tr>
            <input name="usua" style="display:none;" type="text" id="usua" value="<?php echo $usua; ?>" readonly/>      
            <td><a href="../logica/cerrar_sesion2.php">
        <img src="../presentacion/imagenes/atras.png" width="56" height="57" id="cambiar" title="ATRAS" >
        </a></td>
        <!--<td><a href="../presentacion/kpi_visitador.php">
        <img src="../presentacion/imagenes/kpi.png" width="56" height="57" id="cambiar" title="KPI'S" >
         <center> <font style="font-size:10px;" >KPI'S</font></center>
        </a></td>-->
        <td><a href="../presentacion/consultar_gestiones_visitadores.php">
        <img src="../presentacion/imagenes/gestiones.png" width="56" height="57" id="cambiar" title="GESTIONES" >
         <center> <font style="font-size:10px;" >Gestiones</font></center>
        </a></td>
         <!--<td><a href="../presentacion/consulta_pedidos_usuario.php?">
        <img src="../presentacion/imagenes/agn.png" width="56" height="57" id="cambiar" title="CONSULTAR PEDIDOS" >
         <center> <font style="font-size:10px;" >Pedidos</font></center>
        </a></td>-->
          <td><a href="../presentacion/consulta_proximas_vista.php?">
        <img src="../presentacion/imagenes/calendar.png" width="56" height="57" id="cambiar" title="CONSULTAR PR&Oacute;XIMA VISITA" >
         <center> <font style="font-size:10px;" >Pr&oacute;xima <br />Visita</font></center>
        </a></td>
        <td><a href="../presentacion/consulta_productos_visita.php?">
        <img src="../presentacion/imagenes/productos.png" width="56" height="57" id="cambiar" title="CONSULTAR PEDIDOS" >
         <center> <font style="font-size:10px;" >Producto</font></center>
        </a></td>
         <td><a href="../presentacion/reporte_clientes_nuevos.php">
        <img src="../presentacion/imagenes/excel.png" width="48" height="51" id="cambiar" title="REPORTE EXCEL" ><br />
       <center> <font style="font-size:10px;" >Clientes <br />Nuevos</font></center>
        </a></td>
        </tr>
    </table>
    <br />
    <br />
    <br />
<center><img src="../presentacion/imagenes/logo2.png" width="34%" height="17%" />
<h3 style="color:#045491;"><strong>VISITA ODONT&Oacute;LOGO</strong></h3>
</center>
	<div class="form-group" style="display:none">
        <input id="LAT" name="LAT" type="text" class="form-control" autocomplete="off" readonly/>
        <input id="lon" name="lon" type="text" class="form-control" autocomplete="off" readonly/>
    </div>
    <br />
    <br />
    <fieldset style="border:1px solid #045491; border-radius:10px;">
    <legend >Nombre o Numero de identicacion<span class="asterisco">*</span></legend>
    <div class="form-group">
        <center>
            <input placeholder="Nombre o numero de identificacion" type="text" name="nombre" id="nombre" class="form-control" autocomplete="off" autofocus style="line-height:2%;
	width:90.5%;"/>
        </center>
    </div>
    </fieldset>
    <br />
    <table style="width:99%; margin:auto auto;" rules="none" id="tablas">
    </table>
    <br />    
    <div class="div_menu" id="div_menu" style="display:">
            <ul>
                <li id="link1">  <a href="../presentacion/form_cliente_nuevo.php" style="width:99%;"><span class="icon-user-plus"></span>&nbsp;&nbsp;&nbsp;NUEVO ODONTOLOGO</a>
                </li>
            </ul>
            <ul>
                <li id="link1"> <a href="../presentacion/form_cliente_no_efectivo.php" style="width:99%;"><span class="icon-user-plus"></span>&nbsp;&nbsp;&nbsp;NUEVO ODONTOLOGO NO EFECTIVO</a>
                </li>
            </ul>
    </div>
<?php } ?>
</body>
</html>
<?php } else{ ?>
<script type="text/javascript">
		window.onload = window.top.location.href = "../logica/cerrar_sesion2.php";
	</script>
<?php } ?>