<?php
include("../logica/session.php");
header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="css/tablas.css" rel="stylesheet" />
        <link href="css/bootstrap.css" rel="stylesheet" />
        <title>Detalle pedido</title>
        <link rel="shortcut icon" href="imagenes/3m.png" />
        <script src="js/jquery.js"></script>
 </head>
    <?PHP
    include('../datos/conex.php');
    ?>
    <body>
        <form action="../logica/cambio_estado_pedido.php" method="post">

            <table style="width:99%; margin:auto auto;" rules="none" class="table table-striped" >
                <tr>
                    <th colspan='6' class="principal">NUEVA PROMOCI&Oacute;N</th>
                </tr>
                <tr>
                    <th>CATEGORIA     </th>
                    <td >PROMOCI&Oacute;N</td>
                </tr>
                <tr> 
                    <th>SUB CATEGORIA </th>
                    <td>
                        <select class="form-control" name="sub_categoria" required="true">
                            <option value="Dental">Dental</option>
                            <option value="Ortodoncia">Ortodoncia</option> 
                        </select>
                    </td>
                </tr>
                <tr> 
                    <th>STOCK</th>
                    <td><input name="stock" type="text"  required="required" class="form-control"/></td>
                </tr>
                <tr>
                    <th>DESCRIPCI&Oacute;N</th>
                    <td><input name="descripcion" type="text"  required="required" class="form-control"/></td>
                </tr>
                <tr>
                    <th>PRECIO</th>
                    <td><input name="precio" type="number"  required="required" class="form-control" min="0"/></td>
                </tr>
                <tr>  
                    <th>ESTADO</th>
                    <td>

                        <select class="form-control" name="estado" required="true">
                            <option value="0">Inactivo</option>
                            <option value="1">Activo</option> 
                        </select>
                    </td>
                </tr> 

            </table>
            <br /><br />
            <center>

                <button class="btn btn-group-lg" name="crear_promocion">Crear Promoci&oacute;n</button>
            </center>
            <br />

            <br />

        </form>
    </body>
</html>