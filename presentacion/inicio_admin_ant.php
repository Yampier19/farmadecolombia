<?php
	include("../logica/session.php");
        header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

<title>Coordinador</title>
<link href="../presentacion/css/menu_admin.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="../presentacion/fonts.css" />
<script src="../presentacion/js/jquery.js"></script>

<script>

var height= window.innerHeight-10;

var ancho=window.innerWidth;
//alert(ancho);
/*alert(height);*/
$(document).ready(function()
{
	$('#div_menu').css('display','block');
	$('#menu_bar').click(function()
	{
		//alert('ffg');
		$('#div_menu').css('display','block');
	});
	if(ancho<1001)
	{
		
		$('#info').css('height',height);
		var tama_menu=$("#div_menu").height();
		tama_menu=tama_menu*(-1);
		$('#div_menu li').click(function()
		{
			$("#div_menu").animate({marginLeft:'-120%'},500);
			$("#div_menu").css('display','none');
			$('#info').toggle(1000);
		});
		$('#menu_bar a').click(function()
		{
			$("#div_menu").animate({marginLeft:'0'},500);
			$('#info').fadeOut(100);
		});
	}
	else
	{
		$('#info').css('height',height);
		$('#info').css('display','block');
	}
});

function ventanaSecundaria (URL){ 
   window.open(URL,"ventana1","width=1000,height=500,Top=100,Left=200") 
} 
</script>
</head>
<body>
<center><img src="../presentacion/imagenes/3m.png" width="24%" height="17%" />
<h3 style="color:#911108;"><strong>PROGRAMA DE EXPERTOS</strong></h3>
</center>

	<div class="menu_bar" id="menu_bar">
        <a class="bt-menu" style="text-align:left; margin-left:15px;" href="../presentacion/blanco.php" target="info"><span class="icon-list2"></span>Menu</a>
    </div>
<div class="div_menu" id="div_menu" style="width:90%; display:">
        	<ul>
               
				 <li id="link1">
                	<a href="../presentacion/ver_inicio_sesion.php" target="info" style="width:100%;"><span class="icon-stats-bars"></span> INICIO SESION</a>
                </li>
                <li id="link1">
                	<a href="../presentacion/ver_gestion_usuario.php" target="info" style="width:100%;"><span class="icon-stats-bars"></span> GESTION</a>
                </li>
                <li id="link1">
                	<a href="../presentacion/consulta_eliminar_gestion.php" target="info" style="width:100%;"><span class="icon-box-remove"></span> ELIMINAR GESTION</a>
                </li>
                <li id="link1">
                	<a href="../presentacion/consulta_pedidos.php" target="info" style="width:100%;"><span class="icon-credit-card"></span>  PEDIDOS Y TRASFERENCIAS</a>
                </li>
				<li id="link1">
                	<a href="../presentacion/consulta_pedido_entregado.php" target="info" style="width:100%;"><span class="icon-truck"></span>  PEDIDOS ENTREGADOS</a>
                </li>
				<li id="link1">
                	<a href="../presentacion/consulta_dentales_pedidos.php" target="info" style="width:100%;"><span class="icon-truck"></span>  PEDIDOS DISTRIBUIDORES</a>
                </li>
                <li id="link1">
                	<a href="../presentacion/consulta_eliminar_pedido.php" target="info" style="width:100%;"><span class="icon-box-remove"></span>  ELIMINAR PEDIDO</a>
                </li>
                 <li id="link1">
                	<a href="../presentacion/consultar_nuevos_clientes.php" target="info" style="width:100%;"><span class="icon-users"></span> CLIENTES</a>
                </li>
                <li id="link1">
                	<a href="../presentacion/consultar_promociones.php" target="info" style="width:100%;"><span class="icon-stats-bars2"></span>  PROMOCIONES</a>
                </li>
                <li id="link1">
                	<a href="../presentacion/consultar_productos.php" target="info" style="width:100%;"><span class="icon-stats-bars2"></span> PRODUCTOS</a>
                </li>
                   <li id="link1">
                	<a href="../presentacion/asignar_ruta.php" target="info" style="width:100%;"><span class="icon-stats-bars2"></span> RUTERO</a>
                </li>
				<li id="link1">
                	<a href="../presentacion/novedades.php" target="info" style="width:100%;"><span class="icon-box-remove"></span> NOVEDADES</a>
                </li>
                <li id="link1">
                    <a href="../presentacion/form_kpi.php" target="info" style="width:100%;"><span class="icon-box-remove"></span> KPI'S</a>
                </li>
                <li id="link3">
                	<a href="../informes/index.php" target="info" style="width:100%;"><span class="icon-stats-dots"></span>  REPORTE</a>
                </li>
				<li id="link3">
                	<a href="../presentacion/reportes_csv.php" target="info" style="width:100%;"><span class="icon-stats-dots"></span>  CSV</a>
                </li>
           
                <li id="link2" style="border-right:2px solid #911108;">
                
                	<a href="../logica/cerrar_sesion2.php" style="width:100%;" ><span class="icon-switch"></span>  CERRAR SESION</a>
                </li>
            </ul>
    </div>
    <iframe style="width:100%;border:1px solid transparent; display:none; height:500px;" name="info" id="info" scrolling="auto"></iframe>
</body>
</html>