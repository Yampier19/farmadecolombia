<?php
	include("../logica/session.php");
	error_reporting(0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/tablas_encuestas.css" rel="stylesheet" />
<link href="css/bootstrap.css" rel="stylesheet" />
<script src="https://unpkg.com/sweetalert2@7.1.3/dist/sweetalert2.all.js"></script>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<script src="js/jquery.js"></script>
<script src="js/encuesta_consumo.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/alertify.js/0.3.11/alertify.min.js"></script>

         <link href="https://cdn.jsdelivr.net/alertify.js/0.3.11/themes/alertify.core.css" rel="stylesheet" type="text/css"/>
         <link href="https://cdn.jsdelivr.net/alertify.js/0.3.11/themes/alertify.default.css" rel="stylesheet" type="text/css"/>
         <script>
         function obtener()
{
	var parametros={enableHighAccuracy:true}
	navigator.geolocation.watchPosition(mostrar_posicion,gestion_errores,parametros);
	
}
function mostrar_posicion(posicion)
{
	var ubicacion=document.getElementById("UBICACION");
	var latitud=posicion.coords.latitude;
	var longitud=posicion.coords.longitude;
	var exactitud=posicion.coords.accuracy;
	document.getElementById("LAT").value=latitud;
	document.getElementById("lon").value=longitud;	
}
function gestion_errores(error)
{
}
         </script>
		 
		 	<script>
function mayus(e) {
    e.value = e.value.toUpperCase();
}

</script>
<script>
$(document).ready(function(){

$('#categoria_competencia_').change(function()
	{
	
	 var categoria_competencia_ = $("#categoria_competencia_").val();
	if(categoria_competencia_ != '' ){
	
$("#Descripcion_competencia_").removeAttr('disabled', 'disabled');
}else{
$("#Descripcion_competencia_").attr('disabled', 'disabled');
}

});

$('#Descripcion_competencia_').change(function()
	{
	var Descripcion_competencia_ = $("#Descripcion_competencia_").val();
	if(Descripcion_competencia_ != '' ){
	
	$('#guardar_competencia').css("display","block");
	}else{
	$('#guardar_competencia').css("display","none");
	}
	
	
});

});
</script>
</head>

<?php
	//include('../datos/conex.php');
	$consulta_cliente=mysqli_query($conex,"SELECT * FROM 3m_cliente WHERE ID_CLIENTE=$ID_CLIENTE");
	$nreg_clien=mysqli_num_rows($consulta_cliente);
	if($nreg_clien>0)
	{
		while($dato_cliente=mysqli_fetch_array($consulta_cliente))
		{
			$IDENTIFICACION_CLIENTE=$dato_cliente['IDENTIFICACION_CLIENTE'];
			$CELULAR_CLIENTE=$dato_cliente['CELULAR_CLIENTE'];
			$TELEFONO_CLIENTE=$dato_cliente['TELEFONO_CLIENTE'];
		}
		if($IDENTIFICACION_CLIENTE==0)
		{
			$IDENTIFICACION_CLIENTE='';
			$CELULAR_CLIENTE='';
			$TELEFONO_CLIENTE='';
		}
	}
	else if($nreg_clien<=0)
	{
		$IDENTIFICACION_CLIENTE='';
		$CELULAR_CLIENTE='';
		$TELEFONO_CLIENTE='';
	}
	
	$consulta=mysqli_query($conex,"SELECT * FROM 3m_encuesta WHERE ID_CLIENTE_FK_5=$ID_CLIENTE");
	$nreg=mysqli_num_rows($consulta);
	if($nreg>0)
	{
		while($dato=mysqli_fetch_array($consulta))
		{
			$id_encuesta=$dato['ID_ENCUESTA'];
			$pacientes=$dato['CANTIDAD_PACIENTES'];
            $ingresos = $dato['INGRESOS'];
            $casos_nuevos_semana = $dato["CASOS_NUEVOS_SEMANA"];
			$especialida=$dato['ESPECIALIDAD_ODONTOLOGO'];
			$clasificacion=$dato['CLASIFICACION_ODONTOLOGO'];
			$cantida_categoria=$dato['CANTIDAD_CATEGORIA'];
			$distribuidor=$dato['DISTRIBUIDOR_COMPRA'];
			$cumpleanios = $dato['FECHA_CUMPLEANIOS'];
			$dia_visita = $dato['DIA_VISITA'];
			$hora_ini_visita = $dato['HORA_INI_VISITA'];
			$hora_fin_visita = $dato['HORA_FIN_VISITA'];
			
		}
	}
	else if($nreg<=0)
	{
		$id_encuesta='';
		$pacientes='';
		$especialida='Elija...';
		$clasificacion='Elija...';
		$cantida_categoria='';
		$distribuidor='Elija...';
	}
	
	$consulta_RD=mysqli_query($conex,"SELECT DISTINCT(SUBCATEGORIA) FROM 3m_categoria WHERE CATEGORIA='Restauraciones directas'");
	$nreg_a=mysqli_num_rows($consulta_RD);
	
	$consulta_RI=mysqli_query($conex,"SELECT DISTINCT(SUBCATEGORIA) FROM 3m_categoria WHERE CATEGORIA='Restauraciones indirectas'");
	$nreg_b=mysqli_num_rows($consulta_RI);
	
	$consulta_P=mysqli_query($conex,"SELECT DISTINCT(SUBCATEGORIA) FROM 3m_categoria WHERE CATEGORIA='Prevencion'");
	$nreg_c=mysqli_num_rows($consulta_P);
	
        
        $select_consumo = mysqli_query($conex,"SELECT COUNT(*)
                FROM 3m_productos_consumo_historial AS h
                INNER JOIN 3m_detalle_producto_consumo AS p ON p.ID_PRODUCTOS_CONSUMO_FK=h.ID_PRODUCTOS_CONSUMO 
                WHERE ID_CLIENTE_FK=$ID_CLIENTE");
        
	  $datos_consumo=mysqli_fetch_array($select_consumo);
        $numero_productos_usados = $datos_consumo["COUNT(*)"];

if($id_usu!='' && $id_usu!='0'){
?>
<style>
.col-md-3{
	margin-top:5px;
	}
.error
{
	font-size: 130%;
	font-weight: bold;
	color: #fb8305;
	text-transform:uppercase;
	background-color:transparent;
	text-align: center;
	padding:10px;
}
</style>
<body onload="obtener()">
<?php

if(empty($id_usu)){ ?>
				<span style="margin-top:5%;">
			<center>
			<img src="../presentacion/imagenes/advertencia.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
			<p class="error" style=" width:68.9%; margin:auto auto;">
			 LA SESI&Oacute;N HA CADUCADO, POR FAVOR INICIE SESION.
			</p>
			<br />
			<br />
			
			</center>
			</span>
				
			<?php }else{?>
	<div class="form-group" style='display:none;' >
  
        <input name="id_cliente" id="id_cliente" type="text" readonly="readonly"value="<?php echo $ID_CLIENTE ?>"/>
        <input name="distribuidor" id="distribuidor" type="text" readonly="readonly" class="form-control1" value="<?php echo $DISTRIBUIDOR ?>"/>

    </div>
    <header>
        <div class="container" id="div_header">
        	<center><h1>ENCUESTA</h1></center>
        </div>
    </header>
    <input name="id_US" id="id_US" class="form-control" type="hidden" readonly="readonly" value="<?php echo $id_usu ?>"/>
    <input name="id_encuesta" id="id_encuesta" class="form-control" type="hidden" placeholder="Cantidad pacientes" readonly="readonly" value="<?php echo $id_encuesta ?>"/>
	
    <input name="tipo_odontologo" id="tipo_odontologo" class="form-control" value="<?php echo $TIPO_ODONTOLOGO ?>" type="hidden" />
	<input name="fecha_copia" id="fecha_copia" class="form-control" value=""  type="hidden" />
	<input name="observacion_copia" id="observacion_copia" class="form-control" value=""   type="hidden" />
    <div class="form-group">
    
        <div class="col-md-3">
            <label for="cliente">N&uacute;mero de Identificaci&oacute;n</label><span class="asterisco">*</span>
         </div>
         <div class="col-md-3"> 
            <input name="identificacion" id="identificacion" class="form-control" type="text" placeholder="identificacion" maxlength="20" value="<?php echo $IDENTIFICACION_CLIENTE ?>"/>
          </div>
            <div class="col-md-3"> 
            <label for="cliente">N&uacute;mero de Telefono Fijo</label><span class="asterisco">*</span>
           </div>
           
            <div class="col-md-3"> 
            <input name="numero_tel" id="numero_tel" class="form-control" type="text" placeholder="Numero de telefono"   value="<?php echo $TELEFONO_CLIENTE ?>"/>
            </div> 
        </div>
        
    <div class="form-group">
         	<div class="col-md-3">
            <label for="cliente">N&uacute;mero de Celular</label><span class="asterisco">*</span>
          	 </div>
            <div class="col-md-3">
        <input name="numero_cel" id="numero_cel" class="form-control" placeholder="Numero de celular"  maxlength="20" value="<?php echo $CELULAR_CLIENTE ?>" type="text" />
    		</div>  
          	
      </div>
      
    <!--<div class="form-group">
        <div class="col-md-3">
            <label for="cliente">Dia Visita</label>
            </div>
             <div class="col-md-3">       
          <select name="dia_visita" id="dia_visita" class="form-control">
                <option></option>
                <option>Lunes</option>
                <option>Martes</option>
                <option>Miercoles</option>
                <option>Jueves</option>
                <option>Viernes</option>            
                <option>Sabado</option>
                <option>Domingo</option>
            </select>
        </div>
         <div class="col-md-3">
          <label for="cliente">Hora Inicio De Visita</label>
         </div>
          <div class="col-md-3">
            <input name="hora_ini_visita" id="hora_ini_visita" class="form-control" type="time"  value="<?php // echo $hora_ini_visita ?>"/>
        </div>
    </div>-->
    
    <div class="form-group">
         <!--<div class="col-md-3">
          <label for="cliente">Hora Fin De Visita</label>
          </div>
          <div class="col-md-3">
            <input name="hora_fin_visita" id="hora_fin_visita" class="form-control" type="time"  value="<?php // echo $hora_fin_visita ?>"/>
          </div>-->
          <div class="col-md-3">
              <?php
                if($TIPO_ODONTOLOGO==1){ ?>
                   <label for="cliente">Paciente que atienden por semanas</label><span class="asterisco">*</span>  
              <?php  }
              else if($TIPO_ODONTOLOGO==2){ ?>
                    <label for="cliente">N&uacute;mero de cementaciones mensuales</label><span class="asterisco">*</span>  
            <?php   }
              ?>
           </div>
           <div class="col-md-3">
              <input name="cantidad_pacientes" id="cantidad_pacientes" class="form-control" type="number" max="999" min="0"  value="<?php echo $pacientes ?>" autocomplete="off" onChange="pagoOnChange(this)"/>
           </div>
        
        <!--<?php // if($TIPO_ODONTOLOGO==2){ ?>
        <div class="col-md-3">
            <label for="cliente">Casos nuevos por semana</label><span class="asterisco">*</span>  
            
           </div>
           <div class="col-md-3">
              <input name="casos_nuevos" id="casos_nuevos" class="form-control" type="number" max="999" min="0"  value="<?php // echo $casos_nuevos_semana; ?>" autocomplete="off"/>
           </div>
                        <?php // } ?>-->
    </div>
    
    <div class="form-group">
         <div class="col-md-3">
            <label for="cliente">Especialidad Odontologo</label><span class="asterisco">*</span>
            </div>
             <div class="col-md-3">
            <select name="especialidad_odontologo" id="especialidad_odontologo" class="form-control" >
                <option><?PHP echo $especialida ?></option>
                <?php
                $consulta_especialidad=mysqli_query($conex,"SELECT * FROM 3m_especialidad_odontologo WHERE NOMBRE_ESPECIALIDAD!='' AND NOMBRE_ESPECIALIDAD!='$especialida' AND ID_TIPO=".$TIPO_ODONTOLOGO." ORDER BY NOMBRE_ESPECIALIDAD ASC");
                echo mysqli_error($conex);
                while($datos=mysqli_fetch_array($consulta_especialidad))
                {
                ?>
                    <option><?php echo $datos['NOMBRE_ESPECIALIDAD'] ?></option>
                <?php
                }
                ?>
            </select>
        </div>
		
    </div>
	
	<div class="form-group">
		 <div class="col-md-3">
        <label for="cliente">Definici&oacute;n de Ingresos</label><span class="asterisco">*</span>
        </div>
         <div class="col-md-3">
            <?php 
                if($ingresos=="A"){
                    $ingresos_name = "CUENTAS PRINCIPALES (40% DEL TOTAL DE LAS VENTAS)";
                }else if($ingresos=="B"){
                    $ingresos_name = "CUENTAS QUE REPRESENTAN (30% DEL TOTAL DE LAS VENTAS)";
                }else if($ingresos=="C"){
                    $ingresos_name = "CUENTAS QUE REPRESENTAN (20% DEL TOTAL DE LAS VENTAS)";
                }else if($ingresos=="D"){
                    $ingresos_name = "CUENTAS QUE REPRESENTAN (10% DEL TOTAL DE LAS VENTAS)";
                }
            ?>
        <select class="form-control" name="definicion_ingresos" id="definicion_ingresos" onChange="pagoOnChange(this)">
            <option selected="selected" value="<?php echo $ingresos; ?>"><?php echo $ingresos_name; ?></option>
            <option value="A">CUENTAS PRINCIPALES (40% DEL TOTAL DE LAS VENTAS)</option>
            <option value="B">CUENTAS QUE REPRESENTAN (30% DEL TOTAL DE LAS VENTAS)</option>
            <option value="C">CUENTAS QUE REPRESENTAN (20% DEL TOTAL DE LAS VENTAS)</option>
            <option value="D">CUENTAS QUE REPRESENTAN (10% DEL TOTAL DE LAS VENTAS)</option>
        </select>
        <script type="text/javascript">
        function pagoOnChange(sel) {

            var input = $("#cantidad_pacientes").val();
            var select = $('#definicion_ingresos').val();
            var dent_ortodoncia = $('#tipo_odontologo').val();
            
        if (dent_ortodoncia =="1"){

            //dental
            if (input>="26"){
                if(select=="A" || select=="B"){
                    divC = document.getElementById("variable");
                    divC.style.display = "none";

                    divT = document.getElementById("leal");
                    divT.style.display = "block";

                    divT = document.getElementById("defender");
                    divT.style.display = "none";

                    divT = document.getElementById("desarrollar");
                    divT.style.display = "none";

                    divT = document.getElementById("transicion");
                    divT.style.display = "none";
                }
                else if (select=="C" || select=="D") {
                    divC = document.getElementById("variable");
                    divC.style.display = "none";

                    divT = document.getElementById("leal");
                    divT.style.display = "none";

                    divT = document.getElementById("defender");
                    divT.style.display = "none";

                    divT = document.getElementById("desarrollar");
                    divT.style.display = "block";

                    divT = document.getElementById("transicion");
                    divT.style.display = "none";
                }
            }else if (input>="0" && input<="25" ){
                if (select=="A" || select=="B") {
                    divC = document.getElementById("variable");
                    divC.style.display = "none";

                    divT = document.getElementById("leal");
                    divT.style.display = "none";

                    divT = document.getElementById("defender");
                    divT.style.display = "block";

                    divT = document.getElementById("desarrollar");
                    divT.style.display = "none";

                    divT = document.getElementById("transicion");
                    divT.style.display = "none";
                }
                else if (select=="C" || select=="D") {
                    divC = document.getElementById("variable");
                    divC.style.display = "none";

                    divT = document.getElementById("leal");
                    divT.style.display = "none";

                    divT = document.getElementById("defender");
                    divT.style.display = "none";

                    divT = document.getElementById("desarrollar");
                    divT.style.display = "none";

                    divT = document.getElementById("transicion");
                    divT.style.display = "block";
                }
            }
        }else if(dent_ortodoncia =="2"){
            //ortodoncia
            if (input>="16"){
                if(select=="A" || select=="B"){
                    divC = document.getElementById("variable");
                    divC.style.display = "none";

                    divT = document.getElementById("leal");
                    divT.style.display = "block";

                    divT = document.getElementById("defender");
                    divT.style.display = "none";

                    divT = document.getElementById("desarrollar");
                    divT.style.display = "none";

                    divT = document.getElementById("transicion");
                    divT.style.display = "none";
                }
                else if (select=="C" || select=="D") {
                    divC = document.getElementById("variable");
                    divC.style.display = "none";

                    divT = document.getElementById("leal");
                    divT.style.display = "none";

                    divT = document.getElementById("defender");
                    divT.style.display = "none";

                    divT = document.getElementById("desarrollar");
                    divT.style.display = "block";

                    divT = document.getElementById("transicion");
                    divT.style.display = "none";
                }
            }else if (input>="0" && input<="15" ){
                if (select=="A" || select=="B") {
                    divC = document.getElementById("variable");
                    divC.style.display = "none";

                    divT = document.getElementById("leal");
                    divT.style.display = "none";

                    divT = document.getElementById("defender");
                    divT.style.display = "block";

                    divT = document.getElementById("desarrollar");
                    divT.style.display = "none";

                    divT = document.getElementById("transicion");
                    divT.style.display = "none";
                }
                else if (select=="C" || select=="D") {
                    divC = document.getElementById("variable");
                    divC.style.display = "none";

                    divT = document.getElementById("leal");
                    divT.style.display = "none";

                    divT = document.getElementById("defender");
                    divT.style.display = "none";

                    divT = document.getElementById("desarrollar");
                    divT.style.display = "none";

                    divT = document.getElementById("transicion");
                    divT.style.display = "block";
                }
            }
        }
    }
    </script>
    </div>
    <div class="col-md-3">
        <label for="cliente">Clasificacion Odontologo</label><span class="asterisco">*</span>
        </div>
    <div class="col-md-3" style="display: block;" id="variable">
        <input name="clasificacion_odontologo" id="clasificacion_odontologo" class="form-control" type="text" readonly="readonly" value="<?php if (empty($clasificacion)==false){ echo $clasificacion;} ?>"/>
        <?php 
        if ($clasificacion=="CLIENTE LEAL"){ ?>
            <style type="text/css">
                #clasificacion_odontologo{ background-color: #8FFF7E; }
            </style>
        <?php } if ($clasificacion=="CLIENTE A DEFENDER"){ ?>
            <style type="text/css">
                #clasificacion_odontologo{ background-color: #FCFF59; }
            </style>
        <?php } if ($clasificacion=="CLIENTE A DESARROLLAR"){ ?>
            <style type="text/css">
                #clasificacion_odontologo{ background-color: #8FB8FE; }
            </style>
        <?php } if ($clasificacion=="CLIENTE EN TRANSICION"){ ?>
            <style type="text/css">
                #clasificacion_odontologo{ background-color: #E2E4EB; }
            </style>
        <?php }?>
    </div>
    <div class="col-md-3" style="display: none;" id="leal">
        <input name="clasificacion_odontologo" class="form-control" type="text" readonly="readonly" value="CLIENTE LEAL" style="background-color: #8FFF7E" />
    </div>
    <div class="col-md-3" style="display: none;" id="defender">
        <input name="clasificacion_odontologo" class="form-control" type="text" readonly="readonly" value="CLIENTE A DEFENDER" style="background-color: #FCFF59" />
    </div>
    <div class="col-md-3" style="display: none;" id="desarrollar">
        <input name="clasificacion_odontologo" class="form-control" type="text" readonly="readonly" value="CLIENTE A DESARROLLAR" style="background-color: #8FB8FE" />

    </div>
    <div class="col-md-3" style="display: none;" id="transicion">
        <input name="clasificacion_odontologo" class="form-control" type="text" readonly="readonly" value="CLIENTE EN TRANSICION" style="background-color: #E2E4EB" />
    </div>
	
	
	</div>
    <hr style="width: 100%;">
   <div class="table table-responsive">
		<label for="cliente">Categorias de Farma de Colombia(Ultima encuesta)</label>
        <br />
        <table style="width:100%;border:1px solid #000; margin:auto auto;" rules="all" id="tablas_usado">
        </table>
        <br />
    </div>
        <input  name="productos_usados" id="productos_usados"  type="hidden" value="<?php echo $numero_productos_usados; ?>"/>
         <!-- encuesta odontologos -->
                <?php if($TIPO_ODONTOLOGO==1){ ?>
         <div class="form-group">
            <label for="cliente">Productos de Farma de Colombia</label><span class="asterisco">*</span>
            <br />
         </div>
         <div class="col-md-3">
                  <label for="cliente">Categoria</label><span class="asterisco">*</span>
             </div>
             <div class="col-md-3">
                  <select name="categoria" id="categoria" class="form-control" >
                  </select>
             </div>
         <div class="form-group">
            <div class="col-md-3">
                <label for="cliente">Descripci&oacute;n</label><span class="asterisco">*</span>
             </div>
             <div class="col-md-3">
                <select name="Descripcion" id="Descripcion" class="form-control" >
                </select>
            </div>
     
       </div>
       
        <div class="form-group">
            <input name="guardar_usados" type="submit" id="guardar" value="GUARDAR PRODUCTOS USADOS" class="btn btn-danger" formaction="../presentacion/form_ingresar_producto_consumo.php" formmethod="post" formtarget="productos_usados_lista" style="display:none;width:100%"/>
        </div>
        
        <div class="form-group">
            <div class="form-group">
            <iframe src="../presentacion/form_ingresar_producto_consumo.php" style="width:99%; border:none" name="productos_usados_lista" height="200px" id="productos_usados_lista">
                
            </iframe>
            </div> 
    </div>
         
         
         <div class="form-group">
            <label for="cliente">Producto de la competencia</label><span class="asterisco">*</span>
            <br />
         </div>
         <div class="col-md-3">
                  <label for="cliente">Categoria</label><span class="asterisco">*</span>
             </div>
            
             <div class="col-md-3">
                  <select name="categoria_competencia" id="categoria_competencia_" class="form-control" >
				  <option value="" selected="selected" disabled="disabled">ELIJA...</option>
				  <option value="ANTIBIOTICO">ANTIBI&Oacute;TICO</option>
                  </select>
             </div>
              
         <div class="form-group">
            <div class="col-md-3">
                <label for="cliente">Descripci&oacute;n</label><span class="asterisco">*</span>
             </div>
             <div class="col-md-3">
                <input value="prueba" name="Descripcion_competencia" id="Descripcion_competencia_" class="form-control" style="text-transform: uppercase;"  onKeyUp="mayus(this);" disabled="disabled">
                
            </div>
     
       </div>
       
         <div class="form-group">
            <input name="guardar_competencia" type="submit" id="guardar_competencia" value="GUARDAR COMPETENCIA" class="btn btn-danger" formaction="../presentacion/form_ingresar_competencia.php" formmethod="post" formtarget="productos_competencia_lista" style="display:none"/>
        </div>
          <div class="form-group">
            <div class="form-group">
            <iframe src="../presentacion/form_ingresar_competencia.php" style="width:99%; border:none" name="productos_competencia_lista" height="300px" id="productos_competencia_lista">
                
            </iframe>
            </div> 
         </div> 
                
             <!-- fin encuesta odontologos -->
              <!-- encuesta ortondoncista -->
                <?php } else if($TIPO_ODONTOLOGO==2){ ?>
<div class="form-group">
            <label for="cliente">Productos de Farma de Colombia: (Ortondoncia)</label><span class="asterisco">*</span>
            <br />
         </div>
         <div class="col-md-3">
                  <label for="cliente">Categoria </label><span class="asterisco">*</span>
             </div>
             <div class="col-md-3">
                 <select name="categoria" id="ortodoncia_categoria" class="form-control" style="display: block;" >
                  </select>
             </div>
           <div class="form-group">
            <div class="col-md-3">
                <label for="cliente">Descripci&oacute;n</label><span class="asterisco">*</span>
             </div>
             <div class="col-md-3">
                <select name="Descripcion" id="Descripcion_ortodoncia" class="form-control" >
                </select>
            </div>
           </div>
               <div class="form-group">
            <input name="guardar_usados" type="submit" id="guardar_ortodoncia" value="GUARDAR PRODUCTOS USADOS" class="btn btn-danger" formaction="../presentacion/form_ingresar_producto_consumo.php" formmethod="post" formtarget="productos_usados_lista_ortodoncia" style="display:none"/>
        </div>
        
        <div class="form-group">
            <div class="form-group">
            <iframe src="../presentacion/form_ingresar_producto_consumo.php" style="width:99%; border:none" name="productos_usados_lista_ortodoncia" height="200px" id="productos_usados_lista_ortodoncia">
                
            </iframe>
            </div> 
           <!-- PRODUCTOS DE LA COMPETENCIA ---> 
            <div class="form-group">
            <label for="cliente">Producto de la competencia (Ortodoncia)</label><span class="asterisco">*</span>
            <br />
         </div>
         <div class="col-md-3">
                  <label for="cliente">Categoria</label><span class="asterisco">*</span>
             </div>
            
             <div class="col-md-3">
                  <select name="categoria_competencia" id="categoria_competencia_ortodoncia" class="form-control" >
                  </select>
             </div>
       
              
         <div class="form-group">
            <div class="col-md-3">
                <label for="cliente">Descripci&oacute;n</label><span class="asterisco">*</span>
             </div>
             <div class="col-md-3">
                <select name="Descripcion_competencia" id="Descripcion_competencia_ortodoncia" class="form-control" >
                </select>
            </div>
     
       </div>
       
       <div class="form-group">
            <input name="guardar_competencia" type="submit" id="guardar_competencia_ortodoncia" value="GUARDAR COMPETENCIA" class="btn btn-danger" formaction="../presentacion/form_ingresar_competencia.php" formmethod="post" formtarget="productos_competencia_lista_ortodoncia" style="display:none"/>
        </div>
          <div class="form-group">
            <div class="form-group">
            <iframe src="../presentacion/form_ingresar_competencia.php" style="width:99%; border:none" name="productos_competencia_lista_ortodoncia" height="300px" id="productos_competencia_lista_ortodoncia">
                
            </iframe>
            </div> 
         </div> 
   <?php } ?>
          
         <!--  fin encuesta de ortondoncista-->
        
        <!--  <div class="form-group">     
      <div class="col-md-3">
        <label for="cliente">Distribuidor que compra</label><span class="asterisco">*</span>
        </div>
        <div class="col-md-3">
        <select name="distribuidor" id="distribuidor" class="form-control" >
        	<option value="<?php /* echo $distribuidor ?>"><?php echo $distribuidor ?></option>
			
           
		    <?php 
                       $consulta_dentales= mysqli_query($conex,"SELECT DISTINCT d.NOMBRE_DENTAL, u.USER, u.CIUDAD
				FROM 3m_dentales AS d
				INNER JOIN 3m_usuario AS u ON d.CIUDAD_DENTAL=u.CIUDAD
				GROUP BY ID_DENTAL");
				 while($dato=mysqli_fetch_array($consulta_dentales)) { 
			 ?> 
            
			  <option value="<?php echo $dato['NOMBRE_DENTAL']; ?>"><?php echo $dato['NOMBRE_DENTAL']; ?></option>
                <?php } */
            
            ?>
        </select>
    </div>
    </div>-->
<?php
    }
} ?>