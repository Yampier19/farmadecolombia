    <title>Dos mapas</title>
<style type="text/css">
html,body{height:100%;margin:0;padding:0}
#mapa, #mapa2{height:45%}
</style>
   <link rel="shortcut icon" type="image/ico" href="http://www.digitaleando.com/imag/logo.ico" />
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&amp;language=es"></script>
    <script type="text/javascript">
window.onload = function () {
    var estilo = [{
        stylers: [{
            hue: "#008000"
        }, {
            saturation: -20
        }]
    }, {
        featureType: "road",
        elementType: "geometry",
        stylers: [{
            lightness: 100
        }, {
            visibility: "simplified"
        }]
    }];
    var latLng = new google.maps.LatLng(36.508567, -4.866085);
    var opciones = {
        center: latLng,
        zoom: 15,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'estilo']
        }
    };
    var map = new google.maps.Map(document.getElementById("mapa"), opciones);
    var mapType = new google.maps.StyledMapType(estilo, {
        name: "Estilo Green"
    });
    map.mapTypes.set('estilo', mapType);
    map.setMapTypeId('estilo');
    var marker = new google.maps.Marker({
        position: latLng,
        map: map
    });
 
    google.maps.event.addListener(marker, 'click', function () {
        infowindow.open(map, marker);
    });
 
 
    // Segundo mapa 
    var latLng2 = new google.maps.LatLng(36.488661, -4.986935);
    var stylez = [{
        featureType: "all",
        elementType: "all",
        stylers: [{
            saturation: -100
        }]
    }];
    var myOptions = {
        zoom: 15,
        center: latLng2,
        panControl: false,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.TERRAIN, google.maps.MapTypeId.SATELLITE, google.maps.MapTypeId.HYBRID, 'grayscale'],
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
        },
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL,
            position: google.maps.ControlPosition.TOP_LEFT
        },
        mapTypeId: google.maps.MapTypeId.TERRAIN
    };
    map2 = new google.maps.Map(document.getElementById("mapa2"), myOptions);
    var mapType = new google.maps.StyledMapType(stylez, {
        name: "Escala grises"
    });
    map2.mapTypes.set('grayscale', mapType);
    map2.setMapTypeId('grayscale');
    var marker2 = new google.maps.Marker({
        position: latLng2,
        map: map2
    });
 
    google.maps.event.addListener(marker2, 'click', function () {
        infowindow2.open(map2, marker2);
    });
 
}
    </script>
  </head>
  <body>
    <div id="mapa"></div>
    <div id="mapa2"></div>
  </body>