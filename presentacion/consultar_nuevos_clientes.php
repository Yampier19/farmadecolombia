<?php
error_reporting(0);
	include ('../logica/session.php');
	 header('Content-Type: text/html; charset=UTF-8'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CONSULTAR GESTION</title>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css"/>
<link href="css/tablas.css" rel="stylesheet" /> 
<link href="css/bootstrap.css" rel="stylesheet" /> 

<script>
$(document).ready(function()
{
	$('#nombre').on('keyup', function()
	{
		VER_DATOS();
	});
});

function ventanaSecundaria (URL)
{ 
   window.open(URL,"ventana1","width=800,height=500,Top=150,Left=50%") 
}

function VER_DATOS()
{
	var NOMBRE=$('#nombre').val(); 
	var USUA=$('#usua').val();
	
	
	//alert(USUA)
	$.ajax(
	{
		url:'../presentacion/datos_consulta_cliente_admin.php',
		data:
		{
			NOMBRE: NOMBRE,
			USUA: USUA
		},
		type: 'post',
		contentType: "application/x-www-form-urlencoded;charset=utf-8",
		beforeSend: function ()
		{
				$("#tablas").html("Procesando, espere por favor"+'<img src="../presentacion/imagenes/cargando.gif" />');
		},
		success: function(data)
		{
			$('#tablas').html(data);
		}
	})
}


</script>
</head>
<?php
include('../datos/conex.php');

	$string_intro = getenv("QUERY_STRING"); 
	parse_str($string_intro);
$usua;
?>
<body>
<br /><br />

	<div class="card">
	<div class="card-body">
		
			<legend >Nombre o N&uacute;mero de identicaci&oacute;n<span class="asterisco">*</span></legend>
			<div class="form-group">
				<center>
				<input placeholder="Nombre o numero de identificacion" type="text" name="nombre" id="nombre" class="form-control" autocomplete="off" autofocus/>
				</center>
			</div>

    <br />
    <table align="center" width="100%"  class="table table-striped" id="tablas">
    </table>
	</div>
    </div>
    
   
</body>
</html>