<?php
include("../logica/session.php");
error_reporting(0);
?>
<?php header('Content-Type: text/html; charset=UTF-8'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.dataTables.js"></script>
        <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css"/>
        <link href="css/tablas.css" rel="stylesheet" /> 
        <link href="css/bootstrap.css" rel="stylesheet" /> 

        <title>CONSULTA PEDIDO ENTREGADO</title>
        <script>
            $(document).ready(function () {
                $('#pedidos').DataTable();
            });

            function ventanaSecundaria(URL)
            {
                window.open(URL, "ventana1", "width=800,height=500,Top=150,Left=50%")
            }

        </script> 


    </head>
    <?PHP
    include('../datos/conex_copia.php');

    $string_intro = getenv("QUERY_STRING");
    parse_str($string_intro);
    if (isset($pedid)) {
        $ID_CLIENTE = base64_decode($pedid);
    } else {
        $ID_CLIENTE = $ID_CLIENTE;
    }

    $consulta = mysqli_query($conex," 
 SELECT DISTINCT p.ID_PEDIDO, p.TOTAL_PEDIDO,
 cli.NOMBRE_CLIENTE ,
 cli.APELLIDO_CLIENTE ,
p.DISTRIBUIR,p.FECHA_PEDIDO,
p.ESTADO_PEDIDO,cli.CELULAR_CLIENTE,cli.TELEFONO_CLIENTE,cli.DIRECCION_CLIENTE,p.TIPO_PEDIDO,u.USER,
d.RANGO_ENTREGA,
 CONVERT(CAST(CONVERT(d.OBSERVACIONES USING latin1) AS BINARY) USING utf8) as OBSERVACIONES
FROM 3m_pedido AS p
 INNER JOIN 3m_cliente AS cli ON cli.ID_CLIENTE=p.ID_CLIENTE_FK
 INNER JOIN 3m_detalle_pedido AS d ON d.ID_PEDIDO_FK_2 = p.ID_PEDIDO
 INNER JOIN 3m_usuario AS u ON p.ID_USUARIO_FK=u.ID_USUARIO 
 WHERE ESTADO_PEDIDO = 'ENTREGADO' AND USER <>'visita' AND USER<>'MEDELLIN' AND USER <> 'emesa' ORDER BY FECHA_PEDIDO DESC  ");
    ?>
    <body>
        <br />
        <br />

        <div class="col-md-12">
            <form id="cambio_contacto" name="cambio_contacto" method="post" action="#" enctype="multipart/form-data" class="letra">
                <fieldset style="margin:auto auto; width:90%;">
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <label for="mes">MES</label><span class="asterisco">*</span><br />
                            <select class="form-control" name="mes">
                                <option value="">SELECCIONE</option>
                                <option value="1">ENERO</option>
                                <option value="2">FEBRERO</option>
                                <option value="3">MARZO</option>
                                <option value="4">ABRIL</option>
                                <option value="5">MAYO</option>
                                <option value="6">JUNIO</option>
                                <option value="7">JULIO</option>
                                <option value="8">AGOSTO</option>
                                <option value="9">SEPTIEMBRE</option>
                                <option value="10">OCTUBRE</option>
                                <option value="11">NOVIEMBRE</option>
                                <option value="12">DICIEMBRE</option>
                            </select>
                        </div>

                        <div class="col-md-2">
                            <label for="cliente">FECHA GESTI&Oacute;N:</label><span class="asterisco">*</span><br />
                            <input type="date" class="form-control" name="fecha" id="fecha" max="<?php echo date("Y-m-d"); ?>"/>
                        </div>
                        <div class="col-md-4">
                            <label for="fecha">USUARIO:</label><span class="asterisco">*</span><br />
                            <select class="form-control" name="idUsuario[]" multiple="multiple"> 
                                <option value="" disabled="disabled" selected="selected">Seleccione</option>           
<?php
$consultaGestionUsuario = mysqli_query($conex,"
         SELECT ID_USUARIO, USER FROM 3m_usuario
		 WHERE PRIVILEGIOS =2 AND ESTADO =1 AND USER <> 'VISITA' AND USER <> 'MEDELLIN' AND USER <> 'emesa'  
		  ORDER BY USER ASC ;");


while ($dato = mysqli_fetch_array($consultaGestionUsuario)) {
    ?>
                                    <option value="<?php echo $dato['ID_USUARIO']; ?>"><?php echo $dato['USER']; ?></option>
                                <?php } ?>

                            </select>
                        </div>

                        <div class="col-md-3">
                            <label for="cliente"></label><br /><br />
                            <button title="Consultar" name="consultar"><img src="imagenes/lupa1.png" width="43" height="32" style="background-size:cover" title="CONSULTAR"></button>


                        </div>
                    </div>

                </fieldset>

            </form>
            <div class="table table-responsive">


<?php
$total_fecha = '';

if (isset($_POST["consultar"])) {
    $idUsuario = $_POST["idUsuario"];
    $fecha = $_POST["fecha"];
    $mes = $_POST["mes"];
    if (empty($idUsuario) == false) {
        $usuarios_seleccionados = implode(',', $idUsuario);
    }
    if (empty($idUsuario) && empty($fecha) && empty($mes)) {
        echo 'esta vacia la busqueda';
    }

    //BUSQUEDA USUARIO
    else if (empty($idUsuario) == false && empty($fecha) && empty($mes)) {
        $consultaGestionUsuario = mysqli_query($conex,"
			  
SELECT DISTINCT b.TOTAL_PEDIDO, CONCAT(NOMBRE_CLIENTE, ' ', APELLIDO_CLIENTE) AS NOMBRE,  b.DISTRIBUIR, b.FECHA_PEDIDO, b.ESTADO_PEDIDO, a.CELULAR_CLIENTE, a.TELEFONO_CLIENTE, 
a.DIRECCION_CLIENTE, b.TIPO_PEDIDO, d.RANGO_ENTREGA, e.USER, b.ID_PEDIDO
FROM 3m_cliente AS a
INNER JOIN 3m_pedido AS b ON a.ID_CLIENTE = b.ID_CLIENTE_FK
INNER JOIN 3m_detalle_pedido AS d ON b.ID_PEDIDO = d.ID_PEDIDO_FK_2
INNER JOIN 3m_usuario AS e ON b.ID_USUARIO_FK = e.ID_USUARIO
WHERE e.ID_USUARIO IN($usuarios_seleccionados) AND b.ESTADO_PEDIDO='ENTREGADO' AND USER <> 'VISITA' AND USER <> 'MEDELLIN' AND USER <> 'emesa'");

        $valor = mysqli_query($conex,"SELECT SUM(TOTAL_PEDIDO),ID_USUARIO_FK FROM 3m_pedido WHERE ESTADO_PEDIDO='ENTREGADO' AND ID_USUARIO_FK IN ($usuarios_seleccionados) GROUP BY ID_USUARIO_FK");
        while ($datos = mysqli_fetch_array($valor)) {
            $total_fecha = $datos['SUM(TOTAL_PEDIDO)'];
        }
    }
    //BUSQUEDA FECHA 
    else if (empty($idUsuario) && empty($fecha) == false && empty($mes)) {
        $consultaGestionUsuario = mysqli_query($conex,"
			   SELECT DISTINCT b.TOTAL_PEDIDO, CONCAT(NOMBRE_CLIENTE, ' ', APELLIDO_CLIENTE) AS NOMBRE,  b.DISTRIBUIR, b.FECHA_PEDIDO, b.ESTADO_PEDIDO, a.CELULAR_CLIENTE, a.TELEFONO_CLIENTE, 
a.DIRECCION_CLIENTE, b.TIPO_PEDIDO, d.RANGO_ENTREGA, e.USER, b.ID_PEDIDO
FROM 3m_cliente AS a
INNER JOIN 3m_pedido AS b ON a.ID_CLIENTE = b.ID_CLIENTE_FK
INNER JOIN 3m_detalle_pedido AS d ON b.ID_PEDIDO = d.ID_PEDIDO_FK_2
INNER JOIN 3m_usuario AS e ON b.ID_USUARIO_FK = e.ID_USUARIO
WHERE DATE(b.FECHA_PEDIDO)='$fecha' AND b.ESTADO_PEDIDO='ENTREGADO' AND USER <> 'VISITA' AND USER <> 'MEDELLIN' AND USER <> 'emesa'");

        $valor = mysqli_query($conex,"SELECT SUM(TOTAL_PEDIDO) FROM 3m_pedido WHERE DATE(FECHA_PEDIDO)='$fecha' AND ESTADO_PEDIDO='ENTREGADO' AND ID_USUARIO_FK  <> 2 AND ID_USUARIO_FK  <> 4 AND ID_USUARIO_FK  <> 12");
        while ($datos = mysqli_fetch_array($valor)) {
            $total_fecha = $datos['SUM(TOTAL_PEDIDO)'];
        }
    }
    //BUSQUEDA MES
    else if (empty($idUsuario) && empty($fecha) && empty($mes) == false) {
        $consultaGestionUsuario = mysqli_query($conex,"
			   SELECT DISTINCT b.TOTAL_PEDIDO, CONCAT(NOMBRE_CLIENTE, ' ', APELLIDO_CLIENTE) AS NOMBRE,  b.DISTRIBUIR, b.FECHA_PEDIDO, b.ESTADO_PEDIDO, a.CELULAR_CLIENTE, a.TELEFONO_CLIENTE, 
a.DIRECCION_CLIENTE, b.TIPO_PEDIDO, d.RANGO_ENTREGA, e.USER, b.ID_PEDIDO
FROM 3m_cliente AS a
INNER JOIN 3m_pedido AS b ON a.ID_CLIENTE = b.ID_CLIENTE_FK
INNER JOIN 3m_detalle_pedido AS d ON b.ID_PEDIDO = d.ID_PEDIDO_FK_2
INNER JOIN 3m_usuario AS e ON b.ID_USUARIO_FK = e.ID_USUARIO
WHERE MONTH(b.FECHA_PEDIDO)='$mes' AND b.ESTADO_PEDIDO='ENTREGADO' AND USER <> 'VISITA' AND USER <> 'MEDELLIN' AND USER <> 'emesa'");

        $valor = mysqli_query($conex,"SELECT SUM(TOTAL_PEDIDO) FROM 3m_pedido WHERE ESTADO_PEDIDO='ENTREGADO' AND MONTH(FECHA_PEDIDO)='$mes' AND ID_USUARIO_FK  <> 2 AND ID_USUARIO_FK  <> 4 AND ID_USUARIO_FK  <> 12");
        while ($datos = mysqli_fetch_array($valor)) {
            $total_fecha = $datos['SUM(TOTAL_PEDIDO)'];
        }
    }
    //BUSQUEDA USUARIO Y FECHA
    else if (empty($idUsuario) == false && empty($fecha) == false && empty($mes)) {
        $consultaGestionUsuario = mysqli_query($conex,"
			   SELECT DISTINCT b.TOTAL_PEDIDO, CONCAT(NOMBRE_CLIENTE, ' ', APELLIDO_CLIENTE) AS NOMBRE,  b.DISTRIBUIR, b.FECHA_PEDIDO, b.ESTADO_PEDIDO, a.CELULAR_CLIENTE, a.TELEFONO_CLIENTE, 
a.DIRECCION_CLIENTE, b.TIPO_PEDIDO, d.RANGO_ENTREGA, e.USER, b.ID_PEDIDO
FROM 3m_cliente AS a
INNER JOIN 3m_pedido AS b ON a.ID_CLIENTE = b.ID_CLIENTE_FK
INNER JOIN 3m_detalle_pedido AS d ON b.ID_PEDIDO = d.ID_PEDIDO_FK_2
INNER JOIN 3m_usuario AS e ON b.ID_USUARIO_FK = e.ID_USUARIO
WHERE e.ID_USUARIO IN($usuarios_seleccionados) AND DATE(b.FECHA_PEDIDO)='$fecha' AND b.ESTADO_PEDIDO='ENTREGADO' AND USER <> 'VISITA' AND USER <> 'MEDELLIN' AND USER <> 'emesa'");

        $valor = mysqli_query($conex,"SELECT SUM(TOTAL_PEDIDO) FROM 3m_pedido WHERE ESTADO_PEDIDO='ENTREGADO' AND  ID_USUARIO_FK IN ($usuarios_seleccionados) AND DATE(FECHA_PEDIDO)='$fecha'");
        while ($datos = mysqli_fetch_array($valor)) {
            $total_fecha = $datos['SUM(TOTAL_PEDIDO)'];
        }
    }
    //BUSQUEDA USUARIO Y MES
    else if (empty($idUsuario) == false && empty($fecha) && empty($mes) == false) {
        $consultaGestionUsuario = mysqli_query($conex,"
			   SELECT DISTINCT b.TOTAL_PEDIDO, CONCAT(NOMBRE_CLIENTE, ' ', APELLIDO_CLIENTE) AS NOMBRE,  b.DISTRIBUIR, b.FECHA_PEDIDO, b.ESTADO_PEDIDO, a.CELULAR_CLIENTE, a.TELEFONO_CLIENTE, 
a.DIRECCION_CLIENTE, b.TIPO_PEDIDO, d.RANGO_ENTREGA, e.USER, b.ID_PEDIDO
FROM 3m_cliente AS a
INNER JOIN 3m_pedido AS b ON a.ID_CLIENTE = b.ID_CLIENTE_FK
INNER JOIN 3m_detalle_pedido AS d ON b.ID_PEDIDO = d.ID_PEDIDO_FK_2
INNER JOIN 3m_usuario AS e ON b.ID_USUARIO_FK = e.ID_USUARIO
WHERE e.ID_USUARIO IN($usuarios_seleccionados) AND MONTH(b.FECHA_PEDIDO)='$mes' AND b.ESTADO_PEDIDO='ENTREGADO' AND USER <> 'VISITA' AND USER <> 'MEDELLIN' AND USER <> 'emesa'");

        $valor = mysqli_query($conex,"SELECT SUM(TOTAL_PEDIDO) FROM 3m_pedido WHERE ESTADO_PEDIDO='ENTREGADO' AND  ID_USUARIO_FK IN ($usuarios_seleccionados) AND MONTH(FECHA_PEDIDO)=='$mes'");
        while ($datos = mysqli_fetch_array($valor)) {
            $total_fecha = $datos['SUM(TOTAL_PEDIDO)'];
        }
    }
} else {
    $consultaGestionUsuario = mysqli_query($conex,"
				SELECT DISTINCT b.TOTAL_PEDIDO, CONCAT(NOMBRE_CLIENTE, ' ', APELLIDO_CLIENTE) AS NOMBRE,  b.DISTRIBUIR, b.FECHA_PEDIDO, b.ESTADO_PEDIDO, a.CELULAR_CLIENTE, a.TELEFONO_CLIENTE, 
a.DIRECCION_CLIENTE, b.TIPO_PEDIDO, d.RANGO_ENTREGA, e.USER, b.ID_PEDIDO
FROM 3m_cliente AS a
INNER JOIN 3m_pedido AS b ON a.ID_CLIENTE = b.ID_CLIENTE_FK
INNER JOIN 3m_detalle_pedido AS d ON b.ID_PEDIDO = d.ID_PEDIDO_FK_2
INNER JOIN 3m_usuario AS e ON b.ID_USUARIO_FK = e.ID_USUARIO
WHERE b.ESTADO_PEDIDO='ENTREGADO' AND USER <> 'VISITA' AND USER <> 'MEDELLIN' AND USER <> 'emesa'");

    $valor = mysqli_query($conex,"SELECT SUM(TOTAL_PEDIDO) FROM 3m_pedido WHERE ESTADO_PEDIDO='ENTREGADO' AND ID_USUARIO_FK  <> 2 AND ID_USUARIO_FK  <> 4 AND ID_USUARIO_FK  <> 12");
    while ($datos = mysqli_fetch_array($valor)) {
        $total_fecha = $datos['SUM(TOTAL_PEDIDO)'];
    }
}
?>
                <table style="width:99%; margin:auto auto;" rules="none" >
                    <tr>
                        <th colspan='11' class="principal">PEDIDOS ENTREGADOS</th>
                    </tr>
                </table>
                <table style="width:99%; margin:auto auto;" rules="none" id="pedidos">
                    <thead>
                        <tr>
                            <th class="TITULO"></th>
                            <th class="TITULO">TOTAL PEDIDO</th>
                            <th class="TITULO">NOMBRE CLIENTE</th>
                            <th class="TITULO">DISTRIBUIDOR</th>
                            <th class="TITULO">FECHA PEDIDO </th>
                            <th class="TITULO">ESTADO PEDIDO </th>
                            <th class="TITULO">TELEFONO CELULAR</th>
                            <th class="TITULO">TELEFONO FIJO</th>
                            <th class="TITULO">DIRECCION CLIENTE</th>
                            <th class="TITULO">TIPO PEDIDO</th>
                            <th class="TITULO">HORARIO ENTREGA</th>
                            <th class="TITULO">USUARIO</th>
                            <th class="TITULO">VER</th>
                        </tr>
                    </thead>
                    <tbody>
<?PHP
$i = 1;
while ($dato = mysqli_fetch_array($consultaGestionUsuario)) {
    ?>
                            <tr class="datos">
                                <td><b><?php echo $i++ ?></b></td>
                                <td><?php echo $dato["TOTAL_PEDIDO"] ?></td>
                                <td><?php echo $dato["NOMBRE"] ?></td>
                                <td><?php echo $dato["DISTRIBUIR"] ?></td>
                                <td><?php echo $dato["FECHA_PEDIDO"] ?></td>
                                <td><?php echo $dato["ESTADO_PEDIDO"] ?></td>
                                <td><?php echo $dato["CELULAR_CLIENTE"] ?></td>	
                                <td><?php echo $dato["TELEFONO_CLIENTE"] ?></td>	
                                <td><?php echo $dato["DIRECCION_CLIENTE"] ?></td>
                                <td><?php echo $dato["TIPO_PEDIDO"] ?></td>	
                                <td><?php echo $dato["RANGO_ENTREGA"] ?></td>	
                                <td style="text-align:center"><?php echo $dato["USER"] ?></td>
                                <th>
                                    <a  href="javascript:ventanaSecundaria('../presentacion/consulta_pedidos_detalle.php?x=<?php echo base64_encode($dato['ID_PEDIDO']) ?>')" >
                                        <img src="imagenes/lupa1.png" width="43" height="32" style="background-size:cover" title="DETALLE PEDIDO"/>
                                    </a>                
                                </th>	
                            </tr>
    <?php
}
?>
                    </tbody>
                </table>
                <div style="font-size:18px; font-weight:1000">
<?php
if ($total_fecha != '') {
    echo 'Total pedidos: $' . number_format($total_fecha, 0, ',', '.');
}
?>
                </div>
                </body>
                </html>