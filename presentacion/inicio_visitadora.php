<?php
 include ('logica/session.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" href="css/form_visitadora.css" />
<link rel="stylesheet" href="fonts.css" />
<script src="js/jquery.js"></script>
<script type="text/javascript" src="js/direccion.js"></script>
<script type="text/javascript" src="js/direccion_consultorio.js"></script>

<div id='ubicacion'></div>
<script type="text/javascript">
	if (navigator.geolocation)
	{
		navigator.geolocation.getCurrentPosition(mostrarUbicacion);
	}
	else
	{
		alert("¡Error! Este navegador no soporta la Geolocalización.");
	}
function mostrarUbicacion(position)
{
    var times = position.timestamp;
	var latitud = position.coords.latitude;
	var longitud = position.coords.longitude;
    var altitud = position.coords.altitude;	
	var exactitud = position.coords.accuracy;	
	var div = document.getElementById("ubicacion");
	/*div.innerHTML = "Timestamp: " + times + "<br>Latitud: " + latitud + "<br>Longitud: " + longitud + "<br>Altura en metros: " + altitud + "<br>Exactitud: " + exactitud;*/
	$('#Latitud').val(latitud);
	$('#Longitud').val(longitud);
}	
function refrescarUbicacion() {	
	navigator.geolocation.watchPosition(mostrarUbicacion);}	
</script>

<!-- Se escribe un mapa con la localizacion anterior-->
<div id="demo"></div>
<div id="mapholder"></div>
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<!--<button onclick="cargarmap()">Cargar mapa</button>-->
<script type="text/javascript">
var x=document.getElementById("demo");
function cargarmap()
{
navigator.geolocation.getCurrentPosition(showPosition,showError);
function showPosition(position)
{
	lat=position.coords.latitude;
	lon=position.coords.longitude;
	latlon=new google.maps.LatLng(lat, lon)
	mapholder=document.getElementById('mapholder')
	mapholder.style.height='250px';
	mapholder.style.width='500px';
	var myOptions={
	center:latlon,zoom:10,
	mapTypeId:google.maps.MapTypeId.ROADMAP,
	mapTypeControl:false,
	navigationControlOptions:{style:google.maps.NavigationControlStyle.SMALL}
	};
	var map=new google.maps.Map(document.getElementById("mapholder"),myOptions);
	var marker=new google.maps.Marker({position:latlon,map:map,title:"You are here!"});
}
function showError(error)
  {
  switch(error.code) 
    {
    case error.PERMISSION_DENIED:
      x.innerHTML="Denegada la peticion de Geolocalización en el navegador."
      break;
    case error.POSITION_UNAVAILABLE:
      x.innerHTML="La información de la localización no esta disponible."
      break;
    case error.TIMEOUT:
      x.innerHTML="El tiempo de petición ha expirado."
      break;
    case error.UNKNOWN_ERROR:
      x.innerHTML="Ha ocurrido un error desconocido."
      break;
    }
  }}
</script>
<style>
#gotop 
{
	background:#000;
	color:#fff; 
	border-radius:5px; 
	padding:10px; 
	position:fixed; 
	bottom:10px; 
	right:10px;
}
</style>
<script>

$(document).ready(function()
{
	$(window).scroll(function(){
      if ($(this).scrollTop() > 100) {
        $('.ancla-sitio').fadeIn();
      } else {
        $('.ancla-sitio').fadeOut();
      }
    });
	//Click event para que el scroll suba
    $('.ancla-sitio').click(function(){
      $('html, body').animate({scrollTop : 100},800);
      return false;
    });
	$( "body" ).click(function()
	{
		if($(window).width() <= 800)
		{
		}
		if($(window).width() > 800)
		{
			$('nav').css('display','block');
		}
	});
	$("#salir").click(function()
	{
		if (confirm('¿Estas seguro de salir?'))
		{ 
			window.location="index.php";
		}
		else
		{
		}    
	});
	$('#link1').click(function()
	{
		$('#datos2').fadeOut(5);
		$('#datos').fadeOut(5);
		$('#datos_per').fadeToggle(10);
	});
	$('#link2').click(function()
	{
		$('#datos').fadeToggle(10);
		$('#datos2').fadeOut(5);
		$('#datos_per').fadeOut(5);
	});
	$('#link3').click(function()
	{
		$('#datos2').fadeToggle(10);
		$('#datos').fadeOut(5);
		$('#datos_per').fadeOut(5);
	});
	
	
	$('#cambio').click(function()
	{
		$('#cambio_direccion').toggle();
		$('#DIRECCION_RESIDENCIA').val($('#DIRECCION_RESIDENCIA').prop('defaultValue'));		
		$("#VIA option:eq(0)").attr("selected", "selected");
		$("#interior option:eq(0)").attr("selected", "selected");
		$("#interior2 option:eq(0)").attr("selected", "selected");
		$("#interior3 option:eq(0)").attr("selected", "selected");
		$("#TERAPIA option:eq(0)").attr("selected", "selected");
		$('#detalle_via').val('');
		$('#detalle_int').val('');
		$('#detalle_int2').val('');
		$('#detalle_int3').val('');
		$('#numero').val('');
		$('#numero2').val('');

	});
	var via=$('#VIA').val();
	var dt_via=$('#detalle_via').val();
	$('#VIA').change(function()
	{
		dir();
	});
	
	$('#detalle_via').change(function()
	{
		dir();
	});
	$('#numero').change(function()
	{
		dir();
	});
	$('#numero2').change(function()
	{
		dir();
	});
	$('#interior').change(function()
	{
		dir();		
	});
	$('#detalle_int').change(function()
	{
		dir();
	});
	$('#interior2').change(function()
	{
		dir();		
	});
	$('#detalle_int2').change(function()
	{
		dir();
	});
	$('#interior3').change(function()
	{
		dir();		
	});
	$('#detalle_int3').change(function()
	{
		dir();
	});
	
	
	/*DIRECCION CONSULTORIO*/
	$('#cambio2').click(function()
	{
		$('#cambio_cambio2').toggle();
		$('#DIRECCION_CONSULTORIO').val($('#DIRECCION_CONSULTORIO').prop('defaultValue'));		
		$("#VIA_cons option:eq(0)").attr("selected", "selected");
		$("#interior_cons option:eq(0)").attr("selected", "selected");
		$("#interior2_cons option:eq(0)").attr("selected", "selected");
		$("#interior3_cons option:eq(0)").attr("selected", "selected");
		$("#TERAPIA_cons option:eq(0)").attr("selected", "selected");
		$('#detalle_via_cons').val('');
		$('#detalle_int_cons').val('');
		$('#detalle_int2_cons').val('');
		$('#detalle_int3_cons').val('');
		$('#numero_cons').val('');
		$('#numero2_cons').val('');

	});
	var via=$('#VIA_cons').val();
	var dt_via=$('#detalle_via_cons').val();
	$('#VIA_cons').change(function()
	{
		dire();
	});
	
	$('#detalle_via_cons').change(function()
	{
		dire();
	});
	$('#numero_cons').change(function()
	{
		dire();
	});
	$('#numero2_cons').change(function()
	{
		dire();
	});
	$('#interior_cons').change(function()
	{
		dire();		
	});
	$('#detalle_int_cons').change(function()
	{
		dire();
	});
	$('#interior2_cons').change(function()
	{
		dire();		
	});
	$('#detalle_int2_cons').change(function()
	{
		dire();
	});
	$('#interior3_cons').change(function()
	{
		dire();		
	});
	$('#detalle_int3_cons').change(function()
	{
		dire();
	});
	
});
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VISITADORA</title>
<link rel="shortcut icon" href="favicon.ico" />
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
</head>
<body>
<form name="tuformulario" action="form_motorizado.php?USUARIO=<?php echo $USUARIO; ?>" method="post" enctype="multipart/form-data">
<header>
	<div class="menu_bar" style="background-color:#000;">
    <div id="salir" style="float:left; margin-left:3%; margin-top:2%;">
    <span class="icon-exit" title="CERRAR SESION" style="color:#FFF;"></span>
    </div>		
        <p><a href="#" class="bt_menu" id="play" style="text-align:center;">
          <span>
            
          <img src="imagenes/3m.png" width="70" height="105">
          </span>VISITADORAS 3M
        </a></p>          
        </p>
        <div>
        <label style=" margin-bottom:20%; margin-top:2%; color:#FFF;">CODIGO CLIENTE</label>
        <input type="number" id="codigo_cli" name="codigo_cli" style="width:40%; margin-bottom:2%; margin-top:2%; height:5%;" maxlength="12" value="<?php echo $cod;?>"/>
        <input name="boto" id="boto" type="submit" value="B" style="color:#FFF; width:10%; background-color:#000;" />
        </div>
    </div>
	<nav style="background-color:#024959">
<div class="link">
<a name="arriba"></a>
<span class="icon-info div_span">&nbsp;<input type="button" name="link1" id="link1" value="Informaci&oacute;n personal" formaction="#arriba"/></span>
</div>
<div style="display:none;background-color:#FFF;" id="datos_per">
	<br />
	<center><span style="font-weight:bold">INFORMACION PERSONAL</span></center>
    <div class="DIV1">
        <label class="labeltitu">NOMBRE CLIENTE</label>
        <input type="text" name="NOMBRE_CLIENTE" id="NOMBRE_CLIENTE" placeholder=" Nombre cliente" maxlength="50" style="height:4%;" value="<?php echo $NEGOCIO; ?>"/>
        <br/><br/>
        <label class="labeltitu">TELEFONO CLIENTE</label>
        <input type="number" name="TELEFONO_CLIENTE" id="TELEFONO_CLIENTE" placeholder=" Telefono cliente" maxlength="15" style="height:4%;" value="<?php echo $TELEFONO_CONSULTORIO;?>"/>
    </div>
    <div class="DIV1">
        <label class="labeltitu">APELLIDO CLIENTE</label>
        <input type="text" name="APELLIDO_CLIENTE" id="APELLIDO_CLIENTE" placeholder=" Apellido cliente" maxlength="50" style="height:4%;" value="<?php echo $NEGOCIO; ?>"/>
        <br/><br/>
        <label class="labeltitu">BARRIO RESIDENCIA</label>
        <input type="number" name="BARRIO_RESIDENCIA" id="BARRIO_RESIDENCIA" placeholder=" Barrio residencia" maxlength="15" style="height:4%;" value="<?php echo $TELEFONO_CONSULTORIO;?>"/>
    </div>
    <div style="width:90%; margin-left:2%; padding:3%;">
        <label class="labeltitu">DIRECCION RESIDENCIA</label>
            <input type="text" name="DIRECCION_RESIDENCIA" id="DIRECCION_RESIDENCIA" placeholder=" Direccion residencia" style="height:4%;width:88%;" value="<?php echo $TELEFONO_CONSULTORIO;?>" readonly="readonly"/>
    <img src="imagenes/lapiz.png"
id="cambio" name="cambio" title="Editar" style="width:8%; height:4%; margin-left:-50%;" align="right"/>
    </div>
    <div id="cambio_direccion" style="display:none">
    <div class="DIV1">
    <label class="labeltitu">Via:</label>
    <select id="VIA" name="VIA" style="width:96%;height:4%">
        <option value="">Seleccione...</option>
        <option>ANILLO VIAL</option>
        <option>AUTOPISTA</option>
        <option>AVENIDA</option>
        <option>BOULEVAR</option>
        <option>CALLE</option>
        <option>CALLEJON</option>
        <option>CARRERA</option>
        <option>CIRCUNVALAR</option>
        <option>CONDOMINIO</option>
        <option>DIAGONAL</option>
        <option>KILOMETRO</option>
        <option>LOTE</option>
        <option>SALIDA</option>
        <option>SECTOR</option>
        <option>TRANSVERSAL</option>
        <option>VEREDA</option>
        <option>VIA</option>
    </select>
    <br />
    <br />
    <label class="labeltitu">N&uacute;mero:</label>
    <br />
    <input name="numero" id="numero" type="text" maxlength="5" style="width:45%;height:4%"/>
      -
  <input name="numero2" id="numero2" type="text" maxlength="5" style="width:45%;height:4%"/>
  <br />
  <br />
  <label class="labeltitu">Interior:</label>
  <select id="interior" name="interior" style="width:96%;height:4%">
    	<option value="">Seleccione...</option>
        <option>APARTAMENTO</option>
        <option>BARRIO</option>
<option>BLOQUE</option>
        <option>CASA</option>
        <option>CIUDADELA</option>
        <option>CONJUNTO</option>
        <option>CONJUNTO RESIDENCIAL</option>
        <option>EDIFICIO</option>
        <option>ENTRADA</option>
        <option>ETAPA</option>
        <option>INTERIOR</option>
        <option>MANZANA</option>
        <option>NORTE</option>
        <option>OCCIDENTE</option>
        <option>ORIENTE</option>
        <option>PENTHOUSE</option>
        <option>PISO</option>
        <option>PORTERIA</option>
        <option>SOTANO</option>
        <option>SUR</option>
        <option>TORRE</option>
    </select>
    <br />
    <br />
    <label class="labeltitu">Interior:</label>
  <select id="interior2" name="interior2" style="width:96%;height:4%">
    	<option value="">Seleccione...</option>
        <option>APARTAMENTO</option>
        <option>BARRIO</option>
<option>BLOQUE</option>
        <option>CASA</option>
        <option>CIUDADELA</option>
        <option>CONJUNTO</option>
        <option>CONJUNTO RESIDENCIAL</option>
        <option>EDIFICIO</option>
        <option>ENTRADA</option>
        <option>ETAPA</option>
        <option>INTERIOR</option>
        <option>MANZANA</option>
        <option>NORTE</option>
        <option>OCCIDENTE</option>
        <option>ORIENTE</option>
        <option>PENTHOUSE</option>
        <option>PISO</option>
        <option>PORTERIA</option>
        <option>SOTANO</option>
        <option>SUR</option>
        <option>TORRE</option>
    </select>
    <br />
    <br />
    <label class="labeltitu">Interior:</label>
  <select id="interior3" name="interior3" style="width:96%;height:4%">
    	<option value="">Seleccione...</option>
        <option>APARTAMENTO</option>
        <option>BARRIO</option>
<option>BLOQUE</option>
        <option>CASA</option>
        <option>CIUDADELA</option>
        <option>CONJUNTO</option>
        <option>CONJUNTO RESIDENCIAL</option>
        <option>EDIFICIO</option>
        <option>ENTRADA</option>
        <option>ETAPA</option>
        <option>INTERIOR</option>
        <option>MANZANA</option>
        <option>NORTE</option>
        <option>OCCIDENTE</option>
        <option>ORIENTE</option>
        <option>PENTHOUSE</option>
        <option>PISO</option>
        <option>PORTERIA</option>
        <option>SOTANO</option>
        <option>SUR</option>
        <option>TORRE</option>
    </select>
    </div>
    <div class="DIV1">
    <label class="labeltitu">Detalles via:</label>
    <input name="detalle_via" id="detalle_via" type="text" maxlength="15" style="width:95%; height:4%;"/>
    <br />
    <br />
    <label class="labeltitu"></label><br />
    <input name="blanco" id="blanco" type="text" maxlength="15" style="width:95%; height:4%; visibility:hidden;" readonly="readonly"/>
    <br />
    <br />
    <label class="labeltitu">Detalles Interior</label>
    <input name="detalle_int" id="detalle_int" type="text" maxlength="15" readonly style="width:95%;height:4%"/>
    <br />
    <br />
    <label class="labeltitu">Detalles Interior</label>
    <input name="detalle_in2t" id="detalle_int2" type="text" maxlength="15" readonly style="width:95%;height:4%"/>
    <br />
    <br />
    <label class="labeltitu">Detalles Interior</label>
    <input name="detalle_int3" id="detalle_int3" type="text" maxlength="15" readonly style="width:95%;height:4%"/>
    </div>
    </div>
    <br /><br />
    </div>
</div>
<div class="link">
<span class="icon-info div_span">&nbsp;<input type="button" name="link2" id="link2" value="Informaci&oacute;n consultorio" formaction="#arriba"/></span>
</div>
<div style="display:none;background-color:#FFF;" id="datos">
	<br />
	<center><span style="font-weight:bold">INFORMACION CONSULTORIO</span></center>
    <div class="DIV1">
        <label class="labeltitu">NOMBRE CONSULTORIO</label>
        <input type="text" name="NOMBRE_CONSULTORIO" id="NOMBRE_CONSULTORIO" placeholder=" Nombre negocio" maxlength="50" style="height:4%;" value="<?php echo $NEGOCIO; ?>"/>
        <br/><br/>
        <label class="labeltitu">TELEFONO CONSULTORIO</label>
        <input type="number" name="TELEFONO_CONSULTORIO" id="TELEFONO_CONSULTORIO" placeholder=" Telefono consultorio" maxlength="15" style="height:4%;" value="<?php echo $TELEFONO_CONSULTORIO;?>"/>
        <br /><br />
    </div>
    <div class="DIV1">
        <label class="labeltitu">BARRIO CONSULTORIO</label>
        <input type="text" name="BARRIO_CONSULTORIO" id="BARRIO_CONSULTORIO" placeholder=" Barrio consultorio" maxlength="30" style="height:4%;" value="<?php echo $BARRIO_CONSULTORIO;?>"/>
        <br /><br />
        <label class="labeltitu"></label><br />
        <input type="text" name="blanco" id="blanco" placeholder=" Direccion consultorio" maxlength="30" style="height:4%; visibility:hidden;" readonly="readonly"/>
        <br/><br/>
    </div>
    <div style="width:90%; margin-left:2%; padding:3%;">
        <label class="labeltitu">DIRECCION CONSULTORIO</label>
        <input type="text" name="DIRECCION_CONSULTORIO" id="DIRECCION_CONSULTORIO" placeholder=" Direccion consultorio" maxlength="30" style="height:4%;width:88%;" value="<?php echo $DIRECCION_CONSULTORIO;?>" readonly="readonly"/>
    <img src="imagenes/lapiz.png"
id="cambio2" name="cambio2" title="Editar" style="width:8%; height:4%; margin-left:-50%;" align="right"/>
    </div>
    <div id="cambio_cambio2" style="display:none">
    <div class="DIV1">
    <label class="labeltitu">Via:</label>
    <select id="VIA_cons" name="VIA_cons" style="width:96%;height:4%">
        <option value="">Seleccione...</option>
        <option>ANILLO VIAL</option>
        <option>AUTOPISTA</option>
        <option>AVENIDA</option>
        <option>BOULEVAR</option>
        <option>CALLE</option>
        <option>CALLEJON</option>
        <option>CARRERA</option>
        <option>CIRCUNVALAR</option>
        <option>CONDOMINIO</option>
        <option>DIAGONAL</option>
        <option>KILOMETRO</option>
        <option>LOTE</option>
        <option>SALIDA</option>
        <option>SECTOR</option>
        <option>TRANSVERSAL</option>
        <option>VEREDA</option>
        <option>VIA</option>
    </select>
    <br />
    <br />
    <label class="labeltitu">N&uacute;mero:</label>
    <br />
    <input name="numero_cons" id="numero_cons" type="text" maxlength="5" style="width:45%;height:4%"/>
      -
  <input name="numero2_cons" id="numero2_cons" type="text" maxlength="5" style="width:45%;height:4%"/>
  <br />
  <br />
  <label class="labeltitu">Interior:</label>
  <select id="interior_cons" name="interior_cons" style="width:96%;height:4%">
    	<option value="">Seleccione...</option>
        <option>APARTAMENTO</option>
        <option>BARRIO</option>
<option>BLOQUE</option>
        <option>CASA</option>
        <option>CIUDADELA</option>
        <option>CONJUNTO</option>
        <option>CONJUNTO RESIDENCIAL</option>
        <option>EDIFICIO</option>
        <option>ENTRADA</option>
        <option>ETAPA</option>
        <option>INTERIOR</option>
        <option>MANZANA</option>
        <option>NORTE</option>
        <option>OCCIDENTE</option>
        <option>ORIENTE</option>
        <option>PENTHOUSE</option>
        <option>PISO</option>
        <option>PORTERIA</option>
        <option>SOTANO</option>
        <option>SUR</option>
        <option>TORRE</option>
    </select>
    <br />
    <br />
    <label class="labeltitu">Interior:</label>
  <select id="interior2_cons" name="interior2_cons" style="width:96%;height:4%">
    	<option value="">Seleccione...</option>
        <option>APARTAMENTO</option>
        <option>BARRIO</option>
<option>BLOQUE</option>
        <option>CASA</option>
        <option>CIUDADELA</option>
        <option>CONJUNTO</option>
        <option>CONJUNTO RESIDENCIAL</option>
        <option>EDIFICIO</option>
        <option>ENTRADA</option>
        <option>ETAPA</option>
        <option>INTERIOR</option>
        <option>MANZANA</option>
        <option>NORTE</option>
        <option>OCCIDENTE</option>
        <option>ORIENTE</option>
        <option>PENTHOUSE</option>
        <option>PISO</option>
        <option>PORTERIA</option>
        <option>SOTANO</option>
        <option>SUR</option>
        <option>TORRE</option>
    </select>
    <br />
    <br />
    <label class="labeltitu">Interior:</label>
  <select id="interior3_cons" name="interior3_cons" style="width:96%;height:4%">
    	<option value="">Seleccione...</option>
        <option>APARTAMENTO</option>
        <option>BARRIO</option>
<option>BLOQUE</option>
        <option>CASA</option>
        <option>CIUDADELA</option>
        <option>CONJUNTO</option>
        <option>CONJUNTO RESIDENCIAL</option>
        <option>EDIFICIO</option>
        <option>ENTRADA</option>
        <option>ETAPA</option>
        <option>INTERIOR</option>
        <option>MANZANA</option>
        <option>NORTE</option>
        <option>OCCIDENTE</option>
        <option>ORIENTE</option>
        <option>PENTHOUSE</option>
        <option>PISO</option>
        <option>PORTERIA</option>
        <option>SOTANO</option>
        <option>SUR</option>
        <option>TORRE</option>
    </select>
    </div>
    <div class="DIV1">
    <label class="labeltitu">Detalles via:</label>
    <input name="detalle_via_cons" id="detalle_via_cons" type="text" maxlength="15" style="width:95%; height:4%;"/>
    <br />
    <br />
    <label class="labeltitu"></label><br />
    <input name="blanco" id="blanco" type="text" maxlength="15" style="width:95%; height:4%; visibility:hidden;" readonly/>
    <br />
    <br />
    <label class="labeltitu">Detalles Interior</label>
    <input name="detalle_int_cons" id="detalle_int_cons" type="text" maxlength="15" readonly style="width:95%;height:4%"/>
    <br />
    <br />
    <label class="labeltitu">Detalles Interior</label>
    <input name="detalle_int2_cons" id="detalle_int2_cons" type="text" maxlength="15" readonly style="width:95%;height:4%"/>
    <br />
    <br />
    <label class="labeltitu">Detalles Interior</label>
    <input name="detalle_int3_cons" id="detalle_int3_cons" type="text" maxlength="15" readonly style="width:95%;height:4%"/>
    </div>
    </div>    
</div>
<div class="link">
<span class="icon-info div_span">&nbsp;<input type="button" name="link3" id="link3" value="Resultado gestiones"/></span>
</div>
<div style="display:none;" id="datos2">
          <table width="100%" style="background:#FFF" rules="rows">
          <tr>
          		<td colspan="2">
                
                <input name="F_ANTES" type="file" id="F_ANTES" />
              </tr>
          <tr>
          		<td width="32%">
                OBSERVACIONES</td>
                <td width="68%" style="text-align:center">
    <textarea name="OBSERVACIONES" cols="50" rows="3" id="OBSERVACIONES" style="width:97%;"></textarea>
    </td>
              </tr>
          <tr>
          		<td>
                TIPIFICACION</td>
                <td style="text-align:center">                
                <select name="TIPIFICACION" id="TIPIFICACION" style=" height:100%; width:97%;" required>
                        <option selected="selected" VALUE='' style="color:#999;">TIPIFICACION</option>
                        <option>VISITA EFECTIVA</option>
                        <option>VISITA NO EFECTIVA</option>
                </select>
                </td>
              </tr>
              <tr>
              	<td>
                	Latitud
                </td>
                <td>
                	<input name="Latitud" id="Latitud" readonly="readonly" type="text" style=" height:100%"/>
                </td>
              </tr>
              <tr>
              	<td>
                	Longitud
                </td>
                <td>
                	<input name="Longitud" id="Longitud" readonly="readonly" type="text" style=" height:100%"/>
                </td>
              </tr>
          </table>
          </div>
</nav>
<span id="gotop" class="ancla-sitio">Ir arriba</span>
</header>
<section>

<?php
if($varlor=='ok')
{
?>

<center>
<input formaction="actualizacion_datos_cliente.php?USUARIO=<?php echo $USUARIO; ?>&amp;ID=<?php echo $ID; ?>" name="aregistro" class="botones" onclick="return validar(tuformulario)"  type="submit" value=""/>
</center>

<?php
}
?>

<div id="eje1" style="display:none">
COCA COLA
</p>
</div>
</section>
</form>
</body>
</html>
<script>
function validar(tuformulario)
{
	var tip=$('#TIPIFICACION').val(); 
	if(tip=='')
	{
		alert('Seleccione una tipificacion');
		$('#TIPIFICACION').focus();
		return false;
	}
	if(tip=='VISITADO PENDIENTE DOCUMENTACION'|| tip=='AFILIADO')
	{
		var valor=$('#nombre_cliente').val(); 
		if(valor=='')
		{
			alert('El nombre esta vacio');
			$('#nombre_cliente').focus();
			return false;
		}
		var valor=$('#tipo_documento').val();
		if(valor=='')
		{
			alert('Seleccione un tipo de documento');
			$('#tipo_documento').focus();
			return false;
		}
		var valor=$('#documento').val();
		if(valor=='')
		{
			alert('El documento esta vacio');
			$('#documento').focus();
			return false;
		}
		var valor=$('#fecha_nacimiento').val();
		if(valor=='')
		{
			alert('Seleccione una fecha de nacimineto');
			$('#fecha_nacimiento').focus();
			return false;
		}
		var valor=$('#GENERO').val();
		if(valor=='')
		{
			alert('Seleccione un genero');
			$('#GENERO').focus();
			return false;
		}
		var valor=$('#estado_civil').val(); 
		if(valor=='')
		{
			alert('Seleccione un estado civil');
			$('#estado_civil').focus();
			return false;
		}
		var valor=$('#NIVEL_EDUCATIVO').val();
		if(valor=='')
		{
			alert('Seleccione el nivel educativo');
			$('#NIVEL_EDUCATIVO').focus();
			return false;
		}
		/*
		var valor=$('#fecha_INGRESO_EMP').val();
		if(valor=='')
		{
			alert('Seleccione una fecha de ingreso');
			$('#fecha_INGRESO_EMP').focus();
			return false;
		}
		var valor=$('#horas_trabajadas').val();
		if(valor=='')
		{
			alert('Las horas trabajadas estan vacias');
			$('#horas_trabajadas').focus();
			return false;
		}
		var valor=$('#salario').val(); 
		if(valor=='')
		{
			alert('El salario esta vacio');
			$('#salario').focus();
			return false;
		}
		*/
		var valor=$('#DIRECCION').val(); 
		if(valor=='')
		{
			alert('La dirreccion esta vacia');
			$('#DIRECCION').focus();
			return false;
		}
		var valor=$('#MUNICIPIO').val(); 
		if(valor=='')
		{
			alert('El municipio esta vacio');
			$('#MUNICIPIO').focus();
			return false;
		}
		var valor=$('#BARRIO').val();
		if(valor=='')
		{
			alert('El barrio esta vacio');
			$('#BARRIO').focus();
			return false;
		}
		var valor=$('#SECTOR').val();
		if(valor=='')
		{
			alert('El sector esta vacio');
			$('#SECTOR').focus();
			return false;
		}
		var valor=$('#TELEFONO').val();
		if(valor=='')
		{
			alert('El telefono esta vacio');
			$('#TELEFONO').focus();
			return false;
		}
		else
		{
			if(valor.length < 7||valor.length > 12)
			{
				alert("El telefono debe tener minimo 7 numeros y maximo 12");
				return (false)
			}
		}
		var valor=$('#FONDO').val(); 
		if(valor=='')
		{
			alert('Seleccione un fondo');
			$('#FONDO').focus();
			return false;
		}
		var valor=$('#CASA_PROPIA').val();
		 if(valor=='')
		 {
			 alert('Seleccione Casa Propia');
			 $('CASA_PROPIA').focus();
			 return false;
		}
		var valor=$('#TELEFONO_CELULAR').val();
		if(valor=='')
		{
			alert('El numero celular esta vacio');
			$('#TELEFONO_CELULAR').focus();
			return false;
		}
		else
		{
			if(valor.length!=10)
			{
				alert("El numero celular esta incorrecto");
				return (false)
			}
		}
		var valor=$('#EPS').val(); 
		if(valor=='')
		{
			alert('Seleccione una EPS');
			$('#EPS').focus();
			return false;
		}
		/*
		var valor=$('#CORREO').val(); 
		if(valor=='')
		{
			alert('El correo electronico esta vacio');
			$('#CORREO').focus();
			return false;
		}*/
	}
	var OBSERVACIONES=$('#OBSERVACIONES').val(); 
	if(OBSERVACIONES=='')
	{
		alert('Escriba una observacion');
		$('#OBSERVACIONES').focus();
		return false;
	}
	if (confirm('¿Estas seguro de enviar esta informacion?'))
	{ 
		document.tuformulario.submit() 
		return (true)
	}
	else
	{
		return (false);
	} 




}
</script>