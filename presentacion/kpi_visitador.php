<?php
  include("../logica/session.php");
  if($TIPO_ODONTOLOGO_USER == "DENTAL"){
    $tipo = "1";
  }else if($TIPO_ODONTOLOGO_USER == "ORTODONCIA"){
    $tipo = "2";
  }else if($TIPO_ODONTOLOGO_USER == "AMBOS"){
    $tipo = "0";
  }
?>
<?php header('Content-Type: text/html; charset=UTF-8'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css"/>
<link href="css/tablas.css" rel="stylesheet" /> 
<link href="css/bootstrap.css" rel="stylesheet" /> 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<title>KPI'S VISITADOR</title>
<script>
$(document).ready(function() {
    $('#pedidos').DataTable();
} );
</script>

<script language=javascript> 
function ventanaSecundaria (URL)
{ 
   window.open(URL,"ventana1","width=800,height=500,Top=150,Left=50%") 
}
</script> 
<style>
.error
{
  font-size: 130%;
  font-weight: bold;
  color: #fb8305;
  text-transform:uppercase;
  background-color:transparent;
  text-align: center;
  padding:10px;
}
</style>

</head>
<?PHP
include('../datos/conex.php');
if(isset($pedid))
{
  $ID_CLIENTE=base64_decode($pedid);
}
else
{
  $ID_CLIENTE=$ID_CLIENTE;
}
?>
<body>
<?php
if(empty($id_usu)){ ?>
        <span style="margin-top:5%;">
      <center>
      <img src="../presentacion/imagenes/advertencia.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
      <p class="error" style=" width:68.9%; margin:auto auto;">
       LA SESI&Oacute;N HA CADUCADO, POR FAVOR INICIE SESION.
      </p>
      <br />
      <br />
      <center>
          <a href="http://aplicacionesarc.com/3M/" class="btn_continuar">
            <img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" />
          </a>
        </center>
      </center>
      </span>
        
      <?php }else{?>
<div class="container-fluid">
<table align="right" style="width:100%;">
      <tr>  
          <th colspan="6">Bienvenid@  <?php echo $usua?></th> 
          </tr>
          <tr>
            <input name="usua" style="display:none;" type="text" id="usua" value="<?php echo $usua; ?>" readonly/>      
            <td><a href="../presentacion/inicio_visitas.php">
        <img src="../presentacion/imagenes/atras.png" width="56" height="57" id="cambiar" title="ATRAS" >
        </a></td>
        <td><a href="../presentacion/kpi_visitador.php">
        <img src="../presentacion/imagenes/kpi.png" width="56" height="57" id="cambiar" title="KPI'S" >
         <center> <font style="font-size:10px;" >KPI'S</font></center>
        </a></td>
        <td><a href="../presentacion/consultar_gestiones_visitadores.php">
        <img src="../presentacion/imagenes/gestiones.png" width="56" height="57" id="cambiar" title="GESTIONES" >
         <center> <font style="font-size:10px;" >Gestiones</font></center>
        </a></td>
        <td><a href="../presentacion/consulta_pedidos_usuario.php?">
        <img src="../presentacion/imagenes/agn.png" width="56" height="57" id="cambiar" title="CONSULTAR PEDIDOS" >
         <center> <font style="font-size:10px;" >Pedidos</font></center>
        </a></td>
          <td><a href="../presentacion/consulta_proximas_vista.php?">
        <img src="../presentacion/imagenes/calendar.png" width="56" height="57" id="cambiar" title="CONSULTAR PR&Oacute;XIMA VISITA" >
         <center> <font style="font-size:10px;" >Pr&oacute;xima <br />Visita</font></center>
        </a></td>
        <td><a href="../presentacion/consulta_productos_visita.php?">
        <img src="../presentacion/imagenes/productos.png" width="56" height="57" id="cambiar" title="CONSULTAR PEDIDOS" >
         <center> <font style="font-size:10px;" >Productos</font></center>
        </a></td>
         <td><a href="../presentacion/reporte_clientes_nuevos.php">
        <img src="../presentacion/imagenes/excel.png" width="48" height="51" id="cambiar" title="REPORTE EXCEL" ><br />
       <center> <font style="font-size:10px;" >Clientes <br />Nuevos</font></center>
        </a></td>
        </tr>
    </table>
    </div>
    <br />

<br />
<br />
<h3 style="color:#911108;"><strong>MEDICION POR PRECIO</strong></h3><BR>
<form id="cambio_contacto" name="cambio_contacto" method="post" action="#" enctype="multipart/form-data" class="letra">
    <fieldset style="margin:auto auto; width:90%;">
        <div class="col-md-12">
            <div class="col-md-2">
                <label for="mes">MES</label><span class="asterisco">*</span><br />
                    <select class="form-control" name="mes">
                        <option value="">SELECCIONE</option>
                        <option value="Enero">ENERO</option>
                        <option value="Febrero">FEBRERO</option>
                        <option value="Marzo">MARZO</option>
                        <option value="Abril">ABRIL</option>
                        <option value="Mayo">MAYO</option>
                        <option value="Junio">JUNIO</option>
                        <option value="Julio">JULIO</option>
                        <option value="Agosto">AGOSTO</option>
                        <option value="Septiembre">SEPTIEMBRE</option>
                        <option value="Octubre">OCTUBRE</option>
                        <option value="Noviembre">NOVIEMBRE</option>
                        <option value="Diciembre">DICIEMBRE</option>
                    </select>
            </div>
            <div class="col-md-2">
                <label for="año">AÑO</label><span class="asterisco">*</span><br />
                <select class="form-control" name="anio">
                    <option value="">SELECCIONE</option>
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                </select>
            </div>
            <div class="col-md-3">
                <label for="cliente"></label><br /><br />
                    <button title="Consultar" name="consultar"><img src="imagenes/lupa1.png" width="43" height="32" style="background-size:cover" title="CONSULTAR"></button>
                  <br>
                  <br>
            </div>
        </div>
    </fieldset>
</form>

<br />

<?php 
    if (isset($_POST["consultar"])) {

        $MES_Q = $_POST["mes"];
        $ANIO_Q = $_POST["anio"];
?>
      <div class="col-md-6">
<?php 
        $CONSUT_USER=mysql_query("SELECT USER FROM 3m_usuario WHERE ID_USUARIO=".$id_usu,$conex);
        while($USER_DATA=mysql_fetch_array($CONSUT_USER))
        {
          $nombre_usuario=$USER_DATA["USER"];
        }
?>
        <h4>Historico KPI'S de <b> <?php echo $MES_Q; ?></b> del <b> <?php echo $ANIO_Q; ?></b></h4>
        <br>
      </div>
<?php
        if (empty($MES_Q) && empty($ANIO_Q)) {
            echo 'esta vacia la busqueda';
        }
        else if(empty($MES_Q) && empty($ANIO_Q)==false){
            echo 'Por favor utiliza los dos filtros';
        }
        else if(empty($MES_Q)==false && empty($ANIO_Q)){
            echo 'Por favor utiliza los dos filtros';
        }
        else if(empty($MES_Q)==false && empty($ANIO_Q)==false){

    $consult_datos = mysql_query("SELECT * FROM 3m_kpi WHERE MES='".$MES_Q."' AND ANIO='".$ANIO_Q."' AND ID_USUARIO=".$id_usu,$conex);

while($dato=mysql_fetch_array($consult_datos)){
      $CONVERSION_Q                       = $dato["CONVERSION"];
      $PRODUCTO_MES_Q                     = $dato["PRODUCTO_MES"];
      $FECHA_INGRESO_Q                    = $dato["FECHA_INGRESO"];
      $FECHA_ACTUALIZACION_Q              = $dato["MES_CONFECHA_ACTUALIZACIONSULT"];
    }

$consult_academico = mysql_query("SELECT * FROM 3m_kpi_academico WHERE MES_ACADEMICO='".$MES_Q."' AND ANIO_ACADEMICO='".$ANIO_Q."' AND ID_USUARIO=".$id_usu,$conex);
while($dato=mysql_fetch_array($consult_academico)){
      $ACADEMICO_Q                        = $dato["ACADEMICO"];
      $MES_ACADEMICO_Q                    = $dato["MES_ACADEMICO"];
      $ANIO_ACADEMICO_Q                   = $dato["ANIO_ACADEMICO"];
    }

if ($MES_Q=='Enero'){
  $MES_Q="01";
}else if ($MES_Q=='Febrero'){
  $MES_Q="02";
}else if ($MES_Q=='Marzo'){
  $MES_Q="03";
}else if ($MES_Q=='Abril'){
  $MES_Q="04";
}else if ($MES_Q=='Mayo'){
  $MES_Q="05";
}else if ($MES_Q=='Junio'){
  $MES_Q="06";
}else if ($MES_Q=='Julio'){
  $MES_Q="07";
}else if ($MES_Q=='Agosto'){
  $MES_Q='08';
}else if ($MES_Q=='Septiembre'){
  $MES_Q='09';
}else if ($MES_Q=='Octubre'){
  $MES_Q="10";
}else if ($MES_Q=='Noviembre'){
  $MES_Q="11";
}else if ($MES_Q=='Diciembre'){
  $MES_Q="12";
}

$consulta_suma_pedidos = mysql_query("SELECT IF(SUM(TOTAL_PEDIDO)IS NULL,0,SUM(TOTAL_PEDIDO))  AS SUMA FROM 3m_pedido WHERE MONTH(FECHA_PEDIDO)='".$MES_Q."' AND YEAR(FECHA_PEDIDO)='".$ANIO_Q."' AND TIPO_PEDIDO <> 'SUGERIDO' AND ESTADO_PEDIDO<>'RECHAZADO' AND ESTADO_PEDIDO<>'DEVUELTO' AND ESTADO_PEDIDO<>'ELIMINADO' AND ID_USUARIO_FK=".$id_usu,$conex);
while($dato_pedidos=mysql_fetch_array($consulta_suma_pedidos))
  {
    $valor_pedidos_Q = $dato_pedidos["SUMA"];
  }

$consulta_efectivas = mysql_query("SELECT COUNT(*) FROM 3m_gestion WHERE MONTH(FECHA_GESTION)='".$MES_Q."' AND YEAR(FECHA_GESTION)='".$ANIO_Q."' AND TIPIFICACION_GESTION='EFECTIVA' AND ID_ASESOR_GESTION=".$id_usu,$conex);
while($dato_efectivas=mysql_fetch_array($consulta_efectivas))
  {
    $conteo_efectivas_Q = $dato_efectivas["COUNT(*)"];
  }

$consult_novedades=mysql_query("SELECT SUM(numero_novedades) AS SUMA FROM 3m_novedades WHERE id_visitador='".$id_usu."' AND MONTH(fecha_inicio)='".$MES_Q."' AND YEAR(fecha_inicio)='".$ANIO_Q."'",$conex);

while($dato_novedades=mysql_fetch_array($consult_novedades))
  {
    $conteo_novedades_Q = $dato_novedades["SUMA"];
  }
}
  
if ($MES_Q==01){
  $MES_Q='Enero';
}else if ($MES_Q==02){
  $MES_Q='Febrero';
}else if ($MES_Q==03){
  $MES_Q='Marzo';
}else if ($MES_Q==04){
  $MES_Q='Abril';
}else if ($MES_Q==05){
  $MES_Q='Mayo';
}else if ($MES_Q==06){
  $MES_Q='Junio';
}else if ($MES_Q==07){
  $MES_Q='Julio';
}else if ($MES_Q==08){
  $MES_Q='Agosto';
}else if ($MES_Q==09){
  $MES_Q='Septiembre';
}else if ($MES_Q==10){
  $MES_Q='Octubre';
}else if ($MES_Q==11){
  $MES_Q='Noviembre';
}else if ($MES_Q==12){
  $MES_Q='Diciembre';
}

?>

<table width="100%" border="5px" style="border-color: #9E1F1F;
        border-top-style: solid;
        border-right-style: solid;
        border-bottom-style: solid;
        border-left-style: solid;">
 <tr>
   <td width="435px" colspan="4"><h4>&nbsp;<b>Ventas de otros productos:</b></h4>
      <p>
        <h4>
            &nbsp;&nbsp;&nbsp;<span class="label label-danger">
              <?php if($valor_pedidos_Q==0){ echo "$ ". $valor_pedidos_Q; }else{echo "$ ".number_format($valor_pedidos_Q,0,',','.');} ?>
            </span>
        </h4>
      </p>
   </td>
   <td width="435px" colspan="4"><h4>&nbsp;<b># Visitas efectivas:</b></h4>
      <p>
        <h4>
            &nbsp;&nbsp;&nbsp;<span class="label label-danger">
              <?php echo number_format($conteo_efectivas_Q." ",0,',','.'); ?>
            </span>&nbsp;&nbsp;Gestiones
        </h4>
      </p>
   </td>
   <td width="435px" colspan="4"><h4>&nbsp;<b>Novedades:</b></h4>
      <p>
        <h4>
            &nbsp;&nbsp;&nbsp;<span class="label label-danger">
              <?php echo $conteo_novedades_Q; ?>
            </span>&nbsp;&nbsp;&nbsp;
        </h4>
      </p>
   </td>
 </tr>
 <tr>
   <td width="652px" colspan="6"><h4>&nbsp;<b>Conversión:</b></h4>
      <p>
        <h4>
            &nbsp;&nbsp;&nbsp;<span class="label label-danger">
              <?php echo number_format($CONVERSION_Q,0,',','.')." %" ;?>
            </span>
        </h4>
      </p>
   </td>
   
   <td width="652px" colspan="6"><h4>&nbsp;<b>Academico:</b></h4>
      <p>
        <h4>
            &nbsp;&nbsp;&nbsp;<span class="label label-danger">
              <?php echo number_format($ACADEMICO_Q,0,',','.')." %";?></span>&nbsp;&nbsp;&nbsp;
        </h4>
      </p>
   </td>
 </tr>
</table>
<?php }
}
?>

<br>
<br>
<br>
<div style="border: 3px solid #9E1F1F"></div>
<br>
<br>
<br>
<h3 style="color:#911108;"><strong>MEDICION POR UNIDADES</strong></h3><BR>
<form id="cambio_contacto" name="cambio_contacto" method="post" action="#" enctype="multipart/form-data" class="letra">
    <fieldset style="margin:auto auto; width:90%;">
        <div class="col-md-12">
            <div class="col-md-2">
                <label for="mes">MES</label><span class="asterisco">*</span><br />
                    <select class="form-control" name="mes_forecast">
                        <option value="">SELECCIONE</option>
                        <option value="1">ENERO</option>
                        <option value="2">FEBRERO</option>
                        <option value="3">MARZO</option>
                        <option value="4">ABRIL</option>
                        <option value="5">MAYO</option>
                        <option value="6">JUNIO</option>
                        <option value="7">JULIO</option>
                        <option value="8">AGOSTO</option>
                        <option value="9">SEPTIEMBRE</option>
                        <option value="10">OCTUBRE</option>
                        <option value="11">NOVIEMBRE</option>
                        <option value="12">DICIEMBRE</option>
                    </select>
            </div>
            <div class="col-md-2">
                <label for="año">AÑO</label><span class="asterisco">*</span><br />
                <select class="form-control" name="anio_forecast">
                    <option value="">SELECCIONE</option>
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                </select>
            </div>
            <?php 
            if($TIPO_ODONTOLOGO_USER == "AMBOS"){ ?>
              <div class="col-md-2">
                <label for="producto">TIPO PRODUCTO</label><span class="asterisco">*</span><br />
                <select class="form-control" name="tipo_producto">
                    <option value="">SELECCIONE</option>
                    <option value="1">Dental</option>
                    <option value="2">Ortodoncia</option>
                </select>
            </div>
            <?php
            }
            ?>
            <div class="col-md-3">
                <label for="cliente"></label><br /><br />
                    <button title="Consultar" name="consultar_forecast"><img src="imagenes/lupa1.png" width="43" height="32" style="background-size:cover" title="CONSULTAR"></button>
                  <br>
                  <br>
            </div>
        </div>
    </fieldset>
</form>

<br>
<?php 
if (isset($_POST["consultar_forecast"])) {
    $mes_forecast   = $_POST["mes_forecast"];
    $anio_forecast  = $_POST["anio_forecast"];

    if ($TIPO_ODONTOLOGO_USER == "AMBOS"){
      $tipo_odonto = $_POST["tipo_producto"];
    }
  ?>
<div class="table table-responsive">
	<br>
<table style="width:99%; margin:auto auto;" class="table table-responsive" >
        <tr>
            <th colspan='11' class="principal">FORECAST</th>
        </tr>
        </table>
    <br />
      <table style="width:99%; margin:auto auto; text-align:center;" id="gestion">
        <thead>
        <tr>
    		    <th class="TITULO">DESCRIPCION</th>
            <th class="TITULO">CANTIDAD TOTAL DE PRODUCTOS</th>
            <?php
            if($TIPO_ODONTOLOGO_USER == "AMBOS"){ ?>
              <th class="TITULO">TIPO DE PRODUCTO</th>
            <?php }
            ?>
        </tr>
        </thead>
        <tbody>
    <?php
    $query_conteo = mysql_query("SELECT DISTINCT F.ID_FORECAST, F.FORECAST, F.TIPO
    FROM 3m_detalle_pedido AS DP
    INNER JOIN 3m_categoria AS C ON C.STOCK = DP.ID_CATEGORIA_FK_2
    INNER JOIN 3m_detalle_forecast AS DF ON DF.STOCK_FORECAST = C.STOCK
    INNER JOIN 3m_forecast AS F ON F.ID_FORECAST = C.FORECAST
    WHERE DP.ID_USUARIO_FK_2='$id_usu' AND MONTH(DP.FECHA_PEDIDO)='$mes_forecast' AND YEAR(DP.FECHA_PEDIDO)='$anio_forecast'",$conex);
    
    while ($a = mysql_fetch_array($query_conteo)) {
      $descripcion_cont = $a["FORECAST"];
      $descripcion_id = $a["ID_FORECAST"];
      $tipo_odontolo = $a["TIPO"];
      if($tipo_odontolo=="1"){
        $tipo_producto = "DENTAL";
      }else if($tipo_odontolo=="2"){
        $tipo_producto = "ORTODONCIA";
      }
      
    $query=mysql_query("SELECT SUM(DP.CANTIDAD_PRODUCTO * DF.CANTIDAD) AS SUMA 
    FROM 3m_detalle_pedido AS DP
    INNER JOIN 3m_categoria AS C ON C.STOCK = DP.ID_CATEGORIA_FK_2
    INNER JOIN 3m_detalle_forecast AS DF ON DF.STOCK_FORECAST = C.STOCK
    WHERE C.FORECAST='$descripcion_id' AND DP.ID_USUARIO_FK_2='$id_usu' AND MONTH(DP.FECHA_PEDIDO)='$mes_forecast' AND YEAR(DP.FECHA_PEDIDO)='$anio_forecast'",$conex);
    
        while ($datos = mysql_fetch_array($query)) {
        ?>
            <tr class="datos" >
           	    <td style="text-align:center;"><?php echo $descripcion_cont; ?></td>
           	    <td style="text-align:center;"><?php echo $datos["SUMA"]; ?></td>
                <?php if($TIPO_ODONTOLOGO_USER == "AMBOS"){ ?>
                  <td style="text-align:center;"><?php echo $tipo_producto; ?></td>
                <?php } ?>
            </tr>
            <?php }  
      }

      $query_array = mysql_query("SELECT DISTINCT F.FORECAST
        FROM 3m_detalle_pedido AS DP
        INNER JOIN 3m_categoria AS C ON C.STOCK = DP.ID_CATEGORIA_FK_2
        INNER JOIN 3m_detalle_forecast AS DF ON DF.STOCK_FORECAST = C.STOCK
        INNER JOIN 3m_forecast AS F ON F.ID_FORECAST = C.FORECAST
        WHERE DP.ID_USUARIO_FK_2='$id_usu' AND MONTH(DP.FECHA_PEDIDO)='$mes_forecast' AND YEAR(DP.FECHA_PEDIDO)='$anio_forecast'",$conex);

        while( $fila = mysql_fetch_assoc( $query_array )){
          $nuevo_array[] = $fila["FORECAST"];
        }
        $i=0;
            for($i >= 0; $i <= 20; $i++){
              $condicion = "FORECAST <> '$nuevo_array[0]' AND FORECAST <> '$nuevo_array[1]' AND FORECAST <> '$nuevo_array[2]' 
              AND FORECAST <> '$nuevo_array[3]' AND FORECAST <> '$nuevo_array[4]' AND FORECAST <> '$nuevo_array[5]' 
              AND FORECAST <> '$nuevo_array[6]' AND FORECAST <> '$nuevo_array[7]' AND FORECAST <> '$nuevo_array[8]' 
              AND FORECAST <> '$nuevo_array[9]' AND FORECAST <> '$nuevo_array[10]' AND FORECAST <> '$nuevo_array[11]' 
              AND FORECAST <> '$nuevo_array[12]' AND FORECAST <> '$nuevo_array[13]' AND FORECAST <> '$nuevo_array[14]' 
              AND FORECAST <> '$nuevo_array[15]' AND FORECAST <> '$nuevo_array[16]' AND FORECAST <> '$nuevo_array[17]' 
              AND FORECAST <> '$nuevo_array[18]' AND FORECAST <> '$nuevo_array[19]' ";
            }
        
        if($TIPO_ODONTOLOGO_USER == "DENTAL" || $TIPO_ODONTOLOGO_USER == "ORTODONCIA"){
          $query_ceros = mysql_query("SELECT * FROM 3m_forecast WHERE $condicion AND TIPO='$tipo'",$conex);
        }else if ($TIPO_ODONTOLOGO_USER == "AMBOS"){
          $query_ceros = mysql_query("SELECT * FROM 3m_forecast WHERE $condicion AND TIPO='$tipo_odonto'",$conex);
        }
      
      while ($datos_cero = mysql_fetch_array($query_ceros)) {
          $imprim = $datos_cero["FORECAST"]; 
          $tipo_odontolo_1 = $datos_cero["TIPO"];
          if($tipo_odontolo_1=="1"){
            $tipo_producto_1 = "DENTAL";
          }else if($tipo_odontolo_1=="2"){
            $tipo_producto_1 = "ORTODONCIA";
          } ?>
          <tr class="datos">
              <td style="text-align:center;"><?php echo $imprim; ?></td>
              <td style="text-align:center;">0</td>
              <?php if($TIPO_ODONTOLOGO_USER == "AMBOS"){ ?>
                <td style="text-align:center;"><?php echo $tipo_producto_1; ?></td>
              <?php } ?>
          </tr>
      <?php 
      }
      ?>
  </tbody>
  </table>
</div>

<?php
}
?>
</body>
</html>