<?php
	
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Reporte_conversion.xls"');
header('Cache-Control: max-age=0');
	
include('../datos/conex.php');
	
	$consulta_clientes = mysql_query("SELECT a.ID_CLIENTE, CONCAT(NOMBRE_CLIENTE,' ',APELLIDO_CLIENTE)AS cliente,c.ID_PEDIDO,c.FECHA_PEDIDO,c.TOTAL_PEDIDO,c.FORMA_PAGO,c.ESTADO_PEDIDO,c.TIPO_PEDIDO,d.TIPO_PRODUCTO,d.BASE_CONVERSION,d.CONVERSION,a.VALOR_CONVERSION
FROM 3m_consolidado_conversion AS a
INNER JOIN 3m_cliente AS b ON b.ID_CLIENTE = a.ID_CLIENTE
INNER JOIN 3m_pedido AS c ON c.ID_PEDIDO = a.ID_PEDIDO
INNER JOIN 3m_conversion AS d ON d.ID_CONVERSION = a.ID_CONVERSION
",$conex); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<table>
    <tr>
        <th>ID_CLIENTE</th>
        <th>CLIENTE</th>
        <th>ID_PEDIDO</th>
        <th>FECHA_PEDIDO</th> 
		<th>TOTAL_PEDIDO</th> 
        <th>FORMA_PAGO</th>
        <th>ESTADO_PEDIDO</th>
        <th>TIPO_PEDIDO</th>
        <th>TIPO_PRODUCTO</th>
        <th>BASE_CONVERSION</th>
        <th>CONVERSION</th>
        <th>VALOR_CONVERSION</th>
    </tr>
    <?php 
     while ($fila = mysql_fetch_array($consulta_clientes)) { ?>
        <tr>
            <td><?php echo $fila['ID_CLIENTE'] ?></td>
            <td><?php echo $fila['cliente'] ?></td>
            <td><?php echo $fila['ID_PEDIDO'] ?></td>
            <td><?php echo $fila['FECHA_PEDIDO'] ?></td>
			<td><?php echo $fila['TOTAL_PEDIDO'] ?></td>
            <td><?php echo $fila['FORMA_PAGO'] ?></td>
            <td><?php echo $fila['ESTADO_PEDIDO'] ?></td>
            <td><?php echo $fila['TIPO_PEDIDO'] ?></td>
            <td><?php echo $fila['TIPO_PRODUCTO'] ?></td>
            <td><?php echo $fila['BASE_CONVERSION'] ?></td>
            <td><?php echo $fila['CONVERSION'] ?></td>
            <td><?php echo $fila['VALOR_CONVERSION'] ?></td>
        </tr>  
     <?php }
    
    ?>
</table>
	 