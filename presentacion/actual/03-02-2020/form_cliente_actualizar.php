<?php
	include("../logica/session.php");
        header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" http-equiv="conten-type" content="width=device-width, user-scalable=no , initial-scale=1.0, maximun-scale=1.0, minimum-scale=1.0, text/html; charset=UTF-8" />
<link href="css/paciente_nuevo.css" type="text/css" rel="stylesheet" />
<link href="css/validacion_campos.css"type="text/css" rel="stylesheet" />
<script src="https://unpkg.com/sweetalert2@7.1.3/dist/sweetalert2.all.js"></script>
<script src="js/jquery.js"></script>
<!--GEOLOCALIZACION-->
<style>
.error
{
	font-size: 130%;
	font-weight: bold;
	color: #fb8305;
	text-transform:uppercase;
	background-color:transparent;
	text-align: center;
	padding:10px;
}
</style>
<script>
function obtener()
{
	var parametros={enableHighAccuracy:true}
	navigator.geolocation.watchPosition(mostrar_posicion,gestion_errores,parametros);
}
function mostrar_posicion(posicion)
{
	var ubicacion=document.getElementById("UBICACION");
	var latitud=posicion.coords.latitude;
	var longitud=posicion.coords.longitude;
	var exactitud=posicion.coords.accuracy;
	/*alert(exactitud);*/
	document.getElementById("LAT").value=latitud;
	document.getElementById("lon").value=longitud;	
}
function gestion_errores(error)
{
	if(error.code==1)
	{
		swal(
		  {title: 'Debes permitir el uso de la geolocalizacion en tu navegador',
		  confirmButtonColor: '#BC3228'});
	}
}
</script>
<!--AJAX-->
<script>
function ciudades()
{
	var DEPT=$('#departamento').val();
	//alert(DEPT)
	$.ajax(
	{
		url:'../presentacion/consulta_ciudades.php',
		data:
		{
			DEPT: DEPT,
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#ciudad").attr('disabled','disabled');
		},
		success: function(data)
		{
			$('#ciudad').html(data);
			$("#ciudad").removeAttr('disabled');
		}
	})
}

function especialidades()
{
	var tipo_odontologo=$('#tipo_odontologo').val();
	
	//alert(DEPT)
	$.ajax(
	{
		url:'../presentacion/consulta_especialidad.php',
		data:
		{
			tipo_odontologo: tipo_odontologo
		},
		type: 'post',
		
		success: function(data)
		{
			$('#tipo_especialidad').html(data);
		}
	})
}
$(document).ready(function()
{
	$('#departamento').change(function()
	{
		ciudades();
	});
        $('#tipo_odontologo').change(function()
	{
		especialidades();
	});
        
});
</script>
</head>
<?php
include('../datos/conex.php');

$ID_CLIENTE;
$consulta=mysql_query("SELECT * FROM 3m_cliente WHERE ID_CLIENTE = '$ID_CLIENTE'",$conex);
 echo mysql_error($conex);
 
 while($dato=mysql_fetch_array($consulta))
	{		
		$ID_CLIENTE = $dato["ID_CLIENTE"];	
		$NOMBRE_CLIENTE = $dato["NOMBRE_CLIENTE"];
		$APELLIDO_CLIENTE = $dato["APELLIDO_CLIENTE"];
		$TIPO_IDENTIFICACION = $dato["TIPO_IDENTIFICACION"];
		$IDENTIFICACION_CLIENTE = $dato["IDENTIFICACION_CLIENTE"];
		$DIRECCION_CLIENTE = $dato["DIRECCION_CLIENTE"];
		$DEPARTAMENTO_CLIENTE = $dato["DEPARTAMENTO_CLIENTE"];
		$CIUDAD_CLIENTE = $dato["CIUDAD_CLIENTE"];
		$CELULAR_CLIENTE = $dato["CELULAR_CLIENTE"];
		$TELEFONO_CLIENTE = $dato["TELEFONO_CLIENTE"];
		$EMAIL_CLIENTE = $dato["EMAIL_CLIENTE"];
		$RAZON_SOCIAL = $dato["RAZON_SOCIAL"];
		$NIT = $dato["NIT"]; 
		$PAGINA_WEB_NEGOCIO = $dato["PAGINA_WEB_NEGOCIO"];
		$No_EMPLEADOS = $dato["No_EMPLEADOS"];		
	}
	
$encuesta = mysql_query("SELECT DISTRIBUIDOR_COMPRA,ESPECIALIDAD_ODONTOLOGO,TIPO_ODONTOLOGO, ID_CLIENTE_FK_5 FROM 3m_encuesta WHERE ID_CLIENTE_FK_5 = '$ID_CLIENTE'",$conex);
 echo mysql_error($conex);
 
 while($dato=mysql_fetch_array($encuesta))
	{
		$DISTRIBUIDOR_COMPRA = $dato["DISTRIBUIDOR_COMPRA"]; 
		$ESPECIALIDAD_ODONTOLOGO = $dato["ESPECIALIDAD_ODONTOLOGO"]; 
		$TIPO_ODONTOLOGO= $dato["TIPO_ODONTOLOGO"]; 
	}
      
        if(empty($TIPO_ODONTOLOGO)){
            $TIPO_ODONTOLOGO='';
            $DESCRIPCION_TIPO='';
        }
        else{
                if($TIPO_ODONTOLOGO==1){
                    $DESCRIPCION_TIPO='Odontologo';
                }
                else if($TIPO_ODONTOLOGO==2){
                    $DESCRIPCION_TIPO='Ortodoncista';
                }
        }
        
        if(empty($ESPECIALIDAD_ODONTOLOGO)){
            $ESPECIALIDAD_ODONTOLOGO='';
        }
 
if($id_usu!=''&&$id_usu!='0')
{
?>
<body onload="obtener()">
<?php
if(empty($id_usu)){ ?>
				<span style="margin-top:5%;">
			<center>
			<img src="../presentacion/imagenes/advertencia.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
			<p class="error" style=" width:68.9%; margin:auto auto;">
			 LA SESI&Oacute;N HA CADUCADO, POR FAVOR INICIE SESION.
			</p>
			<br />
			<br />
			
			</center>
			</span>
				
			<?php }else{?>
<table align="right">
    	<tr>
            <th>Bienvenid@  <?php echo $usua;?></th>
            <td><a href="../presentacion/inicio_visitas.php">
        <a href="../presentacion/inicio_consulta_cliente.php?x=<?php echo base64_encode($ID_CLIENTE)?>">
        <img src="../presentacion/imagenes/atras.png" width="56" height="57" id="cambiar" title="ATRAS" ><span style="color:#000;">
        </a></td>
        <td><a href="../presentacion/consulta_pedidos_usuario.php?">
        <img src="../presentacion/imagenes/agn.png" width="56" height="57" id="cambiar" title="CONSULTAR PEDIDOS" ><span style="color:#000;">
        </a></td>
        </tr>
    </table>
    <br />
    <br />
    <br />
<center><img src="../presentacion/imagenes/3m.png" width="24%" height="17%" /></center>
<form id="tranferencias" name="tranferencias" action="../logica/actualizar_cliente.php" method="post" style="width:100%;" enctype="multipart/form-data">
	<div class="form-group" style="display:none">
        <input id="LAT" name="LAT" type="text" class="form-control" autocomplete="off"/>
        <input id="lon" name="lon" type="text" class="form-control" autocomplete="off"/>
       
    </div>
	<header>
        <div class="container" id="div_header">
        	<center>
        	  <h1>ODONTOLOGO</h1></center>
        </div>
    </header>
    <div class="form-group">
        <label for="cliente">Apellido</label><span class="asterisco">*</span>
        <br>
        <input name="apellido" type="text" required="required" class="form-control" id="apellido" placeholder="Apellido" value="<?php echo $APELLIDO_CLIENTE;?>"/>
    </div>
    <div class="form-group">
        <label for="cliente">Nombre</label><span class="asterisco">*</span>
        <br>
        <input name="nombre" type="text" required="required" class="form-control" id="nombre" placeholder="Nombre" value="<?php echo $NOMBRE_CLIENTE;?>"/>
    </div>
    <div class="form-group">
        <label for="cliente">Tipo de Identificaci&oacute;n</label><span class="asterisco">*</span>
        <br>
        <select name="tipo_identificacion" id="tipo_identificacion" class="form-control"  required>
        <option><?php echo $TIPO_IDENTIFICACION;?></option>
            <option value="CC">Cedula de Ciudadania</option>
            <option value="CE">Cedula de Extrajeria</option>
        </select>
    </div>
    <div class="form-group">
        <label for="cliente">N&uacute;mero de Identificaci&oacute;n</label><span class="asterisco">*</span>
        <br>
        <input name="identificacion" type="text" required="required" class="form-control" id="identificacion" placeholder="identificacion" value="<?php echo $IDENTIFICACION_CLIENTE;?>" maxlength="20"/>
    </div>
    <div class="form-group">
        <label for="cliente">Direcci&oacute;n Residencial</label><span class="asterisco">*</span>
        <br>
        <input name="direccion" type="text" required="required" class="form-control" id="direccion" placeholder="Direccion" value="<?php echo $DIRECCION_CLIENTE;?>"/>
    </div>
    <div class="form-group">
        <label for="cliente">Departamento</label><span class="asterisco">*</span>
        <br>
        <select name="departamento" id="departamento" class="form-control" required>
        	<option><?php echo $DEPARTAMENTO_CLIENTE; ?></option>
        	<?php
			$consulta_departamento=mysql_query("SELECT * FROM 3m_departamento WHERE NOMBRE_DEPARTAMENTO!='' ORDER BY NOMBRE_DEPARTAMENTO ASC",$conex);
			echo mysql_error($conex);
			while($datos=mysql_fetch_array($consulta_departamento))
			{
			?>
        		<option><?php echo $datos['NOMBRE_DEPARTAMENTO'] ?></option>
            <?php
			}
            ?>
        </select>
    </div>
    <div class="form-group">
        <label for="cliente">Ciudad</label><span class="asterisco">*</span>
        <br>
        <select name="ciudad" id="ciudad" class="form-control" required>
        <option><?php echo $CIUDAD_CLIENTE;?></option>
        </select>
    </div>
    <div class="form-group">
        <label for="cliente">Celular</label>
        <br>
        <input name="celular" type="tel" class="form-control" id="celular" placeholder="Celular" pattern="[0-9]{10}" value="<?php echo $CELULAR_CLIENTE; ?>"  />
    </div>
    <div class="form-group">
        <label for="cliente">Otro n&uacute;mero</label>
        <br>
        <input name="telefono" type="tel" class="form-control" id="telefono" placeholder="Otro n&uacute;mero" value="<?php echo $TELEFONO_CLIENTE; ?>"/>
    </div>
    <div class="form-group">
        <label for="cliente">E-mail</label>
        <br>
        <input name="email" type="email" class="form-control" id="email" placeholder="E-mail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Ej: ejemplo@ejemplo.com" value="<?php echo $EMAIL_CLIENTE; ?>" required/>
    </div>
    
     <div class="form-group">
         <label for="tipo_odontologo">Tipo de odontologo</label><span class="asterisco">*</span>
        <br>
            <select name="tipo_odontologo" id="tipo_odontologo" class="form-control" required="required">
                <option value='<?php echo $TIPO_ODONTOLOGO; ?>'><?php echo $DESCRIPCION_TIPO; ?></option>
                <?php 
                  $consulta_tipo =mysql_query("SELECT  id_tipo,detalle_tipo FROM 3m_tipo_odontologo",$conex);
                  while($dato_tipo=mysql_fetch_array($consulta_tipo)) { ?>
                <option value="<?php echo $dato_tipo['id_tipo']; ?>"><?php echo $dato_tipo['detalle_tipo']; ?></option>
                <?php } ?>
            </select>
    </div>
     <div class="form-group">
         <label for="tipo_especialidad">Especialidad</label><span class="asterisco">*</span>
        <br>
            <select name="tipo_especialidad" id="tipo_especialidad" class="form-control" required="required" >
                <option value="<?php echo $ESPECIALIDAD_ODONTOLOGO ?>"><?php echo $ESPECIALIDAD_ODONTOLOGO ?></option>
            </select>
        </br>
     </div>
    
    
    <div class="form-group">
        <label for="cliente">Distribuidor que compra</label><span class="asterisco">*</span>
        <br>
        <select name="distribuidor" id="distribuidor" class="form-control" required>        	
		<option><?php echo $DISTRIBUIDOR_COMPRA;?></option>

         <?php 
                        $consulta =mysql_query("
         SELECT DISTINCT d.NOMBRE_DENTAL, u.USER, u.CIUDAD
FROM 3m_dentales AS d
INNER JOIN 3m_usuario AS u ON d.CIUDAD_DENTAL=u.CIUDAD
WHERE ID_USUARIO='".$id_usu."' AND ID_DENTAL <> 5 ORDER BY NOMBRE_DENTAL ASC;",$conex);
                 
            if($id_usu!=2){  
			
                  while($dato=mysql_fetch_array($consulta)) {
                      $ciudad = $dato['CIUDAD']; ?>
                <option value="<?php echo $dato['NOMBRE_DENTAL']; ?>"><?php echo $dato['NOMBRE_DENTAL']; ?></option>
                <?php } //ANDRES CHACON MANEJA ORTODONCIA
			   if($ciudad !='ANTIOQUIA'){ ?>
				<option>Dental ALDENTAL</option>
			<?php }
            if ($id_usu==19 || $id_usu==27 || $id_usu==9){?>
            <option>Bracket Ltda-Andres Beltran</option>
			<?php }
			if ($id_usu==7){ ?>
			<option>Dental Nader Villavicencio-Jhovanny Parra</option>
			<option>Bracket Ltda-Andres Beltran</option>
			<option>Luz Elena Sanchez-Casa Dental Sede Norte</option>
			<?php }
			if($id_usu==19){//AINFANTES QUE MANEJA BOGOTA, VILLLAVICENCIO Y FLORENCIA CAQUETA?>
			<option>Dental Nader Villavicencio-Jhovanny Parra</option>	
			<option>Jose Alejo Rodriguez Barragan - Dental Jar</option>
            <option>Luz Karime Zanabria-Vertice S.A.S</option>
			<?php }

			if($id_usu==30){//SACASTELLANOS QUE MANEJA BOGOTA Y VILLLAVICENCIO?>
			<option>Dental Nader Villavicencio-Jhovanny Parra</option>	
			<?php }
			if($id_usu==33){//dlozano ?>
            <option>ODONTOSOLUCIONES-MARIA ISABEL VILLABONA</option>
            <option>Luz Karime Zanabria-Vertice S.A.S</option>
            <?php }
			if($id_usu==29){//fvanstrahlen ?>
			<option>Luz Karime Zanabria-Vertice S.A.S</option>	
			<?php }
			if($id_usu==31){//ALGARCIA MANEJA CIUDAD ARMENIA?>
            <option>Equidentales</option>  
			
            <?php }
			 
			if($id_usu==26){ // usuario visita ?>
			<option>Faride Maestre-Alfa Representaciones dentales</option>					
			<option>Ana Bueno-Elegir Soluciones</option>
			<?php }
			if($id_usu==16){?> 
			<option>Paola Linares-Dental 83 Portafolio 3M ESPE</option>
			<?php }
			if($id_usu==21){?>
			<option>Luz Elena Sanchez-Casa Dental Sede Norte</option>
			<?php }
			if($id_usu==31){?>
			<option>Yina Moncayo-Dental Palermo</option>
			<?php }
			if($id_usu==36){ // lguevara ?> 
            <option>Faiber Laverde-Dentales Y Dentales</option>
            <option>Dental Nader Bogota</option>
            <option>Yina Moncayo-Dental Palermo</option>
			<option>Paola Linares-Dental 83 Portafolio 3M ESPE</option>
			<option>Luz Karime Zanabria-Vertice S.A.S</option>

            <?php }

            //se habilita distribuidor de Vertice a la visitadora SACASTELLANOS
            if($id_usu==30){?>
            <option>Luz Karime Zanabria-Vertice S.A.S</option>
            <?php }

			if($id_usu==9){?> 
			<option>Paola Linares-Dental 83 Portafolio 3M ESPE</option>
			<option>Luz Karime Zanabria-Vertice S.A.S</option>
			<?php }

			if($id_usu>0){ // arestrepo  $id_usu==37 ?> 
            <option>Bracket Ltda-Andres Beltran</option>
			<option>Paola Linares-Dental 83 Portafolio 3M ESPE</option>
            <?php }
			if($id_usu==30){
				$consulta_dentales= mysql_query("SELECT DISTINCT d.NOMBRE_DENTAL, u.USER, u.CIUDAD
				FROM 3m_dentales AS d
				INNER JOIN 3m_usuario AS u ON d.CIUDAD_DENTAL=u.CIUDAD
				WHERE u.CIUDAD='BOGOTA' GROUP BY ID_DENTAL ORDER BY NOMBRE_DENTAL ASC",$conex);
				 while($dato=mysql_fetch_array($consulta_dentales)) { ?>
                <option value="<?php echo $dato['NOMBRE_DENTAL']; ?>"><?php echo $dato['NOMBRE_DENTAL']; ?></option>
                <?php } 
			}
			}
			if($id_usu==2){
				
				$consulta_dentales= mysql_query("SELECT DISTINCT d.NOMBRE_DENTAL, u.USER, u.CIUDAD
				FROM 3m_dentales AS d
				INNER JOIN 3m_usuario AS u ON d.CIUDAD_DENTAL=u.CIUDAD
				 GROUP BY ID_DENTAL ORDER BY NOMBRE_DENTAL ASC",$conex);
				 while($dato=mysql_fetch_array($consulta_dentales)) { ?>
                <option value="<?php echo $dato['NOMBRE_DENTAL']; ?>"><?php echo $dato['NOMBRE_DENTAL']; ?></option>
                <?php } 
			}

			?>
			
        </select>
    </div>
   
    <br />
    <div class="form-group">
            <center>
            	<input name="confirmar" type="submit" id="confirmar" value="GUARDAR" class="form-control btn" onclick="return validar(tranferencias)"/>
            </center>
    </div>
    <br />
</form>
<?php } ?>
</body>
</html>

<?php
}
?>