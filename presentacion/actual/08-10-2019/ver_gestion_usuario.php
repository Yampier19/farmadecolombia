<?php
error_reporting(0);
include ('../logica/session.php');
header('Content-Type: text/html; charset=UTF-8');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>CONSULTAR GESTION</title>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.dataTables.js"></script>
        <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css"/>
        <link href="css/tablas.css" rel="stylesheet" /> 
        <link href="css/bootstrap.css" rel="stylesheet" />

        <script>
            $(document).ready(function () {
                $('#gestion').DataTable();


                $(".botonExcel").click(function (event) {
                    $("#datos_a_enviar").val($("<table>").append($("#gestion_filtro").eq(0).clone()).html());
                    $("#FormularioExportacion").submit();
                });

            });


            function ventanaSecundaria(URL)
            {
                window.open(URL, "ventana1", "width=800,height=500,Top=150,Left=50%")
            }
            function noback() {
                window.location.hash = "no-back-button";
                window.location.hash = "Again-No-back-button"

            }
        </script>
    </head>

    <?php
    include('../datos/conex.php');
    mysql_set_charset('utf8');
    $string_intro = getenv("QUERY_STRING");
    parse_str($string_intro);
    $usua;
    ?>
    <body onload="noback()">
        <br /><br />

        <form id="cambio_contacto" name="cambio_contacto" method="post" action="#" enctype="multipart/form-data" class="letra">
            <fieldset style="margin:auto auto; width:90%;">
                <div class="col-md-12">

                    <div class="col-md-2">
                        <label for="año">AÑO</label><span class="asterisco">*</span><br />
                        <select class="form-control" name="anio">
                            <option value="">SELECCIONE</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>

                        </select>
                    </div>

                    <div class="col-md-2">
                        <label for="mes">MES</label><span class="asterisco">*</span><br />
                        <select class="form-control" name="mes">
                            <option value="">SELECCIONE</option>
                            <option value="1">ENERO</option>
                            <option value="2">FEBRERO</option>
                            <option value="3">MARZO</option>
                            <option value="4">ABRIL</option>
                            <option value="5">MAYO</option>
                            <option value="6">JUNIO</option>
                            <option value="7">JULIO</option>
                            <option value="8">AGOSTO</option>
                            <option value="9">SEPTIEMBRE</option>
                            <option value="10">OCTUBRE</option>
                            <option value="11">NOVIEMBRE</option>
                            <option value="12">DICIEMBRE</option>
                        </select>
                    </div>

                    <div class="col-md-2">
                        <label for="cliente">FECHA GESTI&Oacute;N:</label><span class="asterisco">*</span><br />
                        <input type="date" class="form-control" name="fecha" id="fecha" max="<?php echo date("Y-m-d"); ?>"/>
                    </div>
                    <div class="col-md-4">
                        <label for="fecha">USUARIO:</label><span class="asterisco">*</span><br />
                        <select class="form-control" name="idUsuario[]" multiple="multiple"> 
                            <option value="" disabled="disabled" selected="selected">Seleccione</option>           
                            <?php
                            $consulta = mysql_query("
         SELECT ID_USUARIO, USER FROM 3m_usuario
		 WHERE PRIVILEGIOS =2 AND ESTADO =1 AND USER <> 'VISITA' AND USER <> 'MEDELLIN' AND USER <> 'emesa' AND
			 USER <> 'cali'  
		  ORDER BY USER ASC ;", $conex);


                            while ($dato = mysql_fetch_array($consulta)) {
                                ?>
                                <option value="<?php echo $dato['ID_USUARIO']; ?>"><?php echo $dato['USER']; ?></option>
<?php } ?>

                        </select>
                    </div>
                    <div class="col-md-2">
                        <label for="fecha">TIPIFICACI&Oacute;N</label><span class="asterisco">*</span><br />
                        <select id="tipificacion" name="tipificacion" class="form-control">
                            <option value="">Seleccione</option>
                            <option value="EFECTIVA">EFECTIVA</option>
                            <option value="NO EFECTIVA">NO EFECTIVA</option>
                        </select>
                    </div>

                    <div class="col-md-2">
                        <label for="fecha">CIUDAD</label><span class="asterisco">*</span><br />
                        <select id="ciudad" name="ciudad" class="form-control">
                            <option value="">Seleccione</option>
                            <?PHP
                            $consulta_ciudades = mysql_query("SELECT DISTINCT IF(CIUDAD ='CARTAGENA' OR CIUDAD='BARRANQUILLA','COSTA',CIUDAD) AS CIUDAD_USUARIO FROM 3m_usuario WHERE CIUDAD IS NOT NULL GROUP BY CIUDAD ", $conex);
                            while ($dato = mysql_fetch_array($consulta_ciudades)) {
                                ?>
                                <option value="<?php echo $dato['CIUDAD_USUARIO']; ?>"><?php echo $dato['CIUDAD_USUARIO']; ?></option>
<?php } ?>
                            ?>

                        </select>
                    </div>

                    <div class="col-md-3">
                        <label for="cliente"></label><br /><br />
                        <button title="Consultar" name="consultar"><img src="imagenes/lupa1.png" width="43" height="32" style="background-size:cover" title="CONSULTAR"></button>


                    </div>
                </div>

            </fieldset>

        </form>
        <div class="table table-responsive">
            <?php
            if (isset($_POST["consultar"])) {
                $idUsuario = $_POST["idUsuario"];
                $fecha = $_POST["fecha"];
                $tipificacion = $_POST["tipificacion"];
                $mes = $_POST["mes"];
                $anio = $_POST["anio"];
                $ciudad = $_POST["ciudad"];

                $usuarios_seleccionados = implode(',', $idUsuario);

                if (empty($idUsuario) && empty($fecha) && empty($tipificacion) && empty($mes) && empty($anio) && empty($ciudad)) {
                    echo 'esta vacia la busqueda';
                }
                //BUSQUEDA USUARIO
                else if (empty($idUsuario) == false && empty($fecha) && empty($tipificacion) && empty($mes) && empty($anio) && empty($ciudad)) {
                    $consultaGestionUsuario = mysql_query("
			  SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICACION_ODONTOLOGO
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_encuesta AS c ON c.ID_CLIENTE_FK_5 = b.ID_CLIENTE 
			INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE ID_ASESOR_GESTION IN ($usuarios_seleccionados)
			 AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' AND
			 ASESOR_GESTION <> 'cali' 
			 ORDER BY a.FECHA_GESTION DESC LIMIT 3000", $conex);
                    ?>
            <script>
                $(document).ready(function () {
                $('#FormularioExportacion').css("display","none");
                });
            </script>
            <?php 
                }
                //BUSQUEDA FECHA 
                else if (empty($idUsuario) && empty($fecha) == false && empty($tipificacion) && empty($mes) && empty($anio) && empty($ciudad)) {
                    $consultaGestionUsuario = mysql_query("
			   SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICACION_ODONTOLOGO
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_encuesta AS c ON c.ID_CLIENTE_FK_5 = b.ID_CLIENTE 
			 INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE DATE(a.FECHA_GESTION)='" . $fecha . "'
			 AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' AND
			 ASESOR_GESTION <> 'cali' 
			 ORDER BY a.FECHA_GESTION DESC LIMIT 3000", $conex);
                   ?>
            <script>
                $(document).ready(function () {
                $('#FormularioExportacion').css("display","none");
                });
            </script>
                <?php }
                //BUSQUEDA TIPIFICACION	
                else if (empty($idUsuario) && empty($fecha) && empty($tipificacion) == false && empty($mes) && empty($anio) && empty($ciudad)) {
                    $consultaGestionUsuario = mysql_query("
			     SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICACION_ODONTOLOGO
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_encuesta AS c ON c.ID_CLIENTE_FK_5 = b.ID_CLIENTE 
			 INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE TIPIFICACION_GESTION='" . $tipificacion . "'
			 
			 ORDER BY a.FECHA_GESTION DESC LIMIT 3000", $conex);
                
                   ?>
            <script>
                $(document).ready(function () {
                $('#FormularioExportacion').css("display","none");
                });
            </script>
                <?php }
                //BUSQUEDA MES
                else if (empty($idUsuario) && empty($fecha) && empty($tipificacion) && empty($mes) == false && empty($anio) && empty($ciudad)) {
                    $consultaGestionUsuario = mysql_query("
			   SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICACION_ODONTOLOGO
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_encuesta AS c ON c.ID_CLIENTE_FK_5 = b.ID_CLIENTE 
			 INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE  MONTH(a.FECHA_GESTION)=" . $mes . "
			 AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' AND
			 ASESOR_GESTION <> 'cali' 
			 ORDER BY a.FECHA_GESTION DESC LIMIT 3000", $conex);
                   ?>
            <script>
                $(document).ready(function () {
                $('#FormularioExportacion').css("display","none");
                });
            </script>
                <?php }

                //BUSQUEDA AÑO
                else if (empty($idUsuario) && empty($fecha) && empty($tipificacion) && empty($mes) && empty($anio) == false && empty($ciudad)) {

                    $consultaGestionUsuario = mysql_query("
			   SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,CLASIFICACION_ODONTOLOGO
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_encuesta AS c ON c.ID_CLIENTE_FK_5 = b.ID_CLIENTE 
			 INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE  YEAR(a.FECHA_GESTION)=" . $anio . "
			 AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' AND
			 ASESOR_GESTION <> 'cali' 
			 ORDER BY a.FECHA_GESTION DESC LIMIT 3000", $conex);
                   ?>
            <script>
                $(document).ready(function () {
                $('#FormularioExportacion').css("display","none");
                });
            </script>
                <?php }

                //BUSQUEDA MES Y AÑO
                else if (empty($idUsuario) && empty($fecha) && empty($tipificacion) && empty($mes) == false && empty($anio) == false && empty($ciudad)
                ) {
                    $consultaGestionUsuario = mysql_query("
			   SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICACION_ODONTOLOGO
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_encuesta AS c ON c.ID_CLIENTE_FK_5 = b.ID_CLIENTE 
			 INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE MONTH(a.FECHA_GESTION)='" . $mes . "' AND  YEAR(a.FECHA_GESTION)='" . $anio . "'
			 AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' AND
			 ASESOR_GESTION <> 'cali' 
			 ORDER BY a.FECHA_GESTION DESC LIMIT 3000", $conex);
                   ?>
            <script>
                $(document).ready(function () {
                $('#FormularioExportacion').css("display","none");
                });
            </script>
                <?php }

                //BUSQUEDA USUARIO Y FECHA
                else if (empty($idUsuario) == false && empty($fecha) == false && empty($tipificacion) && empty($mes) && empty($anio) && empty($ciudad)) {
                    $consultaGestionUsuario = mysql_query("
			   SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICACION_ODONTOLOGO
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_encuesta AS c ON c.ID_CLIENTE_FK_5 = b.ID_CLIENTE 
			 INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE ID_ASESOR_GESTION IN ($usuarios_seleccionados) AND  DATE(a.FECHA_GESTION)='" . $fecha . "'
			 AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' AND
			 ASESOR_GESTION <> 'cali' 
			 ORDER BY a.FECHA_GESTION DESC LIMIT 3000", $conex);
                   ?>
            <script>
                $(document).ready(function () {
                $('#FormularioExportacion').css("display","none");
                });
            </script>
                <?php }
                //BUSQUEDA USUARIO Y TIPIFICACION
                else if (empty($idUsuario) == false && empty($fecha) && empty($tipificacion) == false && empty($mes) && empty($ciudad)) {
                    $consultaGestionUsuario = mysql_query("
			    SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICACION_ODONTOLOGO
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_encuesta AS c ON c.ID_CLIENTE_FK_5 = b.ID_CLIENTE  
			 INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE ID_ASESOR_GESTION IN ($usuarios_seleccionados) AND  TIPIFICACION_GESTION='" . $tipificacion . "'
			 AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' AND
			 ASESOR_GESTION <> 'cali' 
			 ORDER BY a.FECHA_GESTION DESC LIMIT 3000", $conex);
                   ?>
            <script>
                $(document).ready(function () {
                $('#FormularioExportacion').css("display","none");
                });
            </script>
                <?php }
                //BUSQUEDA USUARIO Y MES
                else if (empty($idUsuario) == false && empty($fecha) && empty($tipificacion) && empty($mes) == false && empty($ciudad) && empty($anio)==false) {
                    $consultaGestionUsuario = mysql_query("
			   SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,CLASIFICACION_ODONTOLOGO,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_encuesta AS c ON c.ID_CLIENTE_FK_5 = b.ID_CLIENTE 
			 INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE ID_ASESOR_GESTION IN ($usuarios_seleccionados) AND MONTH(a.FECHA_GESTION)=" . $mes . "
			 AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' AND
			 ASESOR_GESTION <> 'cali' 
			 ORDER BY a.FECHA_GESTION DESC LIMIT 3000", $conex);
			
                   ?>
            <script>
                $(document).ready(function () {
                $('#FormularioExportacion').css("display","none");
                });
            </script>
                <?php }
                //BUSQUEDA FECHA Y TIPIFICACION
                else if (empty($idUsuario) && empty($fecha) == false && empty($tipificacion) == false && empty($mes) && empty($ciudad)) {
                    $consultaGestionUsuario = mysql_query("
			    SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICACION_ODONTOLOGO
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_encuesta AS c ON c.ID_CLIENTE_FK_5 = b.ID_CLIENTE 
			INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION			 
			 WHERE DATE(a.FECHA_GESTION)='" . $fecha . "' AND TIPIFICACION_GESTION='" . $tipificacion . "'
			 AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' AND
			 ASESOR_GESTION <> 'cali' 
			 ORDER BY a.FECHA_GESTION DESC LIMIT 3000", $conex);
                   ?>
            <script>
                $(document).ready(function () {
                $('#FormularioExportacion').css("display","none");
                });
            </script>
                <?php }
                //BUSQUEDA TIPIFICACION Y MES
                else if (empty($idUsuario) && empty($fecha) == false && empty($tipificacion) == false && empty($mes) == false && empty($ciudad)) {
                    $consultaGestionUsuario = mysql_query("
			    SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,CLASIFICACION_ODONTOLOGO
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_encuesta AS c ON c.ID_CLIENTE_FK_5 = b.ID_CLIENTE 
				INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION			 
			 WHERE TIPIFICACION_GESTION='" . $tipificacion . "' AND MONTH(a.FECHA_GESTION)=" . $mes . "
			 AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' AND
			 ASESOR_GESTION <> 'cali' 
			 ORDER BY a.FECHA_GESTION DESC LIMIT 3000", $conex);
                   ?>
            <script>
                $(document).ready(function () {
                $('#FormularioExportacion').css("display","none");
                });
            </script>
                <?php }
                //BUSQUEDA USUARIO , FECHA Y TIPIFICACION
                else if (empty($idUsuario) == false && empty($fecha) == false && empty($tipificacion) == false && empty($mes) && empty($ciudad)) {
                    $consultaGestionUsuario = mysql_query("
			    SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICACION_ODONTOLOGO
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_encuesta AS c ON c.ID_CLIENTE_FK_5 = b.ID_CLIENTE 
				INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE ID_ASESOR_GESTION IN ($usuarios_seleccionados) AND DATE(a.FECHA_GESTION)='" . $fecha . "' AND   TIPIFICACION_GESTION='" . $tipificacion . "' AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' AND
			 ASESOR_GESTION <> 'cali' 
			 ORDER BY a.FECHA_GESTION DESC LIMIT 3000", $conex);
                   ?>
            <script>
                $(document).ready(function () {
                $('#FormularioExportacion').css("display","none");
                });
            </script>
                <?php }
                //BUSQUEDA USUARIO , MES Y TIPIFICACION
                else if (empty($idUsuario) == false && empty($fecha) && empty($tipificacion) == false && empty($mes) == false && empty($ciudad) && empty($anio)==false) {
                    $consultaGestionUsuario = mysql_query("
			   SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICACION_ODONTOLOGO
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_encuesta AS c ON c.ID_CLIENTE_FK_5 = b.ID_CLIENTE 
			 INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE ID_ASESOR_GESTION IN ($usuarios_seleccionados) AND MONTH(a.FECHA_GESTION)=" . $mes . " AND YEAR(a.FECHA_GESTION)=" . $anio . " AND   TIPIFICACION_GESTION='" . $tipificacion . "' AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' AND
			 ASESOR_GESTION <> 'cali' 
			 ORDER BY a.FECHA_GESTION DESC LIMIT 3000", $conex);


                    $consultaGestionUsuarioFiltro = mysql_query("
			   SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			 b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	        a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICACION_ODONTOLOGO
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_encuesta AS c ON c.ID_CLIENTE_FK_5 = b.ID_CLIENTE 
			 INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE ID_ASESOR_GESTION IN ($usuarios_seleccionados) AND MONTH(a.FECHA_GESTION)=" . $mes . "  AND YEAR(a.FECHA_GESTION)=" . $anio . " AND   TIPIFICACION_GESTION='" . $tipificacion . "' AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' AND
			 ASESOR_GESTION <> 'cali' 
			 ORDER BY a.FECHA_GESTION DESC LIMIT 3000", $conex);
                    ?> 
            <div style="float: right;width: 50%;text-align:right;margin-right: 15px;">
                    <form action="ver_gestion_filtros.php" method="post"  id="FormularioExportacion" >
                        <button class="btn btn-danger botonExcel">  
                            <img class="rounded-circle img-fluid d-block mx-auto" src="img/excel.png" height="25" width="25" alt="">Exportar Gestiones</button>

                            <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
                    </form>
            </div>
            <div style="display: none;">
                    <table style="width:99%; margin:auto auto;"  id="gestion_filtro">
                        <thead>
                            <tr style=" text-align:center;">
                                <th class="TITULO">ID CLIENTE</th>
                                <th class="TITULO">ACCION</th>
                                <th class="TITULO">TIPIFICACCION</th>
                                <th class="TITULO">SUB TIPIFICACION</th>
                                <th class="TITULO">FECHA GESTION</th>
                                <th class="TITULO">CLASIFICACION</th>
                                <th class="TITULO">USUARIO</th>

                            </tr>
                        </thead>
                        <tbody>
        <?PHP
        while ($dato = mysql_fetch_array($consultaGestionUsuarioFiltro)) {
            ?>
                                <tr class="datos" >

                                    <td style="text-align:center;"><?php echo $dato["ID_CLIENTE"] ?></td>
                                    <td style="text-align:center;"><?php
            if ($dato["ACCION"] == 'CREACION PACIENTE') {
                echo "CREACION ODONTOLOGO";
            } else {
                echo $dato["ACCION"];
            }
            ?></td>
                                    <td style="text-align:center;"><?php echo $dato["TIPIFICACION_GESTION"] ?></td>
                                    <td style="text-align:center;"><?php echo $dato["SUB_TIPIFICACION"] ?></td>	
                                    <td style="text-align:center;"><?php echo $dato["FECHA_GESTION"] ?></td>	
                                    <td style="text-align:center;"><?php echo $dato["CLASIFICACION_ODONTOLOGO"] ?></td>	
                                    <td style="text-align:center;"><?php echo $dato["ASESOR_GESTION"] ?></td>
                                </tr>
            <?php
        }
        ?>
                        </tbody>
                    </table>
            </div>

                                    <?php
                                }
                                //BUSQUEDA CIUDAD 
                                else if (empty($idUsuario) && empty($fecha) && empty($tipificacion) && empty($mes) && empty($ciudad) == false) {
                                    if ($ciudad == 'COSTA') {
                                        $ciudad = 'BARRANQUILLA,CARTAGENA';
                                    }
                                    $consultaGestionUsuario = mysql_query("
			   SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICACION_ODONTOLOGO
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_encuesta AS c ON c.ID_CLIENTE_FK_5 = b.ID_CLIENTE 
			 INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE d.CIUDAD IN ('" . $ciudad . "') AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' AND
			 ASESOR_GESTION <> 'cali' 
			 ORDER BY a.FECHA_GESTION DESC LIMIT 3000", $conex);
                                }
                                //CONSULTA FECHA Y CIUDAD
                                else if (empty($idUsuario) && empty($fecha) == false && empty($tipificacion) && empty($mes) && empty($anio) && empty($ciudad) == false) {
                                    if ($ciudad == 'COSTA') {
                                        $ciudad = 'BARRANQUILLA,CARTAGENA';
                                    }
                                    $consultaGestionUsuario = mysql_query("
			   SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
 			  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
 			  a.FECHA_PROXIMA_VISITA,
			  b.ID_CLIENTE,
			  b.NOMBRE_CLIENTE,
			  b.APELLIDO_CLIENTE,
	          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICACION_ODONTOLOGO
			 FROM 3m_gestion AS a
			 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE 
			 LEFT JOIN  3m_encuesta AS c ON c.ID_CLIENTE_FK_5 = b.ID_CLIENTE 
			 INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
			 WHERE  DATE(a.FECHA_GESTION)='" . $fecha . "' AND d.CIUDAD IN ('" . $ciudad . "')
			 AND ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' AND
			 ASESOR_GESTION <> 'cali' 
			 ORDER BY a.FECHA_GESTION DESC LIMIT 3000", $conex);
                                   ?>
            <script>
                $(document).ready(function () {
                $('#FormularioExportacion').css("display","none");
                });
            </script>
                <?php }
                            } else {
                                $consultaGestionUsuario = mysql_query("
                  SELECT DISTINCT a.ID_GESTION,a.ACCION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
                  CONVERT(CAST(CONVERT(a.OBSERVACION_GESTION USING latin1) AS BINARY) USING utf8) AS OBSERVACION_GESTION,
                  a.FECHA_PROXIMA_VISITA,
                  b.ID_CLIENTE,
                  b.NOMBRE_CLIENTE,
                  b.APELLIDO_CLIENTE,
          b.TIPO_IDENTIFICACION ,b. IDENTIFICACION_CLIENTE,a.ASESOR_GESTION,a.ID_ASESOR_GESTION,CLASIFICACION_ODONTOLOGO
                 FROM 3m_gestion AS a
                 INNER JOIN 3m_cliente AS b ON 	a.ID_CLIENTE_FK = b.ID_CLIENTE
     LEFT JOIN  3m_encuesta AS c ON c.ID_CLIENTE_FK_5 = b.ID_CLIENTE 	
                        INNER JOIN 3m_usuario AS d ON d.ID_USUARIO = a.ID_ASESOR_GESTION
                 WHERE ASESOR_GESTION <> 'call' AND ASESOR_GESTION <> 'admin' AND ASESOR_GESTION <> 'visita' AND ASESOR_GESTION <> 'MEDELLIN' AND
                 ASESOR_GESTION <> 'cali' 
                 ORDER BY a.FECHA_GESTION DESC LIMIT 3000", $conex);
                              ?>
            <script>
                $(document).ready(function () {
                $('#FormularioExportacion').css("display","none");
                });
            </script>
                <?php }
                            ?>
            <table style="width:99%; margin:auto auto;" rules="none" class="table table-responsive" >
                <tr>
                    <th colspan='11' class="principal">GESTI&Oacute;N</th>
                </tr>
            </table>
            <br />
            <table style="width:99%; margin:auto auto;"  id="gestion">
                <thead>
                    <tr style=" text-align:center;">

                        <th class="TITULO">IDENTIFICACI&Oacute;N</th>
                        <th class="TITULO">CLIENTE</th>
                       <th class="TITULO">TIPIFICACCI&Oacute;N</th>
                        <th class="TITULO">SUB TIPIFICACI&Oacute;N</th>
                        <th class="TITULO">OBSERVACION </th>
                        <th class="TITULO">FECHA GESTI&Oacute;N</th>
                        <th class="TITULO">CLASIFICACIÓN</th>
                        <th class="TITULO">USUARIO</th>
                        <th class="TITULO">DETALLE</th>
						<th class="TITULO">ENCUESTA</th>
                    </tr>
                </thead>
                <tbody>
<?PHP
while ($dato = mysql_fetch_array($consultaGestionUsuario)) {

    $CLIENTE = $dato["NOMBRE_CLIENTE"] . " " . $dato["APELLIDO_CLIENTE"];
    ?>
                        <tr class="datos" >

                            <td style="text-align:center;"><?php echo $dato["IDENTIFICACION_CLIENTE"] ?></td>
                            <td style="text-align:center;"><?php echo $CLIENTE; ?></td>
                            <td style="text-align:center;"><?php echo $dato["TIPIFICACION_GESTION"] ?></td>
                            <td style="text-align:center;"><?php echo $dato["SUB_TIPIFICACION"] ?></td>
                            <td style="text-align:center;"><?php echo $dato["OBSERVACION_GESTION"] ?></td>	
                            <td style="text-align:center;"><?php echo $dato["FECHA_GESTION"] ?></td>	
                            <td style="text-align:center;"><?php echo $dato["CLASIFICACION_ODONTOLOGO"] ?></td>	
                            <td style="text-align:center;"><?php echo $dato["ASESOR_GESTION"] ?></td>
                            <td>
                                <a  href="javascript:ventanaSecundaria('../presentacion/detalle_gestion.php?id=<?php echo base64_encode($dato['ID_GESTION']) ?>')" >
                                    <img src="imagenes/lupa1.png" width="43" height="32" style="background-size:cover" title="DETALLE PEDIDO"/>
                                </a> 
                            </td>
							<td>
							    <a  href="javascript:ventanaSecundaria('../presentacion/inicio_consulta_cliente _admin.php?x=<?php echo base64_encode($dato['ID_CLIENTE']) ?>')" >
                                    <img src="imagenes/consultar_encuesta.PNG" width="43" height="32" style="background-size:cover" title="DETALLE PEDIDO"/>
                                </a> 
                            </td>							


                        </tr>
                                <?php
                            }
                            ?>
                </tbody>
            </table>




        </div>
        <script>
            function preguntar() {

                if (!confirm('Desea eliminar el pedido?')) {
                    return false;
                }

            }
        </script>
    </body>
</html>