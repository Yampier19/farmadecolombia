<?php
	include("../logica/session.php");
	header("Content-Type: text/html;charset=utf-8");

if (isset($_SERVER['HTTPS'])) {
    
} else {
    ?>
    <script>
  window.onload = window.top.location.href = "https://app-peoplemarketing.com/3M/";
    </script>
    <?php } ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CLIENTE</title>
<link href="css/menu_visitas.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="../presentacion/fonts.css" />
<link href="css/bootstrap.css" rel="stylesheet" />

<link href="../presentacion/css/encuenta.css" type="text/css" rel="stylesheet" />
<script src="../presentacion/js/jquery.js"></script>
<script src="../presentacion/js/efectividad.js"></script>
<script src="https://unpkg.com/sweetalert2@7.1.3/dist/sweetalert2.all.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
</head>

<script>
	function obtener()
{
	var parametros={enableHighAccuracy:true}
	navigator.geolocation.getCurrentPosition(mostrar_posicion,gestion_errores,parametros);
	
}
function mostrar_posicion(posicion)
{
	var ubicacion=document.getElementById("UBICACION");
	
	var latitud=posicion.coords.latitude;
	var longitud=posicion.coords.longitude;
	var exactitud=posicion.coords.accuracy;
	/*alert(exactitud);*/
	document.getElementById("LAT").value=latitud;
	document.getElementById("lon").value=longitud;
	document.getElementById("LAT1").value=latitud;
	document.getElementById("lon1").value=longitud;
	
}
function gestion_errores(error)
{
	if(error.code==1)
	{
		window.onload = window.top.location.href = "https://app-peoplemarketing.com/3M/";
	}
}
</script>
    <style>
        a font{
          color:blue;  
        }
    </style>
<?php
include('../datos/conex_copia.php');
$string_intro = getenv("QUERY_STRING"); 
parse_str($string_intro);
$transferencia = $_REQUEST['transferencia'];
$x=$_REQUEST["x"];
	
$id_cli=base64_decode($x);

	 
if($id_usu!=''&&$id_usu!='0'){

	if(empty($id_cli)){
		$id_cli=$_SESSION['ID_CLIENTE'];
	}
	
	
	$consulta=mysqli_query($conex,"SELECT * FROM 3m_cliente WHERE ID_CLIENTE='$id_cli'");
echo mysqli_error($conex);
while($datos=(mysqli_fetch_array($consulta)))
{
	$id_cli                 =$datos['ID_CLIENTE'];
	$IDENTIFICACION_CLIENTE =$datos['IDENTIFICACION_CLIENTE'];
	$TELEFONO_CLIENTE       =$datos['TELEFONO_CLIENTE'];
	$CELULAR_CLIENTE        =$datos['CELULAR_CLIENTE'];
	$DIRECCION_CLIENTE      =$datos['DIRECCION_CLIENTE'];
	$email                 = $datos['EMAIL_CLIENTE'];
	$_SESSION['ID_CLIENTE']= $id_cli;
	$cliente			   = $datos['NOMBRE_CLIENTE'].' '.$datos['APELLIDO_CLIENTE'];
	$nombre 			   = $datos['NOMBRE_CLIENTE'];
	$apellido 			   = $datos['APELLIDO_CLIENTE'];
	$_SESSION['NOMBRE_CLIENTE']=$cliente;
	$FECHA_REGISTRO        = $datos["FECHA_REGISTRO"];
	$AUTOR_REGISTRO_FK     = $datos["AUTOR_REGISTRO_FK"];
}

$select = mysqli_query($conex,"SELECT DISTRIBUIDOR_COMPRA,CLASIFICACION_ODONTOLOGO,TIPO_ODONTOLOGO,ESPECIALIDAD_ODONTOLOGO,CANTIDAD_PACIENTES FROM 3m_encuesta WHERE ID_CLIENTE_FK_5='$id_cli'");
echo mysqli_error($conex);
while($fila=(mysqli_fetch_array($select)))
{
    $TIPO_ODONTOLOGO = $fila["TIPO_ODONTOLOGO"];
	$distribuidor=$fila['DISTRIBUIDOR_COMPRA'];
	$_SESSION['DISTRIBUIDOR_COMPRA']=$distribuidor;
	$CLASIFICACION_ODONTOLOGO=$fila['CLASIFICACION_ODONTOLOGO'];
	$ESPECIALIDAD_ODONTOLOGO=$fila['ESPECIALIDAD_ODONTOLOGO'];
    $CANTIDAD_PACIENTES = $fila["CANTIDAD_PACIENTES"];
}

//CONSULTAR FECHA DE LA ULTIMA GESTION 
$consultar_fecha_gestion = mysqli_query($conex,"SELECT MAX(FECHA_GESTION),ASESOR_GESTION
FROM 3m_gestion
WHERE ID_CLIENTE_FK=".$id_cli." AND ACCION IN ('REGISTRO VISITA','INGRESO SOLICITUD PEDIDO') GROUP BY ID_CLIENTE_FK");

	while($datos=(mysqli_fetch_array($consultar_fecha_gestion))){
		$fecha_gestion = $datos["MAX(FECHA_GESTION)"];
                $ultimo_visitador =$datos["ASESOR_GESTION"];
	}
	

$fecha_actual= date("Y-m-d H:i:s");

	
	$fecha_inicial= new DateTime($fecha_gestion);
	$fecha_final = new DateTime($fecha_actual);

	$resta_fecha = $fecha_inicial->diff($fecha_final);
	$dias= $resta_fecha->format("%d");
	$hora= $resta_fecha->format("%H");
	$minuto= $resta_fecha->format("%I");
	$segundo= $resta_fecha->format("%S");
	
	if($dias  > 1  ){
		$tiempo_transcurrido = "Hace ".$dias." dias";
	}
	else if($dias ==1){
		$tiempo_transcurrido="Ayer";
	}
	
   else if($hora >0){
		$tiempo_transcurrido = "Hace ".$hora. "h ".$minuto."m";
	}
	else if($minuto >0){
		$tiempo_transcurrido = "Hace ".$minuto. " min";
	}
	else{
		$tiempo_transcurrido="Justo ahora";
	}

        


$mes_actual = date("m");
$anio_actual = date("Y");

if($CLASIFICACION_ODONTOLOGO =='' || ($CANTIDAD_PACIENTES==0 || $CANTIDAD_PACIENTES=='')){
	//echo 'esta vacia es necesario ingresar encuesta';
	//realizar por primera vez la encuesta consumo
	$realizar_encuesta = 1;
}
else {// ya existe encuesta de consumo
	$realizar_encuesta = 0;
}


$CONSULTAR_NUMERO_EFECTIVAS_MENSUALES = mysqli_query($conex, "SELECT COUNT(*) FROM 3m_gestion "
        . "WHERE MONTH(FECHA_GESTION)='".$mes_actual."' AND YEAR(FECHA_GESTION)='".$anio_actual."' AND TIPIFICACION_GESTION='EFECTIVA' AND ID_CLIENTE_FK=".$id_cli." AND ID_ASESOR_GESTION=".$id_usu);
while($datos_visitas = mysqli_fetch_array($CONSULTAR_NUMERO_EFECTIVAS_MENSUALES)){
    $NUMERO_VISITAS_EFECTVAS = $datos_visitas["COUNT(*)"];
}
	
?>
<script>
	obtener()
    </script>
 
<body>
<div id="dialog-message" title="Download complete">
<?php
if(empty($id_usu)){ ?>
    <div class="col-md-6">
				<span style="margin-top:5%;">
			<center>
			<img src="../presentacion/imagenes/advertencia.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
			<p class="error" style=" width:68.9%; margin:auto auto;">
			 LA SESI&Oacute;N HA CADUCADO, POR FAVOR INICIE SESION.
			</p>
			<br />
                    <a href="https://app-peoplemarketing.com/3M/">Volver al inicio</a>
			<center>
			</center>
			</center>
			</span>
    </div>	
			<?php }else{?>
    
<input name="distribuidor" id="distribuidor" value="<?php echo $distribuidor; ?>" type="hidden"/>
<input name="email" id="email" value="<?php echo $email; ?>" type="hidden"/>
 
  <form id="tranferencias" name="tranferencias" action="../logica/guardar_encuesta_alert.php" method="post" style="width:100%;">
    <input id="LAT" name="LAT" type="hidden" />
    <input id="lon" name="lon"  type="hidden"/>
    <input name="numero_efectivas" id="numero_efectivas" value="<?php echo $NUMERO_VISITAS_EFECTVAS;  ?>" type="hidden">
    <input name="idusuario" value="<?php echo $id_usu; ?>" type="hidden"/>
    <input name="name_usuario" value="<?php echo $usua; ?>" type="hidden" />
  	<input name="encuesta" id="encuesta" value="<?php echo $realizar_encuesta; ?>" type="hidden" />
  	<input name="tipo_odontologo" id="tipo_odontologo" value="<?php echo $TIPO_ODONTOLOGO; ?>" type="hidden" />
  	<input name="encuesta_copia" id="encuesta_copia" value="" type="hidden" />
	<input  name="nombre" value="<?php echo $nombre; ?>" type="hidden"/>
	<input  name="apellido" value="<?php echo $apellido; ?>" type="hidden"/>
   
	<div class="form-group" >
	<table align="right" style="width:100%;text-align:center;">
    	<tr> 	
          <th colspan="6"><center>Bienvenid@  <?php echo $usua?></center></th> 
          </tr>
          <tr>
            <input name="usua" style="display:none;" type="text" id="usua" value="<?php echo $usua; ?>" readonly/>      
            <td><a href="../presentacion/inicio_visitas.php">
        <img src="../presentacion/imagenes/atras.png" width="56" height="57" id="cambiar" title="ATRAS" >
        </a></td>
        <td><a href="../presentacion/consultar_gestiones_visitadores.php">
        <img src="../presentacion/imagenes/gestiones.png" width="56" height="57" id="cambiar" title="GESTIONES" >
         <center> <font style="font-size:10px;" >Gestiones</font></center>
        </a></td>
        
        <td><a href="../presentacion/consulta_pedidos_usuario.php?">
        <img src="../presentacion/imagenes/agn.png" width="56" height="57" id="cambiar" title="CONSULTAR PEDIDOS" >
         <center> <font style="font-size:10px;" >Pedidos</font></center>
        </a></td>
          <td><a href="../presentacion/consulta_proximas_vista.php?">
        <img src="../presentacion/imagenes/calendar.png" width="56" height="57" id="cambiar" title="CONSULTAR PR&Oacute;XIMA VISITA" >
         <center> <font style="font-size:10px;" >Pr&oacute;xima <br />Visita</font></center>
        </a></td>
         <td><a href="../presentacion/reporte_clientes_nuevos.php">
        <img src="../presentacion/imagenes/excel.png" width="48" height="51" id="cambiar" title="REPORTE EXCEL" ><br />
       <center> <font style="font-size:10px;" >Clientes <br />Nuevos</font></center>
        </a></td>
        </tr>
    </table>
    <br />
    <br />
    <br />
<center><img src="../presentacion/imagenes/3m.png" width="24%" height="17%" /></center>

    <br />
    <br />
	<fieldset style="border:1px solid #93271b; border-radius:10px;">
      <legend style="color:#93271b; font-weight:bold;">Informaci&oacute;n Cliente
        
        </legend>
        <table align="center" width="80%" class="table table-condensed">
            <tr style=" text-align: left;">
                <td class="tit">
                    Codigo Interno:
                </td>
                <td>
                    <input name="identifi" style="width:65%; height:25px;  border:none;" type="text" id="identifi" value="<?php echo $id_cli; ?>" readonly="readonly"/>
                    
                    <a href="consulta_pedidos_call.php"><img src="../presentacion/imagenes/pregunta.png" width="20%" height="30px" align="right" style="margin-right:5%;"/></a></td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit" style="width:35%;">
                    Nombre Cliente:
                </td>
                <td>
                    <input name="nombre_negocio" style="width:90%; height:25px; border:none" type="text" id="nombre_negocio" value="<?php echo $cliente; ?>" readonly="readonly"/>
               </td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit">
                    Numero de Identificacion:
                </td>
                <td>
                    <input name="identifi" style="width:90%; height:25px; border:none" type="text" id="identifi" value="<?php echo $IDENTIFICACION_CLIENTE; ?>" readonly="readonly"/>
                </td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit">
                    Telefono:
                </td>
                <td>
                    <input name="telefono" style="width:90%; height:25px; border:none" type="text" id="telefono" value="<?php echo $TELEFONO_CLIENTE; ?>" readonly="readonly"/>
                </td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit">
                    Celular:
                </td>
                <td>
                    <input name="celular" style="width:90%; height:25px; border:none" type="text" id="celular" value="<?php echo $CELULAR_CLIENTE; ?>" readonly="readonly"/>
                </td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit">
                    E-mail:
                </td>
                <td>
                    <input name="e_mail" style="width:90%; height:25px; border:none" type="text" id="e_mail" value="<?php echo $email; ?>" readonly="readonly"/>
                </td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit">
                    Direcci&oacute;n:
                </td>
                <td>
                    <input name="direccion" style="width:90%; height:25px; border:none" type="text" id="direccion" value="<?php echo $DIRECCION_CLIENTE; ?>" readonly="readonly"/>
                </td>
            </tr> 
            <tr style=" text-align: left;">
                <td class="tit">
                    Distribuidor:
                </td>
                <td>
                    <input name="distri" type="text" id="distri" style="width:90%; height:25px; border:none" value="<?php echo $distribuidor; ?>" readonly/>
                </td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit">Clasificaci&oacute;n:</td>
                <td>
                <input name="clasificacion" type="text" id="clasificacion" style="width:90%; height:25px; border:none" value="<?php echo $CLASIFICACION_ODONTOLOGO; ?>" readonly/>
                </td>
            </tr>
          <tr style=" text-align: left;">
                <td class="tit">Especialidad:</td>
                <td>
                <input name="especialidad" type="text" id="especialidad" style="width:90%; height:25px; border:none" value="<?php echo $ESPECIALIDAD_ODONTOLOGO; ?>" readonly/>
                </td>
            </tr>
               <tr style=" text-align: left;">
                <td class="tit">Fecha de Registro:</td>
                <td>
                <input name="fecha_registro" type="text" id="fecha_registro" style="width:90%; height:25px; border:none" value="<?php echo $FECHA_REGISTRO; ?>" readonly/>
                </td>
            </tr>
            <tr style=" text-align: left;">
                <td class="tit">Usuario quien registra:</td>
                <td>
                <input name="usuario_registro" type="text" id="usuario_registro" style="width:90%; height:25px; border:none" value="<?php echo $AUTOR_REGISTRO_FK; ?>" readonly/>
                </td>
            </tr>
              <tr>
            <td class="tit">Tiempo transcurrido ultima visita</td>
            <td>
               <input name="dias_visita" type="text" id="dias_visita" style="width:90%; height:25px; border:none" value="<?php echo $tiempo_transcurrido; ?>" readonly/>
            </td>
            </tr>
            <tr>
            <td class="tit">Fecha &Uacute;ltima visita</td>
            <td>
               <input name="fecha_gestion" type="text" id="fecha_gestion" style="width:90%; height:25px; border:none" value="<?php echo $fecha_gestion; ?>" readonly/>
            </td>
            </tr>
             <tr>
            <td class="tit">Visitador ultima visita</td>
            <td>
               <input name="ultimo_visitador" type="text" id="ultimo_visitador" style="width:90%; height:25px; border:none" value="<?php echo $ultimo_visitador; ?>" readonly/>
            </td>
            </tr>
            <tr style="text-align: center">
                <td colspan="2">
                <a href="form_cliente_actualizar.php"><img src="imagenes/lapiz.png" width="40" height="40" title="Editar Cliente"/></a>
                </td>
          </tr>                        
      </table>
  </fieldset>
    <br />
    <br />

	
    <div class="form-group" style="display:none" >
   
        <input name="id_cliente" id="id_cliente" type="text" readonly="readonly"value="<?php echo $id_cli ?>"/>
        <input name="distribuidor" id="distribuidor" type="text" readonly="readonly" class="form-control1" value="<?php echo $distribuidor ?>"/>
        <input name="email" id="email" type="text" readonly="readonly" class="form-control1" value="<?php echo $email; ?>"/>
        <input type="text" id="compro" name="compro" readonly="readonly"/>
    </div>
           
    <?php 
	 if($transferencia == 1){
	?> 
    
	<iframe style=" width:100%;border:1px solid transparent; height:900px;" src="../presentacion/form_transferencias_med.php" id="pag" name="pag"></iframe>
        <?php }  ?>
    
  <?php if($transferencia !=1){ ?>  
    <div class="form-group" id="resultado_div" style="display:">
    	<br />
         <div class="col-md-3">
    	<label for="Resultado">Resultado Visita:</label><span class="asterisco">*</span>
        </div>
         <div class="col-md-3">
        <select name="resultado_text" id="resultado_text" class="form-control" required="true" >
        	<option selected="selected" disabled="disabled" value="0">Elija...</option>
			<?php 
			$consulta_tipificaciones=mysqli_query($conex,"SELECT id_tifipificacion,tipificacion FROM 3m_tipificaciones");
				while($datos_tipificacion=(mysqli_fetch_array($consulta_tipificaciones)))
				{
			?>
				<option value="<?php echo $datos_tipificacion["id_tifipificacion"] ?>"><?php echo $datos_tipificacion["tipificacion"] ?></option>
				<?php } ?>
        </select>
    	</div>
      </div>
   <?php  } ?>     
    </div>
    
     <!-- Visita no efectiva subtipificacion-->
	<div id="visita_no_efectiva" class="visita_no_efectiva" style="display:none">
         <div class="col-md-3">
            <label for="Resultado">Sub tipificacion:</label><span class="asterisco">*</span>
            </div>
         <div class="col-md-3">
        <select name="sub_tipificacion_no_efectiva" id="sub_tipificacion_no_efectiva" class="form-control ">
                <option selected="selected" disabled="disabled" value="0">Elija...</option>
               <?php 
			$consulta_subtipificaciones_no_efectiva=mysqli_query($conex,"SELECT id_subtipificacion,subtipificacion FROM 3m_subtipificaciones WHERE id_tipificacion IN (2,3)");
				while($datos_subtipificacion_no_efectiva=(mysqli_fetch_array($consulta_subtipificaciones_no_efectiva)))
				{
			?>
				<option value="<?php echo $datos_subtipificacion_no_efectiva["id_subtipificacion"] ?>"><?php echo $datos_subtipificacion_no_efectiva["subtipificacion"] ?></option>
				<?php } ?>
        </select>
        </div>
       
      </div>
      
       <!-- Visita efectiva subtipificacion-->
    <div id="visita_efectiva" style="display:none">
         <div class="col-md-3">
        <label for="Resultado">Sub tipificacion:</label><span class="asterisco">*</span>
        </div>
         <div class="col-md-3">
            <select name="sub_tipificacion_efectiva" id="sub_tipificacion_efectiva" class="form-control ">
			 <option selected="selected" disabled="disabled" value="0">Elija...</option>
                     <?php 
			$consulta_subtipificaciones_efectiva=mysqli_query($conex,"SELECT id_subtipificacion,subtipificacion FROM 3m_subtipificaciones WHERE id_tipificacion=1");
				while($datos_subtipificacion_efectiva=(mysqli_fetch_array($consulta_subtipificaciones_efectiva)))
				{
			?>
				<option value="<?php echo $datos_subtipificacion_efectiva["id_subtipificacion"] ?>"><?php echo $datos_subtipificacion_efectiva["subtipificacion"] ?></option>
				<?php } ?>
            </select>
          </div>  
      </div>
      
        <div id="visita_televenta" style="display:none;">
         <div class="col-md-3">
        <label for="Resultado">Sub tipificacion:</label><span class="asterisco">*</span>
        </div>
         <div class="col-md-3">
            <select name="sub_tipificacion_televenta" id="sub_tipificacion_televenta" class="form-control ">
             <option selected="selected" disabled="disabled" value="0">Elija...</option>
             <option selected="selected" value="TELEVENTA">TELEVENTA</option>
             <option selected="selected" value="CONVERSION">CONVERSION</option>
            </select>
          </div> 
      </div>
    

      <!-- fecha proxima visita -->
	  <div id="fecha_observaciones" style="display:none">
      
      <div class="col-md-3">
        <label class="proxima_visita">Fecha Pr&oacute;xima Visita</label>
        </div>
        <div class="col-md-3">
        <input type="date" name="proxima_visita" id="proxima_visita" class="form-control" step="1" required="true"  min="<?php echo date("Y-m-d");?>">
        </div>
     
      <div class="col-md-3">
        <label for="Resultado">Observaciones:</label><span class="asterisco">*</span>
        </div>
        <div class="col-md-3">
        <textarea class="form-control" style="width:98%" rows="4" name="observacion" id="observacion" required="true"></textarea>
      </div>

	</div>
    <!-- boton de no efectiva -->
      <div style="display:none"  class="form-group" id="boton_no_efectiva">
   		 <div class="col-md-12">
   		 <fieldset>
                 <div >
            <center>
                <input formaction='../logica/guardar_encuesta.php' type="submit" name="GUARDAR_NO_EFECTIVA" id="GUARDAR_NO_EFECTIVA" class="btn btn-primary" value="GUARDAR NO EFECTIVA" title="Guardar No Efectiva" />
            </center>
		</div>
   		</fieldset>
   		 </div>
     </div> 
     <!-- boton de televenta -->
     <div style="display:none" id="boton_televenta" class="form-group">
   		 <div class="col-md-12">
   	
            <center>
                <input formaction='../logica/guardar_encuesta.php' type="submit" name="BTN_TELEVENTA" id="BTN_TELEVENTA" class="btn btn-primary" value="GUARDAR TELEVENTA" title="Guardar Televenta" />
            </center>
			</div>
   	  </div>
    
	 <!-- boton de venta de evento -->
     <div style="display:none" id="boton_venta_evento" class="form-group">
   		 <div class="col-md-12">
   	
            <center>
                <input  formaction='../logica/guardar_encuesta.php' type="submit" name="BTN_EVENTO" id="BTN_EVENTO" class="btn btn-primary" value="GUARDAR VENTA EN EVENTO" title="Guardar venta en evento" />
            </center>
			</div>
   	  </div>
     
     <!--boton efectiva sin encuesta de consumo -->
     <div style="display:none" id="efectiva_sin_encuesta" class="form-group">
   		 <div class="col-md-12">
   	
            <center>
                <input formaction='../logica/guardar_encuesta.php' type="submit" name="BTN_EFECTIVA_SIN_ENCUESTA" id="BTN_EFECTIVA_SIN_ENCUESTA" class="btn btn-primary" value="GUARDAR EFECTIVA" title="Guardar Efectiva"/>
            </center>
			</div>
   	  </div>
    
     
     <!-- encuesta de consumo-->
     <div class="col-md-12" id="encuesta_consumo" style="display:none">
     <div>
     
      <?php
	   include '../presentacion/form_encuesta_consumo.php';
	 
	   ?>
       
       <div class="form-group">
            <center>
            	<input name="confirmar" type="submit" id="confirmar"  class="btn btn-primary" value="GUARDAR ENCUESTA" onclick="return validar(tranferencias)" style="height:40px" title="Guardar Efectiva con Encuesta de Consumo" />
            </center>
    </div>
       </div>
    </div>
    
</form>
<?php }
		?>           
  
</body>
</html>
<?php
}
?>
