<?php
	include("../logica/session.php");
?>
<?php header('Content-Type: text/html; charset=UTF-8'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css"/>
<link href="css/tablas.css" rel="stylesheet" /> 
<link href="css/bootstrap.css" rel="stylesheet" />

<title>CONSULTA PEDIDO</title>
<script>
$(document).ready(function() {
    $('#pedidos').DataTable();
} );
</script>

<script language=javascript> 
function ventanaSecundaria (URL)
{ 
   window.open(URL,"ventana1","width=800,height=500,Top=150,Left=50%") 
}
</script> 
<style>
.error
{
	font-size: 130%;
	font-weight: bold;
	color: #fb8305;
	text-transform:uppercase;
	background-color:transparent;
	text-align: center;
	padding:10px;
}
</style>
</head>
<?PHP
include('../datos/conex_copia.php');
if(isset($pedid))
{
	$ID_CLIENTE=base64_decode($pedid);
}
else
{
	$ID_CLIENTE=$ID_CLIENTE;
}

 
 $consulta=mysqli_query($conex," 

SELECT a.FECHA_PROXIMA_VISITA,a.ID_CLIENTE_FK,a.ID_ASESOR_GESTION,a.ASESOR_GESTION,a.TIPIFICACION_GESTION,a.SUB_TIPIFICACION,a.FECHA_GESTION,
CONCAT(b.NOMBRE_CLIENTE ,' ',b.APELLIDO_CLIENTE) AS NOMBRE,b.TIPO_IDENTIFICACION,b.IDENTIFICACION_CLIENTE,b.DIRECCION_CLIENTE,b.CIUDAD_CLIENTE,
b.CELULAR_CLIENTE,b.TELEFONO_CLIENTE,b.EMAIL_CLIENTE  
FROM 3m_gestion AS a
INNER JOIN 3m_cliente AS b ON a.ID_CLIENTE_FK = b.ID_CLIENTE
WHERE ID_ASESOR_GESTION = $id_usu AND FECHA_PROXIMA_VISITA IS NOT NULL
AND FECHA_PROXIMA_VISITA <> '0000-00-00'
 ORDER BY FECHA_PROXIMA_VISITA DESC;
  ");
?>
<body>
<?php
if(empty($id_usu)){ ?>
				<span style="margin-top:5%;">
			<center>
			<img src="../presentacion/imagenes/advertencia.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
			<p class="error" style=" width:68.9%; margin:auto auto;">
			 LA SESI&Oacute;N HA CADUCADO, POR FAVOR INICIE SESION.
			</p>
			<br />
			<br />
			<center>
					<a href="https://app-peoplemarketing.com/farmadecolombia/"  class="btn_continuar">
						<img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" />
					</a>
				</center>
			</center>
			</span>
				
			<?php }else{?>
<div class="container-fluid">
<table align="right" style="width:100%;">
    	<tr> 	
          <th colspan="6">Bienvenid@  <?php echo $usua?></th> 
          </tr>
          <tr>
            <input name="usua" style="display:none;" type="text" id="usua" value="<?php echo $usua; ?>" readonly/>      
            <td><a href="../presentacion/inicio_visitas.php">
        <img src="../presentacion/imagenes/atras.png" width="56" height="57" id="cambiar" title="ATRAS" >
        </a></td>

        </a></td>
        <!--<td><a href="../presentacion/kpi_visitador.php">
        <img src="../presentacion/imagenes/kpi.png" width="56" height="57" id="cambiar" title="KPI'S" >
         <center> <font style="font-size:10px;" >KPI'S</font></center>
        </a></td>-->

        <td><a href="../presentacion/consultar_gestiones_visitadores.php">
        <img src="../presentacion/imagenes/gestiones.png" width="56" height="57" id="cambiar" title="GESTIONES" >
         <center> <font style="font-size:10px;" >Gestiones</font></center>
        </a></td>
        
        <!--<td><a href="../presentacion/consulta_pedidos_usuario.php?">
        <img src="../presentacion/imagenes/agn.png" width="56" height="57" id="cambiar" title="CONSULTAR PEDIDOS" >
         <center> <font style="font-size:10px;" >Pedidos</font></center>
        </a></td>-->
          <td><a href="../presentacion/consulta_proximas_vista.php?">
        <img src="../presentacion/imagenes/calendar.png" width="56" height="57" id="cambiar" title="CONSULTAR PR&Oacute;XIMA VISITA" >
         <center> <font style="font-size:10px;" >Pr&oacute;xima <br />Visita</font></center>
        </a></td>
        <td><a href="../presentacion/consulta_productos_visita.php?">
        <img src="../presentacion/imagenes/productos.png" width="56" height="57" id="cambiar" title="CONSULTAR PEDIDOS" >
         <center> <font style="font-size:10px;" >Productos</font></center>
        </a></td>
         <td><a href="../presentacion/reporte_clientes_nuevos.php">
        <img src="../presentacion/imagenes/excel.png" width="48" height="51" id="cambiar" title="REPORTE EXCEL" ><br />
       <center> <font style="font-size:10px;" >Clientes <br />Nuevos</font></center>
        </a></td>
        </tr>
    </table>
    </div>
    <br />  
<br />
<br />
<div class="table table-responsive">
<table style="width:99%; margin:auto auto;" rules="none" >
	<tr>
		<th colspan='11' class="principal">PR&Oacute;XIMAS VISITAS</th>
	</tr>
    </table>
    <br />
    <table style="width:99%; margin:auto auto;" rules="none" id="pedidos"class="table table-striped">
    <thead>
	<tr>
		<th class="TITULO">PR&Oacute;XIMA VISITA</th>
        <th class="TITULO">FECHA GESTI&Oacute;N</th>
        <th class="TITULO">TIPIFICACI&Oacute;N</th>
        <th class="TITULO">SUB TIPIFICACI&Oacute;N</th>
        <th class="TITULO">IDENTIFICACI&Oacute;N </th>
        <th class="TITULO">NOMBRE </th>
        <th class="TITULO">CELULAR </th>
		<th class="TITULO">TELEFONO</th>
		<th class="TITULO">DIRECCI&Oacute;N</th>
        <th class="TITULO">CIUDAD</th>
        <th class="TITULO">USUARIO</th>
	</tr>
    </thead>
    <tbody>
    <?PHP
    while($dato=mysqli_fetch_array($consulta))
	{
	?>
		<tr class="datos">
            <td><?php echo $dato["FECHA_PROXIMA_VISITA"]?></td>
          	<td><?php echo $dato["FECHA_GESTION"]?></td>
            <td><?php echo $dato["TIPIFICACION_GESTION"]?></td>
            <td><?php echo $dato["SUB_TIPIFICACION"]?></td>
            <td><?php echo $dato["IDENTIFICACION_CLIENTE"]?></td>
            <td><?php echo $dato["NOMBRE"]?></td>
            <td><?php echo $dato["CELULAR_CLIENTE"]?></td>	
            <td><?php echo $dato["TELEFONO_CLIENTE"]?></td>	
            <td><?php echo $dato["DIRECCION_CLIENTE"]?></td>
            <td><?php echo $dato["CIUDAD_CLIENTE"]?></td>	
            <td><?php echo $dato["ASESOR_GESTION"]?></td>	
            
		</tr>
	<?php 
	}
	?>
    </tbody>
</table>
</div>
<?php } ?>
</body>
</html>