<?php
	include("../logica/session.php");
        header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="conten-type" content="text/html; charset=UTF-8" />
<link href="css/paciente_nuevo.css" type="text/css" rel="stylesheet" />
<link href="css/validacion_campos.css"type="text/css" rel="stylesheet" />
<script src="https://unpkg.com/sweetalert2@7.1.3/dist/sweetalert2.all.js"></script>
<title>ODONTOLOGO NUEVO</title>
<meta name="viewport" content="width=device-width, user-scalable=no , initial-scale=1.0, maximun-scale=1.0, minimum-scale=1.0">
<script src="js/jquery.js"></script>
<!--GEOLOCALIZACION-->
<script>
function obtener()
{
	var parametros={enableHighAccuracy:true}
	navigator.geolocation.watchPosition(mostrar_posicion,gestion_errores,parametros);
}
function mostrar_posicion(posicion)
{
	var ubicacion=document.getElementById("UBICACION");
	var latitud=posicion.coords.latitude;
	var longitud=posicion.coords.longitude;
	var exactitud=posicion.coords.accuracy;
	/*alert(exactitud);*/
	document.getElementById("LAT").value=latitud;
	document.getElementById("lon").value=longitud;	
}
function gestion_errores(error)
{
	if(error.code==1)
	{
		swal(
  		{title: 'Debes permitir el uso de la geolocalizacion en tu navegador',
  		confirmButtonColor: '#BC3228'});
	}
}

function ciudades()
{
	var DEPT=$('#departamento').val();
	//alert(DEPT)
	$.ajax(
	{
		url:'../presentacion/consulta_ciudades.php',
		data:
		{
			DEPT: DEPT
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#ciudad").attr('disabled','disabled');
		},
		success: function(data)
		{
			$('#ciudad').html(data);
			$("#ciudad").removeAttr('disabled');
		}
	});
}
function especialidades()
{
	var tipo_odontologo=$('#tipo_odontologo').val();
	$.ajax(
	{
		url:'../presentacion/consulta_especialidad.php',
		data:
		{
			tipo_odontologo: tipo_odontologo,
		},
		type: 'post',
		beforeSend: function () 
		{
			$("#tipo_especialidad").attr('disabled','disabled');
		},
		success: function(data)
		{
			$('#tipo_especialidad').html(data);
			$("#tipo_especialidad").removeAttr('disabled');
		}
	});
}

$(document).ready(function()
{
	$('#departamento').change(function()
	{
		ciudades();
	});
	$('#tipo_odontologo').change(function()
	{
		especialidades();
	});
	 
});
</script>
<style>
.error
{
	font-size: 130%;
	font-weight: bold;
	color: #fb8305;
	text-transform:uppercase;
	background-color:transparent;
	text-align: center;
	padding:10px;
}
</style>
</head>
<?php
include('../datos/conex_copia.php');
if($id_usu!=''&&$id_usu!='0'){
	$select_user = mysqli_query($conex,"SELECT USER,CIUDAD FROM 3m_usuario WHERE ID_USUARIO = '$id_usu'");
            while($dat=(mysqli_fetch_array($select_user)))
		{ $CIUDAD = $dat["CIUDAD"]; }
?>
<body onload="obtener()">
<?php if(empty($id_usu)){ ?>
        <span style="margin-top:5%;">
                <center>
                <img src="../presentacion/imagenes/advertencia.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
                <p class="error" style=" width:68.9%; margin:auto auto;">
                 LA SESI&Oacute;N HA CADUCADO, POR FAVOR INICIE SESION.
                </p>
                <br />
                <br />
                <center>
                                <a href="https://app-peoplemarketing.com/3M/"  class="btn_continuar">
                                        <img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" />
                                </a>
                        </center>
                </center>
        </span>
				
			<?php }
			else{ ?>
<table align="right">
    	<tr>
            <th>Bienvenid@  <?php echo $usua?></th>
            <td><a href="../presentacion/inicio_visitas.php">
        <img src="../presentacion/imagenes/atras.png" width="56" height="57" id="cambiar" title="ATRAS" ><span style="color:#000;">
        </a></td>
         <td><a href="../presentacion/consultar_encuesta_realizadas.php">
        <img src="../presentacion/imagenes/encuesta.png" width="56" height="57" id="cambiar" title="ENCUESTAS REALIZADAS" ><span style="color:#000;">
        </a></td>
        <td><a href="../presentacion/consulta_pedidos_usuario.php?">
        <img src="../presentacion/imagenes/agn.png" width="56" height="57" id="cambiar" title="CONSULTAR PEDIDOS" ><span style="color:#000;">
        </a></td>
         <td><a href="../presentacion/consulta_proximas_vista.php?">
        <img src="../presentacion/imagenes/calendar.png" width="56" height="57" id="cambiar" title="CONSULTAR PEDIDOS" ><span style="color:#000;">
        </a></td>
        </tr>
    </table>
    <br />
    <br />
    <br />
<center><img src="../presentacion/imagenes/3m.png" width="24%" height="17%" /></center>
<form id="cliente_nuevo" name="cliente_nuevo" action="../logica/registrar_paciente.php" method="post" style="width:100%;" >
	<div class="form-group" style="display:none">
        <input id="LAT" name="LAT" type="text" class="form-control" autocomplete="off"/>
        <input id="lon" name="lon" type="text" class="form-control" autocomplete="off"/>
    </div>
	<header>
        <div class="container" id="div_header">
        	<center><h1>ODONTOLOGO NUEVO</h1></center>
        </div>
    </header>
      <input name="usuario" value="<?php echo $usua; ?>" type="hidden">
<input name="id_usuario" value="<?php echo $id_usu; ?>" type="hidden">
    <div class="form-group">
        <label for="cliente">Apellido</label><span class="asterisco">*</span>
        <br>
        <input name="apellido" id="apellido" class="form-control" type="text" placeholder="Apellidos" required="true" />
    </div>
    <div class="form-group">
        <label for="cliente">Nombre</label><span class="asterisco">*</span>
        <br>
        <input name="nombre" id="nombre" class="form-control" type="text" placeholder="Nombre" required="required" placeholder="Nombres"/>
    </div>
    <div class="form-group">
        <label for="cliente">Tipo de Identificaci&oacute;n</label><span class="asterisco">*</span>
        <br>
        <select name="tipo_identificacion" id="tipo_identificacion" class="form-control" required>
        	<option value="">Elija...</option>
            <option value="CC">C&eacute;dula de Ciudadan&iacute;a </option>
            <option value="CE">C&eacute;dula de Extranjer&iacute;a</option>
			<option value="NIT">NIT</option>
        </select>
    </div>
    <div class="form-group">
        <label for="cliente">N&uacute;mero de Identificaci&oacute;n</label><span class="asterisco">*</span>
        <br>
        <input name="identificacion" id="identificacion" class="form-control" type="text" placeholder="identificacion" required="required" maxlength="20"/>
    </div>
    <div class="form-group">
        <label for="cliente">Direcci&oacute;n Consultorio</label><span class="asterisco">*</span>
        <br>
        <input name="direccion" id="direccion" class="form-control" type="text" placeholder="Direccion" required="required"/>
    </div>
    <div class="form-group">
        <label for="cliente">Departamento</label><span class="asterisco">*</span>
        <br>
        <select name="departamento" id="departamento" class="form-control" required="required">
        	<option value="">ELIJA...</option>
        	<?php
			if($id_usu==2){
				$consulta_departamento=mysqli_query($conex,"SELECT * FROM 3m_departamento WHERE NOMBRE_DEPARTAMENTO!='' ORDER BY NOMBRE_DEPARTAMENTO ASC");
			}else{
                if($CIUDAD=='BOGOTA' && $id_usu != 24 && $id_usu != 19 && $id_usu!=23 && $id_usu!=21 && $id_usu!=9 && $id_usu!=7&& $id_usu!=35){ // VER BOGOTA Y CUNDINAMARCA
                $CONDICION="IN ('BOGOTA','CUNDINAMARCA','CASANARE')";
                }
                else if($CIUDAD=='BOGOTA' && $id_usu==19 ){//VER BOGOTA CUNDINAMARCA Y META Y CAQUETA
                    $CONDICION="IN ('BOGOTA','CUNDINAMARCA','META','CASANARE','CAQUETA','ARAUCA', 'BOYACA', 'SANTANDER', 'ATLANTICO', 'VALLE DEL CAUCA', 'TOLIMA','HUILA','CALDAS','CAUCA')";
                }
                 else if($id_usu==9){ //VER ANTIOQUIA Y BOGOTA Y CUNDINAMARCA
                        $CONDICION="IN ('BOGOTA','CUNDINAMARCA')";
                }
                else if($CIUDAD=='BOGOTA' && $id_usu==21){ //VER ANTIOQUIA Y BOGOTA Y CUNDINAMARCA
                        $CONDICION="IN ('BOGOTA','META','CUNDINAMARCA')";
                }
                else if($CIUDAD=='BOGOTA' && $id_usu==7){ //VER ANTIOQUIA Y BOGOTA Y CUNDINAMARCA
                        $CONDICION="IN ('BOGOTA','ARAUCA','BOYACA','META','CUNDINAMARCA','CALDAS','RISARALDA','VALLE DEL CAUCA', 'ANTIOQUIA')";
                }
                else if($CIUDAD=='BOGOTA' && $id_usu==23){ //ELIMINAR
                    $CONDICION="IN ('CUNDINAMARCA','RISARALDA','NORTE DE SANTANDER','SANTANDER','META','CASANARE','BOGOTA','MAGDALENA','CALI','BOYACA','TOLIMA','HUILA','SANTANDER')";
                }
				
                else if($CIUDAD=='ANTIOQUIA' && $id_usu==37){ //VER ANTIOQUIA
                    $CONDICION="IN ('ANTIOQUIA')";
                }
                else if($CIUDAD=='BOGOTA' && $id_usu==24){ //VER ANTIOQUIA
                    $CONDICION="IN ('BOGOTA','CUNDINAMARCA','ANTIOQUIA','SUCRE')";
                }
                else if($CIUDAD=='CALI' && $id_usu!=34){//VER VALLE DEL CAUCA Y CAUCA
					$CONDICION="IN ('VALLE DEL CAUCA','CAUCA') ";
				}
				else if($CIUDAD=='CALI' && $id_usu==34){//VER VALLE DEL CAUCA Y CAUCA
					$CONDICION="IN ('VALLE DEL CAUCA','CAUCA','NARINO') ";
				}	
                else if($CIUDAD=='BARRANQUILLA' && $id_usu!=7){ //VER ATLANTICO Y BOLIVAR ZONA COSTA
				  if($CIUDAD=='BARRANQUILLA' && $id_usu==27){
                        $CONDICION=" IN('ATLANTICO','BOLIVAR','MAGDALENA','MONTERIA','GUAJIRA','CORDOBA','CESAR','SUCRE')";
						}else{
						$CONDICION=" IN('ATLANTICO','BOLIVAR','MAGDALENA','MONTERIA','GUAJIRA','CORDOBA','CESAR')";
						}
				}
                else if($CIUDAD=='BARRANQUILLA' && $id_usu==7){ //VER ATLANTICO Y BOLIVAR ZONA COSTA
                        $CONDICION=" IN('ATLANTICO','BOLIVAR','MAGDALENA','MONTERIA','GUAJIRA','CORDOBA','CESAR')";
                }
							
                else if($CIUDAD=='CARTAGENA'){ // VER BOLIVAR
                        $CONDICION=" IN('ATLANTICO','BOLIVAR','MAGDALENA','SUCRE'";
                }
                else if($CIUDAD=='BUCARAMANGA'){ // VER BOLIVAR
                        $CONDICION=" IN ('SANTANDER','NORTE DE SANTANDER') ";
                }
    			else if($CIUDAD=='TUNJA'){ // VER BOYACA
                        $CONDICION=' IN("BOYACA","SANTANDER","CASANARE") ';
                }
    			else if($CIUDAD=='MANIZALES'){ // VER EJE CAFETERO
                        $CONDICION=' IN("CALDAS","RISARALDA","QUINDIO") ';
                }
    			else if($CIUDAD=='PASTO'){ // VER EJE CAFETERO
                        $CONDICION=' IN("NARINO") ';
                }
                else if($CIUDAD=='BOGOTA' && $id_usu==35 ){//VER BOGOTA CUNDINAMARCA Y META Y CAQUETA
                    $CONDICION="IN ('BOGOTA','CUNDINAMARCA','CASANARE','SUCRE')";
                }
				
					
				
            $consulta_departamento=mysqli_query($conex,"SELECT * FROM 3m_departamento WHERE NOMBRE_DEPARTAMENTO ".$CONDICION." ORDER BY NOMBRE_DEPARTAMENTO ASC");

        }
			echo mysqli_error($conex);
			while($datos=mysqli_fetch_array($consulta_departamento))
			{
			?>
        		<option><?php echo $datos['NOMBRE_DEPARTAMENTO'] ?></option>
            <?php
			}
            ?>
        </select>
    </div>
    <div class="form-group">
        <label for="cliente">Ciudad</label><span class="asterisco">*</span>
        <br>
        <select name="ciudad" id="ciudad" class="form-control" required disabled="disabled">
        </select>
    </div>
    <div class="form-group">
        <label for="cliente">Celular</label>
        <br>
        <input name="celular" id="celular" class="form-control" type="tel" pattern="[0-9]{10}"  title="Ej: 1115468975" placeholder="Celular" required="required" />
    </div>
    <div class="form-group">
        <label for="cliente">Otro n&uacute;mero</label>
        <br>
        <input name="telefono" id="telefono" class="form-control" type="tel"  title="Ej: 2547896" placeholder="Otro n&uacute;mero"/>
    </div>
    <div class="form-group">
        <label for="cliente">E-mail</label>
        <br>
        <input name="email" id="email" class="form-control" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Ej: ejemplo@ejemplo.com" placeholder="E-mail" required="required" />
    </div>
 
    
    <div class="form-group">
         <label for="tipo_odontologo">Tipo de odontologo</label><span class="asterisco">*</span>
        <br>
            <select name="tipo_odontologo" id="tipo_odontologo" class="form-control" required="required">
                <option value="">Elija...</option>
                <?php
                  $consulta_tipo =mysqli_query($conex,"SELECT id_tipo,detalle_tipo FROM 3m_tipo_odontologo");
                  while($dato_tipo=mysqli_fetch_array($consulta_tipo)) { ?>
                <option value="<?php echo $dato_tipo['id_tipo']; ?>"><?php echo $dato_tipo['detalle_tipo']; ?></option>
                <?php } ?>
            </select>
    </div>
     <div class="form-group">
         <label for="tipo_especialidad">Especialidad</label><span class="asterisco">*</span>
        <br>
            <select name="tipo_especialidad" id="tipo_especialidad" class="form-control" required="required" disabled="true">
            </select>
        </br>
     </div>
    
    <div class="form-group">
        <label for="cliente">Distribuidor que compra</label><span class="asterisco">*</span>
        <br>
        <select name="distribuidor" id="distribuidor" class="form-control" required>
        	<option value="">Elija...</option> 
            
            <?php
         $consulta =mysqli_query($conex,"
         SELECT DISTINCT d.NOMBRE_DENTAL, u.USER, u.CIUDAD
            FROM 3m_dentales AS d
            INNER JOIN 3m_usuario AS u ON d.CIUDAD_DENTAL=u.CIUDAD
            WHERE ID_USUARIO='".$id_usu."' AND ID_DENTAL <> 5 ORDER BY NOMBRE_DENTAL ASC;");

            if($id_usu!=2){     
                  while($dato=mysqli_fetch_array($consulta)) {
                      $ciudad = $dato['CIUDAD'];
                      ?>
                <option value="<?php echo $dato['NOMBRE_DENTAL']; ?>"><?php echo $dato['NOMBRE_DENTAL']; ?></option>
                <?php 
                  }
                  if($ciudad !='ANTIOQUIA'){ ?>
				<option>Dental ALDENTAL</option>
			<?php }
            if ($id_usu==19 || $id_usu==27 || $id_usu==9){?>
            <option>Bracket Ltda-Andres Beltran</option>
            <?php }
                if ($id_usu==7){ ?>
            <option>Dental Nader Villavicencio-Jhovanny Parra</option>        
            <option>Bracket Ltda-Andres Beltran</option>
			<option>Luz Elena Sanchez-Casa Dental Sede Norte</option>
			<?php 
			}
            if($id_usu==33 ){//dlozano ?>
            <option>ODONTOSOLUCIONES-MARIA ISABEL VILLABONA</option>
            <option>Luz Karime Zanabria-Vertice S.A.S</option>
            <?php }
            if($id_usu==23 ){//jneira ?>
            <option>Dental Nader Villavicencio-Jhovanny Parra</option>  
            <?php }
            if($id_usu==9 && $id_usu==21){//achacon ?>
            <option>Dental Nader Villavicencio-Jhovanny Parra</option>  
            <?php }
			if($id_usu==19){//AINFANTES QUE MANEJA BOGOTA Y VILLLAVICENCIO?>
            <option>Dental Nader Villavicencio-Jhovanny Parra</option>  
            <option>Jose Alejo Rodriguez Barragan - Dental Jar</option>
            <option>Luz Karime Zanabria-Vertice S.A.S</option>
            <?php }
            if($id_usu==7 ){//SGONZALES QUE MANEJA BOGOTA Y VISITA VILLLAVICENCIO?>
            <option>Dental Nader Villavicencio-Jhovanny Parra</option>  
            <?php }
            if($id_usu==31  ){//ALGARCIA MANEJA CIUDAD ARMENIA?>
            <option>Equidentales</option>  
            <?php }
            if($id_usu==30 ){//SACASTELLANOS QUE MANEJA BOGOTA Y VILLLAVICENCIO?>
            <option>Dental Nader Villavicencio-Jhovanny Parra</option>  
            <?php }
            if($id_usu==29){//fvanstrahlen ?>
            <option>Luz Karime Zanabria-Vertice S.A.S</option>  
            <?php }
			if($id_usu==26){ // usuario visita ?>
			<option>Faride Maestre-Alfa Representaciones dentales</option>					
			<option>Ana Bueno-Elegir Soluciones</option>					
			<?php }
			if($id_usu==16){?>
			<option>Paola Linares-Dental 83 Portafolio 3M ESPE</option>
			<?php }
			if($id_usu==21){ // crojas ?> 
			<option>Luz Elena Sanchez-Casa Dental Sede Norte</option>
            <option>Dental Nader Villavicencio-Jhovanny Parra</option> 
			<?php }
            if($id_usu==36){ // lguevara ?> 
            <option>Faiber Laverde-Dentales Y Dentales</option>
            <option>Gina Santamaria-Dental Nader</option>
            <option>Yina Moncayo-Dental Palermo</option>
         
            <?php }
            if($id_usu>0){ // arestrepo $id_usu==37 ?> 
            <option>Bracket Ltda-Andres Beltran</option>
            <?php }
			if($id_usu==30){// SACASTELLANOS
				$consulta_dentales= mysqli_query($conex,"SELECT DISTINCT d.NOMBRE_DENTAL, u.USER, u.CIUDAD
				FROM 3m_dentales AS d
				INNER JOIN 3m_usuario AS u ON d.CIUDAD_DENTAL=u.CIUDAD
				WHERE u.CIUDAD in ('BOGOTA') GROUP BY ID_DENTAL ORDER BY NOMBRE_DENTAL ASC");
				 while($dato=mysqli_fetch_array($consulta_dentales)) { ?>
                                <option value="<?php echo $dato['NOMBRE_DENTAL']; ?>"><?php echo $dato['NOMBRE_DENTAL']; ?></option>
                         <?php } 
			}
                    }
			if($id_usu==2){
				$consulta_dentales= mysqli_query($conex,"SELECT DISTINCT d.NOMBRE_DENTAL, u.USER, u.CIUDAD
				FROM 3m_dentales AS d
				INNER JOIN 3m_usuario AS u ON d.CIUDAD_DENTAL=u.CIUDAD
				GROUP BY ID_DENTAL ORDER BY NOMBRE_DENTAL ASC");
				 while($dato=mysqli_fetch_array($consulta_dentales)) { ?>
                <option value="<?php echo $dato['NOMBRE_DENTAL']; ?>"><?php echo $dato['NOMBRE_DENTAL']; ?></option>
                <?php }
			} ?>

        </select>
    </div>

    <br />
    <br />
    <div class="form-group">
            <center>
                <input name="btn_guardar_odontologo" type="submit" id="btn_guardar_odontologo" value="GUARDAR" class="form-control btn" onclick="return validar(cliente_nuevo)"/>
            </center>
    </div>
    <br />
</form>

</body>
</html>

<?php }
} ?>