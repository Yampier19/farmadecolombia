<?php
	include("session.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Confirmar pedido</title>
<style>
.aviso3
{
	font-size: 130%;
	font-weight: bold;
	color: #11a9e3;
	text-transform:uppercase;
	/*font-family: "Trebuchet MS";
	font-family:"Gill Sans MT";
	border-radius:10px;
	background: #11a9e3;*/
	background-color:transparent;
	text-align: center;
	padding:10px;
}
.error
{
	font-size: 130%;
	font-weight: bold;
	color: #fb8305;
	text-transform:uppercase;
	background-color:transparent;
	text-align: center;
	padding:10px;
}
</style>
</head>
<body>
<br />
<br />
<?php
	/*require_once("../dompdf/dompdf_config.inc.php");*/
        header("Content-Type: text/html;charset=utf-8");
	include('../datos/conex_copia.php');
	$string_intro = getenv("QUERY_STRING"); 
	parse_str($string_intro);
	
	if(isset($_POST["btn_cliente_no_efectivo"])){
		$latitud=$_POST['LAT'];
		$longitud=$_POST['lon'];
		$resultado="CREACION ODONTOLOGO NO EFECTIVO";
		$apellido= utf8_decode($_POST['apellido']);
		$nombre= utf8_decode($_POST['nombre']);
		$direccion=$_POST['direccion'];
		$departamento=$_POST['departamento'];
		$ciudad=$_POST['ciudad'];
		$AUTOR_REGISTRO_FK=$_POST["usuario"];
		$id_usuario =$_POST["id_usuario"];
        $id_tipo_odontologo = $_POST["tipo_odontologo"];
        $id_especialidad = $_POST["tipo_especialidad"];
		
		
		//$AUTOR_REGISTRO_FK=$usua;
		$consulta_usuario=mysqli_query($conex,"SELECT * FROM 3m_usuario WHERE ID_USUARIO='$id_usuario'");
		while($informacion=mysqli_fetch_array($consulta_usuario))
		{
			$dato_usu=$informacion['USER'];
			$nombre_usu=$informacion['NOMBRES'];
			$apellido_usu=$informacion['APELLIDOS'];
		}
		
		//CONSULTAR_DUPLICADO_NO EFECTIVO
		$CONSULTAR_DUPLICADO_NO_EFECTIVO = mysqli_query($conex,"SELECT 
ID_CLIENTE FROM 3m_cliente WHERE NOMBRE_CLIENTE='".$nombre."' AND APELLIDO_CLIENTE='".$apellido."' AND DEPARTAMENTO_CLIENTE='".$departamento."' AND CIUDAD_CLIENTE='".$ciudad."'");
		$dato_cliente_duplicado = mysqli_fetch_array($CONSULTAR_DUPLICADO_NO_EFECTIVO);
		$ID_CLIENTE_DUPLICADO = $dato_cliente_duplicado["ID_CLIENTE"];
		

		
		//NO EXISTEN DUPLICADOS __ 
		if(empty($ID_CLIENTE_DUPLICADO)){
					 $INGRESAR_CLIENTE= mysqli_query($conex,"INSERT INTO 3m_cliente (NOMBRE_CLIENTE,APELLIDO_CLIENTE,DIRECCION_CLIENTE,DEPARTAMENTO_CLIENTE,CIUDAD_CLIENTE,FECHA_REGISTRO,AUTOR_REGISTRO_FK,USUARIO_ASIGNADO,ESTADO_ASIGNADO)
						VALUES('".$nombre."','".$apellido."','".$direccion."','".$departamento."','".$ciudad."',NOW(),'".$dato_usu."',".$id_usuario.",'ASIGNADO');
		");
		
			if($INGRESAR_CLIENTE)
				{
					$select=mysqli_query($conex,"SELECT ID_CLIENTE FROM 3m_cliente ORDER BY ID_CLIENTE DESC LIMIT 1");
					echo mysqli_error($conex);	
					while($dato_client=mysqli_fetch_array($select))
					{
						$id_cliente_campo=$dato_client['ID_CLIENTE'];
					}
					
					$INSERT=mysqli_query($conex,"INSERT INTO 3m_gestion
					(ACCION,TIPIFICACION_GESTION,LATITUD_GESTION,LONGITUD_GESTION,ID_ASESOR_GESTION,ASESOR_GESTION,OBSERVACION_GESTION,ID_CLIENTE_FK) 
					VALUES ('CREACION ODONTOLOGO','".$resultado."','".$latitud."','".$longitud."','".$id_usuario."','".$usua."','CREACION ODONTOLOGO NO EFECTIVO','".$id_cliente_campo."')");
					
					$INGRESAR_ENCUESTA=mysqli_query($conex,"INSERT INTO 3m_encuesta (DISTRIBUIDOR_COMPRA,ID_CLIENTE_FK_5,ID_USUARIO_FK_5)
		VALUES('',".$id_cliente_campo.",".$id_usuario.")");

				
			
			?> 
                <span style="margin-top:5%;">
                    <center>
                    <img src="../presentacion/imagenes/CHULO1.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/> 
                    <p class="aviso3" style=" width:68.9%; margin:auto auto;">
                        EL ODONTOLOGO HA SIDO INGRESADO SATISFACTORIAMENTE.
                    </p>
                    <br />
                    <br />
                    <br/>
                    <a href="../presentacion/inicio_consulta_cliente.php?x=<?php echo base64_encode($id_cliente_campo); ?>" class="btn_continuar">
                            <img src="../presentacion/imagenes/BTN_CONTINUAR2.png" style="width:152px; height:37px" />
                        </a>
                </center>
                </span>
			<?php
					}
			}else{
			?>
            	  <span style="margin-top:5%;">
                    <center>
                    	<img src="../presentacion/imagenes/advertencio_1.png" style="width:100px; margin-top:100px;margin-top:5%;"/>
                    </center>
                </span>
                <p class="error" style=" width:68.9%; margin:auto auto;">
                	EL ODONTOLOGO NO HA SIDO INGRESADO.
</p>
                <br />
                <br />
                <center>
                    <a href="javascript:history.go(-1)" target="info" class="btn_continuar">
                        <img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" />
                    </a>
                </center>
                <br/>
            	
            <?php
			}
    }
	else if(isset($_POST['btn_guardar_odontologo']))
	{
		/*LOCALIZACION*/
		$latitud=$_POST['LAT'];
		$longitud=$_POST['lon'];
		$resultado="CREACION ODONTOLOGO";
		/*fin*/
		$apellido=  utf8_decode( $_POST['apellido']);
		$nombre= utf8_decode($_POST['nombre']);
		$tipo_identificacion=$_POST['tipo_identificacion'];
		$identificacion=$_POST['identificacion'];
		$direccion=$_POST['direccion'];
		$departamento=$_POST['departamento'];
		$ciudad=$_POST['ciudad'];
		$celular=$_POST['celular'];
		$telefono=$_POST['telefono'];
		$email=$_POST['email'];
		$razon_social='';
		$nit='';
		$pagina_web='';
		$num_empleados=0;
		$distribuidor=$_POST['distribuidor'];
		
		$AUTOR_REGISTRO_FK=$_POST["usuario"];
		$id_usuario =$_POST["id_usuario"];
        $id_tipo_odontologo = $_POST["tipo_odontologo"];
        $id_especialidad = $_POST["tipo_especialidad"];
        
		
		//$AUTOR_REGISTRO_FK=$usua;
		$consulta_usuario=mysqli_query($conex,"SELECT * FROM 3m_usuario WHERE ID_USUARIO='$id_usuario'");
		while($informacion=mysqli_fetch_array($consulta_usuario))
		{
			$dato_usu=$informacion['USER'];
			$nombre_usu=$informacion['NOMBRES'];
			$apellido_usu=$informacion['APELLIDOS'];
		}
		//$id_usuario=$id_usu;		
		mysqli_query("SET NAMES utf8");
		
	
		$CONSULTAR_DUPLICADO =mysqli_query($conex,"SELECT * FROM 3m_cliente WHERE TIPO_IDENTIFICACION='$tipo_identificacion' AND IDENTIFICACION_CLIENTE='$identificacion'");
		
		$DUPLICADOS = mysqli_num_rows($CONSULTAR_DUPLICADO );
		
	
	if($DUPLICADOS==0 || $identificacion == '01'){	
		
		if($identificacion == '01'){
		
		$SELECT_ULT = mysqli_query($conex,"SELECT * FROM `3m_cliente` ORDER BY `3m_cliente`.`ID_CLIENTE` DESC LIMIT 1");
		
		while($datoid = mysqli_fetch_array($SELECT_ULT)){
		
		$idautc = $datoid['ID_CLIENTE'];
		$identificacion01 = $idautc + 1;
		}
		
		
		$INGRESAR_CLIENTE=mysqli_query($conex,"INSERT INTO 3m_cliente
(NOMBRE_CLIENTE,APELLIDO_CLIENTE,TIPO_IDENTIFICACION,IDENTIFICACION_CLIENTE,DIRECCION_CLIENTE,DEPARTAMENTO_CLIENTE,CIUDAD_CLIENTE,CELULAR_CLIENTE,TELEFONO_CLIENTE,EMAIL_CLIENTE,RAZON_SOCIAL,NIT,PAGINA_WEB_NEGOCIO,No_EMPLEADOS,AUTOR_REGISTRO_FK,FECHA_ASIGNADO,ORDEN_ASIGNADO,ESTADO_ASIGNADO,FECHA_ULTIMO_CARGUE,USUARIO_ULTIMO_CARGUE,USUARIO_ASIGNADO,NOMBRE_USUARIO_ASIGNADO)
VALUE
('$nombre','$apellido','$tipo_identificacion','$identificacion01','$direccion','$departamento','$ciudad','$celular','$telefono','$email','$razon_social','$nit','$pagina_web','$num_empleados','$AUTOR_REGISTRO_FK',CURDATE(),'0','ASIGNADO',CURDATE(),'$AUTOR_REGISTRO_FK','$id_usuario','$AUTOR_REGISTRO_FK')");
		
		}else{
		
		$INGRESAR_CLIENTE=mysqli_query($conex,"INSERT INTO 3m_cliente
(NOMBRE_CLIENTE,APELLIDO_CLIENTE,TIPO_IDENTIFICACION,IDENTIFICACION_CLIENTE,DIRECCION_CLIENTE,DEPARTAMENTO_CLIENTE,CIUDAD_CLIENTE,CELULAR_CLIENTE,TELEFONO_CLIENTE,EMAIL_CLIENTE,RAZON_SOCIAL,NIT,PAGINA_WEB_NEGOCIO,No_EMPLEADOS,AUTOR_REGISTRO_FK,FECHA_ASIGNADO,ORDEN_ASIGNADO,ESTADO_ASIGNADO,FECHA_ULTIMO_CARGUE,USUARIO_ULTIMO_CARGUE,USUARIO_ASIGNADO,NOMBRE_USUARIO_ASIGNADO)
VALUE
('$nombre','$apellido','$tipo_identificacion','$identificacion','$direccion','$departamento','$ciudad','$celular','$telefono','$email','$razon_social','$nit','$pagina_web','$num_empleados','$AUTOR_REGISTRO_FK',CURDATE(),'0','ASIGNADO',CURDATE(),'$AUTOR_REGISTRO_FK','$id_usuario','$AUTOR_REGISTRO_FK')");
		
		}
		
		echo mysqli_error($conex);		
		if($INGRESAR_CLIENTE)
		{
			$select=mysqli_query($conex,"SELECT ID_CLIENTE FROM 3m_cliente ORDER BY ID_CLIENTE DESC LIMIT 1");
			echo mysqli_error($conex);	
			while($dato_client=mysqli_fetch_array($select))
			{
				$id_cliente_campo=$dato_client['ID_CLIENTE'];
			}
			$INSERT=mysqli_query($conex,"INSERT INTO 3m_gestion(ACCION,TIPIFICACION_GESTION,LATITUD_GESTION,LONGITUD_GESTION,ID_ASESOR_GESTION,ASESOR_GESTION,OBSERVACION_GESTION,ID_CLIENTE_FK) VALUES ('CREACION ODONTOLOGO','".$resultado."','".$latitud."','".$longitud."','".$id_usuario."','".$usua."','CREACION ODONTOLOGO NUEVO','".$id_cliente_campo."')");
			echo mysqli_error($conex);
			
			
			$consultar=mysqli_query($conex,"SELECT ID_CLIENTE FROM 3m_cliente ORDER BY ID_CLIENTE DESC LIMIT 1");
			while($datos=mysqli_fetch_array($consultar))
			{
				$ID_CLIENTE=$datos['ID_CLIENTE'];
			}
			
     
			$INGRESAR_ENCUESTA=mysqli_query($conex,"INSERT INTO 3m_encuesta (TIPO_ODONTOLOGO,ESPECIALIDAD_ODONTOLOGO,DISTRIBUIDOR_COMPRA,ID_CLIENTE_FK_5,ID_USUARIO_FK_5)
		VALUES(".$id_tipo_odontologo.",'".$id_especialidad."','$distribuidor','$ID_CLIENTE','$id_usuario')");
			echo mysqli_error($conex);
			
			
			include('../presentacion/email/mail2.php');
			
			?> 
                <span style="margin-top:5%;">
                    <center>
                    <img src="../presentacion/imagenes/CHULO1.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/> 
                    <p class="aviso3" style=" width:68.9%; margin:auto auto;">
                        EL ODONTOLOGO HA SIDO INGRESADO SATISFACTORIAMENTE.
                    </p>
                    <br />
                    <br />
                    <br/>
                   <a href="../presentacion/inicio_consulta_cliente.php?x=<?php echo base64_encode($ID_CLIENTE); ?>" class="btn_continuar">
                            <img src="../presentacion/imagenes/BTN_CONTINUAR2.png" style="width:152px; height:37px" />
                        </a>
                </center>
                </span>
			<?php
		}
		if(!$INGRESAR_CLIENTE)
		{
			?>
                <span style="margin-top:5%;">
                    <center>
                    	<img src="../presentacion/imagenes/advertencio_1.png" style="width:100px; margin-top:100px;margin-top:5%;"/>
                    </center>
                </span>
                <p class="error" style=" width:68.9%; margin:auto auto;">
                	EL ODONTOLOGO NO HA SIDO INGRESADO SATISFACTORIAMENTE.
</p>
                <br />
                <br />
                <center>
                    <a href="javascript:history.go(-1)" target="info" class="btn_continuar">
                        <img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" />
                    </a>
                </center>
                <br/>
			<?php
		}
	}else{ 
	while($dato=mysqli_fetch_array($CONSULTAR_DUPLICADO))
			{
				$ID_CLIENTE=$dato['ID_CLIENTE'];
			}
	
	?> 
	 <span style="margin-top:5%;">
                    <center>
                    	<img src="../presentacion/imagenes/advertencio_1.png" style="width:100px; margin-top:100px;margin-top:5%;"/>
                    </center>
</span>
                <p class="error" style=" width:68.9%; margin:auto auto;">
                	LOS DATOS INGRESADOS YA EXISTEN EN EL SISTEMA, VERIFIQUE E INTENTE NUEVAMENTE
                </p>
                <br />
                <br />
                <center>
                  <a href="javascript:history.go(-1)" target="info" class="btn_continuar"> <img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" /></a>
                     <BR />
                     <a href="../presentacion/form_cliente_actualizar.php?ID_CLIENTE=<?PHP echo $ID_CLIENTE; ?>" class="btn_continuar">    
                              <img src="../presentacion/imagenes/lapiz.png" width="32" height="26" />&nbsp; EDITAR REGISTRO
                    </a>
                </center>
                <br/>
	
	<?php }
	
	}
?>

</body>
</html>