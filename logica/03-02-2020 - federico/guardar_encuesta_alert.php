<?php
include("session.php");
error_reporting(0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Confirmar pedido</title>

        <style>
            .aviso3 
            {
                font-size: 130%;
                font-weight: bold;
                color: #11a9e3;
                text-transform:uppercase;
                /*font-family: "Trebuchet MS";
                font-family:"Gill Sans MT";
                border-radius:10px;
                background: #11a9e3;*/
                background-color:transparent;
                text-align: center;
                padding:10px;
            }
            .error
            {
                font-size: 130%;
                font-weight: bold;
                color: #fb8305;
                text-transform:uppercase;
                background-color:transparent;
                text-align: center;
                padding:10px;
            }
        </style>
    </head>
    <body> 
        <?php

	$marcacion_gestion = $_POST['resultado_text'];
if($marcacion_gestion != 2){
	
        header("Content-Type: text/html;charset=utf-8");
        include('../datos/conex_copia.php');
        mysqli_query("SET NAMES 'utf8'");

       
        $ID_CLIENTE = $ID_CLIENTE;
        $id_usu;

        $identificacion = $_POST['identificacion'];
        $id_encuesta = $_POST['id_encuesta'];
        $editar = $_POST['editar'];
        $nombre = $_POST['nombre'];
        $apellido = $_POST['apellido'];
        $id_usu = $_POST["idusuario"];
        $usua = $_POST["name_usuario"];
        $id_cliente = $_POST['id_cliente'];

        if (empty($id_usu)) {
            ?>
            <span style="margin-top:5%;">
                <center>
                    <img src="../presentacion/imagenes/advertencio_1.png" style="width:100px; margin-top:100px;margin-top:5%;"/>
                </center>
            </span>
            <p class="error" style=" width:68.9%; margin:auto auto;">LA SESI&Oacute;N A CADUCADO, POR FAVOR INICIE SESION NUEVAMENTE</B>
            </p>
            <br />
            <br />

            <br/>

            <?php
        } else {
            $fecha = date("Y-m-d");
            $UPDATE = mysqli_query($conex, "UPDATE 3m_cliente
				SET
				ESTADO_ASIGNADO='GESTIONADO',
				USUARIO_ASIGNADO=".$id_usu.",
				NOMBRE_USUARIO_ASIGNADO='".$usua."',
				FECHA_ASIGNADO='".$fecha."'
				WHERE ID_CLIENTE='".$id_cliente."'");


            $consulta = mysqli_query($conex, "SELECT NOMBRE_CLIENTE,APELLIDO_CLIENTE, IDENTIFICACION_CLIENTE FROM 3m_cliente 
		 WHERE IDENTIFICACION_CLIENTE=$identificacion;");

            while ($dato = mysqli_fetch_array($consulta)) {
                $consulta_nombre = $dato['NOMBRE_CLIENTE'];
                $consulta_apellido = $dato['APELLIDO_CLIENTE'];
            }

            $consulta_duplicados = mysqli_num_rows($consulta);

            if ($consulta_duplicados > 1) {
                ?>
                <span style="margin-top:5%;">
                    <center>
                        <img src="../presentacion/imagenes/advertencio_1.png" style="width:100px; margin-top:100px;margin-top:5%;"/>
                    </center>
                </span>
                <p class="error" style=" width:68.9%; margin:auto auto;">LA IDENTIFICACION YA EXISTE. VERIFIQUE E INTENTE NUEVAMENTE</p>
                <br />
                <br />
                <center>
                    <a href="javascript:history.go(-1)" target="info" class="btn_continuar"><img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" /></a>
                </center>
                <br/>
                <?php
            } else {

                if (empty($consulta_nombre)) {
                    $vacia = 1;
                } else {
                    $vacia = 0;
                }

                if ($vacia == 0) {
                    //if(($nombre != $consulta_nombre) || ($apellido != $consulta_apellido)){
                    //}
                    //else{
                    $coinciden = 1;
                    //}
                }

                if ($coinciden == 1 || $vacia == 1) {
				//instancia de variables desde el formulario

                    $latitud = $_POST['LAT'];
                    $longitud = $_POST['lon'];

                    $id_resultado_text = $_POST['resultado_text'];
                    $id_sub_tipificacion = $_POST['sub_tipificacion_efectiva'];
                    $fecha_proxima_visita = $_POST["proxima_visita"];
                    $observacion =mysqli_real_escape_string($conex, strtoupper($_POST['observacion']));

					//consultar tipificacion
					
					$consultar_tipificacion=mysqli_query($conex,"SELECT tipificacion FROM 3m_tipificaciones WHERE id_tifipificacion=".$id_resultado_text);
					while($dato_tipificacion = mysqli_fetch_array($consultar_tipificacion)){
						$resultado_text=$dato_tipificacion["tipificacion"];
					}
					
					//consultar subtipificacion
				
					$consultar_subtipificacion=mysqli_query($conex,"SELECT subtipificacion FROM 3m_subtipificaciones WHERE id_subtipificacion=".$id_sub_tipificacion);

						while($dato_subtipificacion = mysqli_fetch_array($consultar_subtipificacion)){
							$sub_tipificacion=$dato_subtipificacion["subtipificacion"];
						}

                    if ($id_resultado_text == 4) {
                        $sub_tipificacion = 'VENTA EN EVENTO';
                    }
					else if ($id_resultado_text ==3) {
                        $sub_tipificacion_televenta = $_POST['sub_tipificacion_televenta'];
                        $sub_tipificacion = $sub_tipificacion_televenta;
                        $resultado_text = 'TELEVENTA';
                    } 

					if (empty($sub_tipificacion)) {
						$sub_tipificacion = 'ENCUESTA DE CONSUMO Y TRANSFERENCIA';
					}

                    /* actualizar informacion cliente */

                    $consulta = mysqli_query($conex, "SELECT AUTOR_REGISTRO_FK,USUARIO_ASIGNADO,NOMBRE_USUARIO_ASIGNADO,FECHA_ASIGNADO,ORDEN_ASIGNADO,FECHA_ULTIMO_CARGUE,USUARIO_ULTIMO_CARGUE FROM 3m_cliente 
                        WHERE ID_CLIENTE='" . $id_cliente . "'");

                    while ($dato = mysqli_fetch_array($consulta)) {
                        $AUTOR_REGISTRO_FK = $dato['AUTOR_REGISTRO_FK'];
                        $USUARIO_ASIGNADO = $dato['USUARIO_ASIGNADO'];
                        $NOMBRE_USUARIO_ASIGNADO = $dato['NOMBRE_USUARIO_ASIGNADO'];
                        $FECHA_ASIGNADO = $dato['FECHA_ASIGNADO'];
                        $ORDEN_ASIGNADO = $dato['ORDEN_ASIGNADO'];
                        $FECHA_ULTIMO_CARGUE = $dato['FECHA_ULTIMO_CARGUE'];
                        $USUARIO_ULTIMO_CARGUE = $dato['USUARIO_ULTIMO_CARGUE'];
                    }

                    if (empty($AUTOR_REGISTRO_FK)) {
                        $AUTOR_REGISTRO_FK = $usua;
                    } else {
                        $AUTOR_REGISTRO_FK = $AUTOR_REGISTRO_FK;
                    }

                    if (empty($USUARIO_ASIGNADO)) {
                        $USUARIO_ASIGNADO = $id_usu;
                    } else {
                        $USUARIO_ASIGNADO = $USUARIO_ASIGNADO;
                    }

                    if (empty($NOMBRE_USUARIO_ASIGNADO)) {
                        $NOMBRE_USUARIO_ASIGNADO = $AUTOR_REGISTRO_FK;
                    } else {
                        $NOMBRE_USUARIO_ASIGNADO = $NOMBRE_USUARIO_ASIGNADO;
                    }

                    if (empty($ORDEN_ASIGNADO)) {
                        $ORDEN_ASIGNADO = 0;
                    } else {
                        $ORDEN_ASIGNADO = $ORDEN_ASIGNADO;
                    }

                    if (empty($FECHA_ULTIMO_CARGUE)) {
                        $FECHA_ULTIMO_CARGUE = 'CURDATE()';
                    } else {
                        $FECHA_ULTIMO_CARGUE = $FECHA_ULTIMO_CARGUE;
                    }

                    if (empty($USUARIO_ULTIMO_CARGUE)) {
                        $USUARIO_ULTIMO_CARGUE = $id_usu;
                    } else {
                        $USUARIO_ULTIMO_CARGUE = $USUARIO_ULTIMO_CARGUE;
                    }


                    $fecha_actual = date("Y-m-d");

                    $CONSULTAR_REGISTRO_GESTION = mysqli_query($conex, "
			SELECT *
			FROM 3m_gestion
			WHERE  TIPIFICACION_GESTION ='" . $resultado_text . "' AND SUB_TIPIFICACION <>'TELEVENTA' AND SUB_TIPIFICACION <>'PEDIDO ELIMINADO' AND ID_ASESOR_GESTION =" . $id_usu . " AND ID_CLIENTE_FK=" . $id_cliente . " AND  DATE(FECHA_GESTION)='" . $fecha_actual . "'
			ORDER BY FECHA_GESTION DESC LIMIT 1");

                    $CONSULTAR_INGRESO_PEDIDO = mysqli_query($conex, "SELECT ID_GESTION FROM 3m_gestion 
			WHERE ACCION='INGRESO SOLICITUD PEDIDO' AND SUB_TIPIFICACION <>'TELEVENTA' AND SUB_TIPIFICACION <>'PEDIDO ELIMINADO'
                        AND DATE(FECHA_GESTION)='" . $fecha_actual . "' AND ID_ASESOR_GESTION=" . $id_usu . " AND ID_CLIENTE_FK=" . $id_cliente . "");
                    $CONSULTA_REGISTRO_PEDIDO_ELIMINADO = mysqli_query($conex, "SELECT * 
                        FROM 3m_gestion
                        WHERE ACCION='PEDIDO ELIMINADO' AND TIPIFICACION_GESTION='EFECTIVA'
                        AND DATE(FECHA_GESTION)='" . $fecha_actual . "' AND ID_ASESOR_GESTION=" . $id_usu . " AND ID_CLIENTE_FK=" . $id_cliente . "");

                    $NUMERO_REGISTRO_EFECTIVA = mysqli_num_rows($CONSULTAR_REGISTRO_GESTION);
                    $NUMERO_REGISTRO_TELEVENTA = mysql_num_rows($CONSULTAR_REGISTRO_TELEVENTA);
                    $NUMERO_REGISTRO_PEDIDO = mysqli_num_rows($CONSULTAR_INGRESO_PEDIDO);
                    $NUMERO_PEDIDO_ELIMINADO = mysqli_num_rows($CONSULTA_REGISTRO_PEDIDO_ELIMINADO);


                    if ($NUMERO_REGISTRO_EFECTIVA == 0 || $resultado_text == 'NO EFECTIVA') {
                        //INGRESAR UNA NUEVA GESTION

                        if ($NUMERO_PEDIDO_ELIMINADO > 0) {
                            $UPDATE_PEDIDO_ELIMINADO = mysqli_query($conex, "UPDATE 3m_gestion SET CORREO=0, `TIPIFICACION_GESTION` = 'PEDIDO ELIMINADO'
WHERE DATE(FECHA_GESTION)='" . $fecha_actual . "' AND ID_ASESOR_GESTION=" . $id_usu . " AND ID_CLIENTE_FK=" . $id_cliente . "");
                        }

                        $INSERT = mysqli_query($conex, "
			INSERT INTO 3m_gestion(ACCION,TIPIFICACION_GESTION,SUB_TIPIFICACION,LATITUD_GESTION,LONGITUD_GESTION,
			ID_ASESOR_GESTION,ASESOR_GESTION,OBSERVACION_GESTION,ID_CLIENTE_FK,FECHA_PROXIMA_VISITA) 
			VALUES ('REGISTRO VISITA','" . $resultado_text . "','" . $sub_tipificacion . "','" . $latitud . "','" . $longitud . "','" . $id_usu . "','" . $usua . "','" . $observacion . "','" . $id_cliente . "','" . $fecha_proxima_visita . "')");
                    } else if ($NUMERO_REGISTRO_EFECTIVA > 0 || $resultado_text != 'NO EFECTIVA') {
                        ?>
                        <span style="margin-top:5%;">
                            <center>
                                <img src="../presentacion/imagenes/advertencio_1.png" style="width:100px; margin-top:100px;margin-top:5%;"/>
                            </center>
                        </span>
                        <p class="error" style=" width:68.9%; margin:auto auto;">YA EXISTE UNA GESTION EL DIA HOY PARA ESTE CLIENTE, SI REQUIERE UNA NUEVA GESTION COMUNIQUESE CON EL COORDINADOR, PARA ELIMINAR LA GESTION ANTERIOR.</p>
                        <br />
                        <br />
                        <center>

                            <?php $id_cli = $id_cliente; ?>
                             <a href="../presentacion/inicio_consulta_cliente.php?x=<?php echo base64_encode($id_cli)?>&transferencia=1"  class="btn_continuar"> <img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" /></a>

                            <?PHP
                            $busqueda = mysqli_query($conex, "SELECT ACCION, DATE(FECHA_GESTION), ID_CLIENTE_FK, ID_ASESOR_GESTION 
                                FROM 3m_gestion WHERE ACCION='REGISTRO VISITA' AND DATE(FECHA_GESTION)='" . $fecha_actual . "' AND ID_CLIENTE_FK='" . $id_cliente . "' AND ID_ASESOR_GESTION='" . $id_usu . "'");

                            $conteo_visitas = mysqli_num_rows($busqueda);

                            if ($conteo_visitas > 0) {
                                ?>
                                <p class="error" style=" width:68.9%; margin:auto auto;">DE LO CONTRARIO SI DESEA FINALIZAR LA GESTION ANTERIOR DE CLICK EN CONTINUAR.</p>

                                <?php $id_cli = $id_cliente; ?> 
                                <a href="../presentacion/inicio_consulta_cliente.php?x=<?php echo base64_encode($id_cli)?>&transferencia=1"  class="btn_continuar"><img src="../presentacion/imagenes/BTN_CONTINUAR2.png" style="width:152px; height:37px" /></a>
                            </center>
                            <br/>
                            <br/>
                            <?php
                            die();
                        }
                    } else if ($NUMERO_REGISTRO_EFECTIVA > 0 && $NUMERO_REGISTRO_TELEVENTA == 0) {
                        //ACTUALIZA UNA GESTION
                        ?>
                        <span style="margin-top:5%;">
                            <center>
                                <img src="../presentacion/imagenes/advertencio_1.png" style="width:100px; margin-top:100px;margin-top:5%;"/>
                            </center>
                        </span>
                        <p class="error" style=" width:68.9%; margin:auto auto;">YA EXISTE UNA GESTION EL DIA HOY PARA ESTE CLIENTE, SI REQUIERE UNA NUEVA GESTION COMUNIQUESE CON EL COORDINADOR, PARA ELIMINAR LA GESTION ANTERIOR</B>
                        </p>
                        <br />
                        <br />

                        <center>

                            <?php $id_cli = $id_cliente; ?>
                             <a href="../presentacion/inicio_consulta_cliente.php?x=<?php echo base64_encode($id_cli)?>&transferencia=1"  class="btn_continuar"><img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" /></a>
                        </center>
                        <br/>

                        <?PHP
                        $busqueda = mysqli_query($conex, "SELECT ACCION, DATE(FECHA_GESTION), ID_CLIENTE_FK, ID_ASESOR_GESTION FROM 3m_gestion
WHERE ACCION='REGISTRO VISITA' AND DATE(FECHA_GESTION)='" . $fecha_actual . "' AND ID_CLIENTE_FK='" . $id_cliente . "' AND ID_ASESOR_GESTION='" . $id_usu . "'");

                        $conteo_visitas = mysqli_num_rows($busqueda);

                        if ($conteo_visitas > 0) {
                            ?>
                            <p class="error" style=" width:68.9%; margin:auto auto;">DE LO CONTRARIO SI DESEA FINALIZAR LA GESTION ANTERIOR DE CLICK EN CONTINUAR.</p>
                            <center>
                                <?php $id_cli = $id_cliente; ?> 
                                 <a href="../presentacion/inicio_consulta_cliente.php?x=<?php echo base64_encode($id_cli)?>&transferencia=1"  class="btn_continuar"><img src="../presentacion/imagenes/BTN_CONTINUAR2.png" style="width:152px; height:37px" /></a>
                            </center>
                            <br/>
                            <br/>
                            <?php
                            die();
                        }
                    } else if ($NUMERO_REGISTRO_EFECTIVA == 0 && $NUMERO_REGISTRO_TELEVENTA > 0) {
                        //ACTUALIZA UNA GESTION


                        while ($datos_gestion = (mysqli_fetch_array($CONSULTAR_REGISTRO_GESTION))) {
                            $id_gestion = $datos_gestion["ID_GESTION"];
                        }

                        //ACTUALIZAR EL REGISTRO TELEVENTA
                        $ACTUALIZAR_REGISTRO_GESTION = mysqli_query($conex, "
			UPDATE 3m_gestion SET TIPIFICACION_GESTION='" . $resultado_text . "' , SUB_TIPIFICACION='TELEVENTA', LATITUD_GESTION ='$latitud' ,LONGITUD_GESTION='$longitud', CORREO=0, OBSERVACION_GESTION='".$observacion."',FECHA_PROXIMA_VISITA='$fecha_proxima_visita',FECHA_GESTION=NOW()
			WHERE ID_GESTION='$id_gestion'");
                    } else if ($NUMERO_REGISTRO_EFECTIVA > 0 && $NUMERO_REGISTRO_TELEVENTA > 0) {

                        $ACTUALIZAR_REGISTRO_GESTION = mysqli_query($conex, "
			UPDATE 3m_gestion SET TIPIFICACION_GESTION='" . $resultado_text . "' , SUB_TIPIFICACION='$sub_tipificacion', LATITUD_GESTION ='$latitud' ,LONGITUD_GESTION='$longitud', CORREO=0, OBSERVACION_GESTION='".$observacion."', FECHA_PROXIMA_VISITA='$fecha_proxima_visita', FECHA_GESTION=NOW()
			WHERE ID_GESTION='$id_gestion'");
                    }
                    $especialidad_odontologo = $_POST['especialidad_odontologo'];
                    $distribuidor = $_POST['distribuidor'];
                    $cantidad_pacientes = $_POST['cantidad_pacientes'];
                    $clasificacion_odontologo_ant = $_POST['clasificacion_odontologo'];
                    if ($clasificacion_odontologo_ant == 'Elija...') {
                        $clasificacion_odontologo_ant = '';
                    }

                    $numero_tel = $_POST['numero_tel'];
                    $numero_cel = $_POST['numero_cel'];
                    $dia_visita = $_POST['dia_visita'];
                    $hora_ini_visita = $_POST['hora_ini_visita'];
                    $hora_fin_visita = $_POST['hora_fin_visita'];
                    $id_usuario = $_POST['id_US'];
                    $tipo_odontologo = $_POST["tipo_odontologo"];

                    if ($tipo_odontologo == 2) {
                        $CASOS_NUEVOS_SEMANA = $_POST["casos_nuevos"];
                    } else {
                        $CASOS_NUEVOS_SEMANA = 0;
                    }

                    $resultado = "INGRESO ENCUESTA";

                    //actualizar cliente

                    $actualizar_cliente = mysqli_query($conex, "UPDATE 3m_cliente SET
			IDENTIFICACION_CLIENTE='$identificacion',
			CELULAR_CLIENTE='$numero_cel',
			TELEFONO_CLIENTE='$numero_tel',
			No_EMPLEADOS='$cantidad_pacientes',
			AUTOR_REGISTRO_FK='$AUTOR_REGISTRO_FK',
			USUARIO_ASIGNADO='$USUARIO_ASIGNADO',
			NOMBRE_USUARIO_ASIGNADO = '$NOMBRE_USUARIO_ASIGNADO',
			ORDEN_ASIGNADO ='$ORDEN_ASIGNADO',
			FECHA_ULTIMO_CARGUE =CURDATE(),
			USUARIO_ULTIMO_CARGUE='$USUARIO_ULTIMO_CARGUE'
			WHERE
			ID_CLIENTE='$id_cliente'");

                    if ($id_encuesta == '') {

                        $INGRESAR_ENCUESTA = mysqli_query($conex, "INSERT INTO 3m_encuesta (CASOS_NUEVOS_SEMANA,CANTIDAD_PACIENTES,ESPECIALIDAD_ODONTOLOGO,CLASIFICAION_ODONTOLOGO_ANTERIOR,CLASIFICACION_ODONTOLOGO,DISTRIBUIDOR_COMPRA,ID_CLIENTE_FK_5,ID_USUARIO_FK_5, FECHA_ACTUALIZACION, DIA_VISITA, HORA_INI_VISITA, HORA_FIN_VISITA)
                        VALUES('".$CASOS_NUEVOS_SEMANA."','$cantidad_pacientes','$especialidad_odontologo','$clasificacion_odontologo_ant','clasificacion_odontologo','$distribuidor','$id_cliente','$id_usuario',CURRENT_TIMESTAMP,'$dia_visita', '$hora_ini_visita')");
                        echo mysql_error($conex);
                        if ($INGRESAR_ENCUESTA) {

                            echo mysqli_error($conex);

                            echo mysqli_error($conex);
                            ?> 
                            <span style="margin-top:5%;">
                                <center>
                                    <img src="../presentacion/imagenes/CHULO1.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
                                </center>
                            </span>
                            <p class="aviso3" style=" width:68.9%; margin:auto auto;"><?php echo $sub_tipificacion; ?><br> LA ENCUESTA HA SIDO INGRESADO SATISFACTORIAMENTE.</p>
                            <br />
                            <br />
                            <center>
                                <?php $id_cli = $id_cliente; ?>
                                <a href="../presentacion/inicio_consulta_cliente.php?x=<?php echo base64_encode($id_cli)?>&transferencia=1"  class="btn_continuar"><img src="../presentacion/imagenes/BTN_CONTINUAR2.png" id="btn" title="CONTINUAR"/></a>
                            </center>
                            <br/>
                            <?php
                        }
                        if (!$INGRESAR_ENCUESTA) {
                            ?>
                            <span style="margin-top:5%;">
                                <center>
                                    <img src="../presentacion/imagenes/advertencio_1.png" style="width:100px; margin-top:100px;margin-top:5%;"/>
                                </center>
                            </span>
                            <p class="error" style=" width:68.9%; margin:auto auto;">LA ENCUESTA NO HA SIDO INGRESADO SATISFACTORIAMENTE.</p>
                            <br />
                            <br />
                            <center>
                                <a href="javascript:history.go(-1)" target="info" class="btn_continuar"><img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" /></a>
                            </center>
                            <br/>
                            <?php
                        }
                    }
                    if ($id_encuesta != '') {

                        echo mysqli_error($conex);

                        $actualizar = mysqli_query($conex, "UPDATE 3m_encuesta SET
                        CASOS_NUEVOS_SEMANA               ='$CASOS_NUEVOS_SEMANA',
                        CANTIDAD_PACIENTES                ='$cantidad_pacientes',
            			ESPECIALIDAD_ODONTOLOGO           ='$especialidad_odontologo',
            			CLASIFICAION_ODONTOLOGO_ANTERIOR  ='$clasificacion_odontologo_ant',
            			CLASIFICACION_ODONTOLOGO          ='$clasificacion_odontologo',
            			DISTRIBUIDOR_COMPRA               ='$distribuidor',
            			ID_USUARIO_FK_5                   ='$id_usuario',
            			FECHA_ACTUALIZACION               = CURRENT_TIMESTAMP,
            			DIA_VISITA                        ='$dia_visita',
            			HORA_INI_VISITA                   ='$hora_ini_visita',        
            			HORA_FIN_VISITA                   ='$hora_fin_visita'
            			WHERE
            			ID_CLIENTE_FK_5='$id_cliente'");

                        echo mysqli_error($conex);
                        if ($actualizar && $actualizar_cliente && ($ACTUALIZAR_REGISTRO_GESTION || $INSERT)) {
                            ?> 
                            <span style="margin-top:5%;">
                                <center>
                                    <img src="../presentacion/imagenes/CHULO1.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
                                </center>
                            </span>
                            <p class="aviso3" style=" width:68.9%; margin:auto auto;">  <?php echo $sub_tipificacion; ?><br> LA ENCUESTA SE ACTUALIZO SATISFACTORIAMENTE.</p>
                            <br />
                            <br />
                            <center>
                                <?php $id_cli = $id_cliente; ?>
                                <a href="../presentacion/inicio_consulta_cliente.php?x=<?php echo base64_encode($id_cli)?>&transferencia=1"  class="btn_continuar"><img src="../presentacion/imagenes/BTN_CONTINUAR2.png" id="btn" title="CONTINUAR"/></a>
                            </center>
                            <br/>
                            <?php
                        }

                        //PRODUCTOS CONSUMO

                        if (empty($id_encuesta)) {
                            $id_encuesta = 0;
                        }

                        $INGRESAR_CONSUMO_HISTORIAL = mysqli_query($conex, "INSERT INTO 3m_productos_consumo_historial (ID_CLIENTE_FK,ID_ENCUESTA_FK,FECHA_MODIFICACION,ID_USUARIO_FK,TIPO_INGRESO )
VALUES('$id_cliente','$id_encuesta',CURRENT_TIMESTAMP,'$id_usu','ENCUESTA')");
                        echo mysqli_error($conex);

                        $consulta_id_historial = mysqli_query($conex, "SELECT ID_PRODUCTOS_CONSUMO FROM 3m_productos_consumo_historial ORDER BY ID_PRODUCTOS_CONSUMO DESC LIMIT 1 ");

                        $select = mysqli_query($conex, "SELECT * FROM 3m_detalle_producto_consumo_temp 
                            WHERE ID_CLIENTE_FK='$id_cliente' and ID_USUARIO_FK='$id_usu'");

                        while ($dato = mysqli_fetch_array($consulta_id_historial)) {
                            $ID_PRODUCTOS_CONSUMO = $dato['ID_PRODUCTOS_CONSUMO'];
                        }

                        while ($dato = mysqli_fetch_array($select)) {
                            $ID_DETALLE_PRODUCTOS_CONSUMO = $dato['ID_DETALLE_PRODUCTOS_CONSUMO'];
                            $CATEGORIA = $dato['CATEGORIA'];
                            $SUBCATEGORIA = $dato['SUBCATEGORIA'];
                            $DESCRIPCION = $dato['NOMBRE_PRODUCTO'];
                            $ID_CLIENTE_FK_3 = $dato['ID_CLIENTE'];

                            $ingresar = mysqli_query($conex, "INSERT INTO 3m_detalle_producto_consumo (CATEGORIA,SUBCATEGORIA,NOMBRE_PRODUCTO,ID_PRODUCTOS_CONSUMO_FK,TIPO)
	VALUES('$CATEGORIA','$SUBCATEGORIA','$DESCRIPCION','$ID_PRODUCTOS_CONSUMO',$tipo_odontologo)");
                            echo mysql_error($conex);

                            if ($ingresar) {
                                $eliminar = mysqli_query($conex, "DELETE FROM 3m_detalle_producto_consumo_temp WHERE ID_DETALLE_PRODUCTOS_CONSUMO='$ID_DETALLE_PRODUCTOS_CONSUMO'");
                                echo mysqli_error($conex);
                            }
                        }

                        // actualizar  la calificacion del odontologo:
                        // SI EL CLIENTE ES ODONTOLOGO

                        if ($tipo_odontologo == 1) {
                            $select = mysqli_query($conex, "SELECT h.ID_CLIENTE_FK,h.ID_ENCUESTA_FK,p.CATEGORIA,p.SUBCATEGORIA,p.NOMBRE_PRODUCTO,
                h.FECHA_MODIFICACION,h.ID_USUARIO_FK 
                FROM 3m_productos_consumo_historial AS h
                INNER JOIN 3m_detalle_producto_consumo AS p ON p.ID_PRODUCTOS_CONSUMO_FK=h.ID_PRODUCTOS_CONSUMO
                WHERE ID_CLIENTE_FK=" . $id_cliente . " AND TIPO=1 AND p.SUBCATEGORIA IN ('Resinas','Adhesivos','CEMENTO','Terminado y pulido') 
                GROUP BY SUBCATEGORIA");

                            echo mysqli_error($conex);
                            $nreg_deta = mysqli_num_rows($select);
                          
                            echo $nreg_deta.' '.$cantidad_pacientes;
                            if ($nreg_deta >= 4 && $cantidad_pacientes >= 30) {
                              
                                $clasificacion_odontologo = "A";
                            }
                            elseif ($nreg_deta < 4 && $cantidad_pacientes >= 30) {
                                $clasificacion_odontologo = "A1G";
                            }
                            else if ($nreg_deta >= 4 && $cantidad_pacientes < 30) {
                                $clasificacion_odontologo = "B";
                            }
                            else if ($nreg_deta < 4 && $cantidad_pacientes < 30) {
                                $clasificacion_odontologo = "C";
                            } else {
                                $clasificacion_odontologo = 'C';
                            }
                        } else if ($tipo_odontologo == 2) {
                            

                            $CONSULTAR_CATEGORIA_UNO = mysqli_query($conex, "SELECT p.SUBCATEGORIA
                            FROM 3m_productos_consumo_historial AS h
                            INNER JOIN 3m_detalle_producto_consumo AS p ON p.ID_PRODUCTOS_CONSUMO_FK=h.ID_PRODUCTOS_CONSUMO
                            WHERE ID_CLIENTE_FK=" . $id_cliente . " and TIPO=2 AND  SUBCATEGORIA in('Adhesivos','Resinas')");
                            $conteo_categoria_uno = mysqli_num_rows($CONSULTAR_CATEGORIA_UNO);

                            $CONSULTAR_CATEGORIA_DOS = mysqli_query($conex, "SELECT p.SUBCATEGORIA
                            FROM 3m_productos_consumo_historial AS h
                            INNER JOIN 3m_detalle_producto_consumo AS p ON p.ID_PRODUCTOS_CONSUMO_FK=h.ID_PRODUCTOS_CONSUMO
                            WHERE ID_CLIENTE_FK=" . $id_cliente . " and TIPO=2 AND  SUBCATEGORIA in('Brackets metalicos convencionales')");
                            $conteo_categoria_dos = mysqli_num_rows($CONSULTAR_CATEGORIA_DOS);

                            $CONSULTAR_CATEGORIA_TRES = mysqli_query($conex, "SELECT p.SUBCATEGORIA
                            FROM 3m_productos_consumo_historial AS h
                            INNER JOIN 3m_detalle_producto_consumo AS p ON p.ID_PRODUCTOS_CONSUMO_FK=h.ID_PRODUCTOS_CONSUMO
                            WHERE ID_CLIENTE_FK=" . $id_cliente . " and TIPO=2 AND  SUBCATEGORIA in('Brackets ceramicos convencionales','Brackets ceramicos de autoligado')");
                            $conteo_categoria_tres = mysqli_num_rows($CONSULTAR_CATEGORIA_TRES);

                            $conteo_categoria = $conteo_categoria_uno + $conteo_categoria_dos + $conteo_categoria_tres;

                            //categoria A.
                            if ($cantidad_pacientes >= 30 && $CASOS_NUEVOS_SEMANA >= 5 && $conteo_categoria >= 3) {
                                $clasificacion_odontologo = 'A';
                            }
                            //calificacion A1G
                            else if ($cantidad_pacientes >= 30 && $CASOS_NUEVOS_SEMANA >= 5 && $conteo_categoria < 3) {
                                $clasificacion_odontologo = 'A1G';
                            }
                            //calificacion B
                            else if ($cantidad_pacientes < 30 && $CASOS_NUEVOS_SEMANA < 5 && $conteo_categoria >= 3) {
                                $clasificacion_odontologo = 'B';
                            }
                            //calificacion C
                            else if ($cantidad_pacientes < 30 && $CASOS_NUEVOS_SEMANA < 5 && $conteo_categoria < 3) {
                                $clasificacion_odontologo = 'C';
                            }
                            else {
                                $clasificacion_odontologo = 'C';
                            }
                        }
                    }
                    // actualizar 3m _historial de calificacion actualizar .
                    //consultar la calificacion actual 
                    $CONSULTAR_CALIFICACION = mysqli_query($conex, "SELECT CLASIFICACION_ODONTOLOGO FROM 3m_encuesta WHERE ID_CLIENTE_FK_5=" . $id_cliente);

                    while ($dato_calificacion = mysqli_fetch_array($CONSULTAR_CALIFICACION)) {
                        $CLASIFICAION_ANTERIOR = $dato_calificacion['CLASIFICACION_ODONTOLOGO'];
                    }

                    $consultar_historia_calificacion = mysqli_query($conex, "SELECT * FROM 3m_historia_clasificacion WHERE ID_CLIENTE_FK_7=" . $id_cliente);

                    $historia = mysqli_num_rows($consultar_historia_calificacion);

                    if ($historia == 0) {
                       
                        $INSERT_HISTORIA = mysqli_query($conex, "INSERT INTO 3m_historia_clasificacion(CLASIFICAION_ANTERIOR,CLASIFICAION_ACTUAL,USUARIO_REGISTRO_ID,
                        USUARIO_REGISTRO,ID_CLIENTE_FK_7)
                        VALUES ('" . $CLASIFICAION_ANTERIOR . "','" . $clasificacion_odontologo . "','" . $id_usu . "','" . $usua . "','" . $id_cliente . "')");
                    } else {

                        //actualizar califacion
                        $ACTUALIZAR_CALIFICACION = mysqli_query($conex, "
            UPDATE 3m_historia_clasificacion SET CLASIFICAION_ACTUAL='" . $clasificacion_odontologo . "' , CLASIFICAION_ANTERIOR='" . $CLASIFICAION_ANTERIOR . "' WHERE ID_CLIENTE_FK_7=" . $id_cliente);
                    }

                    // actualizar clasificacion del odontologo u ortodoncista en encuesta

                    $actualizar = mysqli_query($conex, "UPDATE 3m_encuesta SET
                        CLASIFICAION_ODONTOLOGO_ANTERIOR='$CLASIFICAION_ANTERIOR',
			CLASIFICACION_ODONTOLOGO='$clasificacion_odontologo'
			WHERE
			ID_CLIENTE_FK_5='$id_cliente'");

                    if (!$actualizar || !$actualizar_cliente) {
                        ?>
                        <span style="margin-top:5%;">
                            <center>
                                <img src="../presentacion/imagenes/advertencio_1.png" style="width:100px; margin-top:100px;margin-top:5%;"/>
                            </center>
                        </span>
                        <p class="error" style=" width:68.9%; margin:auto auto;">LA ENCUESTA NO HA SIDO INGRESADO SATISFACTORIAMENTE.</p>
                        <br />
                        <br />
                        <center>
                            <a href="javascript:history.go(-1)" target="info" class="btn_continuar"><img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" /></a>
                        </center>
                        <br/>
                <?php
					}
            }
        }
    }
}
?>