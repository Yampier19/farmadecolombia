<?php
header("Content-Type: text/html;charset=utf-8");
?>
<!DOCTYPE html>
<html><head>
<meta name="viewport" content="width=device-width, initial-scale=1.0,text/html; charset=utf-8">
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCVAVPaGOuf3neASoLo_F96Udd_IEuha7s&callback=initMap"
    async defer></script>
<link rel="stylesheet" type="text/css" href="highslide/highslide.css" />
<link href="css/bootstrap.css" rel="stylesheet">
<script type="text/javascript" src="highslide/highslide.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>  
<?php
include("session.php");
?>
<style>
    .aviso3
    {
        font-size: 130%;
        font-weight: bold;
        color: #11a9e3;
        text-transform:uppercase;
        background-color:transparent;
        text-align: center;
        padding:10px;
    }
    .error
    {
        font-size: 130%;
        font-weight: bold;
        color: #fb8305;
        text-transform:uppercase;
        background-color:transparent;
        text-align: center;
        padding:10px;
    }
</style>
</head>
<body>
    <br />
    <br />
    <?php
    include('../datos/conex_copia.php');
    $string_intro = getenv("QUERY_STRING");
    parse_str($string_intro);

    if (isset($_POST['btn_ingresar_datos'])) {

        $idUsuario  = $_POST["idUsuario"];
        $conversion = $_POST['conversion'];
        $producto   = $_POST['producto'];
        $mes        = $_POST['mes'];
        $anio       = $_POST['anio'];

    $VALIDAR_KPI = mysqli_query($conex,"SELECT * FROM 3m_kpi WHERE MES='$mes' AND ANIO='$anio' AND ID_USUARIO='$idUsuario'");
    
    $cont=mysqli_num_rows($VALIDAR_KPI);

    if($cont==0){
        $INGRESAR_NOVEDAD = mysqli_query($conex,"
        INSERT INTO 3m_kpi(CONVERSION,PRODUCTO_MES,ID_USUARIO,FECHA_INGRESO,MES,ANIO)VALUES('$conversion','$producto','$idUsuario',NOW(),'$mes','$anio');");
        ?>
        <span style="margin-top:5%;">
            <center>
                <img src="../presentacion/imagenes/CHULO1.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/> 
                <p class="aviso3" style=" width:68.9%; margin:auto auto;">
                        SI INGRESARON LOS DATOS SATISFACTORIAMENTE.
                    </p>
                    <br/>
                    <br/>
                    <br/>
                    <a href="javascript:history.go(-1)" class="btn_continuar">
                        <img src="../presentacion/imagenes/BTN_CONTINUAR2.png" style="width:152px; height:37px" />
                    </a>
            </center>
        </span>
  <?php
      }else if ($cont==1) {
        $ACTUALIZAR_DATOS = mysqli_query($conex,"UPDATE 3m_kpi SET CONVERSION='$conversion', PRODUCTO_MES='$producto', FECHA_ACTUALIZACION=NOW(), MES='$mes', ANIO=  '$anio' WHERE ID_USUARIO='$idUsuario' AND MES='$mes' AND ANIO='$anio';");
        ?>
        <span style="margin-top:5%;">
                <center>
                    <img src="../presentacion/imagenes/CHULO1.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/> 
                    <p class="aviso3" style=" width:68.9%; margin:auto auto;">
                        SE ACTUALIZARON LOS DATOS SATISFACTORIAMENTE.
                    </p>
                    <br />
                    <br />
                    <br />
                    <a href="javascript:history.go(-1)" class="btn_continuar">
                        <img src="../presentacion/imagenes/BTN_CONTINUAR2.png" style="width:152px; height:37px" />
                    </a>
                </center>
            </span>
        <?php
      }
      else if ($cont==2 || $cont==3)
      {?>
        <span style="margin-top:5%;">
                <center>
                    <img src="../presentacion/imagenes/advertencio_1.png" style="width:100px; margin-top:100px;margin-top:5%;"/>
                </center>
            </span>
            <p class="error" style=" width:68.9%; margin:auto auto;">EL REGISTRO ESTA DUPLICADO, POR FAVOR CONTACTE CON EL WEBMASTER</p>
            <br />
            <br />
        <center>
            <a href="javascript:history.go(-1)" target="info" class="btn_continuar"><img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" /></a>
        </center>
        <br/>
<?php
        }

} else if (isset($_POST["btn_ingresar_academico"])) {

        $idUsuario  = $_POST['idUsuario'];
        $academico  = $_POST['academico'];
        $mes_a      = $_POST['mes_a'];
        $anio_a     = $_POST['anio_a'];

        $CONSULT_ACADEMICA = mysqli_query($conex,"SELECT * FROM 3m_kpi_academico WHERE MES_ACADEMICO='$mes_a' AND ANIO_ACADEMICO='$anio_a' AND ID_USUARIO='$idUsuario'");

        $cont=mysqli_num_rows($CONSULT_ACADEMICA);

        if($cont==0){
        $INGRESAR_NOVEDAD = mysqli_query($conex,"
        INSERT INTO 3m_kpi_academico (MES_ACADEMICO,ANIO_ACADEMICO,ID_USUARIO,FECHA_INGRESO_ACADEMICO,ACADEMICO)VALUES('$mes_a','$anio_a','$idUsuario',NOW(),'$academico');");
        ?>

        <span style="margin-top:5%;">
                <center>
                    <img src="../presentacion/imagenes/CHULO1.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/> 
                    <p class="aviso3" style=" width:68.9%; margin:auto auto;">
                        SI INGRESARON LOS DATOS SATISFACTORIAMENTE.
                    </p>
                    <br/>
                    <br/>
                    <br/>
                    <a href="javascript:history.go(-1)" class="btn_continuar">
                        <img src="../presentacion/imagenes/BTN_CONTINUAR2.png" style="width:152px; height:37px" />
                    </a>
                </center>
            </span>

        <?php
      }else if ($cont==1) {
        $ACTUALIZAR_DATOS = mysqli_query($conex,"UPDATE 3m_kpi_academico SET
                ACADEMICO = '$academico' , 
                MES_ACADEMICO = '$mes_a' , 
                ANIO_ACADEMICO = '$anio_a' , 
                FECHA_ACTUALIZACION_ACADEMICO = NOW()
                WHERE
                ID_USUARIO = '$idUsuario' AND MES_ACADEMICO = '$mes_a' AND ANIO_ACADEMICO = '$anio_a';");
        ?>

        <span style="margin-top:5%;">
                <center>
                    <img src="../presentacion/imagenes/CHULO1.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/> 
                    <p class="aviso3" style=" width:68.9%; margin:auto auto;">
                        SE ACTUALIZARON LOS DATOS SATISFACTORIAMENTE.
                    </p>
                    <br />
                    <br />
                    <br />
                    <a href="javascript:history.go(-1)" class="btn_continuar">
                        <img src="../presentacion/imagenes/BTN_CONTINUAR2.png" style="width:152px; height:37px" />
                    </a>
                </center>
            </span>

        <?php
      }
      else if ($cont==2 || $cont==3)
      {?>
        <span style="margin-top:5%;">
                <center>
                    <img src="../presentacion/imagenes/advertencio_1.png" style="width:100px; margin-top:100px;margin-top:5%;"/>
                </center>
            </span>
            <p class="error" style=" width:68.9%; margin:auto auto;">EL REGISTRO ESTA DUPLICADO, POR FAVOR CONTACTE CON EL WEBMASTER</p>
            <br />
            <br />
        <center>
            <a href="javascript:history.go(-1)" target="info" class="btn_continuar"><img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" /></a>
        </center>
        <br/>
<?php
        }
}
?>
</body>
</html>