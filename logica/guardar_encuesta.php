<?php
include("session.php");
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Confirmar pedido</title>
        <style>
            .aviso3 
            {
                font-size: 130%;
                font-weight: bold;
                color: #11a9e3;
                text-transform:uppercase;
                /*font-family: "Trebuchet MS";
                font-family:"Gill Sans MT";
                border-radius:10px;
                background: #11a9e3;*/
                background-color:transparent;
                text-align: center;
                padding:10px;
            }
            .error
            {
                font-size: 130%;
                font-weight: bold;
                color: #fb8305;
                text-transform:uppercase;
                background-color:transparent;
                text-align: center;
                padding:10px;
            }
        </style>
    </head>
    <body> 
        <br />
        <br />
        <?php
        //header("Content-Type: text/html;charset=utf-8");
        include('../datos/conex.php');

        //mysql_set_charset($conex,"utf8");
        //$string_intro = getenv("QUERY_STRING"); 
        //parse_str($string_intro);
        $ID_CLIENTE = $ID_CLIENTE;
        $id_usu;

// boton evento
        $marcacion_efectiva = $_POST["resultado_text"];

        if ($marcacion_efectiva == 1) {
            if (isset($_POST['BTN_EFECTIVA_SIN_ENCUESTA'])) {
                $latitud = $_POST['LAT'];
                $longitud = $_POST['lon'];
                $id_cliente = $_POST['id_cliente'];
                $resultado = 'EFECTIVA';
                $id_sub_tipificacion = $_POST['sub_tipificacion_efectiva'];
                $fecha_proxima_visita = $_POST["proxima_visita"];

                $fecha_actual = date("Y-m-d");
                $id_usuario = $_POST["idusuario"];
                $name_usuario = $_POST["name_usuario"];
                $observacion = strtoupper($_POST['observacion']);
                $consultar_subtipificacion = mysql_query("SELECT subtipificacion FROM 3m_subtipificaciones WHERE id_subtipificacion=" . $id_sub_tipificacion, $conex);
                
                if($id_sub_tipificacion >= 7 && $id_sub_tipificacion <= 13){
                    $sub_tipificacion='SEGUIMIENTO';
                }else{
                    while ($dato_subtipificacion = mysql_fetch_array($consultar_subtipificacion)) {
                        $sub_tipificacion = $dato_subtipificacion["subtipificacion"];
                    }

                    if (empty($sub_tipificacion)) {
                        $sub_tipificacion = 'ENCUESTA DE CONSUMO';
                    }
                }

                if (empty($id_usuario)) {
                    ?>
                    <span style="margin-top:5%;">
                        <center>
                            <img src="../presentacion/imagenes/advertencio_1.png" style="width:100px; margin-top:100px;margin-top:5%;"/>
                        </center>
                    </span>
                    <p class="error" style=" width:68.9%; margin:auto auto;">LA SESI&Oacute;N A CADUCADO, POR FAVOR INICIE SESION NUEVAMENTE</B>
                    </p>
                    <br />
                    <br />

                    <center>

                        <a href="https://app-peoplemarketing.com/farmadecolombia/"  class="btn_continuar"><img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" /></a>
                    </center>
                    <br/>

            <?php
        } else {

            //consultar tipo de odontologo
            $CONSULTAR_TIPO = mysql_query("SELECT TIPO_ODONTOLOGO,CANTIDAD_PACIENTES,CASOS_NUEVOS_SEMANA FROM 3m_encuesta WHERE ID_CLIENTE_FK_5=" . $id_cliente, $conex);
            while ($dato_tipo = mysql_fetch_array($CONSULTAR_TIPO)) {
                $tipo_odontologo = $dato_tipo['TIPO_ODONTOLOGO'];
                $cantidad_pacientes = $dato_tipo['CANTIDAD_PACIENTES'];
                $CASOS_NUEVOS_SEMANA = $dato_tipo['CASOS_NUEVOS_SEMANA'];
            }

            // actualizar  la calificacion del odontologo:
            // 1 ODONTOLOGO 2 ORTODONCISTA

            if ($tipo_odontologo == 1) {
                $select = mysql_query("SELECT h.ID_CLIENTE_FK,h.ID_ENCUESTA_FK,p.CATEGORIA,p.SUBCATEGORIA,p.NOMBRE_PRODUCTO,
	h.FECHA_MODIFICACION,h.ID_USUARIO_FK 
	FROM 3m_productos_consumo_historial AS h
	INNER JOIN 3m_detalle_producto_consumo AS p ON p.ID_PRODUCTOS_CONSUMO_FK=h.ID_PRODUCTOS_CONSUMO
	WHERE ID_CLIENTE_FK=" . $id_cliente . " AND TIPO=1 AND p.SUBCATEGORIA IN ('Resinas','Adhesivos','CEMENTO','Terminado y pulido') 
	GROUP BY SUBCATEGORIA", $conex);


                $nreg_deta = mysql_num_rows($select);

                if ($nreg_deta >= 4 && $cantidad_pacientes >= 30) {

                    $clasificacion_odontologo = "A";
                } elseif ($nreg_deta < 4 && $cantidad_pacientes >= 30) {
                    $clasificacion_odontologo = "A1G";
                } else if ($nreg_deta >= 4 && $cantidad_pacientes < 30) {
                    $clasificacion_odontologo = "B";
                } else if ($nreg_deta < 4 && $cantidad_pacientes < 30) {
                    $clasificacion_odontologo = "C";
                } else {
                    $clasificacion_odontologo = 'C';
                }
            } else if ($tipo_odontologo == 2) {


                $CONSULTAR_CATEGORIA_UNO = mysql_query("SELECT p.SUBCATEGORIA
			FROM 3m_productos_consumo_historial AS h
			INNER JOIN 3m_detalle_producto_consumo AS p ON p.ID_PRODUCTOS_CONSUMO_FK=h.ID_PRODUCTOS_CONSUMO
			WHERE ID_CLIENTE_FK=" . $id_cliente . " and TIPO=2 AND  SUBCATEGORIA in('Adhesivos','Resinas')", $conex);
                $conteo_categoria_uno = mysql_num_rows($CONSULTAR_CATEGORIA_UNO);

                $CONSULTAR_CATEGORIA_DOS = mysql_query("SELECT p.SUBCATEGORIA
			FROM 3m_productos_consumo_historial AS h
			INNER JOIN 3m_detalle_producto_consumo AS p ON p.ID_PRODUCTOS_CONSUMO_FK=h.ID_PRODUCTOS_CONSUMO
			WHERE ID_CLIENTE_FK=" . $id_cliente . " and TIPO=2 AND  SUBCATEGORIA in('Brackets metalicos convencionales')" . $conex);
                $conteo_categoria_dos = mysql_num_rows($CONSULTAR_CATEGORIA_DOS);

                $CONSULTAR_CATEGORIA_TRES = mysql_query("SELECT p.SUBCATEGORIA
			FROM 3m_productos_consumo_historial AS h
			INNER JOIN 3m_detalle_producto_consumo AS p ON p.ID_PRODUCTOS_CONSUMO_FK=h.ID_PRODUCTOS_CONSUMO
			WHERE ID_CLIENTE_FK=" . $id_cliente . " and TIPO=2 AND  SUBCATEGORIA in('Brackets ceramicos convencionales','Brackets ceramicos de autoligado')", $conex);
                $conteo_categoria_tres = mysql_num_rows($CONSULTAR_CATEGORIA_TRES);

                $conteo_categoria = $conteo_categoria_uno + $conteo_categoria_dos + $conteo_categoria_tres;

                //categoria A.
                if ($cantidad_pacientes >= 30 && $CASOS_NUEVOS_SEMANA >= 5 && $conteo_categoria >= 3) {
                    $clasificacion_odontologo = 'A';
                }
                //calificacion A1G
                else if ($cantidad_pacientes >= 30 && $CASOS_NUEVOS_SEMANA >= 5 && $conteo_categoria < 3) {
                    $clasificacion_odontologo = 'A1G';
                }
                //calificacion B
                else if ($cantidad_pacientes < 30 && $CASOS_NUEVOS_SEMANA < 5 && $conteo_categoria >= 3) {
                    $clasificacion_odontologo = 'B';
                }
                //calificacion C
                else if ($cantidad_pacientes < 30 && $CASOS_NUEVOS_SEMANA < 5 && $conteo_categoria < 3) {
                    $clasificacion_odontologo = 'C';
                } else {
                    $clasificacion_odontologo = 'C';
                }
            }
            // actualizar 3m _historial de calificacion actualizar .
            //consultar la calificacion actual 
            $CONSULTAR_CALIFICACION = mysql_query("SELECT CLASIFICACION_ODONTOLOGO FROM 3m_encuesta WHERE ID_CLIENTE_FK_5=" . $id_cliente, $conex);

            while ($dato_calificacion = mysql_fetch_array($CONSULTAR_CALIFICACION)) {
                $CLASIFICAION_ANTERIOR = $dato_calificacion['CLASIFICACION_ODONTOLOGO'];
            }
            $consultar_historia_calificacion = mysql_query("SELECT * FROM 3m_historia_clasificacion WHERE ID_CLIENTE_FK_7=" . $id_cliente, $conex);

            $historia = mysql_num_rows($consultar_historia_calificacion);

            if ($historia == 0) {

                $INSERT_HISTORIA = mysql_query("INSERT INTO 3m_historia_clasificacion(CLASIFICAION_ANTERIOR,CLASIFICAION_ACTUAL,USUARIO_REGISTRO_ID,
				USUARIO_REGISTRO,ID_CLIENTE_FK_7)
				VALUES ('" . $CLASIFICAION_ANTERIOR . "','" . $clasificacion_odontologo . "','" . $id_usuario . "','" . $name_usuario . "','" . $id_cliente . "')", $conex);
            } else {

                //actualizar califacion
                $ACTUALIZAR_CALIFICACION = mysql_query("
	UPDATE 3m_historia_clasificacion SET CLASIFICAION_ACTUAL='" . $clasificacion_odontologo . "' , CLASIFICAION_ANTERIOR='" . $CLASIFICAION_ANTERIOR . "' WHERE ID_CLIENTE_FK_7=" . $id_cliente, $conex);
            }

            // actualizar clasificacion del odontologo u ortodoncista en encuesta

            $actualizar = mysql_query("UPDATE 3m_encuesta SET
                        CLASIFICAION_ODONTOLOGO_ANTERIOR='$CLASIFICAION_ANTERIOR',
			CLASIFICACION_ODONTOLOGO='$clasificacion_odontologo'
			WHERE
			ID_CLIENTE_FK_5=" . $id_cliente, $conex);


            //ACTUALIZAR USUARIO

            $fecha_hoy = date("Y-m-d");

            $UPDATE = mysql_query("UPDATE 3m_cliente
			SET
			ESTADO_ASIGNADO='GESTIONADO',
			USUARIO_ASIGNADO=" . $id_usuario . ",
			NOMBRE_USUARIO_ASIGNADO='" . $name_usuario . "',
			FECHA_ASIGNADO='" . $fecha_hoy . "'
			WHERE ID_CLIENTE='" . $id_cliente . "'", $conex);

            //CONSULTAR GESTION

            $CONSULTAR_REGISTRO_GESTION = mysql_query("
			SELECT *
			FROM 3m_gestion 
			WHERE TIPIFICACION_GESTION ='EFECTIVA' AND SUB_TIPIFICACION <>'TELEVENTA' AND SUB_TIPIFICACION <>'PEDIDO ELIMINADO' AND ID_ASESOR_GESTION =" . $id_usuario . " AND ID_CLIENTE_FK=" . $id_cliente . " AND  DATE(FECHA_GESTION)='" . $fecha_actual . "'
			ORDER BY FECHA_GESTION DESC LIMIT 1
			", $conex);

            $CONSULTAR_REGISTRO_TELEVENTA = mysql_query("
			SELECT *
			FROM 3m_gestion 
			WHERE  TIPIFICACION_GESTION ='EFECTIVA' AND SUB_TIPIFICACION='TELEVENTA' AND ID_ASESOR_GESTION =" . $id_usuario . " AND ID_CLIENTE_FK=" . $id_cliente . " AND  DATE(FECHA_GESTION)='" . $fecha_actual . "'
			ORDER BY FECHA_GESTION DESC LIMIT 1
			", $conex);

            $CONSULTAR_INGRESO_PEDIDO = mysql_query("SELECT ID_GESTION FROM 3m_gestion 
			WHERE ACCION='INGRESO SOLICITUD PEDIDO' AND SUB_TIPIFICACION <>'TELEVENTA'
			AND DATE(FECHA_GESTION)='" . $fecha_actual . "' AND ID_ASESOR_GESTION=" . $id_usuario . " AND ID_CLIENTE_FK=" . $id_cliente . "", $conex);

            $CONSULTA_REGISTRO_PEDIDO_ELIMINADO = mysql_query("SELECT * 
FROM 3m_gestion
WHERE ACCION='PEDIDO ELIMINADO' AND TIPIFICACION_GESTION='EFECTIVA'
AND DATE(FECHA_GESTION)='" . $fecha_actual . "' AND ID_ASESOR_GESTION=" . $id_usuario . " AND ID_CLIENTE_FK=" . $id_cliente . "", $conex);


            $NUMERO_REGISTRO_EFECTIVA = mysql_num_rows($CONSULTAR_REGISTRO_GESTION);
            $NUMERO_REGISTRO_TELEVENTA = mysql_num_rows($CONSULTAR_REGISTRO_TELEVENTA);
            $NUMERO_REGISTRO_PEDIDO = mysql_num_rows($CONSULTAR_INGRESO_PEDIDO);
            $NUMERO_PEDIDO_ELIMINADO = mysql_num_rows($CONSULTA_REGISTRO_PEDIDO_ELIMINADO);

            if ($NUMERO_REGISTRO_EFECTIVA == 0 && $NUMERO_REGISTRO_TELEVENTA == 0) {
                //INGRESAR UNA NUEVA GESTION

                if ($NUMERO_REGISTRO_PEDIDO == 0) {
                    $INSERT_PEDIDO = mysql_query("INSERT INTO 3m_gestion(ACCION,TIPIFICACION_GESTION,SUB_TIPIFICACION,LATITUD_GESTION,LONGITUD_GESTION,
			ID_ASESOR_GESTION,ASESOR_GESTION,OBSERVACION_GESTION,ID_CLIENTE_FK,FECHA_PROXIMA_VISITA) 
			VALUES ('REGISTRO VISITA','EFECTIVA','".$sub_tipificacion."','".$latitud."','".$longitud."','".$id_usuario."','".$name_usuario."','" . $observacion . "','" . $id_cliente . "','" . $fecha_proxima_visita . "')", $conex);

                    if ($NUMERO_PEDIDO_ELIMINADO > 0) {
                        $INSERT_PEDIDO_ELIMINADO = mysql_query("UPDATE 3m_gestion SET CORREO=0, `TIPIFICACION_GESTION` = 'PEDIDO ELIMINADO'
WHERE DATE(FECHA_GESTION)='" . $fecha_actual . "' AND ID_ASESOR_GESTION=" . $id_usuario . " AND ID_CLIENTE_FK=" . $id_cliente . "", $conex);
                    }
                } else {
                    ?>
                            <span style="margin-top:5%;">
                                <center>
                                    <img src="../presentacion/imagenes/advertencio_1.png" style="width:100px; margin-top:100px;margin-top:5%;"/>
                                </center>
                            </span>
                            <p class="error" style=" width:68.9%; margin:auto auto;">YA EXISTE UNA GESTION EL DIA HOY PARA ESTE CLIENTE, SI REQUIERE UNA NUEVA GESTION COMUNIQUESE CON EL COORDINADOR, PARA ELIMINAR LA GESTION ANTERIOR</B>
                            </p>
                            <br />
                            <br />

                            <center>

                    <?php $id_cli = $id_cliente; ?>
                                <a href="../presentacion/inicio_consulta_cliente.php"  class="btn_continuar"><img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" /></a>
                            </center>
                            <br/>
                    <?php
                    die();
                }
            } else if ($NUMERO_REGISTRO_EFECTIVA > 0 && $NUMERO_REGISTRO_TELEVENTA == 0) {
                //ACTUALIZA UNA GESTION
                ?>
                        <span style="margin-top:5%;">
                            <center>
                                <img src="../presentacion/imagenes/advertencio_1.png" style="width:100px; margin-top:100px;margin-top:5%;"/>
                            </center>
                        </span>
                        <p class="error" style=" width:68.9%; margin:auto auto;">YA EXISTE UNA GESTION EL DIA HOY PARA ESTE CLIENTE, SI REQUIERE UNA NUEVA GESTION COMUNIQUESE CON EL COORDINADOR, PARA ELIMINAR LA GESTION ANTERIOR</B>
                        </p>
                        <br />
                        <br />

                        <center>

                        <?php $id_cli = $id_cliente; ?>
                            <a href="../presentacion/inicio_consulta_cliente.php"  class="btn_continuar"><img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" /></a>
                        </center>
                        <br/>

                <?php
                die();
            } else if ($NUMERO_REGISTRO_EFECTIVA > 0 && $NUMERO_REGISTRO_TELEVENTA > 0) {
                //ACTUALIZA UNA GESTION
                ?>
                        <span style="margin-top:5%;">
                            <center>
                                <img src="../presentacion/imagenes/advertencio_1.png" style="width:100px; margin-top:100px;margin-top:5%;"/>
                            </center>
                        </span>
                        <p class="error" style=" width:68.9%; margin:auto auto;">YA EXISTE UNA GESTION EL DIA HOY PARA ESTE CLIENTE, SI REQUIERE UNA NUEVA GESTION COMUNIQUESE CON EL COORDINADOR, PARA ELIMINAR LA GESTION ANTERIOR</B>
                        </p>
                        <br />
                        <br />

                        <center>

                        <?php $id_cli = $id_cliente; ?>
                            <a href="../presentacion/inicio_consulta_cliente.php"  class="btn_continuar"><img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" /></a>
                        </center>
                        <br/>

                <?php
                die();
            }

            echo mysql_error($conex);
            ?>
                    <span style="margin-top:5%;">
                        <center>
                            <img src="../presentacion/imagenes/CHULO1.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
                            <p class="aviso3" style=" width:68.9%; margin:auto auto;">GESTION EFECTIVA INGRESADA SATISFACTORIAMENTE.</p>
                            <br />
                            <br />

                        </center>
                        <center>
                    <?php $id_cli = $id_cliente; ?>
                            <a href="../presentacion/inicio_consulta_cliente.php?x=<?php echo base64_encode($id_cli) ?>&transferencia=1"  class="btn_continuar"><img src="../presentacion/imagenes/BTN_CONTINUAR2.png" id="btn" title="CONTINUAR"/></a>
                        </center>
                    </span>
                    <br/>
                    <?PHP
                }
            }
        } else if ($marcacion_efectiva == 2) {

            if (isset($_POST['GUARDAR_NO_EFECTIVA'])) {

                $latitud = $_POST['LAT'];
                $longitud = $_POST['lon'];
                $id_cliente = $_POST['id_cliente'];
                $resultado = 'NO EFECTIVA';
                $id_sub_tipificacion = $_POST['sub_tipificacion_no_efectiva'];
                $fecha_proxima_visita = $_POST["proxima_visita"];
                $observacion = strtoupper($_POST['observacion']);
                $id_usu = $_POST["idusuario"];
                $usua = $_POST["name_usuario"];

                $consultar_subtipificacion = mysql_query("SELECT subtipificacion FROM 3m_subtipificaciones WHERE id_subtipificacion=" . $id_sub_tipificacion, $conex);
                
                if($id_sub_tipificacion <=6){
                      $sub_tipificacion = 'ODONTOLOGO NO ESTA';
                }
                else{
                    while ($dato_subtipificacion = mysql_fetch_array($consultar_subtipificacion)) {
                        $sub_tipificacion = $dato_subtipificacion["subtipificacion"];
                    }
                    if (empty($sub_tipificacion)) {
                        $sub_tipificacion = 'ODONTOLOGO NO ESTA';
                    }
                }

                if (empty($id_usu)) {
                    ?>
                    <span style="margin-top:5%;">
                        <center>
                            <img src="../presentacion/imagenes/advertencio_1.png" style="width:100px; margin-top:100px;margin-top:5%;"/>
                        </center>
                    </span>
                    <p class="error" style=" width:68.9%; margin:auto auto;">LA SESI&Oacute;N A CADUCADO, POR FAVOR INICIE SESION NUEVAMENTE</B>
                    </p>
                    <br />
                    <br />

                    <center>

                        <a href="https://app-peoplemarketing.com/farmadecolombia/"  class="btn_continuar"><img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" /></a>
                    </center>
                    <br/>

                    <?php
                } else {


                    $fecha_hoy = date("Y-m-d");

                    $UPDATE = mysql_query("UPDATE 3m_cliente
			SET
			ESTADO_ASIGNADO='GESTIONADO',
			USUARIO_ASIGNADO=" . $id_usu . ",
			NOMBRE_USUARIO_ASIGNADO='" . $usua . "',
			FECHA_ASIGNADO='" . $fecha_hoy . "'
			WHERE ID_CLIENTE='" . $id_cliente . "'", $conex);


                    mysql_query("SET NAMES utf8");

                    $fecha_actual = date("Y-m-d");

                    $CONSULTAR_REGISTRO_GESTION = mysql_query("
			SELECT *
			FROM 3m_gestion 
			WHERE TIPIFICACION_GESTION ='NO EFECTIVA' AND ID_ASESOR_GESTION ='$id_usu' AND ID_CLIENTE_FK='$ID_CLIENTE' AND  DATE(FECHA_GESTION)='$fecha_actual'
			ORDER BY FECHA_GESTION DESC LIMIT 1
			", $conex);

                    echo mysql_error($conex);



                    $NUMERO_REGISTRO = mysql_num_rows($CONSULTAR_REGISTRO_GESTION);

                    if ($NUMERO_REGISTRO > 0) {

                        while ($datos_gestion = (mysql_fetch_array($CONSULTAR_REGISTRO_GESTION))) {
                            $id_gestion = $datos_gestion["ID_GESTION"];
                        }



                        $INSERT = mysql_query("
			UPDATE 3m_gestion SET TIPIFICACION_GESTION='NO EFECTIVA' , SUB_TIPIFICACION='$sub_tipificacion', LATITUD_GESTION ='$latitud' ,LONGITUD_GESTION='$longitud', ASESOR_GESTION='$usua', ID_ASESOR_GESTION='$id_usu', CORREO=0, OBSERVACION_GESTION='" . $observacion . "', FECHA_PROXIMA_VISITA='$fecha_proxima_visita', FECHA_GESTION=NOW()
			WHERE ID_GESTION='$id_gestion'
			", $conex);
                    } else {

                        $INSERT = mysql_query("
		INSERT INTO 3m_gestion(ACCION,TIPIFICACION_GESTION,SUB_TIPIFICACION,LATITUD_GESTION,LONGITUD_GESTION,ID_ASESOR_GESTION,ASESOR_GESTION,OBSERVACION_GESTION,ID_CLIENTE_FK,FECHA_PROXIMA_VISITA) VALUES ('REGISTRO VISITA','NO EFECTIVA','" . $sub_tipificacion . "','" . $latitud . "','" . $longitud . "','" . $id_usu . "','" . $usua . "','" . $observacion . "','" . $ID_CLIENTE . "','" . $fecha_proxima_visita . "')", $conex);
                    }
                    echo mysql_error($conex);
                    if ($INSERT) {
                        ?>

                        <span style="margin-top:5%;">
                            <center>
                                <img src="../presentacion/imagenes/CHULO1.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
                                <p class="aviso3" style=" width:68.9%; margin:auto auto;">GESTION  NO EFECTIVA INGRESADA SATISFACTORIAMENTE.</p>
                                <br />
                                <br />
                                <a href="../presentacion/inicio_visitas.php"><img src="../presentacion/imagenes/BTN_CONTINUAR2.png" id="btn" title="CONTINUAR"/></a>
                            </center>
                        </span>
                        <br/>
                        <?PHP
                    }
                }
            }
        } else if ($marcacion_efectiva == 3) {
            if (isset($_POST['BTN_TELEVENTA'])) {
                $latitud = $_POST['LAT'];
                $longitud = $_POST['lon'];
                $id_cliente = $_POST['id_cliente'];
                $resultado = 'TELEVENTA';
                $fecha_proxima_visita = $_POST["proxima_visita"];
                $observacion = strtoupper($_POST['observacion']);
                $sub_tipificacion = $_POST['sub_tipificacion_televenta'];
                $id_usu = $_POST["idusuario"];
                $usua = $_POST["name_usuario"];


                if (empty($id_usu)) {
                    ?>
                    <span style="margin-top:5%;">
                        <center>
                            <img src="../presentacion/imagenes/advertencio_1.png" style="width:100px; margin-top:100px;margin-top:5%;"/>
                        </center>
                    </span>
                    <p class="error" style=" width:68.9%; margin:auto auto;">LA SESI&Oacute;N A CADUCADO, POR FAVOR INICIE SESION NUEVAMENTE</B>
                    </p>
                    <br />
                    <br />

                    <center>

                        <a href="https://app-peoplemarketing.com/farmadecolombia/"  class="btn_continuar"><img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" /></a>
                    </center>
                    <br/>

                    <?php
                } else {

                    //consultar tipo de odontologo
                    $CONSULTAR_TIPO = mysql_query("SELECT TIPO_ODONTOLOGO,CANTIDAD_PACIENTES,CASOS_NUEVOS_SEMANA FROM 3m_encuesta WHERE ID_CLIENTE_FK_5=" . $id_cliente, $conex);
                    while ($dato_tipo = mysql_fetch_array($CONSULTAR_TIPO)) {
                        $tipo_odontologo = $dato_tipo['TIPO_ODONTOLOGO'];
                        $cantidad_pacientes = $dato_tipo['CANTIDAD_PACIENTES'];
                        $CASOS_NUEVOS_SEMANA = $dato_tipo['CASOS_NUEVOS_SEMANA'];
                    }



                    // actualizar  la calificacion del odontologo:
                    // 1 ODONTOLOGO 2 ORTODONCISTA

                    if ($tipo_odontologo == 1) {

                        $select = mysql_query("SELECT h.ID_CLIENTE_FK,h.ID_ENCUESTA_FK,p.CATEGORIA,p.SUBCATEGORIA,p.NOMBRE_PRODUCTO,
	h.FECHA_MODIFICACION,h.ID_USUARIO_FK 
	FROM 3m_productos_consumo_historial AS h
	INNER JOIN 3m_detalle_producto_consumo AS p ON p.ID_PRODUCTOS_CONSUMO_FK=h.ID_PRODUCTOS_CONSUMO
	WHERE ID_CLIENTE_FK=" . $id_cliente . " AND TIPO=1 AND p.SUBCATEGORIA IN ('Resinas','Adhesivos','CEMENTO','Terminado y pulido') 
	GROUP BY SUBCATEGORIA", $conex);


                        $nreg_deta = mysql_num_rows($select);

                        if ($nreg_deta >= 4 && $cantidad_pacientes >= 30) {

                            $clasificacion_odontologo = "A";
                        } elseif ($nreg_deta < 4 && $cantidad_pacientes >= 30) {
                            $clasificacion_odontologo = "A1G";
                        } else if ($nreg_deta >= 4 && $cantidad_pacientes < 30) {
                            $clasificacion_odontologo = "B";
                        } else if ($nreg_deta < 4 && $cantidad_pacientes < 30) {
                            $clasificacion_odontologo = "C";
                        } else {

                            $clasificacion_odontologo = 'C';
                        }
                    } else if ($tipo_odontologo == 2) {


                        $CONSULTAR_CATEGORIA_UNO = mysql_query("SELECT p.SUBCATEGORIA
			FROM 3m_productos_consumo_historial AS h
			INNER JOIN 3m_detalle_producto_consumo AS p ON p.ID_PRODUCTOS_CONSUMO_FK=h.ID_PRODUCTOS_CONSUMO
			WHERE ID_CLIENTE_FK=" . $id_cliente . " and TIPO=2 AND  SUBCATEGORIA in('Adhesivos','Resinas')", $conex);
                        $conteo_categoria_uno = mysql_num_rows($CONSULTAR_CATEGORIA_UNO);

                        $CONSULTAR_CATEGORIA_DOS = mysql_query("SELECT p.SUBCATEGORIA
			FROM 3m_productos_consumo_historial AS h
			INNER JOIN 3m_detalle_producto_consumo AS p ON p.ID_PRODUCTOS_CONSUMO_FK=h.ID_PRODUCTOS_CONSUMO
			WHERE ID_CLIENTE_FK=" . $id_cliente . " and TIPO=2 AND  SUBCATEGORIA in('Brackets metalicos convencionales')" . $conex);
                        $conteo_categoria_dos = mysql_num_rows($CONSULTAR_CATEGORIA_DOS);

                        $CONSULTAR_CATEGORIA_TRES = mysql_query("SELECT p.SUBCATEGORIA
			FROM 3m_productos_consumo_historial AS h
			INNER JOIN 3m_detalle_producto_consumo AS p ON p.ID_PRODUCTOS_CONSUMO_FK=h.ID_PRODUCTOS_CONSUMO
			WHERE ID_CLIENTE_FK=" . $id_cliente . " and TIPO=2 AND  SUBCATEGORIA in('Brackets ceramicos convencionales','Brackets ceramicos de autoligado')", $conex);
                        $conteo_categoria_tres = mysql_num_rows($CONSULTAR_CATEGORIA_TRES);


                        $conteo_categoria = $conteo_categoria_uno + $conteo_categoria_dos + $conteo_categoria_tres;

                        //categoria A.
                        if ($cantidad_pacientes >= 30 && $CASOS_NUEVOS_SEMANA >= 5 && $conteo_categoria >= 3) {
                            $clasificacion_odontologo = 'A';
                        }
                        //calificacion A1G
                        else if ($cantidad_pacientes >= 30 && $CASOS_NUEVOS_SEMANA >= 5 && $conteo_categoria < 3) {
                            $clasificacion_odontologo = 'A1G';
                        }
                        //calificacion B
                        else if ($cantidad_pacientes < 30 && $CASOS_NUEVOS_SEMANA < 5 && $conteo_categoria >= 3) {
                            $clasificacion_odontologo = 'B';
                        }
                        //calificacion C
                        else if ($cantidad_pacientes < 30 && $CASOS_NUEVOS_SEMANA < 5 && $conteo_categoria < 3) {
                            $clasificacion_odontologo = 'C';
                        } else {
                            $clasificacion_odontologo = 'C';
                        }
                    }
                    // actualizar 3m _historial de calificacion actualizar .
                    //consultar la calificacion actual 
                    $CONSULTAR_CALIFICACION = mysql_query("SELECT CLASIFICACION_ODONTOLOGO FROM 3m_encuesta WHERE ID_CLIENTE_FK_5=" . $id_cliente, $conex);

                    while ($dato_calificacion = mysql_fetch_array($CONSULTAR_CALIFICACION)) {
                        $CLASIFICAION_ANTERIOR = $dato_calificacion['CLASIFICACION_ODONTOLOGO'];
                    }
                    $consultar_historia_calificacion = mysql_query("SELECT * FROM 3m_historia_clasificacion WHERE ID_CLIENTE_FK_7=" . $id_cliente, $conex);

                    $historia = mysql_num_rows($consultar_historia_calificacion);


                    if ($historia == 0) {

                        $INSERT_HISTORIA = mysql_query("INSERT INTO 3m_historia_clasificacion(CLASIFICAION_ANTERIOR,CLASIFICAION_ACTUAL,USUARIO_REGISTRO_ID,
				USUARIO_REGISTRO,ID_CLIENTE_FK_7)
				VALUES ('".$CLASIFICAION_ANTERIOR."','".$clasificacion_odontologo."','".$id_usu."','".$usua."','".$id_cliente."')", $conex);
                    } else {

                        //actualizar califacion
                        $ACTUALIZAR_CALIFICACION = mysql_query("
	UPDATE 3m_historia_clasificacion SET CLASIFICAION_ACTUAL='" . $clasificacion_odontologo . "' , CLASIFICAION_ANTERIOR='" . $CLASIFICAION_ANTERIOR . "' WHERE ID_CLIENTE_FK_7=" . $id_cliente, $conex);
                    }

                    // actualizar clasificacion del odontologo u ortodoncista en encuesta

                    $actualizar = mysql_query("UPDATE 3m_encuesta SET
                        CLASIFICAION_ODONTOLOGO_ANTERIOR='$CLASIFICAION_ANTERIOR',
			CLASIFICACION_ODONTOLOGO='$clasificacion_odontologo'
			WHERE
			ID_CLIENTE_FK_5=" . $id_cliente, $conex);


                    $fecha_hoy = date("Y-m-d");

                    $UPDATE = mysql_query("UPDATE 3m_cliente
			SET
			ESTADO_ASIGNADO='GESTIONADO',
			USUARIO_ASIGNADO=" . $id_usu . ",
			NOMBRE_USUARIO_ASIGNADO='" . $usua . "',
			FECHA_ASIGNADO='" . $fecha_hoy . "'
			WHERE ID_CLIENTE='" . $id_cliente . "'", $conex);

                    $fecha_actual = date("Y-m-d");

                    $INSERT = mysql_query("
			INSERT INTO 3m_gestion(ACCION,TIPIFICACION_GESTION,SUB_TIPIFICACION,LATITUD_GESTION,LONGITUD_GESTION,
			ID_ASESOR_GESTION,ASESOR_GESTION,OBSERVACION_GESTION,ID_CLIENTE_FK,FECHA_PROXIMA_VISITA) 
			VALUES ('REGISTRO VISITA','TELEVENTA','".$sub_tipificacion."','".$latitud."','".$longitud."','".$id_usu."','".$usua."','".$observacion."','".$id_cliente."','".$fecha_proxima_visita."')", $conex);
                    ?>
                    <span style="margin-top:5%;">
                        <center>
                            <img src="../presentacion/imagenes/CHULO1.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
                            <p class="aviso3" style=" width:68.9%; margin:auto auto;">GESTION TELEVENTA INGRESADA SATISFACTORIAMENTE.</p>
                            <br />
                            <br />

                        </center>
                        <center>
                    <?php $id_cli = $id_cliente; ?>
                            <a href="../presentacion/inicio_consulta_cliente.php?x=<?php echo base64_encode($id_cli) ?>&transferencia=1"  class="btn_continuar"><img src="../presentacion/imagenes/BTN_CONTINUAR2.png" id="btn" title="CONTINUAR"/></a>
                        </center>
                    </span>
                    <br/>
                    <?PHP
                }
            }
        } else if ($marcacion_efectiva == 4) {

            if (isset($_POST['BTN_EVENTO'])) {


                $latitud = $_POST['LAT'];
                $longitud = $_POST['lon'];
                $id_cliente = $_POST['id_cliente'];
                $resultado = 'VENTA EN EVENTO';
                $sub_tipificacion = 'VENTA EN EVENTO';
                $fecha_proxima_visita = $_POST["proxima_visita"];
                $observacion = strtoupper($_POST['observacion']);
                $fecha_actual = date("Y-m-d");
                $id_usuario = $_POST["idusuario"];
                $name_usuario = $_POST["name_usuario"];

                if (empty($id_usuario)) {
                    ?>
                    <span style="margin-top:5%;">
                        <center>
                            <img src="../presentacion/imagenes/advertencio_1.png" style="width:100px; margin-top:100px;margin-top:5%;"/>
                        </center>
                    </span>
                    <p class="error" style=" width:68.9%; margin:auto auto;">LA SESI&Oacute;N A CADUCADO, POR FAVOR INICIE SESION NUEVAMENTE</B>
                    </p>
                    <br />
                    <br />

                    <center>

                        <a href="https://app-peoplemarketing.com/farmadecolombia/"  class="btn_continuar"><img src="../presentacion/imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" /></a>
                    </center>
                    <br/>

                    <?php
                } else {

                    //consultar tipo de odontologo
                    $CONSULTAR_TIPO = mysql_query("SELECT TIPO_ODONTOLOGO,CANTIDAD_PACIENTES,CASOS_NUEVOS_SEMANA FROM 3m_encuesta WHERE ID_CLIENTE_FK_5=" . $id_cliente, $conex);
                    while ($dato_tipo = mysql_fetch_array($CONSULTAR_TIPO)) {
                        $tipo_odontologo = $dato_tipo['TIPO_ODONTOLOGO'];
                        $cantidad_pacientes = $dato_tipo['CANTIDAD_PACIENTES'];
                        $CASOS_NUEVOS_SEMANA = $dato_tipo['CASOS_NUEVOS_SEMANA'];
                    }

                    // actualizar  la calificacion del odontologo:
                    // 1 ODONTOLOGO 2 ORTODONCISTA

                    if ($tipo_odontologo == 1) {
                        $select = mysql_query("SELECT h.ID_CLIENTE_FK,h.ID_ENCUESTA_FK,p.CATEGORIA,p.SUBCATEGORIA,p.NOMBRE_PRODUCTO,
	h.FECHA_MODIFICACION,h.ID_USUARIO_FK 
	FROM 3m_productos_consumo_historial AS h
	INNER JOIN 3m_detalle_producto_consumo AS p ON p.ID_PRODUCTOS_CONSUMO_FK=h.ID_PRODUCTOS_CONSUMO
	WHERE ID_CLIENTE_FK=" . $id_cliente . " AND TIPO=1 AND p.SUBCATEGORIA IN ('Resinas','Adhesivos','CEMENTO','Terminado y pulido') 
	GROUP BY SUBCATEGORIA", $conex);


                        $nreg_deta = mysql_num_rows($select);

                        if ($nreg_deta >= 4 && $cantidad_pacientes >= 30) {

                            $clasificacion_odontologo = "A";
                        } elseif ($nreg_deta < 4 && $cantidad_pacientes >= 30) {
                            $clasificacion_odontologo = "A1G";
                        } else if ($nreg_deta >= 4 && $cantidad_pacientes < 30) {
                            $clasificacion_odontologo = "B";
                        } else if ($nreg_deta < 4 && $cantidad_pacientes < 30) {
                            $clasificacion_odontologo = "C";
                        } else {

                            $clasificacion_odontologo = 'C';
                        }
                    } else if ($tipo_odontologo == 2) {


                        $CONSULTAR_CATEGORIA_UNO = mysql_query("SELECT p.SUBCATEGORIA
			FROM 3m_productos_consumo_historial AS h
			INNER JOIN 3m_detalle_producto_consumo AS p ON p.ID_PRODUCTOS_CONSUMO_FK=h.ID_PRODUCTOS_CONSUMO
			WHERE ID_CLIENTE_FK=" . $id_cliente . " and TIPO=2 AND  SUBCATEGORIA in('Adhesivos','Resinas')", $conex);
                        $conteo_categoria_uno = mysql_num_rows($CONSULTAR_CATEGORIA_UNO);

                        $CONSULTAR_CATEGORIA_DOS = mysql_query("SELECT p.SUBCATEGORIA
			FROM 3m_productos_consumo_historial AS h
			INNER JOIN 3m_detalle_producto_consumo AS p ON p.ID_PRODUCTOS_CONSUMO_FK=h.ID_PRODUCTOS_CONSUMO
			WHERE ID_CLIENTE_FK=" . $id_cliente . " and TIPO=2 AND  SUBCATEGORIA in('Brackets metalicos convencionales')" . $conex);
                        $conteo_categoria_dos = mysql_num_rows($CONSULTAR_CATEGORIA_DOS);

                        $CONSULTAR_CATEGORIA_TRES = mysql_query("SELECT p.SUBCATEGORIA
			FROM 3m_productos_consumo_historial AS h
			INNER JOIN 3m_detalle_producto_consumo AS p ON p.ID_PRODUCTOS_CONSUMO_FK=h.ID_PRODUCTOS_CONSUMO
			WHERE ID_CLIENTE_FK=" . $id_cliente . " and TIPO=2 AND  SUBCATEGORIA in('Brackets ceramicos convencionales','Brackets ceramicos de autoligado')", $conex);
                        $conteo_categoria_tres = mysql_num_rows($CONSULTAR_CATEGORIA_TRES);


                        $conteo_categoria = $conteo_categoria_uno + $conteo_categoria_dos + $conteo_categoria_tres;

                        //categoria A.
                        if ($cantidad_pacientes >= 30 && $CASOS_NUEVOS_SEMANA >= 5 && $conteo_categoria >= 3) {
                            $clasificacion_odontologo = 'A';
                        }
                        //calificacion A1G
                        else if ($cantidad_pacientes >= 30 && $CASOS_NUEVOS_SEMANA >= 5 && $conteo_categoria < 3) {
                            $clasificacion_odontologo = 'A1G';
                        }
                        //calificacion B
                        else if ($cantidad_pacientes < 30 && $CASOS_NUEVOS_SEMANA < 5 && $conteo_categoria >= 3) {
                            $clasificacion_odontologo = 'B';
                        }
                        //calificacion C
                        else if ($cantidad_pacientes < 30 && $CASOS_NUEVOS_SEMANA < 5 && $conteo_categoria < 3) {
                            $clasificacion_odontologo = 'C';
                        } else {
                            $clasificacion_odontologo = 'C';
                        }
                    }
                    // actualizar 3m _historial de calificacion actualizar .
                    //consultar la calificacion actual 
                    $CONSULTAR_CALIFICACION = mysql_query("SELECT CLASIFICACION_ODONTOLOGO FROM 3m_encuesta WHERE ID_CLIENTE_FK_5=" . $id_cliente, $conex);

                    while ($dato_calificacion = mysql_fetch_array($CONSULTAR_CALIFICACION)) {
                        $CLASIFICAION_ANTERIOR = $dato_calificacion['CLASIFICACION_ODONTOLOGO'];
                    }
                    $consultar_historia_calificacion = mysql_query("SELECT * FROM 3m_historia_clasificacion WHERE ID_CLIENTE_FK_7=" . $id_cliente, $conex);

                    $historia = mysql_num_rows($consultar_historia_calificacion);

                    if ($historia == 0) {

                        $INSERT_HISTORIA = mysql_query("INSERT INTO 3m_historia_clasificacion(CLASIFICAION_ANTERIOR,CLASIFICAION_ACTUAL,USUARIO_REGISTRO_ID,
				USUARIO_REGISTRO,ID_CLIENTE_FK_7)
				VALUES ('" . $CLASIFICAION_ANTERIOR . "','" . $clasificacion_odontologo . "','" . $id_usuario . "','" . $name_usuario . "','" . $id_cliente . "')", $conex);
                    } else {

                        //actualizar califacion
                        $ACTUALIZAR_CALIFICACION = mysql_query("
	UPDATE 3m_historia_clasificacion SET CLASIFICAION_ACTUAL='" . $clasificacion_odontologo . "' , CLASIFICAION_ANTERIOR='" . $CLASIFICAION_ANTERIOR . "' WHERE ID_CLIENTE_FK_7=" . $id_cliente, $conex);
                    }

                    // actualizar clasificacion del odontologo u ortodoncista en encuesta

                    $actualizar = mysql_query("UPDATE 3m_encuesta SET
                        CLASIFICAION_ODONTOLOGO_ANTERIOR='$CLASIFICAION_ANTERIOR',
			CLASIFICACION_ODONTOLOGO='$clasificacion_odontologo'
			WHERE
			ID_CLIENTE_FK_5=" . $id_cliente, $conex);

                    //ACTUALIZAR USUARIO
                    $fecha_hoy = date("Y-m-d");

                    $UPDATE = mysql_query("UPDATE 3m_cliente
			SET
			ESTADO_ASIGNADO='GESTIONADO',
			USUARIO_ASIGNADO=" . $id_usuario . ",
			NOMBRE_USUARIO_ASIGNADO='" . $name_usuario . "',
			FECHA_ASIGNADO='" . $fecha_hoy . "'
			WHERE ID_CLIENTE='" . $id_cliente . "'", $conex);

                    //CONSULTAR GESTION 

                    $CONSULTAR_REGISTRO_GESTION = mysql_query("
			SELECT *
			FROM 3m_gestion 
			WHERE TIPIFICACION_GESTION ='VENTA EN EVENTO'  AND ID_ASESOR_GESTION =" . $id_usuario . " AND ID_CLIENTE_FK=" . $id_cliente . " AND  DATE(FECHA_GESTION)='" . $fecha_actual . "'
			ORDER BY FECHA_GESTION DESC LIMIT 1
			", $conex);




                    $NUMERO_REGISTRO_EVENTO = mysql_num_rows($CONSULTAR_REGISTRO_GESTION);


                    if ($NUMERO_REGISTRO_EVENTO == 0) {

                        $INSERT = mysql_query("
			INSERT INTO 3m_gestion(ACCION,TIPIFICACION_GESTION,SUB_TIPIFICACION,LATITUD_GESTION,LONGITUD_GESTION,
			ID_ASESOR_GESTION,ASESOR_GESTION,OBSERVACION_GESTION,ID_CLIENTE_FK,FECHA_PROXIMA_VISITA) 
			VALUES ('REGISTRO VISITA','VENTA EN EVENTO','VENTA EN EVENTO','" . $latitud . "','" . $longitud . "','" . $id_usuario . "','" . $name_usuario . "','" . $observacion . "','" . $id_cliente . "','" . $fecha_proxima_visita . "')", $conex);
                    } else {
                        ?>
                        <span style="margin-top:5%;">
                            <center>
                                <img src="imagenes/advertencio_1.png" style="width:100px; margin-top:100px;margin-top:5%;"/>
                            </center>
                        </span>
                        <p class="error" style=" width:68.9%; margin:auto auto;">YA EXISTE UNA GESTION EL DIA HOY PARA ESTE CLIENTE, SI REQUIERE UNA NUEVA GESTION COMUNIQUESE CON EL COORDINADOR, PARA ELIMINAR LA GESTION ANTERIOR</B>
                        </p>
                        <br />
                        <br />

                        <center>

                        <?php $id_cli = $id_cliente; ?>
                            <a href="../presentacion/inicio_consulta_cliente.php?x=<?php echo base64_encode($id_cli) ?>"  class="btn_continuar"><img src="imagenes/BOTON_REGISTRAR_NARANJA_1.png" style="width:152px; height:37px" /></a>
                        </center>
                        <br/>
                        <?php
                        die();
                    }

                    echo mysql_error($conex);
                    ?>
                    <span style="margin-top:5%;">
                        <center>
                            <img src="../presentacion/imagenes/CHULO1.png" width="118" height="117" style="width:100px; margin-top:100px;margin-top:5%;"/>
                            <p class="aviso3" style=" width:68.9%; margin:auto auto;">GESTION VENTA EN EVENTO INGRESADA SATISFACTORIAMENTE.</p>
                            <br />
                            <br />

                        </center>
                        <center>
                    <?php $id_cli = $id_cliente; ?>
                            <a href="../presentacion/inicio_consulta_cliente.php?x=<?php echo base64_encode($id_cli) ?>&transferencia=1"  class="btn_continuar"><img src="../presentacion/imagenes/BTN_CONTINUAR2.png" id="btn" title="CONTINUAR"/></a>
                        </center>
                    </span>
                    <br/>
                    <?PHP
                }
            }//fin boton evento
        }
        ?>

    </body>
</html>